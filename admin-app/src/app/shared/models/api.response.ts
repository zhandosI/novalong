export class ApiResponse {

  constructor(public status: string = '',
              public data: any) { }
}
