export class UnitType {

  constructor(public id: number,
              public code: string,
              public name: string) {}

}
