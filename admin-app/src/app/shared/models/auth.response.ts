export class AuthResponse {

  constructor(public status: string = '',
              public token: string = '') { }
}
