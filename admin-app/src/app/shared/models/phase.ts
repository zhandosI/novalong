export class Phase {

    public id: number;
    public name: string;
    public proteinsPercentage: number;
    public carbsPercentage: number;
    public fatsPercentage: number;

    constructor () { }

}
