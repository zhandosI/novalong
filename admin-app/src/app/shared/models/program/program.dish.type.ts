import { DishType } from '../dish/dish.type';

export class ProgramDishType {

  public programId: number;
  public dishType: DishType;
  public caloriesPercentage: number;

  constructor () {}

}
