import { ProgramDailyRation } from './program.daily.ration';

export class ProgramRationObject {

  public totalCalories: number;
  public totalCaloriesNeeded: number;
  public ration: ProgramDailyRation;
  public minErrorCost: number;
  public avgErrorCost: number;

  constructor () {}
}
