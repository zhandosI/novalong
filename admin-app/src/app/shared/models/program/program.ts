import { Purpose } from '../purpose';
import { ProgramDishType } from './program.dish.type';

export class Program {

  public id: number;
  public name: string;
  public purpose: Purpose;
  public caloriesDifferenceCoefficient: number;
  public programDishTypes: ProgramDishType[];

  constructor () { }

}
