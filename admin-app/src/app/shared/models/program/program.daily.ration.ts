import { DailyMeal } from '../daily.meal';

export class ProgramDailyRation {

  public day: { code: string, name: string };
  public proteins: number;
  public carbohydrates: number;
  public fats: number;
  public calories: number;
  public dailyMeals: DailyMeal[];

  constructor () {}
}
