import { ProgramDailyRation } from './program.daily.ration';

export class ProgramRationArray {

  public totalCalories: number;
  public totalCaloriesNeeded: number;
  public ration: ProgramDailyRation[];

  constructor () {}
}
