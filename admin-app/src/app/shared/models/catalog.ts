export class Catalog {

    public id: number;
    public name: string;
    public description: string;
    public jsonTemplate: string;
    public active: boolean;
    public allergic: boolean;

    constructor () { }

}
