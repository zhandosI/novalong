import { Dish } from './dish/dish';
import { DishType } from './dish/dish.type';

export class DailyMeal {

  public dishType: DishType;
  public meals: Dish[];

  constructor () {}
}
