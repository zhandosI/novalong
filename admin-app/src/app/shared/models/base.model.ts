export class BaseModel {
    id: number;
    name: string;
}

export interface ListResponseModel<TModel> {
    count: number;
    results: TModel[];
    status: string;
    items: {
        limit: number;
        offset: number;
        total: number;
    };
    links: {
        self: number;
        next: number;
        prev: number;
    };
    data: TModel[];
}
