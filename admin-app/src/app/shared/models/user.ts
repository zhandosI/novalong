import { Purpose } from './purpose';
import { ActivityLevel } from './activity.level';
import {Phase} from './phase';

export class User {

  public id: number;
  public username: string;
  public password: string;
  public gender: { code: string, name: string };
  public age: number;
  public height: number;
  public weight: number;
  public purpose: Purpose;
  public activityLevel: ActivityLevel;
  public currentPhase: Phase;

  constructor () { }

}
