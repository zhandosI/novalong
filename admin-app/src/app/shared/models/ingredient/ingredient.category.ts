import {IngredientPropertyType} from "@app/shared/models/ingredient/ingredient.property.type";

export class IngredientCategory {

  constructor(public id: number,
              public code: string,
              public name: string,
              public parentCategoryId: number,
              public allergic: boolean,
              public childCategories: IngredientCategory[],
              public ingredientProperties?: IngredientPropertyType[],
              public hasChildCategories?: boolean) {}

}
