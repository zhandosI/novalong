import { CookingType } from '../cooking.type';
import { IngredientChangableProp } from './ingredient.changable.prop';

export class IngredientCookingType {

  public cookingType: CookingType;
  public changableProp: IngredientChangableProp;
  public differenceCoefficient: number;

  constructor () {}

}
