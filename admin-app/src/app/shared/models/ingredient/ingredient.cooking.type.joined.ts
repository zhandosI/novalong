import { CookingType } from '../cooking.type';
import { IngredientChangablePropDifference } from './ingredient.changable.prop.difference';

export class IngredientCookingTypeJoined {

  public cookingType: CookingType;
  public props: IngredientChangablePropDifference[];

  constructor () {}

}
