export class IngredientChangableProp {

  public id: number;
  public code: string;
  public name: string;

  constructor () {}

}
