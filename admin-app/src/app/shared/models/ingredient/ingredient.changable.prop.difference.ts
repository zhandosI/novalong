import { IngredientChangableProp } from './ingredient.changable.prop';

export class IngredientChangablePropDifference {

  public changableProp: IngredientChangableProp;
  public differenceCoefficient: number;

  constructor () {}

}
