import { IngredientCategory } from './ingredient.category';
import { UnitDataType } from '../unit.data.type';

export class IngredientPropertyType {

  public id: number;
  public type: { id: number, name: string };
  public category: IngredientCategory;
  public unitDataType: UnitDataType;

  constructor() { }

}
