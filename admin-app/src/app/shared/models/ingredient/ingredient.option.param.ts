import {OptionParam} from "@app/shared/models/option.param";

export class IngredientOptionParam {

    public param: OptionParam;
    public id: number;
    public weight: number;

    constructor() {}

}
