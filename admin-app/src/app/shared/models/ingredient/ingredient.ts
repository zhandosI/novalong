import { IngredientUnitType } from './ingredient.unit.type';
import { IngredientCategory } from './ingredient.category';
import { IngredientCookingType } from './ingredient.cooking.type';
import {IngredientOptionParam} from "@app/shared/models/ingredient/ingredient.option.param";

export class Ingredient {

  public id: number;
  public name: string;
  public protein: number;
  public carbohydrate: number;
  public kilocalories: number;
  public fat: number;
  public category: IngredientCategory;
  public glycemicIndex: number;
  public allergic: boolean;
  public unitTypes: IngredientUnitType[];
  public optionalParams: IngredientOptionParam[];
  public cookingTypes: IngredientCookingType[];

  constructor () { }

}
