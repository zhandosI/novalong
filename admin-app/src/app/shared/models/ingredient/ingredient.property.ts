import { IngredientPropertyType } from './ingredient.property.type';

export class IngredientProperty {

  constructor(public property: IngredientPropertyType,
              public value: string) { }

}
