import { UnitType } from '../unit.type';

export class IngredientUnitType {

  public type: UnitType;
  public weight: number;

  constructor() {}

}
