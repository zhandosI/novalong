export class ActivityLevel {

  public id: number;
  public code: string;
  public name: string;
  public description: string;
  
  constructor () { }

}
