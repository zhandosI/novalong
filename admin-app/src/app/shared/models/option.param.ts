export class OptionParam {

    constructor(public id: number,
                public code: string,
                public name: string) {}

}
