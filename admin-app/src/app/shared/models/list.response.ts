export class ListResponse {

  constructor(public status: string = '',
              public results: any[] = [],
              public count: number = 0) { }
}
