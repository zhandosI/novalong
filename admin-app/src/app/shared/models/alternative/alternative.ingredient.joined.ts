import { AlternativeCategory } from './alternative.category';
import { AlternativeRelation } from './alternative.relation';
import { Ingredient } from '../ingredient/ingredient';

export class AlternativeIngredientJoined {

  public category: AlternativeCategory;
  public relation: AlternativeRelation;
  public ingredients: Ingredient[];

  constructor() { }

}
