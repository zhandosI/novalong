import { AlternativeCategory } from './alternative.category';
import { AlternativeRelation } from './alternative.relation';
import { Ingredient } from '../ingredient/ingredient';

export class AlternativeIngredient {

  public category: AlternativeCategory;
  public relation: AlternativeRelation;
  public ingredient: Ingredient;

  constructor() { }

}
