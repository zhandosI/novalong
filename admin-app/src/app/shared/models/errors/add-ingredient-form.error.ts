export class AddIngredientFormError {

  constructor(public name: string ='',
              public category: string ='',
              public allergic: string ='',
              public protein: string ='',
              public carbohydrate: string ='',
              public fat: string ='',
              public weight: string ='',
              public unitTypes: string ='',
              public glycemicIndex: string = '',
              public dishWeight: string = '') { }
}
