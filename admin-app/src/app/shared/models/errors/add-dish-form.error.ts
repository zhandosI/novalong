export class AddDishFormError {

  constructor(public name: string = '',
              public description: string = '',
              public dishTypes: string = '',
              public unitTypes: string = '',
              public dishCategory: string = '',
              public servingType: string ='',
              public servingWeight: string ='',
              public cookingTypes: string = '') { }
}
