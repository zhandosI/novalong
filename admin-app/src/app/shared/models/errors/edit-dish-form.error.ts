export class EditDishFormError {

  constructor(public name: string = '',
              public dishTypes: string = '',
              public dishCategory: string = '',
              public protein: string ='',
              public carbohydrate: string ='',
              public fat: string ='',
              public weight: string ='',
              public servingType: string ='',
              public servingWeight: string ='',
              public cookingTypes: string = '') { }
}
