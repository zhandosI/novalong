import { UnitType } from '../unit.type';

export class DishUnitType {

  public type: UnitType;
  public weight: number;

  constructor() {}

}
