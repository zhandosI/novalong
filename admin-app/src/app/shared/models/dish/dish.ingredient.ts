import {Ingredient} from "@app/shared/models/ingredient/ingredient";
import { UnitType } from "@app/shared/models/unit.type";

export class DishIngredient {

    constructor (public ingredient?: Ingredient,
                 public weight?: number,
                 public unitType?: UnitType,
                 public unitAmount?: number) { }

}