import { DishType } from './dish.type';
import { DishIngredient } from "./dish.ingredient";
import { DishCategory } from './dish.category';
import { DishUnitType } from './dish.unit.type';
import { CookingType } from '../cooking.type';

export class Dish {

  public id: number;
  public name: string;
  public description: string;
  public recipeDescription: string;
  public dateAdded: number;
  public dishTypes: DishType[];
  public cookingTypes: CookingType[];
  public dishIngredients: DishIngredient[];
  public categories: DishCategory[];
  public weight: number;
  public protein: number;
  public carbohydrate: number;
  public fat: number;
  public unitTypes: DishUnitType[];
  public recipeLink: string;
  public photoBase64: string;
  public healthFactor: any;

  constructor () { }

}
