export class UnitDataType {

  constructor(public id: number,
              public name: string,
              public dataTypeCode: string) {}

}
