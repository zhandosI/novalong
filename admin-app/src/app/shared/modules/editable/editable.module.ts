import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {EditableComponent} from './editable.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule
    ],
    declarations: [EditableComponent],
    exports: [EditableComponent]
})
export class EditableModule {
}
