import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
    selector: 'app-editable',
    templateUrl: './editable.component.html',
    styleUrls: ['./editable.component.scss']
})
export class EditableComponent {

    @Input() selector: string;
    SELECTOR_INPUT = 'input';
    SELECTOR_SELECT = 'select';

    @Input() type: number;

    @Input() currItemId: number;
    @Input() editItemId: number;

    @Input() displayable: any;

    @Input() editable: any;
    @Output() editableChange = new EventEmitter();

    @Input() choicesList: any;
    @Input() choicePropId: string;
    @Input() choicePropName: string;
    @Input() choiceExtraPropName: string;
    @Input() choiceOptionCode: string;

    @Input() detectChanges: any;
    @Output() onKeyup = new EventEmitter();

    constructor() {
    }

    onEditableChange() {
        this.editableChange.emit(this.editable);
        if (this.detectChanges) {
            this.onKeyup.emit();
        }
    }

}
