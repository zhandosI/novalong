import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "property"
})
export class PropertyPipe implements PipeTransform {

  constructor() {
  }

  transform(obj: any, propString: string): any {
    const propStrings: string[] = propString.split('|');
    const results: string[] = [];
    propStrings.forEach(p => results.push(this.getObjPropString(obj, p)));
    return results.join(' ');
  }

  getObjPropString(obj: any, propString: string): any {
    if (!propString) return obj;
    var prop, props = propString.split('.');
    for (var i = 0, iLen = props.length - 1; i < iLen; i++) {
      prop = props[i];
      var candidate = obj[prop];
      if (candidate !== undefined) {
        obj = candidate;
      } else {
        break;
      }
    }
    return obj[props[i]];
  }
}
