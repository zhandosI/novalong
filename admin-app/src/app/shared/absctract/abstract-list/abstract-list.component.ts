import {OnInit, ViewChild, AfterViewInit} from '@angular/core';
import {Router} from '@angular/router';
import {merge} from 'rxjs';
import {startWith, delay, tap} from 'rxjs/operators';
import {ListComponent} from '@app/shared/components/list/list.component';
import {PaginatorComponent} from '@app/shared/components/paginator/paginator.component';
import {MatSort} from '@angular/material';

export abstract class AbstractList implements OnInit, AfterViewInit {
    @ViewChild(ListComponent)
    list: ListComponent;

    @ViewChild(PaginatorComponent)
    paginator: PaginatorComponent;

    @ViewChild(MatSort)
    sort: MatSort;

    constructor(protected router: Router) {
    }

    ngAfterViewInit() {
        console.log(this.list);
        this.list.sortChange.subscribe(() => {
            this.paginator.pageIndex = 1;
        });

        this.list.filterCHange.subscribe(() => {
            this.paginator.pageIndex = 1;
        });

        merge(this.list.sortChange, this.paginator.page, this.list.filterCHange)
            .pipe(
                startWith(null),
                delay(0),
                tap(value => {
                    this.updateList();
                })
            )
            .subscribe();
    }

    ngOnInit() {
    }

    abstract updateList(): void;
}
