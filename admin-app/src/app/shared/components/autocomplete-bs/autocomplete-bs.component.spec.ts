import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AutocompleteBsComponent } from '@shared/components/autocomplete-bs/autocomplete-bs.component';


describe('SelectProductTypeComponent', () => {
  let component: AutocompleteBsComponent;
  let fixture: ComponentFixture<AutocompleteBsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AutocompleteBsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteBsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
