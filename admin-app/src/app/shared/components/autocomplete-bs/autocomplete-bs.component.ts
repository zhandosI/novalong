import {
    Component,
    OnInit,
    forwardRef,
    Input,
    Output,
    EventEmitter
} from '@angular/core';
import {
    NG_VALUE_ACCESSOR,
    FormControl,
    ControlValueAccessor
} from '@angular/forms';
import {from} from 'rxjs';
import {Observable} from 'rxjs';
import {ChoicesService} from '@app/shared/services/choices.service';

@Component({
    selector: 'autocomplete-bs',
    templateUrl: './autocomplete-bs.component.html',
    styleUrls: ['./autocomplete-bs.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => AutocompleteBsComponent),
            multi: true
        }
    ]
})
export class AutocompleteBsComponent implements OnInit, ControlValueAccessor {

    control = new FormControl();
    options: Observable<any>;
    dataSource = [];
    currentValue: any;

    @Input()
    key: string;
    @Input()
    placeholder: string;
    @Input()
    default: any;

    @Output()
    change = new EventEmitter<any>();
    onChange = (_: any) => {
    }
    onTouched = () => {
    }

    constructor(private service: ChoicesService) {
    }

    ngOnInit() {
        this.options = from(this.service.model(this.key));
        this.options.subscribe(value => {
            this.dataSource.push({
                id: this.default.id,
                name: this.default.name,
            });
            this.dataSource = this.dataSource.concat(value);
        });
    }

    writeValue(obj: any): void {
        this.options.subscribe(value => {
            value.forEach(item => {
                if (obj == null) {
                    this.control.setValue('');
                }
                if (item.id == obj) {
                    this.control.setValue(item.name);
                }
            });
        });
    }

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState?(isDisabled: boolean): void {
        isDisabled ? this.control.disable() : this.control.enable();
    }

    onSelect(event) {
        this.currentValue = event.item.id;
        this.onChange(this.currentValue);
        this.change.emit(event.item.id);
    }
}
