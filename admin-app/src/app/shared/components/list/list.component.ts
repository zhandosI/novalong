import {
  Component,
  OnInit,
  Input,
  EventEmitter,
  OnChanges,
  SimpleChanges,
  Output,
  HostListener,
  ViewChild,
  ElementRef,
  AfterViewInit
} from '@angular/core';
import {FormGroup, FormBuilder, FormArray, FormControl} from '@angular/forms';

export class SortEvent {
  /** The column chosen for sorting. */
  sort: string;
}

export class FilterEvent {
  /** The column chosen for sorting. */
  filtering: {};
}

export class ClickEvent {
  row: any;

  constructor(row: any) {
    this.row = row;
  }
}

@Component({
  selector: 'list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnChanges, AfterViewInit {
  @Input()
  columns: any[];

  @Input()
  items: any[];

  @Input()
  addRowUrl: string;

  @Input()
  resetFilters: boolean;

  @Input()
  updateButton: boolean;

  @Input()
  sortable = true;

  @Output()
  rowClicked = new EventEmitter<ClickEvent>();

  @ViewChild('table') table: ElementRef;

  @ViewChild('thead') thead: ElementRef;

  filterForm: FormGroup;

  sort = '';
  readonly sortChange = new EventEmitter<SortEvent>();

  filtering = {};
  readonly filterCHange = new EventEmitter<FilterEvent>();

  constructor(private fb: FormBuilder) {
    this.filterForm = fb.group({
      filters: fb.array([])
    });
  }

  ngAfterViewInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.columns) {
      this.fillForm(this.columns);
    }
  }

  ngOnInit() {
  }

  changeOrder(event, column) {
    if (!event.shiftKey) {
      this.columns.forEach(item => {
        if (item != column) {
          item.order = null;
        }
      });
    }

    if (!column.order) {
      column.order = 'asc';
      this.emitSortEvent();
      return;
    }
    if (column.order === 'asc') {
      column.order = 'dsc';
      this.emitSortEvent();
      return;
    }
    if (column.order === 'dsc') {
      column.order = null;
      this.emitSortEvent();
      return;
    }
  }

  emitSortEvent() {
    this.sort = '';
    const pageEvent = new SortEvent();

    this.columns.forEach(column => {
      if (column.order === 'asc') {
        this.sort += '+' + column.value;
      }
      if (column.order === 'dsc') {
        this.sort += '-' + column.value;
      }
    });

    pageEvent.sort = this.sort;
    this.sortChange.emit(pageEvent);
  }

  get getFormArray() {
    return <FormArray>this.filterForm.get('filters');
  }

  fillForm(columns) {
    this.filterForm = this.fb.group({
      filters: this.fb.array([])
    });

    const formArray = this.getFormArray;
    columns.forEach(column => {
      formArray.push(
        new FormGroup({
          filterControl: new FormControl('')
        })
      );
    });
  }

  onSubmit() {
    this.filtering = {};

    const filterEvent = new FilterEvent();

    this.getFormArray.value.forEach((item, index) => {
      if (item.filterControl) {
        this.filtering[this.columns[index].filter.property] =
          item.filterControl;
      }
    });

    this.filterCHange.emit(filterEvent);
  }

  reset() {
    this.getFormArray.reset();
    this.onSubmit();
  }

  click(row) {
    const clickEvent = new ClickEvent(row);
    this.rowClicked.emit(clickEvent);
  }
}
