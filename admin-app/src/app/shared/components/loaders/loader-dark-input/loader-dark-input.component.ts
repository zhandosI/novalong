import { Component, Input } from '@angular/core';

@Component({
  selector: 'loader-dark-input',
  templateUrl: './loader-dark-input.component.html',
  styleUrls: ['./loader-dark-input.component.scss']
})
export class LoaderDarkInputComponent {

  @Input() class: string;

}
