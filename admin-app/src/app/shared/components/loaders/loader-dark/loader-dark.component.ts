import { Component, Input } from '@angular/core';

@Component({
  selector: 'loader-dark',
  templateUrl: './loader-dark.component.html',
  styleUrls: ['./loader-dark.component.scss']
})
export class LoaderDarkComponent {

  @Input() label: string;
  @Input() class: string;

}
