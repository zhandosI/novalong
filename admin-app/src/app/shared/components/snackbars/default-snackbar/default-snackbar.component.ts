import { Component, Input } from '@angular/core';

@Component({
  selector: 'default-snackbar',
  templateUrl: './default-snackbar.component.html',
  styleUrls: ['./default-snackbar.component.scss']
})
export class DefaultSnackbarComponent {

  @Input() message: string;

  public showSnackbar = (): void => {
    let snackbar = document.getElementById("default-snackbar");
    snackbar.className = "show";
    setTimeout(() => snackbar.className = snackbar.className.replace("show", ""), 2000);
  }

}
