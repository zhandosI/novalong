import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent,
    HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { environment } from '@env/environment';
import 'rxjs/add/operator/do';

@Injectable()
export class RequestInterceptor implements HttpInterceptor {

    constructor (private router: Router) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const api_token = localStorage.getItem('api_token');

        if (api_token) {
          request = request.clone({
            setHeaders: { 'Authorization': `Bearer ${api_token}` }
          });
        }
        request = request.clone({url: environment.backendContext + request.url});

        return next.handle(request).do(() => {}, (err: any) => {
          if (err instanceof HttpErrorResponse) {
            if (err.status === 401 || err.error.code === 'EXPIRED_TOKEN' || err.error.code === 'INVALID_TOKEN') {
              console.log('there\'s an authentication error out here!', err);
              localStorage.removeItem('api_token');
              this.router.navigate(['/auth/login']);
            }

            if (err.status === 500 || err.status === 504) {
              console.log('there\'s a connection error out here!', err);
              this.router.navigate(['/error']);
            }
          }
        });
    }
}
