import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LoaderLightComponent} from './components/loaders/loader-light/loader-light.component';
import {LoaderDarkComponent} from './components/loaders/loader-dark/loader-dark.component';
import {LoaderDarkInputComponent} from './components/loaders/loader-dark-input/loader-dark-input.component';
import {LoaderImageComponent} from './components/loaders/loader-image/loader-image.component';
import {SuccessMessageComponent} from './components/messages/success-message/success-message.component';
import {WarningMessageComponent} from './components/messages/warning-message/warning-message.component';
import {InfoMessageComponent} from './components/messages/info-message/info-message.component';
import {ErrorMessageComponent} from './components/messages/error-message/error-message.component';
import {PageHeaderComponent} from './components/page-header/page-header.component';
import {DefaultSnackbarComponent} from './components/snackbars/default-snackbar/default-snackbar.component';
import {ListComponent} from './components/list/list.component';
import {PaginatorComponent} from '@app/shared/components/paginator/paginator.component';
import {MaterialModule} from '@app/shared/material.module';
import {RouterModule} from '@angular/router';
import {AutocompleteBsComponent} from '@app/shared/components/autocomplete-bs/autocomplete-bs.component';
import {TypeaheadModule} from 'ngx-bootstrap';
import {PropertyPipe} from '@app/shared/pipes/property.pipe';
import {SafeHtmlPipe} from "@app/shared/pipes/image.pipe";

@NgModule({
    imports: [
        CommonModule,
        MaterialModule,
        RouterModule,
        TypeaheadModule.forRoot(),
    ],
    declarations: [
        LoaderLightComponent,
        LoaderDarkComponent,
        LoaderDarkInputComponent,
        LoaderImageComponent,
        SuccessMessageComponent,
        InfoMessageComponent,
        ErrorMessageComponent,
        WarningMessageComponent,
        PageHeaderComponent,
        DefaultSnackbarComponent,
        ListComponent,
        PaginatorComponent,
        AutocompleteBsComponent,
        PropertyPipe,
        SafeHtmlPipe
    ],
    exports: [
        LoaderLightComponent,
        LoaderDarkComponent,
        LoaderDarkInputComponent,
        LoaderImageComponent,
        SuccessMessageComponent,
        InfoMessageComponent,
        ErrorMessageComponent,
        WarningMessageComponent,
        PageHeaderComponent,
        DefaultSnackbarComponent,
        ListComponent,
        PaginatorComponent,
        AutocompleteBsComponent,
        PropertyPipe,
        SafeHtmlPipe
    ]
})
export class SharedModule {
}
