import * as Fraction from 'fraction.js';

// Returns amount of kilocalories in dish or ingredient
export const kilocaloriesFromMNs = (prots: number, fats: number, carbs: number) =>
  roundToTwoDecimal(4 * (prots + carbs) + 9 * fats);

export const kilocaloriesFromMNsParsed = (prots: any, fats: any, carbs: any) => {
  let res: number = roundToTwoDecimal(4 * (Number.parseInt(prots) + Number.parseInt(carbs)) + 9 * Number.parseInt(fats));

  if (!res || isNaN(res))
    return 0;

  return res;
};

export const roundToTwoDecimal = val => Math.round(val * 100) / 100;

export const doubleToFraction = val => {
  val = roundToTwoDecimal(val);
  let f: any = new Fraction(val);
  return f.toFraction(true);
};
