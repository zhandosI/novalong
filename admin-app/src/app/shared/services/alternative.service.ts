import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { ApiResponse } from '../models/api.response';

import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AlternativeService {

  private readonly apiUrl = '/api/v1/alternatives';

  constructor(private http: HttpClient) {}

  getAllCategories = (): Observable<any> =>
    this.http.get(`${this.apiUrl}/categories`)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
          return res.data;
      })
      .catch(this.handleError);

  getAllRelations = (): Observable<any> =>
    this.http.get(`${this.apiUrl}/relations`)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
          return res.data;
      })
      .catch(this.handleError);

  private handleError = (error: HttpErrorResponse) => Observable.throw(error);
}
