import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {ApiResponse} from "@app/shared/models/api.response";

@Injectable()
export class CatalogService {

    private readonly apiUrl = '/api/v1';
    private readonly catalogApiUrl = '/api/v1/catalogs';

    constructor(private http: HttpClient) {}

    toggleCatalogDataActivation = (catalogName: string, catalogDataId: number): Observable<any> =>
        this.http.delete(`${this.apiUrl}/${catalogName}/${catalogDataId}`)
            .map((res: ApiResponse) => {
                if (res.status === 'success')
                    return res.data;
            })
            .catch(this.handleError);

    addCatalogData = (catalogName: string, catalogData: any): Observable<any> =>
        this.http.post(`${this.apiUrl}/${catalogName}`, catalogData)
            .map((res: ApiResponse) => {
                if (res.status === 'success')
                    return res.data;
            })
            .catch(this.handleError);

    updateCatalogData = (catalogName: string, catalogDataId: number, catalogData: any): Observable<any> =>
        this.http.put(`${this.apiUrl}/${catalogName}/${catalogDataId}`, catalogData)
            .map((res: ApiResponse) => {
                if (res.status === 'success')
                    return res.data;
            })
            .catch(this.handleError);

    getCatalogAllData = (catalogName: string): Observable<any> =>
        this.http.get(`${this.apiUrl}/${catalogName}`)
            .map((res: ApiResponse) => {
                if (res.status === 'success')
                    return res.data;
            })
            .catch(this.handleError);

    getCatalogFkData = (catalogName: string, fkName: string): Observable<any> =>
        this.http.get(`${this.apiUrl}/${catalogName}/${fkName}`)
            .map((res: ApiResponse) => {
                if (res.status === 'success')
                    return res.data;
            })
            .catch(this.handleError);

    getCatalog = (id: number): Observable<any> =>
        this.http.get(`${this.catalogApiUrl}/${id}`)
            .map((res: ApiResponse) => {
                if (res.status === 'success')
                    return res.data;
            })
            .catch(this.handleError);

    updateCatalog = (catalogId: number, catalog: any): Observable<any> =>
        this.http.put(`${this.catalogApiUrl}/${catalogId}`, catalog)
            .map((res: ApiResponse) => {
                if (res.status === 'success')
                    return res.data;
            })
            .catch(this.handleError);

    getAllCatalogs = (): Observable<any> =>
        this.http.get(this.catalogApiUrl)
            .map((res: ApiResponse) => {
                if (res.status === 'success')
                    return res.data;
            })
            .catch(this.handleError);

    private handleError = (error: HttpErrorResponse) => Observable.throw(error);
}
