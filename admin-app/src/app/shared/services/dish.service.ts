import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { ApiResponse } from '../models/api.response';
import { Dish } from '../models/dish/dish';

import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class DishService {

  private readonly apiUrl = '/api/v1/dishes';

  constructor(private http: HttpClient) {}

  getDish = (id: number): Observable<any> =>
    this.http.get(`${this.apiUrl}/${id}`)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
          return res.data;
      })
      .catch(this.handleError);

  getAllDishes = (): Observable<any> =>
    this.http.get(this.apiUrl)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
          return res.data;
      })
      .catch(this.handleError);

  getAllDishesByParams = (params: string): Observable<any> =>
    this.http.get(`${this.apiUrl}?${params}`)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
            return res.data;
      })
      .catch(this.handleError);

  getAllDishesByComplexParams = (matrix: any, query: string): Observable<any> => {
    const params = <any>matrix;
    return this.http.get(`${this.apiUrl}?${query}`, {params})
      .map((res: ApiResponse) => {
        if (res.status === 'success')
            return res;
      })
      .catch(this.handleError);
  };

  getDishesHealthFactors = (matrix: any, query: string): Observable<any> => {
    const params = <any>matrix;
    return this.http.get(`${this.apiUrl}?${query}health_factor`, {params})
      .map((res: ApiResponse) => {
        if (res.status === 'success')
            return res.data;
      })
      .catch(this.handleError);
  };

  saveDish = (dish: Dish): Observable<any> =>
    this.http.post(this.apiUrl, dish)
      .map((res: ApiResponse) => {
        return res.status === 'success';
      })
      .catch(this.handleError);

  editDish = (id: number, dish: Dish): Observable<any> =>
    this.http.put(`${this.apiUrl}/${id}`, dish)
      .map((res: ApiResponse) => {
        return res.status === 'success';
      })
      .catch(this.handleError);

  deleteDish = (id: number): Observable<any> =>
    this.http.delete(`${this.apiUrl}/${id}`)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
          return true;
      })
      .catch(this.handleError);

  getAllDishTypes = (): Observable<any> =>
    this.http.get(`${this.apiUrl}/types`)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
          return res.data;
      })
      .catch(this.handleError);

  getAllDishUnitTypes = (): Observable<any> =>
    this.http.get(`${this.apiUrl}/unit_types`)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
          return res.data;
      })
      .catch(this.handleError);

  getAllDishCategories = (): Observable<any> =>
    this.http.get(`${this.apiUrl}/categories`)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
          return res.data;
      })
      .catch(this.handleError);

  getDishHealthFactor = (id: number): Observable<any> =>
    this.http.get(`${this.apiUrl}/${id}?health_factor`)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
          return res.data;
      })
      .catch(this.handleError);

  loadAllDishPhoto = (matrix: any, query: string): Observable<any> => {
      const params = <any>matrix;
      return this.http.get(`${this.apiUrl}/photos?${query}`, {params})
          .map((res: ApiResponse) => {
              if (res.status === 'success')
                  return res;
          })
          .catch(this.handleError);
  };

  loadDishPhoto = (id: number): Observable<any> =>
      this.http.get(`${this.apiUrl}/${id}/photos`)
          .map((res: ApiResponse) => {
              if (res.status === 'success')
                  return res;
          })
          .catch(this.handleError);

  uploadPhoto(formData: FormData):  Observable<any> {
      return  this.http.post(`${this.apiUrl}/photos`, formData, {
          headers: new HttpHeaders({
              'Accept': 'application/json'})
      })
  }
  private handleError = (error: HttpErrorResponse) => Observable.throw(error);
}
