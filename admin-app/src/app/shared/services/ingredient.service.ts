import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { ApiResponse } from '@app/shared/models/api.response';
import { Ingredient } from '@app/shared/models/ingredient/ingredient';
import { IngredientProperty } from '@app/shared/models/ingredient/ingredient.property';
import { AlternativeIngredient } from '@app/shared/models/alternative/alternative.ingredient';
import { IngredientCookingType } from '@app/shared/models/ingredient/ingredient.cooking.type';

import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {ListResponse} from '@app/shared/models/list.response';
import {IngredientOptionParam} from "@app/shared/models/ingredient/ingredient.option.param";

@Injectable()
export class IngredientService {

  private readonly apiUrl = '/api/v1/ingredients';

  constructor(private http: HttpClient) {}

  getIngredient = (id: number): Observable<any> =>
    this.http.get(`${this.apiUrl}/${id}`)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
          return res.data;
      })
      .catch(this.handleError);

  deleteIngredient = (id: number): Observable<any> =>
    this.http.delete(`${this.apiUrl}/${id}`)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
          return true;
      })
      .catch(this.handleError);

  getAllIngredients = (filter: any): Observable<any> => {
      const params = <any>filter;
      return this.http.get(this.apiUrl, { params })
          .map((res: ListResponse) => {
              if (res.status === 'success')
                  return res;
          })
          .catch(this.handleError);
  };

  getIngredientsByName = (name: string): Observable<any> =>
    this.http.get(`${this.apiUrl}?name=${name}`)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
          return res.data;
      })
      .catch(this.handleError);

  saveIngredient = (ingredient: Ingredient): Observable<any> =>
    this.http.post(this.apiUrl, ingredient)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
          return res.data;
      })
      .catch(this.handleError);

  editIngredient = (id: number, ingredient: Ingredient): Observable<any> =>
    this.http.put(`${this.apiUrl}/${id}`, ingredient)
      .map((res: ApiResponse) => {
        return res.status === 'success';
      })
      .catch(this.handleError);

  getIngredientOptionalProps = (id: number): Observable<any> =>
    this.http.get(`${this.apiUrl}/${id}/optional_props`)
        .map((res: ApiResponse) => {
            if (res.status === 'success')
                return res.data;
        })
        .catch(this.handleError);

  saveIngredientOptionalProps = (id: number, optionparams: IngredientOptionParam[]): Observable<any> =>
    this.http.post(`${this.apiUrl}/${id}/optional_props`, optionparams)
        .map((res: ApiResponse) => {
            if (res.status === 'success')
                return res.data;
        })
        .catch(this.handleError);

  deleteIngredientOptionalProps = (propId: number): Observable<any> =>
    this.http.delete(`${this.apiUrl}/optional_props/${propId}`)
      .map((res: ApiResponse) => {
        return res.status === 'success';
      })
      .catch(this.handleError);

  /********************* INGREDIENT CATEGORY METHODS *********************/
  // TODO: active param is hardcoded down low - could be done better

  getIngredientsCategoryByName = (name: string): Observable<any> =>
    this.http.get(`${this.apiUrl}/categories?name=${name}&active=true`)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
          return res.data;
      })
      .catch(this.handleError);

  getIngredientsCategoriesByParent = (parentCategoryId: number): Observable<any> =>
    this.http.get(`${this.apiUrl}/categories?parent_id=${parentCategoryId}&active=true`)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
          return res.data;
      })
      .catch(this.handleError);

  getAllIngredientsCategories = (): Observable<any> =>
    this.http.get(`${this.apiUrl}/categories`)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
          return res.data;
      })
      .catch(this.handleError);
  /***********************************************************************/

  getAllIngredientsUnitTypes = (): Observable<any> =>
    this.http.get(`${this.apiUrl}/unit_types`)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
          return res.data;
      })
      .catch(this.handleError);

  getAllIngredientsOptionParams = (): Observable<any> =>
    this.http.get(`${this.apiUrl}/optional_props`)
        .map((res: ApiResponse) => {
            if (res.status === 'success')
                return res.data;
        })
        .catch(this.handleError);
  getAllIngredientsChangableProps = (): Observable<any> =>
    this.http.get(`${this.apiUrl}/changable_props`)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
          return res.data;
      })
      .catch(this.handleError);

  getAllIngredientsAlternatives = (id: number, joined: boolean): Observable<any> =>
    this.http.get(`${this.apiUrl}/${id}/alternatives?joined=${joined}`)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
          return res.data;
      })
      .catch(this.handleError);

  saveIngredientAlternatives = (alternativeIngredientModels: AlternativeIngredient[],
                                ingredientId: number): Observable<any> =>
    this.http.post(`${this.apiUrl}/${ingredientId}/alternatives`, alternativeIngredientModels)
      .map((res: ApiResponse) => {
        return res.status === 'success';
      })
      .catch(this.handleError);

    getAllIngredientsProperties = (id: number): Observable<any> =>
      this.http.get(`${this.apiUrl}/${id}/properties`)
        .map((res: ApiResponse) => {
          if (res.status === 'success')
            return res.data;
        })
        .catch(this.handleError);

  saveIngredientProperties = (properties: IngredientProperty[],
                              ingredientId: number): Observable<any> =>
    this.http.post(`${this.apiUrl}/${ingredientId}/properties`, properties)
      .map((res: ApiResponse) => {
        return res.status === 'success';
      })
      .catch(this.handleError);

  getIngredientCookingTypes = (ingredientId: number, joined: boolean): Observable<any> =>
    this.http.get(`${this.apiUrl}/${ingredientId}/cookingtypes?joined=${joined}`)
      .map((res: ApiResponse) => {
        if (res.status === 'success')
          return res.data;
      })
      .catch(this.handleError);

  saveIngredientCookingTypes = (cookingTypes: IngredientCookingType[],
                              ingredientId: number): Observable<any> =>
    this.http.post(`${this.apiUrl}/${ingredientId}/cookingtypes`, cookingTypes)
      .map((res: ApiResponse) => {
        return res.status === 'success';
      })
      .catch(this.handleError);

  private handleError = (error: HttpErrorResponse) => Observable.throw(error);
}
