import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { AuthResponse } from '../models/auth.response';
import { User } from '../models/user';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AuthService {

  constructor(private http: HttpClient) {}

    auth = (user: User): Observable<any> =>
      this.http.post('/api/auth', user).map((res: AuthResponse) => {
        if (res.status === 'success')
          localStorage.setItem('api_token', res.token);

        return res;
      }).catch(this.handleError);

    logout = (): void => localStorage.removeItem('api_token');

    private handleError = (error: HttpErrorResponse) => Observable.throw(error);
}
