import { Injectable } from '@angular/core';

@Injectable()
export class DataService {

  private _successMsg: string;
  private _successEdit: string;
  private _successDelete: string;

  constructor() {}

  get successMsg() { return this._successMsg; }
  set successMsg(val: string) { this._successMsg = val; }

  get successEdit() { return this._successEdit; }
  set successEdit(val: string) { this._successEdit = val; }

  get successDelete() { return this._successDelete; }
  set successDelete(val: string) { this._successDelete = val; }

}
