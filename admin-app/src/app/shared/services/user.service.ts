import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { ApiResponse } from '../models/api.response';

import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

const InterceptorSkipHeader = 'X-Skip-Interceptor';
const reqOptions = { headers: { [InterceptorSkipHeader]: '' } };

@Injectable()
export class UserService {

  private readonly apiUrl = '/api/v1/users';

  constructor(private http: HttpClient) {}

  getUser = (id: number): Observable<any> =>
    this.http.get(`${this.apiUrl}/${id}`, reqOptions)
      .map((res: ApiResponse) => {
        if (res.status === 'success') {
          return res.data;
        }
      })
      .catch(this.handleError)

  getUserDailyRation = (id: number, regenerate: boolean): Observable<any> =>
    this.http.get(`${this.apiUrl}/${id}/rations?regenerate=${regenerate}`, reqOptions)
      .map((res: ApiResponse) => {
        if (res.status === 'success') {
          return res.data;
        }
      })
      .catch(this.handleError)

  getUserRationHealthFactor = (id: number): Observable<any> =>
    this.http.get(`${this.apiUrl}/${id}/rations?health_factor`, reqOptions)
      .map((res: ApiResponse) => {
        if (res.status === 'success') {
          return res.data;
        }
      })
      .catch(this.handleError)

  getUserWeeklyRation = (id: number, regenerate: boolean): Observable<any> =>
    this.http.get(`${this.apiUrl}/${id}/rations?regenerate=${regenerate}`, reqOptions)
      .map((res: ApiResponse) => {
        if (res.status === 'success') {
          return res.data;
        }
      })
      .catch(this.handleError)

  generateDishType = (id: number, dishTypeId: number, dayCode: string, regenerate: boolean) =>
    this.http.get(`${this.apiUrl}/${id}/rations?regenerate=${regenerate}&day=${dayCode}&dish_type_id=${dishTypeId}`, reqOptions)
      .map((res: ApiResponse) => {
        if (res.status === 'success') {
          return res.data;
        }
      })
      .catch(this.handleError)

  private handleError = (error: HttpErrorResponse) => Observable.throw(error);
}
