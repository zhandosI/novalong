import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseModel, ListResponseModel} from '@app/shared/models/base.model';

@Injectable({
    providedIn: 'root'
})
export class ChoicesService {

    private readonly apiUrl = '/api/v1';

    constructor(private http: HttpClient) {
    }

    model(key: string, params?: any): Promise<BaseModel[]> {
        params = params ? params : {};
        params.page = 1;
        params.page_size = 1000000;
        return this.http.get<ListResponseModel<any>>(`${this.apiUrl}/${key}`, {params})
            .toPromise().then(m => {
                return m.data;
            });
    }
}
