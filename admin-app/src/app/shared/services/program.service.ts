import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { ApiResponse } from '../models/api.response';

import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

const InterceptorSkipHeader = 'X-Skip-Interceptor';
const reqOptions = { headers: { [InterceptorSkipHeader]: '' } };

@Injectable()
export class ProgramService {

  private readonly apiUrl = '/api/v1/programs';

  constructor(private http: HttpClient) {}

  getProgram = (id: number): Observable<any> =>
    this.http.get(`${this.apiUrl}/${id}`, reqOptions)
      .map((res: ApiResponse) => {
        if (res.status === 'success') {
          return res.data;
        }
      })
      .catch(this.handleError)

  getAllPrograms = (): Observable<any> =>
    this.http.get(this.apiUrl, reqOptions)
      .map((res: ApiResponse) => {
        if (res.status === 'success') {
          return res.data;
        }
      })
      .catch(this.handleError)

  private handleError = (error: HttpErrorResponse) => Observable.throw(error);
}
