import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Injectable()
export class RouterService {

  private prevUrl: string = undefined;
  private currUrl: string = undefined;

  constructor(private router: Router) {
    this.currUrl = this.router.url;
    router.events
      .subscribe(event => {
        if (event instanceof NavigationEnd) {
            this.prevUrl = this.currUrl;
            this.currUrl = event.url;
        }
      })
  }

  public getPrevUrl = (): string => this.prevUrl;
  public getCurrUrl = (): string => this.currUrl;
}
