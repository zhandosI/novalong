import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class LoaderFullService {

  public isLoaderVisible: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  show() {
    this.isLoaderVisible.next(true);
  }

  hide() {
    this.isLoaderVisible.next(false);
  }
}
