import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoaderFullComponent} from '@app/core/components/loader-full/loader-full.component';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {LoaderInterceptor} from '@app/core/interceptors/loader.intercepter';
import {RouterModule} from '@angular/router';
import {LoaderFullService} from '@app/core/services/loader-full.service';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
    ],
    declarations: [
        LoaderFullComponent,
    ],
    exports: [
        LoaderFullComponent,
    ],
    providers: [
        LoaderFullService
    ]
})
export class CoreModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: CoreModule,
            providers: [
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: LoaderInterceptor,
                    multi: true
                },
            ]
        };
    }
}
