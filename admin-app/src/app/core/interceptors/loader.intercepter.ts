import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import {LoaderFullService} from '@app/core/services/loader-full.service';

const InterceptorSkipHeader = 'X-Skip-Interceptor';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  constructor(private service: LoaderFullService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // To skip paths with InterceptorSkipHeader from showing loader
    if (req.headers.has(InterceptorSkipHeader)) {
      const headers = req.headers.delete(InterceptorSkipHeader);
      return next.handle(req.clone({ headers }));
    }

    if (!this.service.isLoaderVisible.getValue()) {
        this.service.show();
    }

    return next.handle(req)
      .pipe(
        finalize(() => {
            this.service.hide();
        })
      );
  }
}
