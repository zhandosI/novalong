import {AfterViewInit, Component} from '@angular/core';
import {LoaderFullService} from '@app/core/services/loader-full.service';

@Component({
    selector: 'app-loader-full',
    templateUrl: './loader-full.component.html',
    styleUrls: ['./loader-full.component.scss']
})
export class LoaderFullComponent implements AfterViewInit {

    showLoader: boolean;

    constructor(private service: LoaderFullService) {
    }

    ngAfterViewInit() {
        this.service.isLoaderVisible.subscribe((val: boolean) => {
            this.showLoader = val;
        });
    }

}
