import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@app/shared/guards/auth.guard';

const routes: Routes = [
  { path: '', loadChildren: './modules/admin/admin.module#AdminModule', canActivate: [AuthGuard] },
  { path: 'auth', loadChildren: './modules/auth/auth.module#AuthModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {}
