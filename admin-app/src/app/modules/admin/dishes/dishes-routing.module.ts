import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DishesComponent } from './dishes.component';
import { AddDishComponent } from './components/add-dish/add-dish.component';
import { EditDishComponent } from './components/edit-dish/edit-dish.component';
import { EditDishIngredientsComponent } from './components/edit-dish-ingredients/edit-dish-ingredients.component';
import { DishDetailComponent } from './components/dish-detail/dish-detail.component';

const routes: Routes = [
  { path: '', component: DishesComponent },
  { path: 'new', component: AddDishComponent },
  {
    path: ':id',
    children: [
      { path: '', component: DishDetailComponent },
      { path: 'edit', component: EditDishComponent },
      { path: 'edit/ingredients', component: EditDishIngredientsComponent }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DishesRoutingModule { }
