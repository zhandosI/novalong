import {Component, OnDestroy, OnInit} from '@angular/core';
import {routerTransition} from '@app/router.animations';

import {DishService} from '@app/shared/services/dish.service';
import {DataService} from '@app/shared/services/data.service';

import {Dish} from '@app/shared/models/dish/dish';
import {DishCategory} from '@app/shared/models/dish/dish.category';
import {DishType} from '@app/shared/models/dish/dish.type';

import {timestampToDateEn} from '@app/shared/utils/date.format';
import {kilocaloriesFromMNs} from '@app/shared/utils/calculation.util.ts';

@Component({
    selector: 'app-dishes',
    templateUrl: './dishes.component.html',
    styleUrls: ['./dishes.component.scss', '../../../shared/styles/loader.css'],
    animations: [routerTransition()]
})
export class DishesComponent implements OnInit, OnDestroy {

    dishes: Dish[];
    dishCategories: DishCategory[];
    dishTypes: DishType[];

    successDelete: string;
    loading = false;
    searching = false;
    filterFormCollapsed = false;
    dishesChangesMade = false;

    page = 0;
    pageSize = 5;
    total: number;
    query = '';
    loadAvatar = false;

    collectionSize: number;

    constructor(private dishService: DishService,
                private dataService: DataService) {
    }

    ngOnInit() {
        this.successDelete = this.dataService.successDelete;
        this.fetchData();
    }

    ngOnDestroy() {
        this.dataService.successDelete = null;
    }

    searchDishes = (query: string) => {
        // this.searching = true;
        this.dishesChangesMade = true;

        if (!query) {
            this.dishesChangesMade = false;
            this.query = '';
        } else {
            this.dishesChangesMade = true;
            this.query = query;
        }
        this.getAllDishesByComplexParams();
    };

    energyAmount = (protein: number, fat: number, carb: number) => kilocaloriesFromMNs(protein, fat, carb);

    collapseFilterForm = () => {
        this.filterFormCollapsed = !this.filterFormCollapsed;

        if (this.filterFormCollapsed) {
            this.dishesChangesMade = false;
        }

        if (this.dishesChangesMade) {
            // this.searching = true;
            this.getAllDishesByComplexParams();
        }
    };

    onPageChanged = (currentPage: number) => {
        this.page = currentPage;
        this.getAllDishesByComplexParams();
    };

    onPageSizeChange = (currentPageSize: number) => {
        this.pageSize = currentPageSize;
        this.collectionSize = this.total * 10 / currentPageSize;
        this.getAllDishesByComplexParams();
    };

    convertDate = timestamp => timestampToDateEn(timestamp);

    private fetchData = () => {

        this.loading = true;
        this.loadAvatar = true;

        this.dishService.getAllDishesByComplexParams(this.getPageParams(), '')
            .mergeMap(res => {
                this.dishes = res.data;
                this.total = res.items.total;
                this.collectionSize = this.total * 2;
                return this.dishService.getAllDishCategories();
            })
            .mergeMap(dishCategories => {
                this.dishCategories = dishCategories;
                return this.dishService.getAllDishTypes();
            })
            .mergeMap(dishTypes => {
                this.dishTypes = dishTypes;
                this.loading = false;
                return this.dishService.getDishesHealthFactors(this.getPageParams(), '');
            })
            .mergeMap(factors => {
                this.dishes.forEach(d => d.healthFactor = factors.find(f => f.dishId === d.id));
                return this.dishService.loadAllDishPhoto(this.getPageParams(), '');
            })
            .subscribe(dishPhoto => {
                this.dishes.forEach(dish=>{
                    dish.photoBase64 = dishPhoto.data[dish.id];
                });
                this.loadAvatar = false;
            });
    };

    private getAllDishesByComplexParams = (matrixParams = {}) => {

        this.loading = true;
        this.loadAvatar = true;
        const customParams = Object.assign(matrixParams, this.getPageParams());

        this.dishService.getAllDishesByComplexParams(customParams, this.query)
          .mergeMap(res => {
            this.dishes = res !== undefined ? res.data : [];
            this.total = res !== undefined ? res.items.total : 0;
            this.loading = false;
            return this.dishService.getDishesHealthFactors(customParams, this.query);
          })
          .mergeMap(factors => {
            this.dishes.forEach(d => d.healthFactor = factors.find(f => f.dishId === d.id));
            // TODO: partial returns from backend
            return this.dishService.loadAllDishPhoto(this.getPageParams(), '');
          })
          .subscribe(dishPhoto => {
            this.dishes.forEach(d => d.photoBase64 = dishPhoto.data[d.id]);
            this.loadAvatar = false;
          });

        window.scrollTo(0, 0);
    };

    private getPageParams = () => {
        return <any>{
            offset: this.page * this.pageSize,
            limit: this.pageSize,
        };
    }

}
