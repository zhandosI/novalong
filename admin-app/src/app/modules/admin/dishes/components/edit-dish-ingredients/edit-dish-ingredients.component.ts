import { Component, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { routerTransition } from '@app/router.animations';

import { DishService } from '@app/shared/services/dish.service';
import { IngredientService } from '@app/shared/services/ingredient.service';
import { RouterService } from "@app/shared/services/router.service";
import { DataService } from "@app/shared/services/data.service";

import { Dish } from '@app/shared/models/dish/dish';
import { DishIngredient } from "@app/shared/models/dish/dish.ingredient";
import { Ingredient } from '@app/shared/models/ingredient/ingredient';
import { DefaultSnackbarComponent } from '@app/shared/components/snackbars/default-snackbar/default-snackbar.component';

@Component({
  selector: 'app-edit-dish-ingredients',
  templateUrl: './edit-dish-ingredients.component.html',
  styleUrls: ['./edit-dish-ingredients.component.scss'],
  animations: [routerTransition()]
})
export class EditDishIngredientsComponent implements OnInit {

  @ViewChild(DefaultSnackbarComponent) snackbarRef: DefaultSnackbarComponent;

  dish: Dish;
  ingredients: Ingredient[];

  ingredientSearchName: string;
  ingredientNotFound: string = '';
  loading: boolean = false;
  searching: boolean = false;
  saving: boolean = false;
  weightInputDisabled: boolean[] = [];
  error: string;

  constructor(public routerService: RouterService,
              private dishService: DishService,
              private ingredientService: IngredientService,
              private route: ActivatedRoute,
              private router: Router,
              private dataService: DataService) { }

  ngOnInit() {
    this.fetchData();
  }

  onSubmit = () => {
    console.log('dish', this.dish);

    if (this.isValid(this.dish)) {
      this.editDish(this.dish);
    }
  };

  addIngredient = (ingredient: Ingredient) => {
    if (!this.dish.dishIngredients)
      this.dish.dishIngredients = new Array(new DishIngredient(ingredient, 0));
    else if (this.dish.dishIngredients.filter(i => i.ingredient.id === ingredient.id).length === 0)
      this.dish.dishIngredients.push(new DishIngredient(ingredient, 0));

    this.weightInputDisabled.push(false);
    this.snackbarRef.showSnackbar();
  };

  removeIngredient = (ingredientId: number) => {
    const dishIngredient = this.dish.dishIngredients.find(i => i.ingredient.id === ingredientId);
    const index = this.dish.dishIngredients.indexOf(dishIngredient);

    if (index > -1)
      this.dish.dishIngredients.splice(index, 1);

    this.changeDisableInputs();
  };

  onSearchIngredientChange = () => {
    if (this.ingredientSearchName)
      setTimeout(() => this.searchIngredient(this.ingredientSearchName), 500);
    else
      this.ingredients = [];
  };

  onUnitTypeChange = (dishIngredient: DishIngredient) => {
    if (dishIngredient.unitType.code === 'BYTASTE') {
      this.weightInputDisabled[this.dish.dishIngredients.indexOf(dishIngredient)] = true;
      dishIngredient.unitAmount = 0;
    }
    else
      this.weightInputDisabled[this.dish.dishIngredients.indexOf(dishIngredient)] = false;
  };

  private editDish = (dish: Dish): void => {
    this.saving = true;

    this.dishService.editDish(dish.id, dish)
      .subscribe(saved => {
        if (saved) {
          this.dataService.successEdit = `Changes for dish: ${this.dish.name} has been saved!`;
          this.saving = false;
          this.router.navigate(['/dishes', dish.id]);
        }
      });
  };

  private searchIngredient = (name: string) => {
    this.searching = true;

    setTimeout(() => this.ingredientService.getIngredientsByName(name)
      .subscribe(ingredients => {
        if (!ingredients || ingredients.length === 0) this.ingredientNotFound = 'There is no such ingredient!';
        else this.ingredientNotFound = '';

        this.ingredients = ingredients;
        this.searching = false;
      }), 500)
  };

  private isValid = (dish: Dish): boolean => {
    this.error = '';

    if (dish.dishIngredients) {
      for (let dishIngredient of dish.dishIngredients) {
          if ((!dishIngredient.unitAmount && dishIngredient.unitAmount !== 0) || !dishIngredient.unitType) this.error = 'You forgot to fill unit type or value';
      }
    }
    return !this.error;
  };

  private changeDisableInputs = (): void => {
    for (let i in this.dish.dishIngredients) {
      this.weightInputDisabled[i] = this.dish.dishIngredients[i].unitType.code === 'BYTASTE';
    }
  };

  private fetchData = () => {
    this.loading = true;

    this.route.params
      .mergeMap(params =>
        this.dishService.getDish(+params['id']))
      .subscribe(dish => {
        this.dish = dish;
        this.changeDisableInputs();
        this.loading = false;
      });
  };

}
