import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseChartDirective } from 'ng2-charts';
import { routerTransition } from '@app/router.animations';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import 'rxjs/add/operator/mergeMap';

import { DishService } from '@app/shared/services/dish.service';
import { RouterService } from '@app/shared/services/router.service';
import { DataService } from '@app/shared/services/data.service';

import { kilocaloriesFromMNs, doubleToFraction, roundToTwoDecimal } from '@app/shared/utils/calculation.util.ts';

import { Dish } from '@app/shared/models/dish/dish';

@Component({
  selector: 'app-dish-detail',
  templateUrl: './dish-detail.component.html',
  styleUrls: ['./dish-detail.component.scss', '../../../../../shared/styles/loader.css'],
  animations: [routerTransition()]
})
export class DishDetailComponent implements OnInit, OnDestroy {

  @ViewChild(BaseChartDirective) private _chart;

  dish: Dish;
  energyAmount: any = kilocaloriesFromMNs;
  roundToTwoDecimal: any = roundToTwoDecimal;
  totalServingsNumber: string;
  healthFactor: any;

  loading: boolean = false;
  deleting: boolean = false;
  closeResult: string;
  successEdit: string;
  coef: number = 1;
  loadAvatar = false;
  element: HTMLElement;
  public pieChartLabels: string[] = ['Proteins', 'Fats', 'Carbohydrates'];
  public pieChartData: number[];

  constructor(public routerService: RouterService,
              private dishService: DishService,
              private route: ActivatedRoute,
              private router: Router,
              private modalService: NgbModal,
              private dataService: DataService) { }

  ngOnInit() {
    this.successEdit = this.dataService.successEdit;
    this.fetchData(); 
  }

  ngOnDestroy() {
    this.dataService.successEdit = null;
  }

  deleteDish = () => {
    this.deleting = true;

    this.dishService.deleteDish(this.dish.id)
      .subscribe(deleted => {
        if (deleted) {
          this.deleting = false;
          this.dataService.successDelete = `Dish: ${this.dish.name} has been deleted!`;
          this.router.navigate(['/dishes']);
        }
      });
  };

  onCaloriesTriggered = (caloriesTrigger: any) => {
    let weight: number;

    for (let i = 0, n = caloriesTrigger.options.length; i < n ; i++) {
      if (caloriesTrigger.options[i].selected)
        weight = Number.parseFloat(caloriesTrigger.options[i].value);
    }

    this.coef = weight / this.dish.weight;
    this.pieChartData = [
      roundToTwoDecimal(this.coef * this.dish.protein) ,
      roundToTwoDecimal(this.coef * this.dish.fat),
      roundToTwoDecimal(this.coef * this.dish.carbohydrate)
    ];

    this._chart.refresh();
  };

  openConfirmModal = (deleteConfirm) => this.modalService
    .open(deleteConfirm).result
    .then(result => this.closeResult = `Closed with: ${result}`,
          reason => this.closeResult = `Dismissed ${this.getDismissReason(reason)}`);

  private getDismissReason = (reason: any): string => {
    if (reason === ModalDismissReasons.ESC)
      return 'by pressing ESC';
    else if (reason === ModalDismissReasons.BACKDROP_CLICK)
      return 'by clicking on a backdrop';
    else
      return `with: ${reason}`;
  };

  private fetchData = () => {
    this.loading = true;
    this.loadAvatar = true;

    let dishId;
    this.route.params
      .mergeMap(params => {
        dishId = +params['id'];
        return this.dishService.getDish(dishId);
      })
      .mergeMap(dish => {
        this.dish = dish;
        this.init();
        return this.dishService.loadDishPhoto(dish.id);
      })
      .mergeMap(photo => {
        this.dish.photoBase64 = photo.data;
        this.loadAvatar = false;
        return this.dishService.getDishHealthFactor(dishId);
      })
      .subscribe(healthFactor => {
        this.healthFactor = healthFactor;
        this.loading = false;
      });
  };

  fileChange(event){
      this.dish.photoBase64 = null;
      this.loadAvatar = true;
      let fileList: FileList = event.target.files;
      if(fileList.length > 0) {
        let file: File = fileList[0];
        let formData:FormData = new FormData();

        formData.append('uploadFile', file, file.name);
        formData.append("dishId",  this.dish.id.toString());

        this.dishService.uploadPhoto(formData)
          .mergeMap(() => this.dishService.loadDishPhoto(this.dish.id))
          .subscribe(photo => {
            this.dish.photoBase64 = photo.data;
            this.loadAvatar = false;
          });
      }
  }

  fakeClick(){
      this.element = document.getElementById('uploadPhoto') as HTMLElement;
      this.element.click();
  }

  private init = () => {
    this.pieChartData = [this.dish.protein, this.dish.fat, this.dish.carbohydrate];
    this.totalServingsNumber = this.dish.unitTypes && this.dish.unitTypes.length
      ? doubleToFraction(this.dish.weight / this.dish.unitTypes[0].weight) +
        ' (' + this.dish.unitTypes[0].weight + ' grams in each)'
      : 'Not defined';
  };

}
