import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'search-dish-form',
  templateUrl: './search-dish-form.component.html',
  styleUrls: ['./search-dish-form.component.scss']
})
export class SearchDishFormComponent {

  @Output() search: EventEmitter<string> = new EventEmitter();
  
  public query: string;
  private timeout: any = null;

  onSearchInputChange = () => {
    if (this.timeout) window.clearTimeout(this.timeout);

    if (this.query) {
      this.timeout = window.setTimeout(() => {
        this.timeout = null;
        this.search.emit(`q=${this.query}`);
      }, 1000);
    } else {
      this.search.emit(undefined);
    }
  }

}
