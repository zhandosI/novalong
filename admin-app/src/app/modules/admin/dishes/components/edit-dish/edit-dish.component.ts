import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { routerTransition } from '@app/router.animations';

import { DishService } from '@app/shared/services/dish.service';
import { CookingTypeService } from "@app/shared/services/cooking.type.service";
import { RouterService } from "@app/shared/services/router.service";
import { DataService } from "@app/shared/services/data.service";

import { Dish } from '@app/shared/models/dish/dish';
import { DishType } from '@app/shared/models/dish/dish.type';
import { DishCategory } from "@app/shared/models/dish/dish.category";
import { DishUnitType } from "@app/shared/models/dish/dish.unit.type";
import { CookingType } from '@app/shared/models/cooking.type';

@Component({
  selector: 'app-edit-dish',
  templateUrl: './edit-dish.component.html',
  styleUrls: ['./edit-dish.component.scss'],
  animations: [routerTransition()]
})
export class EditDishComponent implements OnInit {

  dish: Dish;
  dishTypes: DishType[];
  dishUnitTypes: DishUnitType[];
  dishCategories: DishCategory;
  cookingTypes: CookingType[];

  dishNameHeading: string;
  loading: boolean = false;
  saving: boolean = false;

  constructor(public routerService: RouterService,
              private dishService: DishService,
              private cookingTypeService: CookingTypeService,
              private route: ActivatedRoute,
              private router: Router,
              private dataService: DataService) { }

  ngOnInit() {
    this.fetchData();
  }

  editDish = (dish: Dish): void => {
    this.saving = true;

    this.dishService.editDish(dish.id, dish)
      .subscribe(saved => {
        if (saved) {
          this.dataService.successEdit = `Changes for dish: ${this.dish.name} has been saved!`;
          this.saving = false;
          this.router.navigate(['/dishes', dish.id]);
        }
      });
  };

  private fetchData = () => {
    this.loading = true;

    this.route.params
      .mergeMap(params =>
        this.dishService.getDish(+params['id']))
      .mergeMap(dish => {
        this.dish = dish;
        this.dishNameHeading = dish.name;
        return this.dishService.getAllDishTypes();
      })
      .mergeMap(dishTypes => {
        this.dishTypes = dishTypes;
        return this.dishService.getAllDishCategories();
      })
      .mergeMap(dishCategories => {
        this.dishCategories = dishCategories;
        return this.dishService.getAllDishUnitTypes();
      })
      .mergeMap(dishUnitTypes => {
        this.dishUnitTypes = dishUnitTypes;
        return this.cookingTypeService.getAllCookingTypes();
      })
      .subscribe(cookingTypes => {
        this.cookingTypes = cookingTypes;
        this.loading = false;
      });
  };

}
