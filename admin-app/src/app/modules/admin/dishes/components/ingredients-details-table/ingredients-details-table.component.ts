import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

import { Ingredient } from '@app/shared/models/ingredient/ingredient';
import { AlternativeIngredientJoined } from '@app/shared/models/alternative/alternative.ingredient.joined';

@Component({
  selector: 'ingredients-details-table',
  templateUrl: './ingredients-details-table.component.html',
  styleUrls: ['./ingredients-details-table.component.scss']
})
export class IngredientsDetailsTableComponent implements OnInit {

  @Input() ingredients: Ingredient[];
  @Input() ingredientAlternatives: AlternativeIngredientJoined[];
  @Input() ingredientDetailsFetching: boolean;
  @Input() ingredientDetails: Ingredient;
  @Input() energyAmount: any;
  @Input() noDetails: boolean;
  
  @Output() addIngredient: EventEmitter<Ingredient> = new EventEmitter();
  @Output() fetchDetails: EventEmitter<number> = new EventEmitter();
  isCollapsed: boolean[] = [];

  ngOnInit() {
    for (let i in this.ingredients) {
      this.isCollapsed.push(true);
    }
  }

  onIngredientClick = (ingredientId: number) => {
    let ingredient = this.ingredients.find(ingredient => ingredient.id === ingredientId);
    this.addIngredient.emit(ingredient);
    this.collapseAll();
  };

  onAlternativeIngredientClick = (ingredient: Ingredient) => {
    this.addIngredient.emit(ingredient);
    this.collapseAll();
  };

  ingredientAlternativesIsEmpty = (): boolean => {
    let empties: boolean[] = [];

    for (let i in this.ingredientAlternatives) {
        const al = this.ingredientAlternatives[i];

        empties[i] = !al.ingredients || al.ingredients.length === 0;
    }
    // if all elements in empties[] is true, the ingredient has got no alternatives
    return empties.filter(e => !e).length <= 0;
  };

  collapseIngredientDetails = (index: number, ingredientId: number) => {
    this.isCollapsed[index] = !this.isCollapsed[index];
    this.fetchDetails.emit(ingredientId);
    this.collapseOthers(index);
  };

  private collapseOthers = (index: number) => {
    for (let i in this.ingredients) {
      if (+i !== index)
        this.isCollapsed[i] = true;
    }
  };

  private collapseAll = () => {
    for (let i in this.ingredients)
      this.isCollapsed[i] = true;
  };
}
