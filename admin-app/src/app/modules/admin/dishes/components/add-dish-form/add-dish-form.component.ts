import { Component, OnInit, Input, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';

import { AddDishFormError } from '@app/shared/models/errors/add-dish-form.error';
import { Dish } from '@app/shared/models/dish/dish';
import { DishType } from '@app/shared/models/dish/dish.type';
import { Ingredient } from '@app/shared/models/ingredient/ingredient';
import { CookingType } from '@app/shared/models/cooking.type';
import { AlternativeIngredientJoined } from '@app/shared/models/alternative/alternative.ingredient.joined';
import { DishIngredient } from "@app/shared/models/dish/dish.ingredient";
import { DishUnitType } from "@app/shared/models/dish/dish.unit.type";
import { DishCategory } from "@app/shared/models/dish/dish.category";
import { kilocaloriesFromMNs } from '@app/shared/utils/calculation.util.ts';
import { DefaultSnackbarComponent } from '@app/shared/components/snackbars/default-snackbar/default-snackbar.component';

@Component({
  selector: 'add-dish-form',
  templateUrl: './add-dish-form.component.html',
  styleUrls: ['./add-dish-form.component.scss']
})
export class AddDishFormComponent implements OnInit {

  @ViewChild('form') formRef: ElementRef;
  @ViewChild(DefaultSnackbarComponent) snackbarRef: DefaultSnackbarComponent;

  @Input() dishTypes: DishType[];
  @Input() dishCategories: DishCategory[];
  @Input() dishUnitTypes: DishUnitType[];
  @Input() cookingTypes: CookingType[];
  @Input() ingredients: Ingredient[];
  @Input() ingredientAlternatives: AlternativeIngredientJoined[];
  @Input() searching: boolean;
  @Input() ingredientDetailsFetching: boolean;
  @Input() ingredientDetails: Ingredient;
  @Input() ingredientNotFound: string;

  @Output() submitDish: EventEmitter<Dish> = new EventEmitter();
  @Output() searchIngredient: EventEmitter<string> = new EventEmitter();
  @Output() fetchIngredientDetails: EventEmitter<number> = new EventEmitter();

  dish: Dish;
  energyAmount: any = kilocaloriesFromMNs;

  error: AddDishFormError = new AddDishFormError();
  ingredientSearchName: string;
  servingAmountDisabled: boolean = true;
  isCollapsed: boolean[] = [];

  ngOnInit() {
    this.initDish();
  }

  onSubmit = () => {
    console.log('dish', this.dish);

    if (this.isValid(this.dish)) {
      let dishToSubmit = JSON.parse(JSON.stringify(this.dish));

      if (!this.dish.unitTypes[0].type)
        dishToSubmit.unitTypes = null;

      this.submitDish.emit(dishToSubmit);
    }
  };

  /*
  * Resetting the form inputs and clearing selected ingredients
  */
  reloadForm = () => {
    this.formRef.nativeElement.reset();
    this.dish.dishIngredients = [];
  };

  onSearchIngredientChange = () => {
    if (this.ingredientSearchName)
      setTimeout(() => this.searchIngredient.emit(this.ingredientSearchName), 500);
    else
      this.ingredients = [];
  };

  addIngredient = (ingredient: Ingredient) => {
    if (!this.dish.dishIngredients) {
      this.dish.dishIngredients = new Array(new DishIngredient(ingredient, 0));
      this.snackbarRef.showSnackbar();
    }
    else if (this.dish.dishIngredients.filter(i => i.ingredient.id === ingredient.id).length === 0) {
      this.dish.dishIngredients.push(new DishIngredient(ingredient, 0));
      this.snackbarRef.showSnackbar();
    }

    this.collapseAll();
  };

  removeIngredient = (ingredientId: number) => {
    const dishIngredient = this.dish.dishIngredients.find(i => i.ingredient.id === ingredientId);
    const index = this.dish.dishIngredients.indexOf(dishIngredient);

    if (index > -1)
      this.dish.dishIngredients.splice(index, 1);

    this.collapseAll();
  };

  collapseIngredientDetails = (index: number, ingredientId: number) => {
    this.isCollapsed[index] = !this.isCollapsed[index];
    this.fetchIngredientDetails.emit(ingredientId);
    this.collapseOthers(index);
  };

  /*
  * Checks is ingredient has got no alternatives in all categories
  */
  ingredientAlternativesIsEmpty = (): boolean => {
    let empties: boolean[] = [];

    for (let i in this.ingredientAlternatives) {
      const al = this.ingredientAlternatives[i];

      empties[i] = !al.ingredients || al.ingredients.length === 0;
    }
    // if all elements in empties[] is true, the ingredient has got no alternatives
    return empties.filter(e => !e).length <= 0;
  };

  /*
  * Temporary solution for only one serving unit type
  */
  onServingInputChange = () => this.servingAmountDisabled = !!this.dish.unitTypes[0].type;

  private collapseOthers = (index: number) => {
    for (let i in this.dish.dishIngredients) {
      if (+i !== index)
        this.isCollapsed[i] = true;
    }
  };

  private collapseAll = () => {
    for (let i in this.dish.dishIngredients)
      this.isCollapsed[i] = true;
  };

  private initDish = () => {
    this.dish = new Dish();
    this.dish.unitTypes = [];
    this.dish.unitTypes.push(new DishUnitType);
  };

  private isValid = (dish: Dish): boolean => {
    for (let e in this.error)
      this.error[e] = '';

    if (!dish.name) this.error.name = 'Name cannot be empty!';
    // if (!dish.description) this.error.description = 'Description cannot be empty!';
    if (!dish.dishTypes || dish.dishTypes.length === 0) this.error.dishTypes = 'Dish types cannot be empty!';
    if (!dish.categories || dish.categories.length === 0) this.error.dishCategory = 'Dish category cannot be empty!';
    // if (!dish.unitTypes[0].type) this.error.servingType = 'Serving cannot be empty!';
    // if (!dish.unitTypes[0].weight) this.error.servingWeight = 'Serving grams cannot be empty!';

    if (dish.dishIngredients) {
      for (let dishIngredient of dish.dishIngredients) {
          if (!dishIngredient.unitAmount || !dishIngredient.unitType) this.error.unitTypes = 'You forgot to fill unit type or value';
      }
    }

    for (let e in this.error) {
      if (this.error[e]) return false;
    }
    return true;
  }

}
