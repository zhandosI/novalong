import { Component, Input, Output, EventEmitter } from '@angular/core';

import { DishCategory } from "@app/shared/models/dish/dish.category";
import { DishType } from '@app/shared/models/dish/dish.type';

@Component({
  selector: 'filter-dish-form',
  templateUrl: './filter-dish-form.component.html',
  styleUrls: ['./filter-dish-form.component.scss']
})
export class FilterDishFormComponent {

  @Input() dishCategories: DishCategory[];
  @Input() dishTypes: DishType[];
  @Output() pushUrl: EventEmitter<string> = new EventEmitter();

  selectedCategories: DishCategory[];

  private categoryParam: string = '';
  private dishTypeParams: string[] = [];

  pushUrlParams = () => {
    let paramUrl: string = '';

    if (this.categoryParam) {
      paramUrl += `categories=${this.categoryParam}`;
    }

    if (this.dishTypeParams.length) {
      let dishTypesParamStr: string = '';

      if (this.dishTypeParams.length > 1) {

        for (let p of this.dishTypeParams) {
          dishTypesParamStr += `${p},`;
        }
        dishTypesParamStr = dishTypesParamStr.slice(0, -1);
      }
      else if (this.dishTypeParams.length === 1)
        dishTypesParamStr = this.dishTypeParams[0];

      if (paramUrl)
        paramUrl += '&';

      paramUrl += `dish_types=${dishTypesParamStr}`;
    }

    this.pushUrl.emit(paramUrl);
  };

  onCategoryChange = () => {
    let s: string = '';
    for (let i = 0, n = this.selectedCategories.length; i < n; i++) {
      if (i === n - 1) {
        s += this.selectedCategories[i].name;
        break;
      }

      s += this.selectedCategories[i].name + ',';
    }
    this.categoryParam = s;

    this.pushUrlParams();
  };

  onDishTypeChange = (typeName: string, checkbox: any) => {
    if (!checkbox.checked) {
      let idx = this.dishTypeParams.indexOf(this.dishTypeParams.find(p => p === typeName));

      if (idx > -1)
        this.dishTypeParams.splice(idx, 1);
    } else {
      this.dishTypeParams.push(typeName);
    }

    this.pushUrlParams();
  };

}
