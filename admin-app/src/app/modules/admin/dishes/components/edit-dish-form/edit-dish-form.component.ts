import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Dish } from '@app/shared/models/dish/dish';
import { DishType } from '@app/shared/models/dish/dish.type';
import { DishCategory } from "@app/shared/models/dish/dish.category";
import { DishUnitType } from "@app/shared/models/dish/dish.unit.type";
import { CookingType } from '@app/shared/models/cooking.type';

import { EditDishFormError } from '@app/shared/models/errors/edit-dish-form.error';

@Component({
  selector: 'edit-dish-form',
  templateUrl: './edit-dish-form.component.html',
  styleUrls: ['./edit-dish-form.component.scss']
})
export class EditDishFormComponent implements OnInit {

  @Input() dish: Dish;
  @Input() dishTypes: DishType[];
  @Input() dishCategories: DishCategory[];
  @Input() dishUnitTypes: DishUnitType[];
  @Input() cookingTypes: CookingType[];

  @Output() submitDish: EventEmitter<Dish> = new EventEmitter();

  error: EditDishFormError = new EditDishFormError();
  servingAmountDisabled: boolean = true;

  ngOnInit() {
    this.initDish();
  }

  onSubmit = () => {
    console.log('dish', this.dish);

    if (this.isValid(this.dish)) {
      let dishToSubmit = JSON.parse(JSON.stringify(this.dish));

      if (!this.dish.unitTypes[0].type)
        dishToSubmit.unitTypes = null;

      this.submitDish.emit(dishToSubmit);
    }
  };

  onServingInputChange = () => this.servingAmountDisabled = !!this.dish.unitTypes[0].type;

  private initDish = () => {
    if (!this.dish.unitTypes)
      this.dish.unitTypes = [];

    if (!this.dish.unitTypes.length)
      this.dish.unitTypes.push(new DishUnitType);
  };

  private isValid = (dish: Dish): boolean => {
    for (let e in this.error)
      this.error[e] = '';

    if (!dish.name) this.error.name = 'Name cannot be empty!';
    if (!dish.dishTypes || dish.dishTypes.length === 0) this.error.dishTypes = 'Dish types cannot be empty!';
    if (!dish.categories || dish.categories.length === 0) this.error.dishCategory = 'Dish category cannot be empty!';
    if (!dish.protein) this.error.protein = 'Protein cannot be empty!';
    if (!dish.fat) this.error.fat = 'Fat cannot be empty!';
    if (!dish.carbohydrate) this.error.carbohydrate = 'Carbohydrate cannot be empty!';
    if (!dish.weight) this.error.weight = 'Weight cannot be empty!';
    // if (!dish.unitTypes[0].type) this.error.servingType = 'Serving cannot be empty!';
    // if (!dish.unitTypes[0].weight) this.error.servingWeight = 'Serving grams cannot be empty!';

    // if (dish.dishIngredients) {
    //   for (let dishIngredient of dish.dishIngredients) {
    //       if (!dishIngredient.unitAmount || !dishIngredient.unitType) this.error.unitTypes = 'You forgot to fill unit type or value';
    //   }
    // }

    for (let e in this.error) {
      if (this.error[e]) return false;
    }
    return true;
  }

}
