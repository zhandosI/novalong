import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '@app/router.animations';
import 'rxjs/add/operator/mergeMap';

import { AddDishFormComponent } from '../add-dish-form/add-dish-form.component';

import { IngredientService } from '@app/shared/services/ingredient.service';
import { DishService } from '@app/shared/services/dish.service';
import { CookingTypeService } from "@app/shared/services/cooking.type.service";

import { AlternativeIngredientJoined } from '@app/shared/models/alternative/alternative.ingredient.joined';
import { Ingredient } from '@app/shared/models/ingredient/ingredient';
import { DishType } from '@app/shared/models/dish/dish.type';
import { Dish } from '@app/shared/models/dish/dish';
import { DishCategory } from "@app/shared/models/dish/dish.category";
import { DishUnitType } from "@app/shared/models/dish/dish.unit.type";
import { CookingType } from '@app/shared/models/cooking.type';

@Component({
  selector: 'app-add-dish',
  templateUrl: './add-dish.component.html',
  styleUrls: ['./add-dish.component.scss'],
  animations: [routerTransition()]
})
export class AddDishComponent implements OnInit {

  @ViewChild(AddDishFormComponent) form: AddDishFormComponent;

  ingredients: Ingredient[];
  ingredientDetails: Ingredient;
  ingredientAlternatives: AlternativeIngredientJoined[];
  dishTypes: DishType[];
  dishCategories: DishCategory[];
  dishUnitTypes: DishUnitType[];
  cookingTypes: CookingType[];

  loading: boolean = false;
  processing: boolean = false;
  searching: boolean = false;
  ingredientDetailsFetching: boolean = false;
  dishSaved: boolean;
  dishSavedError: boolean;
  ingredientNotFound: string = '';

  constructor(private dishService: DishService,
              private cookingTypeService: CookingTypeService,
              private ingredientService: IngredientService) { }

  ngOnInit() {
    this.loading = true;
    this.fetchData();
  }

  createDish = (dish: Dish) => {
    this.dishSaved = this.dishSavedError = false;
    this.processing = true;

    this.dishService.saveDish(dish).subscribe(saved => {
      if (saved) this.dishSaved = true;
      else this.dishSavedError = true;

      this.form.reloadForm();
      this.processing = false;
    });
  };

  searchIngredient = (name: string) => {
    this.searching = true;

    setTimeout(() => this.ingredientService.getIngredientsByName(name).subscribe(ingredients => {
      if (!ingredients || ingredients.length === 0) this.ingredientNotFound = 'There is no such ingredient!';
      else this.ingredientNotFound = '';

      this.ingredients = ingredients;
      this.searching = false;
    }), 500);
  };

  fetchIngredientDetails = (ingredientId: number) => {
    this.ingredientDetailsFetching = true;

    this.ingredientService.getIngredient(ingredientId)
      .mergeMap(ingredientDetails => {
        this.ingredientDetails = ingredientDetails;
        return this.ingredientService.getAllIngredientsAlternatives(ingredientId, true);
      })
      .subscribe(ingredientAlternatives => {
        this.ingredientAlternatives = ingredientAlternatives;
        this.ingredientDetailsFetching = false;
      });
  };

  private fetchData = () => this.dishService.getAllDishTypes()
    .mergeMap(dishTypes => {
      this.dishTypes = dishTypes;
      return this.dishService.getAllDishCategories();
    })
    .mergeMap(dishCategories => {
      this.dishCategories = dishCategories;
      return this.dishService.getAllDishUnitTypes();
    })
    .mergeMap(dishUnitTypes => {
      this.dishUnitTypes = dishUnitTypes;
      return this.cookingTypeService.getAllCookingTypes();
    })
    .subscribe(cookingTypes => {
      this.cookingTypes = cookingTypes;
      this.loading = false;
    });

}
