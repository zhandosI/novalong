import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import { MatTooltipModule } from '@angular/material/tooltip';

import { SharedModule } from '@app/shared/shared.module';
import { DishService } from '@app/shared/services/dish.service';
import { IngredientService } from '@app/shared/services/ingredient.service';
import { RouterService } from "@app/shared/services/router.service";
import { DataService } from "@app/shared/services/data.service";
import { CookingTypeService } from "@app/shared/services/cooking.type.service";

import { DishesRoutingModule } from './dishes-routing.module';
import { DishesComponent } from './dishes.component';
import { AddDishComponent } from './components/add-dish/add-dish.component';
import { AddDishFormComponent } from './components/add-dish-form/add-dish-form.component';
import { EditDishComponent } from './components/edit-dish/edit-dish.component';
import { EditDishFormComponent } from './components/edit-dish-form/edit-dish-form.component';
import { DishDetailComponent } from './components/dish-detail/dish-detail.component';
import { EditDishIngredientsComponent } from './components/edit-dish-ingredients/edit-dish-ingredients.component';
import { IngredientsDetailsTableComponent } from './components/ingredients-details-table/ingredients-details-table.component';
import { PaginationComponent } from '../components/pagination/pagination.component';
import { SearchDishFormComponent } from './components/search-dish-form/search-dish-form.component';
import { FilterDishFormComponent } from './components/filter-dish-form/filter-dish-form.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgSelectModule,
    DishesRoutingModule,
    SharedModule,
    NgbModule,
    Ng2Charts,
    MatTooltipModule
  ],
  providers: [
    DishService,
    IngredientService,
    RouterService,
    DataService,
    CookingTypeService
  ],
  declarations: [
    DishesComponent,
    AddDishComponent,
    AddDishFormComponent,
    EditDishComponent,
    EditDishFormComponent,
    EditDishIngredientsComponent,
    IngredientsDetailsTableComponent,
    DishDetailComponent,
    PaginationComponent,
    SearchDishFormComponent,
    FilterDishFormComponent
  ]
})
export class DishesModule { }
