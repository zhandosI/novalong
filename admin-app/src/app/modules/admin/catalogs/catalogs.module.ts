import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogsRoutingModule } from './catalogs-routing.module';
import { CatalogsComponent } from './catalogs.component';
import {FormsModule} from "@angular/forms";
import {NgSelectModule} from "@ng-select/ng-select";
import {SharedModule} from "@app/shared/shared.module";
import {DataService} from "@app/shared/services/data.service";
import {CatalogService} from "@app/shared/services/catalogs/catalog.service";
import { CatalogDetailComponent } from './components/catalog-detail/catalog-detail.component';
import {EditableModule} from "@app/shared/modules/editable/editable.module";
import {RouterService} from "@app/shared/services/router.service";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgSelectModule,
        EditableModule,
        CatalogsRoutingModule,
        SharedModule
    ],
    providers: [CatalogService, DataService, RouterService],
    declarations: [CatalogsComponent, CatalogDetailComponent]
})
export class CatalogsModule { }
