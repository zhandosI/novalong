import {Component, OnDestroy, OnInit} from '@angular/core';
import {DataService} from "@app/shared/services/data.service";
import {routerTransition} from "@app/router.animations";
import {Catalog} from "@app/shared/models/catalog";
import {CatalogService} from "@app/shared/services/catalogs/catalog.service";

import * as $ from 'jquery';
import 'datatables.net';
import 'datatables.net-bs4';

@Component({
    selector: 'app-catalogs',
    templateUrl: './catalogs.component.html',
    styleUrls: ['./catalogs.component.scss'],
    animations: [routerTransition()]
})
export class CatalogsComponent implements OnInit, OnDestroy {

    catalogs: Catalog[];
    editCatalogModel = new Catalog();
    loading: boolean = false;
    successDelete: string;

    constructor(private catalogService: CatalogService,
        private dataService: DataService) { }

    ngOnInit() {
        this.successDelete = this.dataService.successDelete;
        this.fetchData();
    }

    ngOnDestroy() {
        this.dataService.successDelete = null;
    }

    private fetchData = () => {
        this.loading = true;

        this.catalogService.getAllCatalogs().subscribe(catalogs => {
            this.catalogs = catalogs;

            $(document).ready(() => ($("#dataTable") as any).DataTable({
                "pageLength": 25
            }));
            this.loading = false;
        });
    }

    editCatalog(editCatalogModel: Catalog) {
        this.editCatalogModel = editCatalogModel;
    }

    cancelEditCatalog() {
        this.editCatalogModel = new Catalog();
        this.fetchData();
    }

    updateCatalog(catalogId: number) {
        console.log(this.editCatalogModel);

        this.catalogService.updateCatalog(catalogId, this.editCatalogModel).subscribe(response => {
            this.editCatalogModel = new Catalog();
            this.fetchData();
        }, error => {
            this.editCatalogModel = new Catalog();
            console.log(error);
        });
        // this._apiService.updateCatalog(this.editCatalogModel).subscribe(response => {
        //     this.editCatalogModel = new Catalog();
        //     console.log(response);
        //     this.requestConstructor();
        // }, error => {
        //     this.editCatalogModel = new Catalog();
        //     console.log(error);
        // });
    }
}
