import {Component, OnDestroy, OnInit} from '@angular/core';
import {RouterService} from "@app/shared/services/router.service";
import { ActivatedRoute, Router } from '@angular/router';
import {CatalogService} from "@app/shared/services/catalogs/catalog.service";
import {DataService} from "@app/shared/services/data.service";
import {routerTransition} from "@app/router.animations";
import {Catalog} from "@app/shared/models/catalog";

import 'rxjs/add/operator/mergeMap';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: 'app-catalog-detail',
    templateUrl: './catalog-detail.component.html',
    styleUrls: ['./catalog-detail.component.scss'],
    animations: [routerTransition()],
})
export class CatalogDetailComponent implements OnInit, OnDestroy {

    successEdit: string;
    loading: boolean = true;
    modalReference: any;
    catalog: Catalog;
    catalogData: any[];
    editCatalogDataModel: any = {};
    addCatalogDataModel: any = {};

    propertyKeys: any[];
    // primary: any;
    // foreignKeys: string[] = [];
    foreignKeys: any;
    fkMap: any;
    booleanChoicesList: any[] = [{id: "true", name: "Да"}, {id: "false", name: "Нет"}];


    uneditableKeys: string[] = ['id', 'code', 'active', 'hasChildCategories', 'isDish']; // exception keys that are not editable
    hiddenKeys: string[] = ['ingredientProperties', 'programDishTypes', 'purpose', 'dataType', 'dishType', 'program', 'parentCategory', 'colorCode']; // exception keys that are not editable

    constructor(public routerService: RouterService,
                private route: ActivatedRoute,
                private router: Router,
                private modalService: NgbModal,
                private catalogService: CatalogService,
                private dataService: DataService) {
    }

    ngOnInit() {
        this.foreignKeys = new Set();
        this.fkMap = new Map();
        this.successEdit = this.dataService.successEdit;
        this.fetchFullData();
    }

    ngOnDestroy() {
        this.dataService.successEdit = null;
    }

    private fetchFullData = () => {
        this.loading = true;
        let tableId: number;

        this.route.params
            .mergeMap(params => {
                tableId = +params['id'];
                return this.catalogService.getCatalog(tableId);
            })
            .mergeMap(catalog => {
                this.catalog = catalog;
                // this.addCatalogDataModel = this.catalog.jsonTemplate;
                return this.catalogService.getCatalogAllData(this.catalog.name);
            })
            .subscribe(catalogData => {
                // this.catalogData = catalogData;
                this.catalogData = catalogData;

                this.propertyKeys = this.getFilteredPropertyNamesOfObject(Object.keys(catalogData[0]));
                console.log('propertyKeys: ' + JSON.stringify(this.propertyKeys));

                this.loading = false;
        });
    };

    private fetchCatalogData = () => {
        this.loading = true;

        this.catalogService.getCatalogAllData(this.catalog.name)
            .subscribe(catalogData => {
                this.catalogData = catalogData;
                this.loading = false;
            });
    };

    private getFilteredPropertyNamesOfObject(keys: any) {
        for(let key of keys) {
            // this.isForeign(key) ||
            if(this.hiddenKeys.includes(key)) {
                keys = keys.filter(function(el) {
                    return el !== key;
                });
            }
        }
        return keys;
    }

    private fillForeignKeyMap() {
        console.log('this.foreignKeys: ' + JSON.stringify(this.foreignKeys.size));
        console.log('fkMap size: ' + this.fkMap.size);

        if (this.foreignKeys !== undefined && this.foreignKeys.size > 0 && this.fkMap.size < this.foreignKeys.size) {
            const self = this;
            console.log('foreignKeys size ' + this.foreignKeys.size);
            // let fks = [];
            this.foreignKeys.forEach(function (key) {
                // console.log('id: ' + key);
                // fks.push(key);

                self.catalogService.getCatalogFkData(self.catalog.name, key.slice(0, -2))
                    .subscribe(fkData => {
                        // console.log('fk data response: ' + fkData);
                        self.fkMap.set(key, fkData);
                    });
            });
            // console.log('fks to be added: ' + fks);
        }
    }

    newCatalogDataModal(addNew) {
        this.fillForeignKeyMap();

        this.modalReference = this.modalService.open(addNew, { centered: true });
        this.modalReference.result.then((result) => {
            console.log(result);
            // this.resetForm();
        }, (reason) => {
            console.log(reason);
            // this.resetForm();
        });
    }

    addNewCatalogData() {
        console.log('addNewCatalogData: ' + JSON.stringify(this.addCatalogDataModel));
        this.catalogService.addCatalogData(this.catalog.name, this.addCatalogDataModel).subscribe(response => {
            this.addCatalogDataModel = {};
            this.modalReference.close();
            console.log('addNewCatalogData response: ' + response);
            this.fetchCatalogData();
        }, error => {
            this.addCatalogDataModel = {};
            console.log(error);
        });
    }

    toggleCatalogDataActivation(catalogDataId: number) {
        this.catalogService.toggleCatalogDataActivation(this.catalog.name, catalogDataId).subscribe(response => {
            this.fetchCatalogData();
        });
    }

    editCatalogData(editCatalogDataModel: Catalog) {
        this.editCatalogDataModel = editCatalogDataModel;
        this.fillForeignKeyMap();
    }

    cancelEditCatalogData() {
        this.editCatalogDataModel = new Catalog();
        this.fetchCatalogData();
    }

    updateCatalogData(catalogDataId: number) {
        console.log(this.editCatalogDataModel);
        this.catalogService.updateCatalogData(this.catalog.name, catalogDataId, this.editCatalogDataModel)
            .subscribe(response => {
            this.editCatalogDataModel = {};
            console.log('updateCatalogData response: ' + response);
            this.fetchCatalogData();
        }, error => {
            this.editCatalogDataModel = {};
            console.log(error);
        });
    }

    isForeign(key){
        if(key.includes('Id')) {
            this.foreignKeys.add(key);
            return true;
        }
        return false;
    }

    isObject(val) {
        return typeof val === 'object';
    }

    isInteger(attr) {
        let val = JSON.parse(this.catalog.jsonTemplate)[attr];
        // console.log('isInteger attr: ' + attr + " " + val + ": " + (typeof val === 'number'));
        return typeof val === 'number';
    }

    isBoolean(attr) {
        let val = JSON.parse(this.catalog.jsonTemplate)[attr];
        return typeof val === 'boolean';
    }

    isBooleanVal(val) {
        return typeof val === 'boolean';
    }

    isString(attr) {
        let val = JSON.parse(this.catalog.jsonTemplate)[attr];

        if(!this.isDate(val)){
            return typeof val === 'string';
        }else{
            return false;
        }
    }

    isDate(attr) {
        let val = JSON.parse(this.catalog.jsonTemplate)[attr];
        return val === '1970-01-01T00:00:00';
    }

    previousState() {
        window.history.back();
    }
}
