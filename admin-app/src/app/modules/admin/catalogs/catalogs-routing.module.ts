import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CatalogsComponent} from "@app/modules/admin/catalogs/catalogs.component";
import {CatalogDetailComponent} from "@app/modules/admin/catalogs/components/catalog-detail/catalog-detail.component";

const routes: Routes = [
    { path: '', component: CatalogsComponent },
    { path: ':id', component: CatalogDetailComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CatalogsRoutingModule { }
