import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent {

    @Input() collectionSize: number;
    @Output() changePage: EventEmitter<number> = new EventEmitter();
    @Output() changePageSize: EventEmitter<number> = new EventEmitter();

    currentPage: number;
    currentPageSize: number;

    constructor() {
      this.currentPage = 1;
      this.currentPageSize = 5;
    }

    onPageChange = () => {
      this.changePage.emit(this.currentPage - 1);
    }
    onPageSizeChange = () => {
      this.currentPage = 0;
      this.changePage.emit(0);
      this.changePageSize.emit(this.currentPageSize);
    }

}
