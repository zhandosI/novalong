import { Component, OnDestroy, OnInit } from '@angular/core';
import { routerTransition } from '@app/router.animations';

import { IngredientService } from '@app/shared/services/ingredient.service';
import { DataService } from '@app/shared/services/data.service';

import { Ingredient } from '@app/shared/models/ingredient/ingredient';
import { AbstractList } from '@app/shared/absctract/abstract-list/abstract-list.component';
import { Router } from '@angular/router';

@Component({
    selector: 'app-ingredients',
    templateUrl: './ingredients.component.html',
    styleUrls: ['./ingredients.component.scss'],
    animations: [routerTransition()]
})
export class IngredientsComponent extends AbstractList implements OnInit, OnDestroy {

    list$ = {data: [], items: { total: 0 }};
    ingredients: Ingredient[];
    successDelete: string;

    columns: any[] = [
        {
            value: 'id',
            name: 'ID',
            width: '5%',
            property: 'id',
            type: null,
            sortable: true,
            filter: {
                type: null,
            }
        },
        {
            value: 'name', // sorting value for query
            name: 'Name', // thead label
            width: '20%', // column width in table
            property: 'name', // property name
            type: null, // type: null, 'embedded'
            sortable: true, // is sortable or not
            filter: {
                property: 'name', // parameter name for query
                type: 'text', // parameter type: [null, 'text', 'autocomplete']
                key: null, // key for entity list
                placeholder: 'Filter by name' // filter-input placeholder
            }
        },
        {
            value: 'protein',
            name: 'Protein',
            width: '9%',
            property: 'protein',
            type: null,
            sortable: false,
            filter: {
                type: null,
            }
        },
        {
            value: 'fat',
            name: 'Fat',
            width: '5%',
            property: 'fat',
            type: null,
            sortable: false,
            filter: {
                type: null,
            }
        },
        {
            value: 'carbohydrate',
            name: 'Carbohydrate',
            width: '13%',
            property: 'carbohydrate',
            type: null,
            sortable: false,
            filter: {
                type: null,
            }
        },
        {
            value: 'kilocalories',
            name: 'Energy amount',
            width: '13%',
            property: 'kilocalories',
            type: null,
            sortable: false,
            filter: {
                type: null,
            }
        },
        {
            value: 'allergic',
            name: 'Allergen',
            width: '15%',
            property: 'allergic',
            type: null,
            sortable: true,
            filter: {
                type: null,
            }
        },
        {
            value: 'category.name',
            name: 'Category',
            width: '20%',
            property: 'category',
            embedded_property: 'name',
            type: 'embedded',
            sortable: true,
            filter: {
                property: 'category',
                type: 'autocomplete',
                key: 'ingredients_categories',
                placeholder: 'Filter by category',
                default: {
                    id: 0,
                    name:  'All categories',
                }
            }
        },
    ];

    constructor(private ingredientService: IngredientService,
                private dataService: DataService,
                router: Router) {
        super(router);
    }

    ngOnInit() {
        this.successDelete = this.dataService.successDelete;
    }

    ngOnDestroy() {
        this.dataService.successDelete = null;
    }

    onRowClicked(event) {
        window.open(`/#/ingredients/${event.row.id}/`, '_blank'); // in new tab
    }

    updateList() {
        const filter = {
            offset: (this.paginator.pageIndex - 1) * this.paginator.limit,
            limit: this.paginator.limit,
            sort: '+category.name,+name',
        };

        if (this.list.sort !== null && this.list.sort !== undefined && this.list.sort !== '') {
            filter.sort = this.list.sort;
        }
        Object.assign(filter, this.list.filtering);
        this.fetchData(filter);
    }

    private fetchData(filter: any) {
      this.ingredientService
        .getAllIngredients(filter)
        .subscribe(response => this.list$ = response);
    }
}
