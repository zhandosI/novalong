import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IngredientsComponent } from './ingredients.component';
import { IngredientDetailComponent } from './components/ingredient-detail/ingredient-detail.component';
import { AddIngredientComponent } from './components/add-ingredient/add-ingredient.component';
import { AddIngredientAlternativesComponent } from './components/add-ingredient-alternatives/add-ingredient-alternatives.component';
import { AddIngredientCookingTypesComponent } from './components/add-ingredient-cooking-types/add-ingredient-cooking-types.component';
import { EditIngredientComponent } from './components/edit-ingredient/edit-ingredient.component';
import { UploadIngredientsComponent } from './components/upload-ingredients/upload-ingredients.component';

const routes: Routes = [
  { path: '', component: IngredientsComponent },
  { path: 'new', component: AddIngredientComponent },
  { path: 'upload', component: UploadIngredientsComponent },
  {
    path: ':id',
    children: [
      { path: '', component: IngredientDetailComponent },
      { path: 'alternatives', component: AddIngredientAlternativesComponent },
      { path: 'cookingtypes', component: AddIngredientCookingTypesComponent },
      { path: 'edit', component: EditIngredientComponent }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IngredientsRoutingModule { }
