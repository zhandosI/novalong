import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';
import { ChartsModule as Ng2Charts } from 'ng2-charts';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { FileDropModule } from 'ngx-file-drop';

import { SharedModule } from '@app/shared/shared.module';
import { IngredientService } from '@app/shared/services/ingredient.service';
import { AlternativeService } from '@app/shared/services/alternative.service';
import { RouterService } from "@app/shared/services/router.service";
import { CookingTypeService } from "@app/shared/services/cooking.type.service";
import { DishService } from '@app/shared/services/dish.service';
import { DataService } from "@app/shared/services/data.service";
import { FileService } from "@app/shared/services/file.service";

import { IngredientsRoutingModule } from './ingredients-routing.module';
import { IngredientsComponent } from './ingredients.component';
import { IngredientDetailComponent } from './components/ingredient-detail/ingredient-detail.component';
import { AddIngredientComponent } from './components/add-ingredient/add-ingredient.component';
import { AddIngredientAlternativesComponent } from './components/add-ingredient-alternatives/add-ingredient-alternatives.component';
import { AddIngredientFormComponent } from './components/add-ingredient-form/add-ingredient-form.component';
import { IngredientsTableComponent } from './components/ingredients-table/ingredients-table.component';
import { AddIngredientCookingTypesComponent } from './components/add-ingredient-cooking-types/add-ingredient-cooking-types.component';
import { EditIngredientComponent } from './components/edit-ingredient/edit-ingredient.component';
import { EditIngredientFormComponent } from './components/edit-ingredient-form/edit-ingredient-form.component';
import { UnitTypeInputFormComponent } from './components/unit-type-input-form/unit-type-input-form.component';
import { IngredientUnitTypesComponent } from './components/ingredient-unit-types/ingredient-unit-types.component';
import { CategoryInputFormComponent } from './components/category-input-form/category-input-form.component';
import { OptionParamInputFormComponent } from './components/option-param-input-form/option-param-input-form.component';
import { IngredientOptionParamsComponent } from './components/ingredient-option-params/ingredient-option-params.component';
import { UploadIngredientsComponent } from './components/upload-ingredients/upload-ingredients.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    Ng2Charts,
    NgbDropdownModule.forRoot(),
    FileDropModule,
    NgSelectModule,
    IngredientsRoutingModule,
    SharedModule
  ],
  providers: [
    IngredientService,
    AlternativeService,
    RouterService,
    CookingTypeService,
    DishService,
    DataService,
    FileService
  ],
  declarations: [
    IngredientsComponent,
    IngredientDetailComponent,
    AddIngredientComponent,
    AddIngredientAlternativesComponent,
    AddIngredientFormComponent,
    UnitTypeInputFormComponent,
    IngredientsTableComponent,
    AddIngredientCookingTypesComponent,
    EditIngredientComponent,
    EditIngredientFormComponent,
    IngredientUnitTypesComponent,
    CategoryInputFormComponent,
    OptionParamInputFormComponent,
    IngredientOptionParamsComponent,
    UploadIngredientsComponent
  ],
  entryComponents: [
    UnitTypeInputFormComponent,
    CategoryInputFormComponent,
    OptionParamInputFormComponent
  ]
})
export class IngredientsModule { }
