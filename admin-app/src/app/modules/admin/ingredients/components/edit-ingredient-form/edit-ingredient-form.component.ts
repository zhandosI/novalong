import { Component, OnInit, Input, Output, EventEmitter,
         ComponentFactoryResolver, Type,
         ViewChild, ViewContainerRef } from '@angular/core';

 import { IngredientService } from '@app/shared/services/ingredient.service';

 import { CategoryInputFormComponent } from '../category-input-form/category-input-form.component';

import { Ingredient } from '@app/shared/models/ingredient/ingredient';
import { IngredientCategory } from '@app/shared/models/ingredient/ingredient.category';
import { UnitType } from '@app/shared/models/unit.type';
import { EditIngredientFormError } from '@app/shared/models/errors/edit-ingredient-form.error';

import { kilocaloriesFromMNsParsed } from '@app/shared/utils/calculation.util.ts'

@Component({
  selector: 'edit-ingredient-form',
  templateUrl: './edit-ingredient-form.component.html',
  styleUrls: ['./edit-ingredient-form.component.scss']
})
export class EditIngredientFormComponent implements OnInit {
  @ViewChild('categoryFormsContainer', {read: ViewContainerRef}) categoriesContainer: ViewContainerRef;

  @Input() ingredient: Ingredient;
  @Input() categories: IngredientCategory[];
  @Input() unitTypes: UnitType[];
  @Output() submitIngredient: EventEmitter<Ingredient> = new EventEmitter();

  energyAmount: any = kilocaloriesFromMNsParsed;
  error: EditIngredientFormError = new EditIngredientFormError();

  categoryFormClass = CategoryInputFormComponent;
  categoriesDepth: number = 0;

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private ingredientService: IngredientService) {}

  ngOnInit() {
    this.addCategoryForm(this.categoryFormClass, this.categories);
  }

  onSubmit = () => {
    console.log('ingredient', this.ingredient);

    if (this.isValid(this.ingredient)) {
      this.submitIngredient.emit(this.ingredient);
    }
  };

  onCategorySelected = (val: any) => {
    this.ingredient.category = val.category;

    if (val.category.hasChildCategories) {
      this.ingredientService
        .getIngredientsCategoriesByParent(val.category.id)
        .subscribe(categories => {
          if (this.categoriesDepth > val.index + 1)
            this.removeCategoryForms(val.index);

          this.addCategoryForm(this.categoryFormClass, categories);
        });
    } else {
      this.removeCategoryForms(val.index);
    }
  };

  addCategoryForm = (formClass: Type<any>, categories: IngredientCategory[]) => {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(formClass);
    const form = this.categoriesContainer.createComponent(componentFactory);

    form.instance.categories = categories;
    form.instance.index = this.categoriesDepth;
    form.instance.submitCategory.subscribe(category => this.onCategorySelected(category));

    ++this.categoriesDepth;
  };

  /****************** REMOVES ALL FORMS AFTER INDEX PASSED *****************/
  removeCategoryForms = (index: number) => {
    for (let i = index + 1; i < this.categoriesDepth; i ++) {
      this.categoriesContainer.remove(i);
    }
    this.categoriesDepth = index + 1;
  };

  private isValid = (ingredient: Ingredient): boolean => {
    for (let e in this.error)
      this.error[e] = '';

    if (!ingredient.name) this.error.name = 'Name cannot be empty!';
    if (!ingredient.category) this.error.category = 'Category cannot be empty!';
    if (ingredient.allergic === undefined || ingredient.allergic === null) this.error.allergic = 'Allerginicity cannot be empty!';
    if (!ingredient.protein && ingredient.protein !== 0) this.error.protein = 'Protein cannot be empty!';
    if (!ingredient.carbohydrate && ingredient.carbohydrate !== 0) this.error.carbohydrate = 'Carbohydrate cannot be empty!';
    if (!ingredient.fat && ingredient.fat !== 0) this.error.fat = 'Fat cannot be empty!';
    // if (!ingredient.glycemicIndex) this.error.glycemicIndex = 'Glycemic index cannot be empty!';

    for (let e in this.error) {
      if (this.error[e]) return false;
    }
    return true;
  }

}
