import {Component, DoCheck, EventEmitter, Input, Output} from '@angular/core';
import {UnitType} from "@app/shared/models/unit.type";
import {Ingredient} from "@app/shared/models/ingredient/ingredient";
import {IngredientUnitType} from "@app/shared/models/ingredient/ingredient.unit.type";
import {OptionParam} from "@app/shared/models/option.param";
import {IngredientOptionParam} from "@app/shared/models/ingredient/ingredient.option.param";

@Component({
  selector: 'app-ingredient-option-params',
  templateUrl: './ingredient-option-params.component.html',
  styleUrls: ['./ingredient-option-params.component.scss']
})
export class IngredientOptionParamsComponent implements DoCheck {

  // TODO: Change the logic with save and delete button
  
  @Input() ingredientOptionParamsInput: IngredientOptionParam[];

  @Input() optionParams: OptionParam[];
  @Output() saveOptionParams: EventEmitter<IngredientOptionParam[]> = new EventEmitter();
  @Output() deleteOptionParams: EventEmitter<number[]> = new EventEmitter();

  newOptionParamCollapsed: boolean = false;
  changeOptionParamCollapsed: boolean = false;
  changesMade: boolean = false;

  ingredientOptionParams: IngredientOptionParam[] = [];
  newOptionParam: IngredientOptionParam;

  removeOptionParam:boolean = false;
  removeIndexes: number[] = [];

    ngOnInit(): void {
       this.ingredientOptionParams  = this.ingredientOptionParamsInput.map(x => Object.assign({}, x));
    }

    ngDoCheck(): void {
      this.changesMade =  !!(this.compareArrays(this.ingredientOptionParams, this.ingredientOptionParamsInput) || this.newOptionParam);
    }

    saveOptionParamChanges = () => {
        if(this.removeOptionParam === true && this.removeIndexes !== null && this.removeIndexes !== undefined &&
            this.removeIndexes.length !== 0){
            this.deleteOptionParams.emit(this.removeIndexes);
        }else{
            if (this.newOptionParam && this.newOptionParam.param)
                this.ingredientOptionParamsInput.push(this.newOptionParam);

            this.saveOptionParams.emit(this.ingredientOptionParamsInput);
        }
    };

    toggleNewOptionParam = () => {
        if (this.newOptionParamCollapsed === true)
            this.newOptionParam = undefined;
        this.newOptionParamCollapsed = !this.newOptionParamCollapsed;
    };

    toggleChangeOptionParam = () => {
        this.removeOptionParam = false;
        this.removeIndexes = [];
        this.changeOptionParamCollapsed = !this.changeOptionParamCollapsed;
        if(this.changeOptionParamCollapsed === false){
            this.ingredientOptionParams  = this.ingredientOptionParamsInput.map(x => Object.assign({}, x));
        }
    };

    onNewOptionParamChange = (selectForm: any) => {
        for (let i = 0; i < selectForm.options.length; i++) {
            if (selectForm.options[i].selected) {
                let code = selectForm.options[i].value;

                if (!this.newOptionParam) {
                    this.newOptionParam = new IngredientOptionParam();
                }

                this.newOptionParam.param = this.optionParams.find(u => u.code === code);
            }
        }
    };

    onNewOPWeightChange = (inputForm: any) => {
        if (!this.newOptionParam)
            this.newOptionParam = new IngredientOptionParam();
        this.newOptionParam.weight = Number.parseInt(inputForm.value);
    };

    removeOptionalParam = (index: number): void => {
        this.removeOptionParam = true;
        this.removeIndexes.push(this.ingredientOptionParamsInput[index].id);
        if (index > -1) {
            this.ingredientOptionParamsInput.splice(index, 1);
        }
    };

    private compareArrays = (arr1: any[], arr2: any[]): boolean => JSON.stringify(arr1) != JSON.stringify(arr2);

}
