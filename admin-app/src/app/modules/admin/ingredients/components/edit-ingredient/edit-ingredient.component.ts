import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { routerTransition } from '@app/router.animations';

import { IngredientService } from "@app/shared/services/ingredient.service";
import { RouterService } from "@app/shared/services/router.service";
import { DataService } from "@app/shared/services/data.service";

import { Ingredient } from '@app/shared/models/ingredient/ingredient';
import { IngredientCategory } from '@app/shared/models/ingredient/ingredient.category';
import { UnitType } from '@app/shared/models/unit.type';

@Component({
  selector: 'app-edit-ingredient',
  templateUrl: './edit-ingredient.component.html',
  styleUrls: ['./edit-ingredient.component.scss'],
  animations: [routerTransition()]
})
export class EditIngredientComponent implements OnInit {

  ingredient: Ingredient;
  categories: IngredientCategory[];
  unitTypes: UnitType[];

  ingredientNameHeading: string;
  loading: boolean = false;
  saving: boolean = false;

  constructor(public routerService: RouterService,
              private ingredientService: IngredientService,
              private route: ActivatedRoute,
              private router: Router,
              private dataService: DataService) { }

  ngOnInit() {
    this.fetchData();
  }

  editIngredient = (ingredient: Ingredient): void => {
    this.saving = true;

    this.ingredientService.editIngredient(ingredient.id, ingredient)
      .subscribe(saved => {
        if (saved) {
          this.dataService.successEdit = `Changes for ingredient: ${this.ingredient.name} has been saved!`;
          this.saving = false;
          this.router.navigate(['/ingredients', ingredient.id]);
        }
      });
  };

  private fetchData = () => {
    this.loading = true;

    this.route.params
      .mergeMap(params =>
        this.ingredientService.getIngredient(+params['id']))
      .mergeMap(ingredient => {
        this.ingredient = ingredient;
        this.ingredientNameHeading = ingredient.name;
        return this.ingredientService.getIngredientsCategoryByName('ROOT');
      })
      .mergeMap(categories => {
        return this.ingredientService.getIngredientsCategoriesByParent(categories[0].id);
      })
      .mergeMap(categories => {
        this.categories = categories;
        return this.ingredientService.getAllIngredientsUnitTypes();
      })
      .subscribe(unitTypes => {
        this.unitTypes = unitTypes;
        this.loading = false;
      });
  };

}
