import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { routerTransition } from '@app/router.animations';

import { IngredientService } from '@app/shared/services/ingredient.service';
import { RouterService } from "@app/shared/services/router.service";
import { CookingTypeService } from "@app/shared/services/cooking.type.service";

import { Ingredient } from '@app/shared/models/ingredient/ingredient';
import { IngredientChangableProp } from '@app/shared/models/ingredient/ingredient.changable.prop';
import { IngredientCookingType } from '@app/shared/models/ingredient/ingredient.cooking.type';
import { CookingType } from '@app/shared/models/cooking.type';

@Component({
  selector: 'app-add-ingredient-cooking-types',
  templateUrl: './add-ingredient-cooking-types.component.html',
  styleUrls: ['./add-ingredient-cooking-types.component.scss'],
  animations: [routerTransition()]
})
export class AddIngredientCookingTypesComponent implements OnInit {

  ingredient: Ingredient;
  cookingTypes: CookingType[];
  changableProps: IngredientChangableProp[];

  selectedCookingTypes: CookingType[];
  ingredientCookingTypes: IngredientCookingType[] = [];

  formRef: any;
  error: string;
  loading: boolean = false;
  saved: boolean = false;
  processing: boolean = false;
  submitDisabled: boolean = true;

  constructor(public routerService: RouterService,
              private ingredientService: IngredientService,
              private cookingTypeService: CookingTypeService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.fetchData();
  }

  onSubmit = (form: any) => {
      console.log('ingredient cooking types', this.ingredientCookingTypes);

      this.formRef = form;
      this.error = '';

      if (this.isValid(this.ingredientCookingTypes)) {
        this.saveIngredientCookingTypes(this.ingredientCookingTypes);
      }
  };

  onChange = (type: CookingType, input: any): void => {
    let ingrCookingType;

    for (let iType of this.ingredientCookingTypes) {
      if (iType.cookingType.name === type.name)
        ingrCookingType = iType;
    }

    if (!ingrCookingType) {
      ingrCookingType = new IngredientCookingType();
      ingrCookingType.cookingType = type;
      this.ingredientCookingTypes.push(ingrCookingType);
    }

    switch (input.name) {
      case 'property':
        let name;
        for (let i = 0, n = input.options.length; i < n; i++) {
          if (input.options[i].selected)
            name = input.options[i].value;
        }
        let prop = this.changableProps.find(el => el.name === name);
        ingrCookingType.changableProp = prop;
        break;

      case 'difference':
        ingrCookingType.differenceCoefficient = input.value / 100;
        break;
    }
  };

  onCookingTypeChanged = (): void => {
    if (!this.selectedCookingTypes || this.selectedCookingTypes.length === 0) {
      this.ingredientCookingTypes = [];
      this.submitDisabled = true;
    } else {
      this.submitDisabled = false;
    }
  };

  private reloadForm = () => this.formRef.reset();

  private saveIngredientCookingTypes = (ingredientCookingTypes: IngredientCookingType[]): void => {
    this.processing = true;
    this.saved = false;

    this.ingredientService.saveIngredientCookingTypes(ingredientCookingTypes, this.ingredient.id)
      .subscribe(saved => {
        if (saved) {
          this.saved = true;
          this.processing = false;
          this.reloadForm();
        }
      },
      err => {
          if (err.status === 409) {
            this.error = err.error.message;
          }

          this.processing = false;
        });
  };

  private fetchData = () => {
    this.loading = true;

    this.route.params
      .mergeMap(params =>
          this.ingredientService.getIngredient(+params['id']))
      .mergeMap(ingredient => {
          this.ingredient = ingredient;
          return this.ingredientService.getAllIngredientsChangableProps();
      })
      .mergeMap(changableProps => {
        this.changableProps = changableProps;
        return this.cookingTypeService.getAllCookingTypes();
      })
      .subscribe(cookingTypes => {
        this.cookingTypes = cookingTypes;
        this.loading = false;
      });
  };

  private isValid = (ingredientCookingTypes: IngredientCookingType[]): boolean => {
    if (!ingredientCookingTypes || ingredientCookingTypes.length === 0) {
        this.error = 'You\'ve forgotten something to add!';
        return false;
    }

    for (let type of ingredientCookingTypes) {
      if (!type.cookingType || !type.changableProp || !type.differenceCoefficient) {
        this.error = 'You\'ve forgotten something to add!';
        return false;
      }
    }

    return true;
  };

}
