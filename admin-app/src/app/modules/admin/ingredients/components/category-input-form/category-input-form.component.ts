import { Component, Input, Output, EventEmitter } from '@angular/core';

import { IngredientCategory } from '@app/shared/models/ingredient/ingredient.category';

@Component({
  selector: 'category-input-form',
  templateUrl: './category-input-form.component.html',
  styleUrls: ['./category-input-form.component.scss']
})
export class CategoryInputFormComponent {

  @Input() categories: IngredientCategory[];
  @Input() index: number;

  @Output() submitCategory: EventEmitter<any> = new EventEmitter();

  onCategorySelected = (form: any) => {
    let category: IngredientCategory;

    for (let i = 0, n = form.options.length; i < n ; i++) {
      if (form.options[i].selected)
        category = this.categories.find(c => c.id == form.options[i].value);
    }
    this.submitCategory.emit({ category, index: this.index });
  }

}
