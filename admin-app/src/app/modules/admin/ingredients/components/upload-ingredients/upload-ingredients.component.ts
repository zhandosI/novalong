import { Component, OnInit } from '@angular/core';
import { routerTransition } from '@app/router.animations';
import { UploadEvent, UploadFile, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';

import { FileService } from "@app/shared/services/file.service";

@Component({
  selector: 'app-upload-ingredients',
  templateUrl: './upload-ingredients.component.html',
  styleUrls: ['./upload-ingredients.component.scss'],
  animations: [routerTransition()]
})
export class UploadIngredientsComponent implements OnInit {

  constructor(private fileService: FileService) { }

  ngOnInit() {
  }

  public files: File[] = [];
  public uploadFiles: UploadFile[] = [];

  public dropped(event: UploadEvent) {
    this.uploadFiles = event.files;

    for (const droppedFile of event.files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          this.files.push(file)
          console.log(droppedFile.relativePath, file);
        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

  public upload(event) { console.log('ff', this.uploadFiles)
    if (this.uploadFiles.length > 0) {
      for (const droppedFile of this.uploadFiles) {

        if (droppedFile.fileEntry.isFile) {
          const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;

          fileEntry.file((file: File) => {

            const formData = new FormData()
            formData.append('ingredients', file, droppedFile.relativePath)

            this.fileService.uploadIngredientsCsv(formData)
              .subscribe(r => console.log('rr', r))
          });
        } else {
          // It was a directory (empty directories are added, otherwise only files)
          const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
          console.log(droppedFile.relativePath, fileEntry);
        }
      }
    }
  }

  public fileOver(event){
    // console.log(event);
  }

  public fileLeave(event){
    // console.log(event);
  }

}
