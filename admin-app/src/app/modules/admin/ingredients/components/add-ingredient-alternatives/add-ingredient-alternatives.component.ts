import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { routerTransition } from '@app/router.animations';
import 'rxjs/add/operator/mergeMap';

import { IngredientService } from '@app/shared/services/ingredient.service';
import { AlternativeService } from '@app/shared/services/alternative.service';
import { RouterService } from "@app/shared/services/router.service";

import { Ingredient } from '@app/shared/models/ingredient/ingredient';
import { AlternativeIngredient } from '@app/shared/models/alternative/alternative.ingredient';
import { AlternativeCategory } from '@app/shared/models/alternative/alternative.category';
import { AlternativeRelation } from '@app/shared/models/alternative/alternative.relation';

@Component({
  selector: 'app-add-ingredient-alternatives',
  templateUrl: './add-ingredient-alternatives.component.html',
  styleUrls: ['./add-ingredient-alternatives.component.scss'],
  animations: [routerTransition()]
})
export class AddIngredientAlternativesComponent implements OnInit {

  alternativeIngredientModels: AlternativeIngredient[] = [];

  ingredients: Ingredient[];
  alternativeIngredients: Ingredient[] = [];

  ingredient: Ingredient;
  categories: AlternativeCategory[];
  relations: AlternativeRelation[];

  loading: boolean = false;
  searching: boolean = false;
  processing: boolean = false;
  ingredientSearchName: string;
  error: string;

  ingredientAlternativeSaved: boolean = false;

  constructor(public routerService: RouterService,
              private ingredientService: IngredientService,
              private alternativeService: AlternativeService,
              private route: ActivatedRoute) {}

  ngOnInit() {
    this.fetchData();
  }

  onSubmit = () => {
    this.error = '';

    if (this.isValid(this.alternativeIngredientModels))
      this.postAlternativeIngredients(this.alternativeIngredientModels, this.ingredient.id);
  };

  addAlternativeIngredient = (ingredient: Ingredient) => {
    this.error = '';

    if (this.alternativeIngredients.filter(i => i.id === ingredient.id).length === 0) {
      this.alternativeIngredients.push(ingredient);

      let alternativeIngredientModel = new AlternativeIngredient();
      alternativeIngredientModel.ingredient = ingredient;
      this.alternativeIngredientModels.push(alternativeIngredientModel);
    }
  };

  removeAlternativeIngredient = (ingredientId: number) => {
    this.error = '';

    const ingredient = this.alternativeIngredients.find(i => i.id === ingredientId);
    const index = this.alternativeIngredients.indexOf(ingredient);

    if (index > -1)
      this.alternativeIngredients.splice(index, 1);

    const model = this.alternativeIngredientModels.find(model => model.ingredient.id === ingredientId);
    const modelIndex = this.alternativeIngredientModels.indexOf(model);

    if (modelIndex > -1)
      this.alternativeIngredientModels.splice(modelIndex, 1);
  };

  onCategoryChange = (ingredientId: number, form: any) => {
    this.error = '';

    const model = this.alternativeIngredientModels.find(model => model.ingredient.id === ingredientId);
    const index = this.alternativeIngredientModels.indexOf(model);

    let code;
    for (let i = 0, n = form.options.length; i<n ;i++) {
      if (form.options[i].selected)
        code = form.options[i].value;
    }
    const category = this.categories.find(c => c.code === code);
    this.alternativeIngredientModels[index].category = category;
  };

  onRelationChange = (ingredientId: number, form: any) => {
    this.error = '';

    const model = this.alternativeIngredientModels.find(model => model.ingredient.id === ingredientId);
    const index = this.alternativeIngredientModels.indexOf(model);

    let code;
    for (let i = 0, n = form.options.length; i<n ;i++) {
      if (form.options[i].selected)
        code = form.options[i].value;
    }
    const relation = this.relations.find(r => r.code === code);
    this.alternativeIngredientModels[index].relation = relation;
  };

  onSearchIngredientChange = () => {
    if (this.ingredientSearchName)
      setTimeout(() => this.searchIngredient(this.ingredientSearchName), 500);
    else
      this.ingredients = [];
  };

  private postAlternativeIngredients = (alternativeIngredientModels: AlternativeIngredient[],
                                        ingredientId: number) => {
    this.ingredientAlternativeSaved = false;
    this.processing = true;

    this.ingredientService
      .saveIngredientAlternatives( alternativeIngredientModels, ingredientId).subscribe(
      saved => {
        if (saved) {
          this.ingredientAlternativeSaved = true;
          this.ingredients = [];
          this.alternativeIngredients = [];
          this.alternativeIngredientModels = [];
          this.ingredientSearchName = '';
        }

        this.processing = false;
    },
    err => {
        if (err.status === 409) {
          this.error = err.error.message;
        }

        this.processing = false;
      }
    );
  };

  private searchIngredient = (name: string) => {
    this.error = '';
    this.searching = true;

    setTimeout(() => this.ingredientService.getIngredientsByName(name).subscribe(ingredients => {
      this.ingredients = ingredients;
      this.searching = false;
    }), 500);
  };

  private fetchData = () => {
    this.loading = true;

    this.route.params
      .mergeMap(params =>
          this.ingredientService.getIngredient(+params['id']))
      .mergeMap(ingredient => {
          this.ingredient = ingredient;
          return this.alternativeService.getAllCategories();
      })
      .mergeMap(categories => {
          this.categories = categories;
          return this.alternativeService.getAllRelations();
      })
      .subscribe(relations => {
          this.relations = relations;
          this.loading = false;
      });
  };

  private isValid = (alternativeIngredientModels: AlternativeIngredient[]): boolean => {
    if (!this.alternativeIngredientModels || this.alternativeIngredientModels.length === 0) {
      this.error = 'Sie müssen mindestens eine Zutat hinzufügen!';
      return false;
    }

    for (let model of alternativeIngredientModels) {
      if (!model.ingredient || !model.category || !model.relation) {
        this.error = 'Das geht nicht, Sie haben vergessen etwas zu füllen!';
        return false;
      }
    }
    return true;
  };

}
