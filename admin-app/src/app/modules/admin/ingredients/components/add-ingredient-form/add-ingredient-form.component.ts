import { Component, OnInit, Input, Output, EventEmitter,
         ComponentFactoryResolver, Type, ElementRef,
         ViewChild, ViewContainerRef } from '@angular/core';

import { IngredientService } from '@app/shared/services/ingredient.service';

import { UnitTypeInputFormComponent } from '../unit-type-input-form/unit-type-input-form.component';
import { OptionParamInputFormComponent } from '../option-param-input-form/option-param-input-form.component';

import { CategoryInputFormComponent } from '../category-input-form/category-input-form.component';

import { Ingredient } from '@app/shared/models/ingredient/ingredient';
import { Dish } from '@app/shared/models/dish/dish';
import { IngredientCategory } from '@app/shared/models/ingredient/ingredient.category';
import { IngredientUnitType } from '@app/shared/models/ingredient/ingredient.unit.type';



import { IngredientProperty } from '@app/shared/models/ingredient/ingredient.property';
import { IngredientPropertyType } from '@app/shared/models/ingredient/ingredient.property.type';
import { UnitType } from '@app/shared/models/unit.type';
import { AddIngredientFormError } from '@app/shared/models/errors/add-ingredient-form.error';

import { kilocaloriesFromMNsParsed } from '@app/shared/utils/calculation.util.ts'
import {OptionParam} from "@app/shared/models/option.param";
import {IngredientOptionParam} from "@app/shared/models/ingredient/ingredient.option.param";

@Component({
  selector: 'add-ingredient-form',
  templateUrl: './add-ingredient-form.component.html',
  styleUrls: ['./add-ingredient-form.component.scss']
})
export class AddIngredientFormComponent implements OnInit {
  @ViewChild('unitTypeFormsContainer', {read: ViewContainerRef}) unitTypesContainer: ViewContainerRef;
  @ViewChild('optionParamFormsContainer', {read: ViewContainerRef}) optionParamContainer: ViewContainerRef;
  @ViewChild('categoryFormsContainer', {read: ViewContainerRef}) categoriesContainer: ViewContainerRef;
  @ViewChild('form') formRef: ElementRef;

  @Input() categories: IngredientCategory[];
  @Input() unitTypes: UnitType[];
  @Input() optionParams: OptionParam[];
  @Output() submitIngredient: EventEmitter<Ingredient> = new EventEmitter();
  @Output() submitDish: EventEmitter<Dish> = new EventEmitter();
  @Output() submitIngredientProperties: EventEmitter<IngredientProperty[]> = new EventEmitter();

  ingredient: Ingredient;
  ingredientProperties: IngredientProperty[] = [];
  ingredientPropValue: string;
  isADish: boolean = false;
  dish: Dish;

  error: AddIngredientFormError = new AddIngredientFormError();
  energyAmount: any = kilocaloriesFromMNsParsed;

  unitTypeFormComponents = [];
  optionParamFormComponents = [];

  formClass = UnitTypeInputFormComponent;
  optionParamClass = OptionParamInputFormComponent;
  categoryFormClass = CategoryInputFormComponent;

  categoriesDepth: number = 0;

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private ingredientService: IngredientService) {}

  ngOnInit() {
    this.addUnitTypeForm(this.formClass);
    this.addOptionParamForm(this.optionParamClass);
    this.addCategoryForm(this.categoryFormClass, this.categories);
    this.initIngredient();
  }

  onSubmit = () => {
    console.log('ingredient', this.ingredient);

    for (let e in this.error)
      this.error[e] = '';

    if (this.isValid(this.ingredient)) {
      if (this.isADish) {
        this.dish.name = this.ingredient.name;
        this.dish.fat = this.ingredient.fat;
        this.dish.carbohydrate = this.ingredient.carbohydrate;
        this.dish.protein = this.ingredient.protein;

        this.submitDish.emit(this.dish);
      }

      this.submitIngredientProperties.emit(this.ingredientProperties);
      this.submitIngredient.emit(this.ingredient);
    }
  };

  onCategorySelected = (val: any) => {
    this.ingredient.category = val.category;
    this.ingredient.allergic = val.category.allergic;

    if (val.category.hasChildCategories) {
      this.ingredientService
        .getIngredientsCategoriesByParent(val.category.id)
        .subscribe(categories => {
          if (this.categoriesDepth > val.index + 1)
            this.removeCategoryForms(val.index);

          this.addCategoryForm(this.categoryFormClass, categories);
        });
    } else {
      this.removeCategoryForms(val.index);
    }

    this.ingredientProperties = [];
  };

  addCategoryForm = (formClass: Type<any>, categories: IngredientCategory[]) => {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(formClass);
    const form = this.categoriesContainer.createComponent(componentFactory);

    form.instance.categories = categories;
    form.instance.index = this.categoriesDepth;
    form.instance.submitCategory.subscribe(category => this.onCategorySelected(category));

    ++this.categoriesDepth;
  };

  /****************** REMOVES ALL FORMS AFTER INDEX PASSED *****************/
  removeCategoryForms = (index: number) => {
    for (let i = index + 1; i < this.categoriesDepth; i ++) {
      this.categoriesContainer.remove(i);
    }
    this.categoriesDepth = index + 1;
  };

  /****************** OPTIONAL PARAMS *****************/
  addOptionParamForm = (formClass: Type<any>) => {

      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(formClass);
      const form = this.optionParamContainer.createComponent(componentFactory);

      /* @Input */
      form.instance.optionParams = this.optionParams;
      /* @Output */
      form.instance.submitIngredientOptionParam
          .subscribe(ingredientOptionParam => this.addIngredientOptionParam(ingredientOptionParam));
      this.optionParamFormComponents.push(form);
  };

  removeOptionParamForm = (formClass: Type<any>) => {
      // const component = this.optionParamFormComponents.find(component => component.instance instanceof formClass);
      const component = this.optionParamFormComponents[this.optionParamFormComponents.length - 1];
      const componentIndex = this.optionParamFormComponents.indexOf(component);

      if (componentIndex !== -1) {
          this.optionParamContainer.remove(this.optionParamContainer.indexOf(component));
          this.optionParamFormComponents.splice(componentIndex, 1);
      }
  };

  addIngredientOptionParam = (ingredientOptionParam: IngredientOptionParam) => {
      if (this.ingredient.optionalParams) {
          const optionParam = this.ingredient.optionalParams.find(u => u.param.id === ingredientOptionParam.param.id);

          if (optionParam) {
              const index = this.ingredient.optionalParams.indexOf(optionParam);

              if (index > -1) {
                  this.ingredient.optionalParams.splice(index, 1);
              }
          }

          this.ingredient.optionalParams.push(ingredientOptionParam);
      }
      else
          this.ingredient.optionalParams = new Array(ingredientOptionParam);
  };

  /****************** UNIT TYPES *****************/
  addUnitTypeForm = (formClass: Type<any>) => {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(formClass);
    const form = this.unitTypesContainer.createComponent(componentFactory);

    /* @Input */
    form.instance.unitTypes = this.unitTypes;
    /* @Output */
    form.instance.submitIngredientUnitType
      .subscribe(ingredientUnitType => this.addIngredientUnitType(ingredientUnitType));

    this.unitTypeFormComponents.push(form);
  };

  removeUnitTypeForm = (formClass: Type<any>) => {
    //const component = this.unitTypeFormComponents.find(component => component.instance instanceof formClass);
    const component = this.unitTypeFormComponents[this.unitTypeFormComponents.length - 1];
    const componentIndex = this.unitTypeFormComponents.indexOf(component);

    if (componentIndex !== -1) {
      this.unitTypesContainer.remove(this.unitTypesContainer.indexOf(component));
      this.unitTypeFormComponents.splice(componentIndex, 1);
    }
  };

  addIngredientUnitType = (ingredientUnitType: IngredientUnitType) => {
    if (this.ingredient.unitTypes) {
      const unitType = this.ingredient.unitTypes.find(u => u.type.id === ingredientUnitType.type.id);

      if (unitType) {
        const index = this.ingredient.unitTypes.indexOf(unitType);

        if (index > -1) {
          this.ingredient.unitTypes.splice(index, 1);
        }
      }

      this.ingredient.unitTypes.push(ingredientUnitType);
    }
    else
      this.ingredient.unitTypes = new Array(ingredientUnitType);
  };

  onIngredientPropertyChange = (property: IngredientPropertyType) =>  {
    const prop = this.ingredientProperties.find(p => p.property.id === property.id);

    if (!prop) {
      this.ingredientProperties.push(new IngredientProperty(property, this.ingredientPropValue));
    }
    else {
      const index = this.ingredientProperties.indexOf(prop);
      this.ingredientProperties[index].value = this.ingredientPropValue;
    }
  };

  reloadForm = () => this.formRef.nativeElement.reset();

  private initIngredient = (): void => {
    this.ingredient = new Ingredient();
    this.ingredient.fat = 0;
    this.ingredient.carbohydrate = 0;
    this.ingredient.protein = 0;

    this.dish = new Dish();
  };

  private isValid = (ingredient: Ingredient): boolean => {
    if (!ingredient.name) this.error.name = 'Name cannot be empty!';
    if (!ingredient.category) this.error.category = 'Category cannot be empty!';
    if (ingredient.allergic === undefined || ingredient.allergic === null) this.error.allergic = 'Allerginicity cannot be empty!';
    if (!ingredient.protein && ingredient.protein !== 0) this.error.protein = 'Protein cannot be empty!';
    if (!ingredient.carbohydrate && ingredient.carbohydrate !== 0) this.error.carbohydrate = 'Carbohydrate cannot be empty!';
    if (!ingredient.fat && ingredient.fat !== 0) this.error.fat = 'Fat cannot be empty!';
    // if (!ingredient.glycemicIndex) this.error.glycemicIndex = 'Glycemic index cannot be empty!';

    if (this.isADish && !this.dish.weight) this.error.dishWeight = 'Dish weight cannot be empty!';

    for (let e in this.error) {
      if (this.error[e]) return false;
    }
    return true;
  }

}
