import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { routerTransition } from '@app/router.animations';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import 'rxjs/add/operator/mergeMap';

import { IngredientService } from '@app/shared/services/ingredient.service';
import { RouterService } from "@app/shared/services/router.service";
import { DataService } from "@app/shared/services/data.service";

import { Ingredient } from '@app/shared/models/ingredient/ingredient';
import { AlternativeIngredientJoined } from '@app/shared/models/alternative/alternative.ingredient.joined';
import { IngredientProperty } from "@app/shared/models/ingredient/ingredient.property";
import { UnitType } from '@app/shared/models/unit.type';
import { IngredientUnitType } from '@app/shared/models/ingredient/ingredient.unit.type';
import { IngredientCookingTypeJoined } from '@app/shared/models/ingredient/ingredient.cooking.type.joined';
import { OptionParam } from "@app/shared/models/option.param";
import { IngredientOptionParam } from "@app/shared/models/ingredient/ingredient.option.param";

@Component({
  selector: 'ingredient-detail',
  templateUrl: './ingredient-detail.component.html',
  styleUrls: ['./ingredient-detail.component.scss'],
  animations: [routerTransition()]
})
export class IngredientDetailComponent implements OnInit, OnDestroy {

  ingredient: Ingredient;
  unitTypes: UnitType[];
  optionParams: OptionParam[];

  ingredientAlternatives: AlternativeIngredientJoined[];
  ingredientProps: IngredientProperty[];
  ingredientCookingTypes: IngredientCookingTypeJoined[];
  ingredientOptionParams: IngredientOptionParam[];

  loading: boolean = true;
  deleting: boolean = false;

  closeResult: string;
  successEdit: string;
  successUnitTypesEdit: string;
  successOptionParamsEdit: string;

  public pieChartLabels: string[] = ['Proteins', 'Fats', 'Carbohydrates'];
  public pieChartData: number[];

  constructor(public routerService: RouterService,
              private ingredientService: IngredientService,
              private route: ActivatedRoute,
              private router: Router,
              private modalService: NgbModal,
              private dataService: DataService) {}

  ngOnInit() {
    this.successEdit = this.dataService.successEdit;
    this.fetchData();
  }

  ngOnDestroy() {
    this.dataService.successEdit = null;
  }

  saveUnitTypes = (ingredientUnitTypes: IngredientUnitType[]) => {
    this.loading = true;
    // copies an ingredient object into new one with new unit types
    let ingredientToSubmit: Ingredient = JSON.parse(JSON.stringify(this.ingredient));
    ingredientToSubmit.unitTypes = ingredientUnitTypes;

    this.ingredientService.editIngredient(ingredientToSubmit.id, ingredientToSubmit)
      .subscribe(saved => {
        if (saved) {
          this.fetchData();
          this.successUnitTypesEdit = `Unit types for ingredient: ${this.ingredient.name} has been saved!`;
        }
      });
  };

  saveOptionParams = (ingredientOptionParams: IngredientOptionParam[]) => {
      this.loading = true;
      this.ingredientService.saveIngredientOptionalProps(this.ingredient.id, ingredientOptionParams)
          .subscribe(saved => {
              this.fetchData();
              this.successOptionParamsEdit = `Optional params for ingredient: ${this.ingredient.name} has been saved!`;
      });
  };

  deleteOptionParams = (paramIds: number[]) => {
      this.loading = true;
      for(let paramId of paramIds){
          this.ingredientService.deleteIngredientOptionalProps(paramId)
              .subscribe(saved => {
                  if (saved) {
                      this.fetchData();
                      this.successOptionParamsEdit = `Optional param for ingredient: ${this.ingredient.name} has been deleted!`;
                  }
              });
      }
  };
    /*
    * Checks if ingredient has got no alternatives in all categories
    */
  ingredientAlternativesIsEmpty = (): boolean => {
    let empties: boolean[] = [];

    for (let i in this.ingredientAlternatives) {
      const al = this.ingredientAlternatives[i];

      empties[i] = !al.ingredients || al.ingredients.length === 0;
    }
    // if all elements in empties[] is true, the ingredient has got no alternatives
    return empties.filter(e => !e).length <= 0;
  };

  deleteIngredient = () => {
    this.deleting = true;

    this.ingredientService.deleteIngredient(this.ingredient.id)
      .subscribe(deleted => {
        if (deleted) {
          this.deleting = false;
          this.dataService.successDelete = `Ingredient: ${this.ingredient.name} has been deleted!`;
          this.router.navigate(['/ingredients']);
        }
      });
  };

  openConfirmModal = (deleteConfirm) => this.modalService
    .open(deleteConfirm).result
    .then(result => this.closeResult = `Closed with: ${result}`,
          reason => this.closeResult = `Dismissed ${this.getDismissReason(reason)}`);

  private getDismissReason = (reason: any): string => {
    if (reason === ModalDismissReasons.ESC)
      return 'by pressing ESC';
    else if (reason === ModalDismissReasons.BACKDROP_CLICK)
      return 'by clicking on a backdrop';
    else
      return `with: ${reason}`;
  };

  private fetchData = () => {
    this.loading = true;
    let ingredientId: number;

    this.route.params
      .mergeMap(params => {
          ingredientId = +params['id'];
          return this.ingredientService.getIngredient(ingredientId)
      })
      .mergeMap(ingredient => {
          this.ingredient = ingredient;
          this.pieChartData = [ingredient.protein, ingredient.fat, ingredient.carbohydrate];
          return this.ingredientService.getIngredientOptionalProps(ingredientId);
      })
      .mergeMap(ingredientOptionParams => {
        this.ingredientOptionParams = ingredientOptionParams;        
        return this.ingredientService.getAllIngredientsAlternatives(ingredientId, true);
      })
      .mergeMap(ingredientAlternatives => {
          this.ingredientAlternatives = ingredientAlternatives;
          return this.ingredientService.getAllIngredientsProperties(ingredientId)
      })
      .mergeMap(ingredientProps => {
          this.ingredientProps = ingredientProps;
          return this.ingredientService.getAllIngredientsOptionParams();
      })
      .mergeMap(optionParams => {
          this.optionParams = optionParams;
          return this.ingredientService.getAllIngredientsUnitTypes();
      })
      .mergeMap(unitTypes => {
        this.unitTypes = unitTypes;
        return this.ingredientService.getIngredientCookingTypes(ingredientId, true);
      })
      .subscribe(ingredientCookingTypes => {
        this.ingredientCookingTypes = ingredientCookingTypes;
        this.loading = false;
      });
  };
}
