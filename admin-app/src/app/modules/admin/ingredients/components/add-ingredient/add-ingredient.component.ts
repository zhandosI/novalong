import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '@app/router.animations';

import { EMPTY } from 'rxjs'
import 'rxjs/add/operator/mergeMap';

import { IngredientService } from '@app/shared/services/ingredient.service';
import { DishService } from '@app/shared/services/dish.service';

import { AddIngredientFormComponent } from '../add-ingredient-form/add-ingredient-form.component';
import { IngredientCategory } from '@app/shared/models/ingredient/ingredient.category';
import { IngredientProperty } from '@app/shared/models/ingredient/ingredient.property';
import { Ingredient } from '@app/shared/models/ingredient/ingredient';
import { Dish } from '@app/shared/models/dish/dish';
import { UnitType } from '@app/shared/models/unit.type';
import {OptionParam} from "@app/shared/models/option.param";

@Component({
  selector: 'app-add-ingredient',
  templateUrl: './add-ingredient.component.html',
  styleUrls: ['./add-ingredient.component.scss'],
  animations: [routerTransition()]
})
export class AddIngredientComponent implements OnInit {

  @ViewChild(AddIngredientFormComponent) form: AddIngredientFormComponent;

  categories: IngredientCategory[];
  ingredientProps: IngredientProperty[];
  unitTypes: UnitType[];
  optionParams: OptionParam[];
  loading: boolean = false;
  processing: boolean = false;
  ingredientSaved: boolean = false;
  ingredientsPropsSaved: boolean = false;
  ingredientSavedError: boolean = false;
  newIngredientId: number;
  dishSaved: boolean = false;
  
  constructor(private ingredientService: IngredientService,
              private dishService: DishService) { }

  ngOnInit() {
    this.loading = true;
    this.fetchData();
  }

  createIngredient = (ingredient: Ingredient) => {
    this.ingredientSaved = this.ingredientSavedError = false;
    this.ingredientsPropsSaved = false;

    this.processing = true;

    this.ingredientService.saveIngredient(ingredient)
      .mergeMap(newIngredient => {
        if (newIngredient.id) {
          this.ingredientSaved = true;
          this.newIngredientId = newIngredient.id;

          return this.createIngredientProperties(this.ingredientProps, this.newIngredientId);
        }
        else {
          this.ingredientSavedError = true;
          return EMPTY;
        }
      })
      .subscribe(ingredientsPropsSaved => {
        if (ingredientsPropsSaved)
          this.ingredientsPropsSaved = ingredientsPropsSaved;

        this.form.reloadForm();
        this.processing = false;
      });
  };

  createIngredientProperties = (props: IngredientProperty[], ingredientId: number) =>
    this.ingredientService.saveIngredientProperties(props, ingredientId);

  setIngredientProperties = (props: IngredientProperty[]) => {
    this.ingredientProps = props;
  };

  createDish = (dish: Dish) =>
    this.dishService.saveDish(dish).subscribe(saved => {
      if (saved) this.dishSaved = true;
    });

  private fetchData = () => this.ingredientService.getIngredientsCategoryByName('ROOT')
    .mergeMap(categories => {
      return this.ingredientService.getIngredientsCategoriesByParent(categories[0].id);
    })
    .mergeMap(categories => {
      this.categories = categories;
      return this.ingredientService.getAllIngredientsOptionParams();
    })
    .mergeMap(optionParams => {
        this.optionParams = optionParams;
        return this.ingredientService.getAllIngredientsUnitTypes();
    })
    .subscribe(unitTypes => {
      this.unitTypes = unitTypes;
      this.loading = false;
    });

}
