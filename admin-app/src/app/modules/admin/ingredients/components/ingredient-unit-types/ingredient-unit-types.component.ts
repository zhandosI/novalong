import { Component, Input, Output, EventEmitter, OnInit, DoCheck } from '@angular/core';

import { Ingredient } from '@app/shared/models/ingredient/ingredient';
import { IngredientUnitType } from '@app/shared/models/ingredient/ingredient.unit.type';
import { UnitType } from '@app/shared/models/unit.type';

@Component({
  selector: 'ingredient-unit-types',
  templateUrl: './ingredient-unit-types.component.html',
  styleUrls: ['./ingredient-unit-types.component.scss']
})
export class IngredientUnitTypesComponent implements OnInit, DoCheck {

  @Input() ingredient: Ingredient;
  @Input() unitTypes: UnitType[];
  @Output() saveUnitTypes: EventEmitter<IngredientUnitType[]> = new EventEmitter();

  newUnitTypeCollapsed: boolean = false;
  changeUnitTypeCollapsed: boolean = false;
  changesMade: boolean = false;
  weightInputDisabled: boolean = false;

  ingredientUnitTypes: IngredientUnitType[] = [];
  newUnitType: IngredientUnitType;

  constructor() {}

  ngOnInit(): void {
    this.ingredientUnitTypes  = this.ingredient.unitTypes.map(x => Object.assign({}, x));
  }

  ngDoCheck(): void {
    this.changesMade = !!(this.compareArrays(this.ingredientUnitTypes, this.ingredient.unitTypes) || this.newUnitType);
  }

  saveUnitTypeChanges = () => {
      console.log("ingredientOptionParams" + JSON.stringify(this.ingredientUnitTypes));
    if (this.newUnitType && this.newUnitType.type)
      this.ingredientUnitTypes.push(this.newUnitType);
    this.saveUnitTypes.emit(this.ingredientUnitTypes);
  };

  toggleNewUnitType = () => {
    if (this.newUnitTypeCollapsed === true)
      this.newUnitType = undefined;
    this.newUnitTypeCollapsed = !this.newUnitTypeCollapsed;
  };

  toggleChangeUnitType = () => {
      this.changeUnitTypeCollapsed = !this.changeUnitTypeCollapsed;
      if(this.changeUnitTypeCollapsed === false){
         this.ingredientUnitTypes  = this.ingredient.unitTypes.map(x => Object.assign({}, x));
      }
  }

  onNewUnitTypeChange = (selectForm: any) => {
    for (let i = 0; i < selectForm.options.length; i++) {
      if (selectForm.options[i].selected) {
        let code = selectForm.options[i].value;

        if (!this.newUnitType) {
          this.newUnitType = new IngredientUnitType();
        }

        if (code === 'BYTASTE') {
          this.weightInputDisabled = true;
          this.newUnitType.weight = 0;
        } else {
          this.weightInputDisabled = false;
        }

        this.newUnitType.type = this.unitTypes.find(u => u.code === code);
      }
    }
  };

  onNewUTWeightChange = (inputForm: any) => {
    if (!this.newUnitType)
      this.newUnitType = new IngredientUnitType();

    if (this.newUnitType.type && this.newUnitType.type.code === 'BYTASTE')
      this.newUnitType.weight = 0;
    else
      this.newUnitType.weight = Number.parseInt(inputForm.value);
  };

  removeUnitType = (index: number): void => {
    if (index > -1) {
      this.ingredientUnitTypes.splice(index, 1);
    }
  };

  private compareArrays = (arr1: any[], arr2: any[]): boolean => JSON.stringify(arr1) != JSON.stringify(arr2);

}
