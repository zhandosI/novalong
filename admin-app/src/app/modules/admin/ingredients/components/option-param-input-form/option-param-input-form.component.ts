import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {OptionParam} from "@app/shared/models/option.param";
import {IngredientOptionParam} from "@app/shared/models/ingredient/ingredient.option.param";

@Component({
  selector: 'app-option-param-input-form',
  templateUrl: './option-param-input-form.component.html',
  styleUrls: ['./option-param-input-form.component.scss']
})
export class OptionParamInputFormComponent implements OnInit {

  @Input() optionParams: OptionParam[];
  @Output() submitIngredientOptionParam: EventEmitter<IngredientOptionParam> = new EventEmitter();

  ingredientOptionParam: IngredientOptionParam = new IngredientOptionParam();

  constructor() { }

  ngOnInit() {

  }

  onChange = () => {
      if (this.ingredientOptionParam.param && this.ingredientOptionParam.weight)
          this.submitIngredientOptionParam.emit(this.ingredientOptionParam);
  };

}
