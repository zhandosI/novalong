import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { UnitType } from '@app/shared/models/unit.type';
import { IngredientUnitType } from '@app/shared/models/ingredient/ingredient.unit.type';

@Component({
  selector: 'unit-type-input-form',
  templateUrl: './unit-type-input-form.component.html',
  styleUrls: ['./unit-type-input-form.component.scss']
})
export class UnitTypeInputFormComponent implements OnInit {

  @Input() unitTypes: UnitType[];
  @Output() submitIngredientUnitType: EventEmitter<IngredientUnitType> = new EventEmitter();

  ingredientUnitType: IngredientUnitType = new IngredientUnitType();

  ngOnInit() {
    this.unitTypes = this.filterUnitTypes(this.unitTypes);
  }

  onChange = () => {
    if (this.ingredientUnitType.type && this.ingredientUnitType.weight)
      this.submitIngredientUnitType.emit(this.ingredientUnitType);
  };

  private filterUnitTypes = (unitTypes: UnitType[]) => unitTypes.filter(u => u.code !== 'GRAMS');

}
