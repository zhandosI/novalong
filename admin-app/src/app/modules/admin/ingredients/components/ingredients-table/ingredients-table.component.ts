import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Ingredient } from '@app/shared/models/ingredient/ingredient';

@Component({
  selector: 'ingredients-table',
  templateUrl: './ingredients-table.component.html',
  styleUrls: ['./ingredients-table.component.scss']
})
export class IngredientsTableComponent {

  @Input() ingredients: Ingredient[];
  @Output() addIngredient: EventEmitter<Ingredient> = new EventEmitter();

  onIngredientClick = (ingredientId: number) => {
    let ingredient = this.ingredients.find(ingredient => ingredient.id === ingredientId);
    this.addIngredient.emit(ingredient);
  }

}
