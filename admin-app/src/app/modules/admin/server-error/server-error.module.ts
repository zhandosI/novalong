import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@app/shared/shared.module';
import { ServerErrorRoutingModule } from './server-error-routing.module';
import { ServerErrorComponent } from './server-error.component';

@NgModule({
  imports: [
    CommonModule,
    ServerErrorRoutingModule,
    SharedModule
  ],
  declarations: [ServerErrorComponent]
})
export class ServerErrorModule { }
