import { Component, OnInit } from '@angular/core';
import { routerTransition } from '@app/router.animations';

@Component({
  selector: 'server-error',
  templateUrl: './server-error.component.html',
  styleUrls: ['./server-error.component.scss'],
  animations: [routerTransition()]
})
export class ServerErrorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
