import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbDropdownModule} from '@ng-bootstrap/ng-bootstrap';

import {AdminRoutingModule} from './admin-routing.module';
import {AdminComponent} from './admin.component';
import {HeaderComponent} from './components/header/header.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';

import {AuthService} from '@app/shared/services/auth.service';
import {FormsModule} from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        AdminRoutingModule,
        NgbModule.forRoot(),
        NgbDropdownModule.forRoot()
    ],
    providers: [
        AuthService
    ],
    declarations: [
        AdminComponent,
        HeaderComponent,
        SidebarComponent
    ]
})
export class AdminModule {
}
