import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { routerTransition } from '@app/router.animations';
import * as jwtDecode from 'jwt-decode';
import { BaseChartDirective } from 'ng2-charts';
import 'rxjs/add/operator/mergeMap';
import { EMPTY } from 'rxjs';

import { UserService } from '@app/shared/services/user.service';
import { ProgramService } from '@app/shared/services/program.service';

import { User } from '@app/shared/models/user';
import { ProgramRationObject } from '@app/shared/models/program/program.ration.object';
import { Program } from '@app/shared/models/program/program';

@Component({
  selector: 'app-program-generation-demo',
  templateUrl: './program-generation-demo.component.html',
  styleUrls: ['./program-generation-demo.component.scss'],
  animations: [routerTransition()]
})
export class ProgramGenerationDemoComponent implements OnInit {

  @ViewChild(BaseChartDirective) private _chart;

  user: User;
  program: Program;
  programRation: ProgramRationObject;
  healthFactor: any;

  userBMR: number;
  totalCals: number;
  totalProts: number;
  totalCarbs: number;
  totalFats: number;
  todayDayCode = '';
  loading = false;
  loadingDT: boolean[] = [false, false, false, false, false];
  fetchingRation = false;

  public pieChartLabels: string[] = ['Proteins', 'Fats', 'Carbohydrates'];
  public pieChartData: number[];

  constructor(private programService: ProgramService,
              private userService: UserService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    const decodedToken: any = jwtDecode(localStorage.getItem('api_token'));
    const userId: number = decodedToken.userId;

    this.fetchData(userId);
  }

  generateDailyRation = () => {
    this.fetchUserDailyRation();
  }

  regenerateDishType = (dishTypeId: number, idx: number) => {
    this.loadingDT[idx] = true;

    this.userService.generateDishType(this.user.id, dishTypeId, this.todayDayCode, true)
      .subscribe(res => {
        this.programRation.ration.dailyMeals[idx] = res.ration;
        this.programRation.ration.calories = res.calories;
        this.loadingDT[idx] = false;
      });
  }

  round = (val: number) => Math.round(val * 100) / 100;

  paramNameByCode = (code: string) => {
    switch (code) {
      case 'SUGAR':
        return 'Sugar';
      case 'SALT':
        return 'Salt';
      case 'FATS':
        return 'Fats';
      case 'FATS_SATURATED':
        return 'Saturated fats';
      case 'FATS_TRANS':
        return 'Trans fats';
      case 'COOKING_TYPE':
        return 'Type of cooking';
      default:
        return 'Unknown param';
    }
  }

  private fetchUserDailyRation = () => {
    this.fetchingRation = true;

    this.userService.getUserDailyRation(this.user.id, true)
      .mergeMap(programRation => {
        this.programRation = programRation;
        this.pieChartData = [
          programRation.ration.proteins,
          programRation.ration.fats,
          programRation.ration.carbohydrates
        ];
        this.todayDayCode = programRation.ration.dayCode;
        // this._chart.refresh();

        if (this.programRation) {
          return this.userService.getUserRationHealthFactor(this.user.id);
        } else {
          return EMPTY;
        }

      })
      .subscribe(healthFactor => {
        this.healthFactor = healthFactor;
        this.fetchingRation = false;
      });
  }

  private fetchData = (userId: number) => {
    this.loading = true;
    let programId: number;

    this.route.params
      .mergeMap(params => {
          programId = +params['id'];
          return this.programService.getProgram(programId);
      })
      .mergeMap(program => {
        this.program = program;
        return this.userService.getUserDailyRation(userId, false);

      })
      .mergeMap(programRation => {
        this.programRation = programRation;

        if (programRation.ration) {
          this.pieChartData = [
            programRation.ration.proteins,
            programRation.ration.fats,
            programRation.ration.carbohydrates
          ];

          this.todayDayCode = programRation.ration.dayCode;
        }

        return this.userService.getUser(userId);
      })
      .mergeMap(user => {
        this.user = user;

        let userBMR = user.weight * 10 + user.height * 6.25 - user.age * 5;
        if (user.gender.code === 'MALE') {
            userBMR += 5;
        } else {
          userBMR -= 161;
        }
        this.userBMR = userBMR;

        this.totalCals = Math.round(this.program.caloriesDifferenceCoefficient * user.activityLevel.coefficient * userBMR);
        this.totalProts = Math.round(this.totalCals * user.currentPhase.proteinsPercentage);
        this.totalCarbs = Math.round(this.totalCals * user.currentPhase.carbsPercentage);
        this.totalFats = Math.round(this.totalCals * user.currentPhase.fatsPercentage);

        if (this.programRation) {
          return this.userService.getUserRationHealthFactor(user.id);
        } else { return EMPTY; }
      })
      .subscribe(healthFactor => {
        this.healthFactor = healthFactor;
        this.loading = false;
      });
  }

}
