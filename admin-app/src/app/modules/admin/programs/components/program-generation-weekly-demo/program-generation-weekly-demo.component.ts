import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { routerTransition } from '@app/router.animations';
import * as jwtDecode from 'jwt-decode';
import 'rxjs/add/operator/mergeMap';

import { UserService } from '@app/shared/services/user.service';
import { ProgramService } from '@app/shared/services/program.service';

import { User } from '@app/shared/models/user';
import { ProgramRationArray } from '@app/shared/models/program/program.ration.array';
import { Program } from '@app/shared/models/program/program';

@Component({
  selector: 'app-program-generation-weekly-demo',
  templateUrl: './program-generation-weekly-demo.component.html',
  styleUrls: ['./program-generation-weekly-demo.component.scss'],
  animations: [routerTransition()]
})
export class ProgramGenerationWeeklyDemoComponent implements OnInit {

  user: User;
  program: Program;
  programRation: ProgramRationArray;

  userBMR: number;
  totalCals: number;
  totalProts: number;
  totalCarbs: number;
  totalFats: number;
  loading: boolean = false;
  fetchingRation: boolean = false;

  constructor(private programService: ProgramService,
              private userService: UserService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    const decodedToken: any = jwtDecode(localStorage.getItem('api_token'));
    const userId: number = decodedToken.userId;

    this.fetchData(userId);
  }

  generateWeeklyRation = () => {
    this.fetchUserWeeklyRation();
  };

  private fetchUserWeeklyRation = () => {
    this.fetchingRation = true;

    this.userService.getUserWeeklyRation(this.user.id, true)
      .subscribe(programRation => {
        this.programRation = programRation;
        this.fetchingRation = false;
      });
  };

  private fetchData = (userId: number) => {
    this.loading = true;
    let programId: number;

    this.route.params
      .mergeMap(params => {
          programId = +params['id'];
          return this.programService.getProgram(programId);
      })
      .mergeMap(program => {
        this.program = program;
        return this.userService.getUserWeeklyRation(userId, false);
      })
      .mergeMap(programRation => {
        this.programRation = programRation;
        return this.userService.getUser(userId);
      })
      .subscribe(user => {
        this.user = user;

        let userBMR = user.weight * 10 + user.height * 6.25 - user.age * 5;
        if (user.gender.code === 'MALE')
            userBMR += 5;
        else
          userBMR -= 161;
        this.userBMR = userBMR;

        this.totalCals = Math.round(this.program.caloriesDifferenceCoefficient * user.activityLevel.coefficient * userBMR);
        this.totalProts = Math.round(this.totalCals * user.currentPhase.proteinsPercentage);
        this.totalCarbs = Math.round(this.totalCals * user.currentPhase.carbsPercentage);
        this.totalFats = Math.round(this.totalCals * user.currentPhase.fatsPercentage);

        this.loading = false;
      });
  };

}
