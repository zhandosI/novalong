import { Component, OnInit } from '@angular/core';
import { routerTransition } from '@app/router.animations';

import { ProgramService } from '@app/shared/services/program.service';

import { Program } from '@app/shared/models/program/program';

@Component({
  selector: 'app-programs',
  templateUrl: './programs.component.html',
  styleUrls: ['./programs.component.scss'],
  animations: [routerTransition()]
})
export class ProgramsComponent implements OnInit {

  programs: Program[];
  loading: boolean = false;

  constructor(private programService: ProgramService) { }

  ngOnInit() {
    this.fetchData();
  }

  private fetchData = () => {
    this.loading = true;

    this.programService.getAllPrograms()
      .subscribe(programs => {
        this.programs = programs;
        this.loading = false
      });
  };

}
