import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProgramsComponent } from './programs.component';
import { ProgramGenerationDemoComponent } from './components/program-generation-demo/program-generation-demo.component';
import { ProgramGenerationWeeklyDemoComponent } from './components/program-generation-weekly-demo/program-generation-weekly-demo.component';

const routes: Routes = [
  { path: '', component: ProgramsComponent },
  { path: ':id/daily', component: ProgramGenerationDemoComponent },
  { path: ':id/weekly', component: ProgramGenerationWeeklyDemoComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProgramsRoutingModule { }
