import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule as Ng2Charts  } from 'ng2-charts';
import { RoundProgressModule } from 'angular-svg-round-progressbar';

import { SharedModule } from '@app/shared/shared.module';
import { ProgramsRoutingModule } from './programs-routing.module';

import { ProgramService } from '@app/shared/services/program.service';
import { UserService } from '@app/shared/services/user.service';

import { ProgramsComponent } from './programs.component';
import { ProgramDetailComponent } from './components/program-detail/program-detail.component';
import { ProgramGenerationDemoComponent } from './components/program-generation-demo/program-generation-demo.component';
import { ProgramGenerationWeeklyDemoComponent } from './components/program-generation-weekly-demo/program-generation-weekly-demo.component';

@NgModule({
  imports: [
    CommonModule,
    Ng2Charts,
    RoundProgressModule,
    ProgramsRoutingModule,
    SharedModule
  ],
  providers: [
    ProgramService,
    UserService
  ],
  declarations: [
    ProgramsComponent,
    ProgramDetailComponent,
    ProgramGenerationDemoComponent,
    ProgramGenerationWeeklyDemoComponent
  ]
})
export class ProgramsModule { }
