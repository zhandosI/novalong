import { Component, OnInit } from '@angular/core';
import { routerTransition } from '@app/router.animations';

@Component({
  selector: 'app-bmi-calculator',
  templateUrl: './bmi-calculator.component.html',
  styleUrls: ['./bmi-calculator.component.scss'],
  animations: [routerTransition()]
})
export class BmiCalculatorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
