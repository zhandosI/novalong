import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BmiCalculatorComponent } from './bmi-calculator.component';

const routes: Routes = [
  { path: '', component: BmiCalculatorComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BmiCalculatorRoutingModule { }
