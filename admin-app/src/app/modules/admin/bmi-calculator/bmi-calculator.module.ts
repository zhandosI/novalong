import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@app/shared/shared.module';
import { BmiCalculatorRoutingModule } from './bmi-calculator-routing.module';
import { BmiCalculatorComponent } from './bmi-calculator.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    BmiCalculatorRoutingModule
  ],
  declarations: [BmiCalculatorComponent]
})
export class BmiCalculatorModule { }
