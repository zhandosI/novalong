import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { AuthService } from '@app/shared/services/auth.service';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule
  ],
  providers: [
    AuthService
  ],
  declarations: []
})
export class AuthModule { }
