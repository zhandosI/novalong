import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '@app/shared/services/auth.service';
import { User } from '@app/shared/models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loading: boolean = false;
  globalError: boolean = false;
  globalErrorMsg: string = 'Username or password is incorrect';

  constructor(private authService: AuthService,
              private router: Router) {}

  auth = (user: User) => {
    this.loading = true;
    this.globalError = false;

    this.authService.auth(user).subscribe(
      res => {
        this.loading = false;
        this.router.navigate(['/']);
      },
      err => {
        if (err.status === 401) {
          this.globalError = true;
        }
        this.loading = false;
      }
    )
  }

  ngOnInit() {

  }

}
