import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { User } from '@app/shared/models/user';
import { LoginFormError } from '@app/shared/models/errors/login-form.error';

@Component({
  selector: 'login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  user: User;
  error: LoginFormError;
  @Output() submitCredentials: EventEmitter<User> = new EventEmitter();
  @Input() loading: boolean;
  @Input() globalError: boolean;
  @Input() globalErrorMsg: string;

  ngOnInit() {
    this.user = new User();
    this.error = new LoginFormError();
  }

  onSubmit = () => {
    this.error.username = this.error.password = '';

    if (this.isValid(this.user))
      this.submitCredentials.emit(this.user);
  }

  private isValid = (user: User): boolean => {
    if (!user.username) this.error.username = 'Username cannot be empty!';
    if (!user.password) this.error.password = 'Password cannot be empty!';

    if (!this.error.username && !this.error.password) return true;
    else return false;
  }
}
