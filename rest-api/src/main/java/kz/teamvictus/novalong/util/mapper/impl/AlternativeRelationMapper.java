package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.alternative.AlternativeRelationDto;
import kz.teamvictus.novalong.rest.model.entity.alternative.AlternativeRelation;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("alternativeRelationMapper")
public class AlternativeRelationMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<AlternativeRelation, AlternativeRelationDto> {

    @Override
    public AlternativeRelationDto toDto(AlternativeRelation entity) {
        return getMapper().map(entity, AlternativeRelationDto.class);
    }

    @Override
    public AlternativeRelation toEntity(AlternativeRelationDto dto) {
        return getMapper().map(dto, AlternativeRelation.class);
    }

    @Override
    public List<AlternativeRelationDto> toDto(List<AlternativeRelation> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<AlternativeRelation> toEntity(List<AlternativeRelationDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
