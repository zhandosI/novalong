package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.ProgramDto;
import kz.teamvictus.novalong.rest.model.entity.Program;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("programMapper")
public class ProgramMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<Program, ProgramDto> {

    @Override
    public ProgramDto toDto(Program entity) {
        return getMapper().map(entity, ProgramDto.class);
    }

    @Override
    public Program toEntity(ProgramDto dto) {
        return getMapper().map(dto, Program.class);
    }

    @Override
    public List<ProgramDto> toDto(List<Program> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<Program> toEntity(List<ProgramDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
