package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.ingredient.IngredientCookingTypeDto;
import kz.teamvictus.novalong.rest.model.dto.ingredient.IngredientCookingTypeJoinedDto;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientCookingType;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientCookingTypeJoined;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("ingredientCookingTypeDtoMapper")
public class IngredientCookingTypeMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<IngredientCookingType, IngredientCookingTypeDto> {

    @Override
    public IngredientCookingTypeDto toDto(IngredientCookingType entity) {
        return getMapper().map(entity, IngredientCookingTypeDto.class);
    }

    @Override
    public IngredientCookingType toEntity(IngredientCookingTypeDto dto) {
        return getMapper().map(dto, IngredientCookingType.class);
    }

    @Override
    public List<IngredientCookingTypeDto> toDto(List<IngredientCookingType> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<IngredientCookingType> toEntity(List<IngredientCookingTypeDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

    public IngredientCookingTypeJoinedDto toDtoJoined(IngredientCookingTypeJoined entity) {
        return getMapper().map(entity, IngredientCookingTypeJoinedDto.class);
    }

    public List<IngredientCookingTypeJoinedDto> toDtoJoined(List<IngredientCookingTypeJoined> entities) {
        return entities.stream()
                .map(this::toDtoJoined)
                .collect(Collectors.toList());
    }

}
