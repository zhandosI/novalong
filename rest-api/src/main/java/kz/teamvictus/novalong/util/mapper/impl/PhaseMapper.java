package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.PhaseDto;
import kz.teamvictus.novalong.rest.model.entity.Phase;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("phaseMapper")
public class PhaseMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<Phase, PhaseDto> {

    @Override
    public PhaseDto toDto(Phase entity) {
        return getMapper().map(entity, PhaseDto.class);
    }

    @Override
    public Phase toEntity(PhaseDto dto) {
        return getMapper().map(dto, Phase.class);
    }

    @Override
    public List<PhaseDto> toDto(List<Phase> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<Phase> toEntity(List<PhaseDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
