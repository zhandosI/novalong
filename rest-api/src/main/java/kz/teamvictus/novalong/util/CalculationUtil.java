package kz.teamvictus.novalong.util;

public final class CalculationUtil {

    private CalculationUtil() {}

    public static double roundToInt(double val) {
        return (double) Math.round(val);
    }

    public static double roundToOnePoint(double val) {
        return (double) Math.round(val * 10D) / 10D;
    }

    public static double roundToTwoPoints(double val) {
        return (double) Math.round(val * 100D) / 100D;
    }

    public static double roundToFourPoints(double val) {
        return (double) Math.round(val * 10000D) / 10000D;
    }

    public static float roundToInt(float val) {
        return (float) Math.round(val);
    }

    public static float roundToOnePoint(float val) {
        return (float) Math.round(val * 10F) / 10F;
    }

    public static float roundToTwoPoints(float val) {
        return (float) Math.round(val * 100F) / 100F;
    }

    public static float roundToFourPoints(float val) {
        return (float) Math.round(val * 10000F) / 10000F;
    }

    public static int randomIndex(int range) {
        return (int) (Math.random() * range);
    }

}
