package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.dish.DishTypeDto;
import kz.teamvictus.novalong.rest.model.entity.dish.DishType;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("dishTypeMapper")
public class DishTypeMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<DishType, DishTypeDto> {

    @Override
    public DishTypeDto toDto(DishType entity) {
        return getMapper().map(entity, DishTypeDto.class);
    }

    @Override
    public DishType toEntity(DishTypeDto dto) {
        return getMapper().map(dto, DishType.class);
    }

    @Override
    public List<DishTypeDto> toDto(List<DishType> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<DishType> toEntity(List<DishTypeDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
