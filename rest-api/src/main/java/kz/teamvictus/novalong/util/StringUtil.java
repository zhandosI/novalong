package kz.teamvictus.novalong.util;

public final class StringUtil {

    public static boolean isDouble(String str) {
        if (str == null)
            return false;

        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

}
