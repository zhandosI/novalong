package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.dish.DishMandatoryDto;
import kz.teamvictus.novalong.rest.model.entity.dish.DishMandatory;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("dishMandatoryMapper")
public class DishMandatoryMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<DishMandatory, DishMandatoryDto> {

    @Override
    public DishMandatoryDto toDto(DishMandatory entity) {
        return getMapper().map(entity, DishMandatoryDto.class);
    }

    @Override
    public DishMandatory toEntity(DishMandatoryDto dto) {
        return getMapper().map(dto, DishMandatory.class);
    }

    @Override
    public List<DishMandatoryDto> toDto(List<DishMandatory> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<DishMandatory> toEntity(List<DishMandatoryDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
