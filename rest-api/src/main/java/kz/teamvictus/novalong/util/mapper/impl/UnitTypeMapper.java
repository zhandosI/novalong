package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.UnitTypeDto;
import kz.teamvictus.novalong.rest.model.entity.UnitType;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("unitTypeMapper")
public class UnitTypeMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<UnitType, UnitTypeDto> {

    @Override
    public UnitTypeDto toDto(UnitType entity) {
        return getMapper().map(entity, UnitTypeDto.class);
    }

    @Override
    public UnitType toEntity(UnitTypeDto dto) {
        return getMapper().map(dto, UnitType.class);
    }

    @Override
    public List<UnitTypeDto> toDto(List<UnitType> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<UnitType> toEntity(List<UnitTypeDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

}
