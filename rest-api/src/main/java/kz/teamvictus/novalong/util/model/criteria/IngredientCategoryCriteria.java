package kz.teamvictus.novalong.util.model.criteria;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IngredientCategoryCriteria extends SearchCriteria {

    private String name;
    private Long parentCategoryId;

}
