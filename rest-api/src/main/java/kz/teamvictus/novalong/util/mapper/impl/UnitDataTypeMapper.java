package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.UnitDataTypeDto;
import kz.teamvictus.novalong.rest.model.entity.UnitDataType;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("unitDataTypeMapper")
public class UnitDataTypeMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<UnitDataType, UnitDataTypeDto> {

    @Override
    public UnitDataTypeDto toDto(UnitDataType entity) {
        return getMapper().map(entity, UnitDataTypeDto.class);
    }

    @Override
    public UnitDataType toEntity(UnitDataTypeDto dto) {
        return getMapper().map(dto, UnitDataType.class);
    }

    @Override
    public List<UnitDataTypeDto> toDto(List<UnitDataType> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<UnitDataType> toEntity(List<UnitDataTypeDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
