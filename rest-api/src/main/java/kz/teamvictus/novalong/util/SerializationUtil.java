package kz.teamvictus.novalong.util;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public final class SerializationUtil {

    private SerializationUtil() {}

    public static String getExceptionString(final Exception e) {
        Throwable cause;
        Throwable result = e;

        while(null != (cause = e.getCause())  && (e != cause) )
            result = cause;

        ByteArrayOutputStream os = new ByteArrayOutputStream();

        PrintStream ps = new PrintStream(os);
        ps.println("Root cause: " + result);

        e.printStackTrace(ps);

        return os.toString();
    }
}
