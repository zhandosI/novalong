package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.PurposeDto;
import kz.teamvictus.novalong.rest.model.entity.Purpose;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("purposeMapper")
public class PurposeMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<Purpose, PurposeDto> {

    @Override
    public PurposeDto toDto(Purpose entity) {
        return getMapper().map(entity, PurposeDto.class);
    }

    @Override
    public Purpose toEntity(PurposeDto dto) {
        return getMapper().map(dto, Purpose.class);
    }

    @Override
    public List<PurposeDto> toDto(List<Purpose> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<Purpose> toEntity(List<PurposeDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
