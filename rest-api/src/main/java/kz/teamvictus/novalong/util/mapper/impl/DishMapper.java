package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.dish.DishDto;
import kz.teamvictus.novalong.rest.model.entity.dish.Dish;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("dishMapper")
public class DishMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<Dish, DishDto> {

    @Override
    public DishDto toDto(Dish entity) {
        return getMapper().map(entity, DishDto.class);
    }

    @Override
    public Dish toEntity(DishDto dto) {
        return getMapper().map(dto, Dish.class);
    }

    @Override
    public List<DishDto> toDto(List<Dish> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<Dish> toEntity(List<DishDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
