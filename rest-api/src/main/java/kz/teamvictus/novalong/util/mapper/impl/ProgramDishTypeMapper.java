package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.ProgramDishTypeDto;
import kz.teamvictus.novalong.rest.model.entity.ProgramDishType;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("programDishTypeMapper")
public class ProgramDishTypeMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<ProgramDishType, ProgramDishTypeDto> {

    @Override
    public ProgramDishTypeDto toDto(ProgramDishType entity) {
        return getMapper().map(entity, ProgramDishTypeDto.class);
    }

    @Override
    public ProgramDishType toEntity(ProgramDishTypeDto dto) {
        return getMapper().map(dto, ProgramDishType.class);
    }

    @Override
    public List<ProgramDishTypeDto> toDto(List<ProgramDishType> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ProgramDishType> toEntity(List<ProgramDishTypeDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
