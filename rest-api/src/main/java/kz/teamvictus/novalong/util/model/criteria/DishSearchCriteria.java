package kz.teamvictus.novalong.util.model.criteria;

import kz.teamvictus.novalong.rest.model.entity.dish.DishCategory;
import kz.teamvictus.novalong.rest.model.entity.dish.DishType;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class DishSearchCriteria extends SearchCriteria {

    private Set<DishCategory> categories;
    private Set<DishType> dishTypes;

}
