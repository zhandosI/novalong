package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.ActivityLevelDto;
import kz.teamvictus.novalong.rest.model.entity.ActivityLevel;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("activityLevelMapper")
public class ActivityLevelMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<ActivityLevel, ActivityLevelDto> {

    @Override
    public ActivityLevelDto toDto(ActivityLevel entity) {
        return getMapper().map(entity, ActivityLevelDto.class);
    }

    @Override
    public ActivityLevel toEntity(ActivityLevelDto dto) {
        return getMapper().map(dto, ActivityLevel.class);
    }

    @Override
    public List<ActivityLevelDto> toDto(List<ActivityLevel> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ActivityLevel> toEntity(List<ActivityLevelDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
