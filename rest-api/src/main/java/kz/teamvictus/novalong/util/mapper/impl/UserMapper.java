package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.UserDto;
import kz.teamvictus.novalong.rest.model.entity.User;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("userMapper")
public class UserMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<User, UserDto> {

    @Override
    public UserDto toDto(User entity) {
        return getMapper().map(entity, UserDto.class);
    }

    @Override
    public User toEntity(UserDto dto) {
        return getMapper().map(dto, User.class);
    }

    @Override
    public List<UserDto> toDto(List<User> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<User> toEntity(List<UserDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

}
