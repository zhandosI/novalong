package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.ingredient.IngredientOptParamValueDto;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientOptParamValue;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("ingredientOptionalValuePropMapper")
public class IngredientOptParamValueMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<IngredientOptParamValue, IngredientOptParamValueDto> {

    @Override
    public IngredientOptParamValueDto toDto(IngredientOptParamValue entity) {
        return getMapper().map(entity, IngredientOptParamValueDto.class);
    }

    @Override
    public IngredientOptParamValue toEntity(IngredientOptParamValueDto dto) {
        return getMapper().map(dto, IngredientOptParamValue.class);
    }

    @Override
    public List<IngredientOptParamValueDto> toDto(List<IngredientOptParamValue> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<IngredientOptParamValue> toEntity(List<IngredientOptParamValueDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

}
