package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.ColorCodeDto;
import kz.teamvictus.novalong.rest.model.entity.ColorCode;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("colorCodeMapper")
public class ColorCodeMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<ColorCode, ColorCodeDto> {

    @Override
    public ColorCodeDto toDto(ColorCode entity) {
        return getMapper().map(entity, ColorCodeDto.class);
    }

    @Override
    public ColorCode toEntity(ColorCodeDto dto) {
        return getMapper().map(dto, ColorCode.class);
    }

    @Override
    public List<ColorCodeDto> toDto(List<ColorCode> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ColorCode> toEntity(List<ColorCodeDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
