package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.alternative.AlternativeCategoryDto;
import kz.teamvictus.novalong.rest.model.entity.alternative.AlternativeCategory;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("alternativeCategoryMapper")
public class AlternativeCategoryMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<AlternativeCategory, AlternativeCategoryDto> {

    @Override
    public AlternativeCategoryDto toDto(AlternativeCategory entity) {
        return getMapper().map(entity, AlternativeCategoryDto.class);
    }

    @Override
    public AlternativeCategory toEntity(AlternativeCategoryDto dto) {
        return getMapper().map(dto, AlternativeCategory.class);
    }

    @Override
    public List<AlternativeCategoryDto> toDto(List<AlternativeCategory> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<AlternativeCategory> toEntity(List<AlternativeCategoryDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
