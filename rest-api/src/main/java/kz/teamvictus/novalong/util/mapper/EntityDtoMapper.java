package kz.teamvictus.novalong.util.mapper;

import kz.teamvictus.novalong.rest.model.BaseEntity;

import java.util.List;

public interface EntityDtoMapper<E extends BaseEntity, D> {

    D toDto(E entity);
    E toEntity(D dto);
    List<D> toDto(List<E> entities);
    List<E> toEntity(List<D> dtos);

}
