package kz.teamvictus.novalong.util.mapper;

import org.modelmapper.ModelMapper;

public abstract class AbstractEntityDtoMapper {

    private static final ModelMapper mapper = new ModelMapper();

    static {
        mapper.getConfiguration().setAmbiguityIgnored(true);
    }

    protected synchronized ModelMapper getMapper() {
        return mapper;
    }

}
