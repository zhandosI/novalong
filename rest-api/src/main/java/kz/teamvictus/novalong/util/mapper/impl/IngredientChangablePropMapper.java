package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.ingredient.IngredientChangablePropDto;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientChangableProp;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("ingredientChangablePropMapper")
public class IngredientChangablePropMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<IngredientChangableProp, IngredientChangablePropDto> {

    @Override
    public IngredientChangablePropDto toDto(IngredientChangableProp entity) {
        return getMapper().map(entity, IngredientChangablePropDto.class);
    }

    @Override
    public IngredientChangableProp toEntity(IngredientChangablePropDto dto) {
        return getMapper().map(dto, IngredientChangableProp.class);
    }

    @Override
    public List<IngredientChangablePropDto> toDto(List<IngredientChangableProp> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<IngredientChangableProp> toEntity(List<IngredientChangablePropDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

}
