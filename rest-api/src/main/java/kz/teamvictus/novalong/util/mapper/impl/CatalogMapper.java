package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.CatalogDto;
import kz.teamvictus.novalong.rest.model.entity.Catalog;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("catalogMapper")
public class CatalogMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<Catalog, CatalogDto> {

    @Override
    public CatalogDto toDto(Catalog entity) {
        return getMapper().map(entity, CatalogDto.class);
    }

    @Override
    public Catalog toEntity(CatalogDto dto) {
        return getMapper().map(dto, Catalog.class);
    }

    @Override
    public List<CatalogDto> toDto(List<Catalog> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<Catalog> toEntity(List<CatalogDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
