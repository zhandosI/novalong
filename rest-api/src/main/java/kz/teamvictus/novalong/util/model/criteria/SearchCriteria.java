package kz.teamvictus.novalong.util.model.criteria;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class SearchCriteria {

    private String searchQuery;
    protected String sort;
    protected Integer offset;
    protected Integer limit;
    protected boolean count;
    private Boolean active;

}
