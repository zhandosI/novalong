package kz.teamvictus.novalong.util;

import org.springframework.util.MultiValueMap;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

public final class UriUtil {

    public static String buildRequestUri() {
        return ServletUriComponentsBuilder.fromCurrentRequestUri()
                .toUriString();
    }

    public static String buildRequestUri(MultiValueMap<String, String> params) {
        return ServletUriComponentsBuilder.fromCurrentRequestUri()
                .queryParams(params)
                .toUriString();
    }

}
