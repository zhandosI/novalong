package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.ingredient.IngredientOptParamNameDto;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientOptParamName;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("ingredientOptionalPropMapper")
public class IngredientOptParamNameMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<IngredientOptParamName, IngredientOptParamNameDto> {

    @Override
    public IngredientOptParamNameDto toDto(IngredientOptParamName entity) {
        return getMapper().map(entity, IngredientOptParamNameDto.class);
    }

    @Override
    public IngredientOptParamName toEntity(IngredientOptParamNameDto dto) {
        return getMapper().map(dto, IngredientOptParamName.class);
    }

    @Override
    public List<IngredientOptParamNameDto> toDto(List<IngredientOptParamName> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<IngredientOptParamName> toEntity(List<IngredientOptParamNameDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

}
