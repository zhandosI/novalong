package kz.teamvictus.novalong.util;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * Util class for Holding and manipulating with day codes
 *
 * @author Sanzhar Kudaibergen
 */
public final class DateUtil {

    private DateUtil() {}

    /**
     * @param dayCode - if null, the next day from today is returned
     * @return the day code of the next day after {@param dayCode}
     */
    public static String getNextDayCode(String dayCode) {
        if (!dayCodeExists(dayCode))
            return getDayCodeOfWeek(getTodayDay() + 1);

        return getDayCodeOfWeek(getDayByCode(dayCode.trim().toUpperCase()) + 1);
    }

    /**
     * @param dayCode - if null, the prev day from today is returned
     * @return the day code of the prev day after {@param dayCode}
     */
    public static String getPrevDayCode(String dayCode) {
        if (!dayCodeExists(dayCode))
            return getDayCodeOfWeek(getTodayDay() - 1);

        return getDayCodeOfWeek(getDayByCode(dayCode.trim().toUpperCase()) - 1);
    }

    /**
     * @return number of days from today till sunday including both
     */
    public static int getDaysNumTillWeekEnd() {
        return ((8 - getTodayDay()) % 7) + 1;
    }

    public static int getTodayDay() {
        Calendar calendar = Calendar.getInstance();
        return calendar.get(Calendar.DAY_OF_WEEK);
    }

    public static Date getDateFromToday(int daysNo) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(getTodayDate());
        calendar.add(Calendar.DAY_OF_YEAR, daysNo);
        return calendar.getTime();
    }

    public static int getDayByCode(String code) {
        switch (code) {
            case "SUN":
                return 1;
            case "MON":
                return 2;
            case "TUE":
                return 3;
            case "WED":
                return 4;
            case "THU":
                return 5;
            case "FRI":
                return 6;
            case "SAT":
                return 7;
            default:
                return 0;
        }
    }

    public static String getDayCodeOfWeek(int dayVal) {
        String day = "";
        switch (dayVal) {
            case 1:
                day = "SUN";
                break;
            case 2:
                day = "MON";
                break;
            case 3:
                day = "TUE";
                break;
            case 4:
                day = "WED";
                break;
            case 5:
                day = "THU";
                break;
            case 6:
                day = "FRI";
                break;
            case 7:
                day = "SAT";
                break;
        }
        return day;
    }

    public static boolean dayCodeExists(String code) {
        if (code == null)
            return false;

        return Arrays.asList(new String[]{"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"}).contains(code.trim().toUpperCase());
    }

    private static Date getTodayDate() {
        return new Date();
    }

}
