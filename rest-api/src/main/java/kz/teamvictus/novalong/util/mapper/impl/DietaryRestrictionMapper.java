package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.DietaryRestrictionDto;
import kz.teamvictus.novalong.rest.model.entity.DietaryRestriction;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("dietaryRestrictionMapper")
public class DietaryRestrictionMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<DietaryRestriction, DietaryRestrictionDto> {

    @Override
    public DietaryRestrictionDto toDto(DietaryRestriction entity) {
        return getMapper().map(entity, DietaryRestrictionDto.class);
    }

    @Override
    public DietaryRestriction toEntity(DietaryRestrictionDto dto) {
        return getMapper().map(dto, DietaryRestriction.class);
    }

    @Override
    public List<DietaryRestrictionDto> toDto(List<DietaryRestriction> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<DietaryRestriction> toEntity(List<DietaryRestrictionDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
