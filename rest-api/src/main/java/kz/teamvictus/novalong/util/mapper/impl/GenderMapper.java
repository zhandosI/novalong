package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.GenderDto;
import kz.teamvictus.novalong.rest.model.entity.Gender;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("genderMapper")
public class GenderMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<Gender, GenderDto> {

    @Override
    public GenderDto toDto(Gender entity) {
        return getMapper().map(entity, GenderDto.class);
    }

    @Override
    public Gender toEntity(GenderDto dto) {
        return getMapper().map(dto, Gender.class);
    }

    @Override
    public List<GenderDto> toDto(List<Gender> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<Gender> toEntity(List<GenderDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

}
