package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.ingredient.IngredientCategoryDto;
import kz.teamvictus.novalong.rest.model.dto.ingredient.IngredientDto;
import kz.teamvictus.novalong.rest.model.entity.ingredient.Ingredient;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("ingredientMapper")
public class IngredientMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<Ingredient, IngredientDto> {

    @Override
    public IngredientDto toDto(Ingredient entity) {
        IngredientDto dto = getMapper().map(entity, IngredientDto.class);
        dto.setCategory(getMapper().map(entity.getCategory(), IngredientCategoryDto.class));
        return dto;
    }

    @Override
    public Ingredient toEntity(IngredientDto dto) {
        return getMapper().map(dto, Ingredient.class);
    }

    @Override
    public List<IngredientDto> toDto(List<Ingredient> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<Ingredient> toEntity(List<IngredientDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

}
