package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.ingredient.IngredientPropMapDto;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientPropMap;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("ingredientPropertiesMapper")
public class IngredientPropertiesMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<IngredientPropMap, IngredientPropMapDto> {

    @Override
    public IngredientPropMapDto toDto(IngredientPropMap entity) {
        return getMapper().map(entity, IngredientPropMapDto.class);
    }

    @Override
    public IngredientPropMap toEntity(IngredientPropMapDto dto) {
        return getMapper().map(dto, IngredientPropMap.class);
    }

    @Override
    public List<IngredientPropMapDto> toDto(List<IngredientPropMap> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<IngredientPropMap> toEntity(List<IngredientPropMapDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
