package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.ingredient.IngredientCategoryDto;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientCategory;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("ingredientCategoryMapper")
public class IngredientCategoryMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<IngredientCategory, IngredientCategoryDto> {

    @Override
    public IngredientCategoryDto toDto(IngredientCategory entity) {
        return getMapper().map(entity, IngredientCategoryDto.class);
    }

    @Override
    public IngredientCategory toEntity(IngredientCategoryDto dto) {
        return getMapper().map(dto, IngredientCategory.class);
    }

    @Override
    public List<IngredientCategoryDto> toDto(List<IngredientCategory> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<IngredientCategory> toEntity(List<IngredientCategoryDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

}
