package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.ingredient.IngredientAlternativeDto;
import kz.teamvictus.novalong.rest.model.dto.ingredient.IngredientAlternativesJoinedDto;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientAlternative;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientAlternativesJoined;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("ingredientAlternativeMapper")
public class IngredientAlternativeMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<IngredientAlternative, IngredientAlternativeDto> {

    @Override
    public IngredientAlternativeDto toDto(IngredientAlternative entity) {
        return getMapper().map(entity, IngredientAlternativeDto.class);
    }

    @Override
    public IngredientAlternative toEntity(IngredientAlternativeDto dto) {
        return getMapper().map(dto, IngredientAlternative.class);
    }

    @Override
    public List<IngredientAlternativeDto> toDto(List<IngredientAlternative> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<IngredientAlternative> toEntity(List<IngredientAlternativeDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }


    public IngredientAlternativesJoinedDto toDtoJoined(IngredientAlternativesJoined entity) {
        return getMapper().map(entity, IngredientAlternativesJoinedDto.class);
    }

    public List<IngredientAlternativesJoinedDto> toDtoJoined(List<IngredientAlternativesJoined> entities) {
        return entities.stream()
                .map(this::toDtoJoined)
                .collect(Collectors.toList());
    }
}
