package kz.teamvictus.novalong.util.model;

public final class SearchParams {

    public static final String LIMIT = "limit";
    public static final String OFFSET = "offset";
    public static final String SORT = "sort";
    public static final String DISH_TYPES = "dish_types";
    public static final String QUERY = "q";
    public static final String CATEGORIES = "categories";
    public static final String CATEGORY = "category";
    public static final String NAME = "name";
    public static final String COUNT = "count";
    public static final String ACTIVE = "active";
    public static final String PARENT_ID = "parent_id";

}
