package kz.teamvictus.novalong.util.model.criteria;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IngredientSearchCriteria extends SearchCriteria {

    private Integer category;
    private String name;

}
