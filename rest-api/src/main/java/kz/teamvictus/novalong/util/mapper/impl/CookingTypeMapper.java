package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.CookingTypeDto;
import kz.teamvictus.novalong.rest.model.entity.CookingType;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("cookingTypeMapper")
public class CookingTypeMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<CookingType, CookingTypeDto> {

    @Override
    public CookingTypeDto toDto(CookingType entity) {
        return getMapper().map(entity, CookingTypeDto.class);
    }

    @Override
    public CookingType toEntity(CookingTypeDto dto) {
        return getMapper().map(dto, CookingType.class);
    }

    @Override
    public List<CookingTypeDto> toDto(List<CookingType> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<CookingType> toEntity(List<CookingTypeDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

}
