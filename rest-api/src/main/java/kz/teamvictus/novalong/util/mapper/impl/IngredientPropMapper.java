package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.ingredient.IngredientPropDto;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientProp;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("ingredientPropMapper")
public class IngredientPropMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<IngredientProp, IngredientPropDto> {

    @Override
    public IngredientPropDto toDto(IngredientProp entity) {
        return getMapper().map(entity, IngredientPropDto.class);
    }

    @Override
    public IngredientProp toEntity(IngredientPropDto dto) {
        return getMapper().map(dto, IngredientProp.class);
    }

    @Override
    public List<IngredientPropDto> toDto(List<IngredientProp> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<IngredientProp> toEntity(List<IngredientPropDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

}
