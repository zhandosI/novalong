package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.dish.DishIngredientAlternativeDto;
import kz.teamvictus.novalong.rest.model.entity.dish.DishIngredientAlternative;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("dishIngredientAlternativeMapper")
public class DishIngredientAlternativeMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<DishIngredientAlternative, DishIngredientAlternativeDto> {

    @Override
    public DishIngredientAlternativeDto toDto(DishIngredientAlternative entity) {
        return getMapper().map(entity, DishIngredientAlternativeDto.class);
    }

    @Override
    public DishIngredientAlternative toEntity(DishIngredientAlternativeDto dto) {
        return getMapper().map(dto, DishIngredientAlternative.class);
    }

    @Override
    public List<DishIngredientAlternativeDto> toDto(List<DishIngredientAlternative> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<DishIngredientAlternative> toEntity(List<DishIngredientAlternativeDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }
}
