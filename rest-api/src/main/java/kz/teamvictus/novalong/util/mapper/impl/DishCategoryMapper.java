package kz.teamvictus.novalong.util.mapper.impl;

import kz.teamvictus.novalong.rest.model.dto.dish.DishCategoryDto;
import kz.teamvictus.novalong.rest.model.entity.dish.DishCategory;
import kz.teamvictus.novalong.util.mapper.AbstractEntityDtoMapper;
import kz.teamvictus.novalong.util.mapper.EntityDtoMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component("dishCategoryMapper")
public class DishCategoryMapper extends AbstractEntityDtoMapper implements EntityDtoMapper<DishCategory, DishCategoryDto> {

    @Override
    public DishCategoryDto toDto(DishCategory entity) {
        return getMapper().map(entity, DishCategoryDto.class);
    }

    @Override
    public DishCategory toEntity(DishCategoryDto dto) {
        return getMapper().map(dto, DishCategory.class);
    }

    @Override
    public List<DishCategoryDto> toDto(List<DishCategory> entities) {
        return entities.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<DishCategory> toEntity(List<DishCategoryDto> dtos) {
        return dtos.stream()
                .map(this::toEntity)
                .collect(Collectors.toList());
    }

}
