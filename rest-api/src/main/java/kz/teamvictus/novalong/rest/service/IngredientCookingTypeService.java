package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientCookingType;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientCookingTypeJoined;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.UniqueConstraintViolationException;

import java.util.List;

public interface IngredientCookingTypeService {

    List<IngredientCookingType> findAllByIngredient(Long ingredientId) throws InternalException;
    List<IngredientCookingTypeJoined> findAllByIngredientJoined(Long ingredientId) throws InternalException;

    void save(IngredientCookingType cookingType, Long ingredientId) throws InternalException, UniqueConstraintViolationException;
    void save(List<IngredientCookingType> cookingTypes, Long ingredientId) throws InternalException, UniqueConstraintViolationException;

}
