package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.Gender;
import kz.teamvictus.novalong.security.exceptions.InternalException;

import java.util.List;

public interface GenderService {

    Gender findById(Long genderId) throws InternalException;
    List<Gender> findAll() throws InternalException;

}
