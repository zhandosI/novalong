package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.CookingType;
import kz.teamvictus.novalong.rest.repository.CookingTypeRepository;
import kz.teamvictus.novalong.rest.service.CookingTypeService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CookingTypeServiceImpl implements CookingTypeService {

    private CookingTypeRepository cookingTypeRepository;

    @Autowired
    public void setCookingTypeRepository(CookingTypeRepository cookingTypeRepository) {
        this.cookingTypeRepository = cookingTypeRepository;
    }

    @Override
    public List<CookingType> findAll() throws InternalException {
        try {
            return cookingTypeRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<CookingType> findActive() throws InternalException {
        try {
            return cookingTypeRepository.findCookingTypesByActive(true);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public CookingType findById(Long id) throws InternalException {
        try {
            try {
                CookingType cookingType = cookingTypeRepository.findOne(id);

                if (cookingType == null)
                    throw new ResourceNotFoundException("There is no cookingType with id: " + id, ErrorCode.RESOURCE_NOT_FOUND);

                return cookingType;
            } catch (ResourceNotFoundException e) {
                throw e;
            } catch (Exception e) {
                throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
            }
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public CookingType save(CookingType cookingType) throws InternalException {
        try {
            if (cookingType.getName() == null)
                throw new InternalException("There is no cookingType name " + cookingType.getName(), ErrorCode.EMPTY_CODE);

            cookingType.setActive(true);

            return cookingTypeRepository.save(cookingType);
        } catch (InternalException ie) {
            throw ie;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public CookingType update(Long cookingTypeId, CookingType cookingType) throws InternalException, ResourceNotFoundException {
        try {
            CookingType cookingTypeToUpdate = cookingTypeRepository.getOne(cookingTypeId);

            if (cookingTypeToUpdate.getId() == null)
                throw new ResourceNotFoundException("There is no cookingType with id: " + cookingTypeId, ErrorCode.RESOURCE_NOT_FOUND);

            cookingTypeToUpdate.setName(cookingType.getName());
            cookingTypeToUpdate.setColorCodeId(cookingType.getColorCodeId());
            cookingTypeToUpdate.setOutOfMealPlan(cookingType.isOutOfMealPlan());

            cookingTypeRepository.save(cookingTypeToUpdate);

            return cookingTypeRepository.findOne(cookingTypeId);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void toggle(Long cookingTypeId) throws InternalException, ResourceNotFoundException {
        try {
            CookingType cookingType = cookingTypeRepository.getOne(cookingTypeId);

            if (cookingType.getId() == null)
                throw new ResourceNotFoundException("There is no cookingType with id: " + cookingTypeId, ErrorCode.RESOURCE_NOT_FOUND);

            cookingType.setActive(!cookingType.isActive());

            cookingTypeRepository.save(cookingType);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
