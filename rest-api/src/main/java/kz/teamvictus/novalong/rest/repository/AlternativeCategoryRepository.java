package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.alternative.AlternativeCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AlternativeCategoryRepository extends JpaRepository<AlternativeCategory, Long> {

    List<AlternativeCategory> findAlternativeCategoriesByActive(Boolean isActive);
}
