package kz.teamvictus.novalong.rest.repository.custom;

import kz.teamvictus.novalong.rest.model.entity.ingredient.Ingredient;
import kz.teamvictus.novalong.util.model.criteria.IngredientSearchCriteria;

import java.util.List;

public interface IngredientCustomRepository {

    List<Ingredient> findAllByCriteria(IngredientSearchCriteria ingredientCriteria);
    Long countAllByCriteria(IngredientSearchCriteria ingredientCriteria);

}
