package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.Purpose;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;

import java.util.List;

public interface PurposeService {

    List<Purpose> findAll() throws InternalException;
    List<Purpose> findActive() throws InternalException;
    Purpose findById(Long id) throws InternalException;

    Purpose save(Purpose purpose) throws InternalException;
    Purpose update(Long purposeId, Purpose purpose)
            throws InternalException, ResourceNotFoundException;
    void toggle(Long purposeId) throws InternalException, ResourceNotFoundException;

}
