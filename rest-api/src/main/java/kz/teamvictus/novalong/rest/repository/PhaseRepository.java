package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.Phase;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PhaseRepository extends JpaRepository<Phase, Long> {

    Phase findByPriority(int priority);
    List<Phase> findPhasesByActive(Boolean isActive);
}
