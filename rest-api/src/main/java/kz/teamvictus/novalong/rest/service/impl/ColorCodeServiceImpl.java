package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.ColorCode;
import kz.teamvictus.novalong.rest.repository.ColorCodeRepository;
import kz.teamvictus.novalong.rest.service.ColorCodeService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ColorCodeServiceImpl implements ColorCodeService {

    private ColorCodeRepository colorCodeRepository;

    @Autowired
    public void setColorCodeRepository(ColorCodeRepository colorCodeRepository) {
        this.colorCodeRepository = colorCodeRepository;
    }

    @Override
    public List<ColorCode> findAll() throws InternalException {
        try {
            return colorCodeRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ColorCode findById(Long id) throws InternalException {
        try {
            try {
                ColorCode colorCode = colorCodeRepository.findOne(id);

                if (colorCode == null)
                    throw new ResourceNotFoundException("There is no colorCode with id: " + id, ErrorCode.RESOURCE_NOT_FOUND);

                return colorCode;
            } catch (ResourceNotFoundException e) {
                throw e;
            } catch (Exception e) {
                throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
            }
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
