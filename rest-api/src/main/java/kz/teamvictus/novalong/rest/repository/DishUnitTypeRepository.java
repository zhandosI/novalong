package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.dish.DishUnitType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DishUnitTypeRepository extends JpaRepository<DishUnitType, Long> {
}
