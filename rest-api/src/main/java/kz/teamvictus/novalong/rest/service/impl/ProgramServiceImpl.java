package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.Program;
import kz.teamvictus.novalong.rest.model.entity.Purpose;
import kz.teamvictus.novalong.rest.repository.ProgramRepository;
import kz.teamvictus.novalong.rest.service.ProgramService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProgramServiceImpl implements ProgramService {

    private ProgramRepository programRepository;

    @Autowired
    public void setProgramRepository(ProgramRepository programRepository) {
        this.programRepository = programRepository;
    }

    @Override
    public Program findById(Long programId) throws InternalException, ResourceNotFoundException {
        try {
            Program program = programRepository.findOne(programId);

            if (program == null)
                throw new ResourceNotFoundException("There is no program with id: " + programId, ErrorCode.RESOURCE_NOT_FOUND);

            return program;
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public Program findByPurpose(Purpose purpose) throws InternalException {
        try {
            return programRepository.findByPurpose(purpose);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<Program> findAll() throws InternalException {
        try {
            return programRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<Program> findActive() throws InternalException {
        try {
            return programRepository.findProgramsByActive(true);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public Program save(Program program) throws InternalException {
        try {
            if (program.getName() == null)
                throw new InternalException("There is no program code " + program.getName(), ErrorCode.EMPTY_CODE);

            program.setActive(true);

            return programRepository.save(program);
        } catch (InternalException ie) {
            throw ie;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public Program update(Long programId, Program program) throws InternalException, ResourceNotFoundException {
        try {
            Program programToUpdate = programRepository.getOne(programId);

            if (programToUpdate.getId() == null)
                throw new ResourceNotFoundException("There is no program with id: " + programId, ErrorCode.RESOURCE_NOT_FOUND);

            programToUpdate.setName(program.getName());
            programToUpdate.setPurposeId(program.getPurposeId());
            programToUpdate.setCaloriesDifferenceCoefficient(program.getCaloriesDifferenceCoefficient());

            programRepository.save(programToUpdate);

            return programRepository.findOne(programId);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void toggle(Long programId) throws InternalException, ResourceNotFoundException {
        try {
            Program program = programRepository.getOne(programId);

            if (program.getId() == null)
                throw new ResourceNotFoundException("There is no program with id: " + programId, ErrorCode.RESOURCE_NOT_FOUND);

            program.setActive(!program.isActive());

            programRepository.save(program);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
