package kz.teamvictus.novalong.rest.model.entity;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "unit_types")
@Getter
@Setter
@ToString
public class UnitType extends BaseEntity {

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "unit_data_type_id")
    private Long dataTypeId;

    @ManyToOne
    @JoinColumn(name = "unit_data_type_id", insertable = false, updatable = false)
    private UnitDataType dataType;

    @Column(name = "is_dish")
    private Boolean isDish;

    @Column(name = "active")
    private boolean active;
}
