package kz.teamvictus.novalong.rest.repository.custom;


public interface UserDailyRationCustomRepository {

    double dailyCalories(Long rationId, String dayCode);
    void deactivateAll(Long rationId, String dayCode);
    void deactivateAll(Long rationId, String dayCode, Long dishTypeId);

}
