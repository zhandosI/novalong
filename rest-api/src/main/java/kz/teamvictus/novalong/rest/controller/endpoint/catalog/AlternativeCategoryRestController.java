package kz.teamvictus.novalong.rest.controller.endpoint.catalog;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.rest.model.dto.alternative.AlternativeCategoryDto;
import kz.teamvictus.novalong.rest.model.entity.alternative.AlternativeCategory;
import kz.teamvictus.novalong.rest.service.AlternativeCategoryService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.util.mapper.impl.AlternativeCategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/alternatives_categories")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AlternativeCategoryRestController extends BaseController {

    private final AlternativeCategoryService alternativeCategoryService;
    private final AlternativeCategoryMapper alternativeCategoryMapper;

    @Autowired
    public AlternativeCategoryRestController(AlternativeCategoryService alternativeCategoryService,
                                             AlternativeCategoryMapper alternativeCategoryMapper) {
        this.alternativeCategoryService = alternativeCategoryService;
        this.alternativeCategoryMapper = alternativeCategoryMapper;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAlternativeCategorys() throws InternalException {
        return buildResponse(success(alternativeCategoryMapper.toDto(alternativeCategoryService.findAll())), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAlternativeCategory(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        return buildResponse(success(alternativeCategoryMapper.toDto(alternativeCategoryService.findById(id))), HttpStatus.OK);
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveAlternativeCategory(@RequestBody AlternativeCategoryDto alternativeCategoryDto) throws InternalException {
        AlternativeCategory newAlternativeCategory = alternativeCategoryService.save(alternativeCategoryMapper.toEntity(alternativeCategoryDto));

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(buildLocationUri(newAlternativeCategory.getId(), "/{id}"));

        return buildResponse(success(alternativeCategoryMapper.toDto(newAlternativeCategory)), headers, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> toggleAlternativeCategory(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        alternativeCategoryService.toggle(id);

        return buildResponse(success(), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> updateAlternativeCategory(@RequestBody AlternativeCategoryDto alternativeCategoryDto,
                                        @PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        AlternativeCategory updatedAlternativeCategory = alternativeCategoryService.update(id, alternativeCategoryMapper.toEntity(alternativeCategoryDto));

        return buildResponse(success(alternativeCategoryMapper.toDto(updatedAlternativeCategory)), HttpStatus.OK);
    }

}
