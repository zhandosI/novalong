package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.CookingType;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;

import java.util.List;

public interface CookingTypeService {

    CookingType findById(Long id) throws InternalException;

    List<CookingType> findAll() throws InternalException;
    List<CookingType> findActive() throws InternalException;

    CookingType save(CookingType cookingType) throws InternalException;
    CookingType update(Long cookingTypeId, CookingType cookingType) throws InternalException, ResourceNotFoundException;

    void toggle(Long cookingTypeId) throws InternalException, ResourceNotFoundException;

}
