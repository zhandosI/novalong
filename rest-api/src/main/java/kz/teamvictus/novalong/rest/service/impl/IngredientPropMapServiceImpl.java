package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientPropMap;
import kz.teamvictus.novalong.rest.repository.IngredientPropMapRepository;
import kz.teamvictus.novalong.rest.service.IngredientPropMapService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientPropMapServiceImpl implements IngredientPropMapService {

    private IngredientPropMapRepository propMapRepository;

    @Autowired
    public void setPropMapRepository(IngredientPropMapRepository propMapRepository) {
        this.propMapRepository = propMapRepository;
    }

    @Override
    public List<IngredientPropMap> findAll() throws InternalException {
        try {
            return propMapRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<IngredientPropMap> findAllByIngredient(Long ingredientId) throws InternalException {
        try {
            return propMapRepository.findAllByIngredientId(ingredientId);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void save(List<IngredientPropMap> properties, Long ingredientId) throws InternalException {
        try {
            properties.forEach(p -> p.setIngredientId(ingredientId));
            propMapRepository.save(properties);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

}
