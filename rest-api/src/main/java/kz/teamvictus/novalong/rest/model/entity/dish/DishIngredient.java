package kz.teamvictus.novalong.rest.model.entity.dish;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import kz.teamvictus.novalong.rest.model.entity.UnitType;
import kz.teamvictus.novalong.rest.model.entity.ingredient.Ingredient;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "dishes_ingredients")
@Getter
@Setter
@ToString
public class DishIngredient extends BaseEntity {

    @Column(name = "dish_id")
    private Long dishId;

    @ManyToOne
    @JoinColumn(name = "ingredient_id")
    private Ingredient ingredient;

    @ManyToOne
    @JoinColumn(name = "unit_type_id")
    private UnitType unitType;

    @Column(name = "unit_type_amount")
    private double unitAmount;

    @Column(name = "weight")
    private float weight;

}
