package kz.teamvictus.novalong.rest.controller.endpoint.catalog;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.rest.model.dto.DietaryRestrictionDto;
import kz.teamvictus.novalong.rest.model.entity.DietaryRestriction;
import kz.teamvictus.novalong.rest.service.DietaryRestrictionService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.util.mapper.impl.DietaryRestrictionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/dietary_restrictions")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class DietaryRestrictionRestController extends BaseController {

    private final DietaryRestrictionService alternativeRelationService;
    private final DietaryRestrictionMapper alternativeRelationMapper;

    @Autowired
    public DietaryRestrictionRestController(DietaryRestrictionService alternativeRelationService,
                                            DietaryRestrictionMapper alternativeRelationMapper) {
        this.alternativeRelationService = alternativeRelationService;
        this.alternativeRelationMapper = alternativeRelationMapper;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getDietaryRestrictions() throws InternalException {
        return buildResponse(success(alternativeRelationMapper.toDto(alternativeRelationService.findAll())), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getDietaryRestriction(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        return buildResponse(success(alternativeRelationMapper.toDto(alternativeRelationService.findById(id))), HttpStatus.OK);
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveDietaryRestriction(@RequestBody DietaryRestrictionDto alternativeRelationDto) throws InternalException {
        DietaryRestriction newDietaryRestriction = alternativeRelationService.save(alternativeRelationMapper.toEntity(alternativeRelationDto));

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(buildLocationUri(newDietaryRestriction.getId(), "/{id}"));

        return buildResponse(success(alternativeRelationMapper.toDto(newDietaryRestriction)), headers, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> toggleDietaryRestriction(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        alternativeRelationService.toggle(id);

        return buildResponse(success(), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> updateDietaryRestriction(@RequestBody DietaryRestrictionDto alternativeRelationDto,
                                        @PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        DietaryRestriction updatedDietaryRestriction = alternativeRelationService.update(id, alternativeRelationMapper.toEntity(alternativeRelationDto));

        return buildResponse(success(alternativeRelationMapper.toDto(updatedDietaryRestriction)), HttpStatus.OK);
    }

}
