package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.dish.DishLight;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;

public interface DishLightService {

    DishLight findById(Long dishId) throws InternalException, ResourceNotFoundException;

}
