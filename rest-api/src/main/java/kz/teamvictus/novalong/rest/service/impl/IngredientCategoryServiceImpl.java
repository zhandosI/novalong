package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.ingredient.Ingredient;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientCategory;
import kz.teamvictus.novalong.rest.repository.IngredientCategoryRepository;
import kz.teamvictus.novalong.rest.repository.custom.IngredientCategoryCustomRepository;
import kz.teamvictus.novalong.rest.service.IngredientCategoryService;
import kz.teamvictus.novalong.rest.service.IngredientService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import kz.teamvictus.novalong.util.model.SearchParams;
import kz.teamvictus.novalong.util.model.criteria.IngredientCategoryCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class IngredientCategoryServiceImpl implements IngredientCategoryService {

    private final IngredientCategoryRepository categoryRepository;
    private final IngredientCategoryCustomRepository categoryCustomRepository;
    private final IngredientService ingredientService;

    @Autowired
    public IngredientCategoryServiceImpl(IngredientCategoryRepository categoryRepository,
                                         IngredientCategoryCustomRepository categoryCustomRepository,
                                         @Lazy IngredientService ingredientService) {
        this.categoryRepository = categoryRepository;
        this.categoryCustomRepository = categoryCustomRepository;
        this.ingredientService = ingredientService;
    }

    @Override
    public IngredientCategory findById(Long categoryId) throws InternalException, ResourceNotFoundException {
        try {
            IngredientCategory category = categoryRepository.findOne(categoryId);

            IngredientCategory soq = this.findByCode("SUGAR");

            if (category == null)
                throw new ResourceNotFoundException("There is no ingredient category with id: " + categoryId, ErrorCode.RESOURCE_NOT_FOUND);

            return category;
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public IngredientCategory findByCode(String code) throws InternalException {
        try {
            return categoryRepository.findByCode(code);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public IngredientCategory findByName(String name) throws InternalException {
        try {
            return categoryRepository.findByName(name);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<IngredientCategory> findAll() throws InternalException {
        try {
            return categoryRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<IngredientCategory> findAll(Long parentCategoryId) throws InternalException {
        try {
            if (parentCategoryId == null)
                return categoryRepository.findAll();

            return categoryRepository.findAllByParentCategoryId(parentCategoryId);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<IngredientCategory> findAll(Map<String, String> params) throws InternalException {
        try {
            if (!params.isEmpty()) {
                List<IngredientCategory> categories = this.categoryCustomRepository.findAllByCriteria(this.parseParams(params));

                if (categories == null || categories.isEmpty())
                    return Collections.emptyList();

                return categories;
            }

            return categoryRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<IngredientCategory> findChild(Long parentCategoryId) throws InternalException {
        try {
            // TODO: Mappers for Iterable, refactor all services to return Iterable
            return new ArrayList<>(categoryRepository.findOne(parentCategoryId).getChildCategories());
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public IngredientCategory save(IngredientCategory category) throws InternalException {
        try {
            if (category.getName() == null)
                throw new InternalException("There is no ingredientCategory code " + category.getName(), ErrorCode.EMPTY_CODE);

            category.setActive(true);

            return categoryRepository.save(category);
        } catch (InternalException ie) {
            throw ie;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public IngredientCategory update(Long categoryId, IngredientCategory category) throws InternalException, ResourceNotFoundException {
        try {
            IngredientCategory ingredientCategoryToUpdate = categoryRepository.getOne(categoryId);

            if (ingredientCategoryToUpdate.getId() == null)
                throw new ResourceNotFoundException("There is no ingredientCategory with id: " + categoryId, ErrorCode.RESOURCE_NOT_FOUND);

            ingredientCategoryToUpdate.setName(category.getName());
            ingredientCategoryToUpdate.setParentCategoryId(category.getParentCategoryId());

            List<IngredientCategory> updatableList = new ArrayList<>();
            updateAllergic(updatableList, categoryRepository.save(ingredientCategoryToUpdate), category.isAllergic());
            categoryRepository.save(updatableList);

            return categoryRepository.findOne(categoryId);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void toggle(Long categoryId) throws InternalException, ResourceNotFoundException {
        try {
            IngredientCategory ingredientCategory = categoryRepository.getOne(categoryId);

            if (ingredientCategory.getId() == null)
                throw new ResourceNotFoundException("There is no ingredientCategory with id: " + categoryId, ErrorCode.RESOURCE_NOT_FOUND);

            List<IngredientCategory> updatableCategories = new ArrayList<>();
            boolean trigger = !ingredientCategory.isActive();

            if (trigger)
                this.updateActive(updatableCategories, ingredientCategory, trigger);
            else {
                // If deactivating a category all the ingredients hierarchy is pushed to ROOT category
                List<Ingredient> updatableIngredients = new ArrayList<>();
                this.updateActive(updatableCategories, updatableIngredients, ingredientCategory, trigger);
                IngredientCategory root = this.findByName("ROOT");

                updatableIngredients.forEach(i -> i.setCategory(root));
                ingredientService.update(updatableIngredients);
            }

            categoryRepository.save(updatableCategories);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    private void updateAllergic(List<IngredientCategory> updatableList, IngredientCategory category, boolean allergic) {
        for (IngredientCategory c: category.getChildCategories()) {
            c.setAllergic(allergic);
            this.updateAllergic(updatableList, c, allergic);
        }
        category.setAllergic(allergic);

        updatableList.add(category);
    }

    private void updateActive(List<IngredientCategory> updatableList, IngredientCategory category, boolean active) {
        for (IngredientCategory c: category.getChildCategories()) {
            this.updateActive(updatableList, c, active);
        }
        category.setActive(active);

        updatableList.add(category);
    }

    private void updateActive(List<IngredientCategory> updatableCategories,
                              List<Ingredient> updatableIngredients,
                              IngredientCategory category, boolean active) throws InternalException {
        if (category == null)
            return;

        Queue<IngredientCategory> queue = new LinkedList<>();
        queue.add(category);

        while (!queue.isEmpty()) {
            IngredientCategory current = queue.poll();

            if (current == null)
                break;

            current.setActive(active);

            updatableCategories.add(current);
            updatableIngredients.addAll(ingredientService.findAll(current));

            if (!current.getChildCategories().isEmpty())
                queue.addAll(current.getChildCategories());
        }
    }

    private IngredientCategoryCriteria parseParams(Map<String,String> params) {
        IngredientCategoryCriteria criteria = new IngredientCategoryCriteria();

        for (Map.Entry<String, String> entry: params.entrySet()) {
            switch (entry.getKey()) {
                case SearchParams.QUERY:
                    criteria.setSearchQuery(entry.getValue());
                    break;

                case SearchParams.NAME:
                    criteria.setName(entry.getValue());
                    break;

                case SearchParams.PARENT_ID:
                    criteria.setParentCategoryId(Long.valueOf(entry.getValue()));
                    break;

                case SearchParams.ACTIVE:
                    criteria.setActive(Boolean.valueOf(entry.getValue()));
                    break;
            }
        }

        return criteria;
    }

}
