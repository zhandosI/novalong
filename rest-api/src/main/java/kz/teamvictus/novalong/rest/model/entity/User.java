package kz.teamvictus.novalong.rest.model.entity;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "users")
@Where(clause = "active = 1")
@Getter
@Setter
@ToString
public class User extends BaseEntity {

    @Column(name = "username")
    private String username;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "social_media_id")
    private String socialMediaId;

    @ManyToOne
    @JoinColumn(name = "gender_id")
    private Gender gender;

    @Column(name = "birth_date")
    private Date birthDate;

    @Column(name = "height")
    private double height;

    @Column(name = "weight")
    private double weight;

    @ManyToOne
    @JoinColumn(name = "purpose_id")
    private Purpose purpose;

    @ManyToMany
    @JoinTable(name = "users_dietary_restrictions",
                   joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "dietary_restriction_id"))
    private Set<DietaryRestriction> restrictions;

    @ManyToOne
    @JoinColumn(name = "activity_level_id")
    private ActivityLevel activityLevel;

    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    @ManyToOne
    @JoinColumn(name = "current_phase_id")
    private Phase currentPhase;

    @Column(name = "phase_update_date")
    private Date phaseUpdateDate;

    @Column(name = "active")
    private boolean active;

    @Transient
    private transient int age;

    @PostLoad
    public void calculateAge() {
        LocalDate today = LocalDate.now();
        LocalDate birthDate = this.birthDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        Period period = Period.between(birthDate, today);
        this.age = period.getYears();
    }

}
