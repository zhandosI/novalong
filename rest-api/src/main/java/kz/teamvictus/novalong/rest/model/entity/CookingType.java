package kz.teamvictus.novalong.rest.model.entity;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "cooking_types")
@Getter
@Setter
@ToString
public class CookingType extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "color_code_id")
    private Long colorCodeId;

    @ManyToOne
    @JoinColumn(name = "color_code_id", insertable = false, updatable = false)
    private ColorCode colorCode;

    @Column(name = "out_of_meal_plan")
    private boolean outOfMealPlan;

    @Column(name = "active")
    private boolean active;

}
