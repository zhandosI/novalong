package kz.teamvictus.novalong.rest.controller.endpoint.catalog;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.rest.model.dto.ProgramDishTypeDto;
import kz.teamvictus.novalong.rest.model.entity.ProgramDishType;
import kz.teamvictus.novalong.rest.service.DishTypeService;
import kz.teamvictus.novalong.rest.service.ProgramDishTypeService;
import kz.teamvictus.novalong.rest.service.ProgramService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.util.mapper.impl.DishTypeMapper;
import kz.teamvictus.novalong.util.mapper.impl.ProgramDishTypeMapper;
import kz.teamvictus.novalong.util.mapper.impl.ProgramMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/programs_dish_types")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class ProgramDishTypeRestController extends BaseController {

    private final ProgramDishTypeService programDishTypeService;
    private final ProgramService programService;
    private final DishTypeService dishTypeService;
    private final ProgramDishTypeMapper programDishTypeMapper;
    private final ProgramMapper programMapper;
    private final DishTypeMapper dishTypeMapper;

    @Autowired
    public ProgramDishTypeRestController(ProgramDishTypeService programDishTypeService,
                                         ProgramService programService, DishTypeService dishTypeService,
                                         ProgramDishTypeMapper programDishTypeMapper,
                                         ProgramMapper programMapper, DishTypeMapper dishTypeMapper) {
        this.programDishTypeService = programDishTypeService;
        this.programService = programService;
        this.dishTypeService = dishTypeService;
        this.programDishTypeMapper = programDishTypeMapper;
        this.programMapper = programMapper;
        this.dishTypeMapper = dishTypeMapper;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getProgramDishTypes() throws InternalException {
//        return buildResponse(success(programDishTypeMapper.toDto(programDishTypeService.findAll())), HttpStatus.OK);
        return buildResponse(success(programDishTypeService.findAll()), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getProgramDishType(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
//        return buildResponse(success(programDishTypeMapper.toDto(programDishTypeService.findById(id))), HttpStatus.OK);
        return buildResponse(success(programDishTypeService.findById(id)), HttpStatus.OK);
    }

    @GetMapping(value = "program", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getPrograms() throws InternalException {
//        return buildResponse(success(programMapper.toDto(programService.findActive())), HttpStatus.OK);
        return buildResponse(success(programService.findActive()), HttpStatus.OK);
    }

    @GetMapping(value = "dishType", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getDishTypes() throws InternalException {
        return buildResponse(success(dishTypeMapper.toDto(dishTypeService.findActive())), HttpStatus.OK);
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveProgramDishType(@RequestBody ProgramDishTypeDto programDishTypeDto) throws InternalException {
        ProgramDishType newProgramDishType = programDishTypeService.save(programDishTypeMapper.toEntity(programDishTypeDto));

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(buildLocationUri(newProgramDishType.getId(), "/{id}"));

        return buildResponse(success(programDishTypeMapper.toDto(newProgramDishType)), headers, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> toggleProgramDishType(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        programDishTypeService.toggle(id);

        return buildResponse(success(), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> updateProgramDishType(@RequestBody ProgramDishType programDishType,
                                        @PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
//        ProgramDishType updatedProgramDishType = programDishTypeService.update(id, programDishTypeMapper.toEntity(programDishTypeDto));
        ProgramDishType updatedProgramDishType = programDishTypeService.update(id, programDishType);

        return buildResponse(success(programDishTypeMapper.toDto(updatedProgramDishType)), HttpStatus.OK);
    }

}
