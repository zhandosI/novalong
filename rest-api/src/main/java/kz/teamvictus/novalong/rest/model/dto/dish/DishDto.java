package kz.teamvictus.novalong.rest.model.dto.dish;

import kz.teamvictus.novalong.rest.model.dto.CookingTypeDto;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.Set;

@Getter
@Setter
@ToString
public class DishDto {

    private Long id;
    private Long userId;
    private String name;
    private String description;
    private String recipeDescription;
    private Date dateAdded;
    private Set<DishTypeDto> dishTypes;
    private Set<DishCategoryDto> categories;
    private Set<CookingTypeDto> cookingTypes;
    private Set<DishIngredientDto> dishIngredients;
    private Double weight;
    private Float protein;
    private Float fat;
    private Float carbohydrate;
    private Float kilocalories;
    private Set<DishUnitTypeDto> unitTypes;
    private String recipeLink;
    private Boolean active;

}
