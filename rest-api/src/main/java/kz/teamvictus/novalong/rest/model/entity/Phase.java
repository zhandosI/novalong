package kz.teamvictus.novalong.rest.model.entity;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "phases")
@Getter
@Setter
@ToString
public class Phase extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "priority")
    private int priority;

    @Column(name = "proteins_percentage")
    private float proteinsPercentage;

    @Column(name = "carbs_percentage")
    private float carbsPercentage;

    @Column(name = "fats_percentage")
    private float fatsPercentage;

    @Column(name = "active")
    private boolean active;

}
