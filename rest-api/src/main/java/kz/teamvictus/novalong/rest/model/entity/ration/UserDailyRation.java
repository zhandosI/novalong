package kz.teamvictus.novalong.rest.model.entity.ration;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import kz.teamvictus.novalong.rest.model.entity.UnitType;
import kz.teamvictus.novalong.rest.model.entity.dish.DishLight;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "user_daily_rations")
@Getter
@Setter
@ToString
public class UserDailyRation extends BaseEntity {

    @Column(name = "user_ration_id")
    private Long userRationId;

    @Column(name = "dish_id")
    private Long dishId;

    @Column(name = "dish_type_id")
    private Long dishTypeId;

    @ManyToOne
    @JoinColumn(name = "dish_id", insertable = false, updatable = false)
    private DishLight dish;

    @Column(name = "day_code")
    private String dayCode;

    @Column(name = "protein")
    private float protein;

    @Column(name = "carbohydrate")
    private float carbohydrate;

    @Column(name = "fat")
    private float fat;

    @ManyToOne
    @JoinColumn(name = "unit_type_id")
    private UnitType unitType;

    @Column(name = "unit_amount")
    private double unitAmount;

    @Column(name = "active")
    private boolean active;

}
