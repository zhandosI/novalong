package kz.teamvictus.novalong.rest.model.dto;

import kz.teamvictus.novalong.rest.model.dto.dish.DishTypeDto;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ProgramDishTypeDto {

    private Long id;
//    private ProgramDto program;
    private Long programId;
    private DishTypeDto dishType;
    private Long dishTypeId;
    private double caloriesPercentage;
    private boolean active;

}
