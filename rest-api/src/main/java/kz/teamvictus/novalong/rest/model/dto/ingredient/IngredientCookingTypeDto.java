package kz.teamvictus.novalong.rest.model.dto.ingredient;

import kz.teamvictus.novalong.rest.model.dto.CookingTypeDto;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class IngredientCookingTypeDto {

    private CookingTypeDto cookingType;
    private IngredientChangablePropDto changableProp;
    private double differenceCoefficient;

}
