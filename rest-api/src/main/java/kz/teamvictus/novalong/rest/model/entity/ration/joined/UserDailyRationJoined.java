package kz.teamvictus.novalong.rest.model.entity.ration.joined;

import kz.teamvictus.novalong.rest.model.entity.dish.joined.DishTypeDishesJoined;
import lombok.*;

import java.util.Set;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserDailyRationJoined extends RationContainer {

    private String dayCode;
    private Float proteins;
    private Float carbohydrates;
    private Float fats;
    private Float calories;
    private Set<DishTypeDishesJoined> dailyMeals;

}
