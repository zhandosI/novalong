package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.UnitDataType;
import kz.teamvictus.novalong.security.exceptions.InternalException;

import java.util.List;

public interface UnitDataTypeService {

    List<UnitDataType> findAll() throws InternalException;
    UnitDataType findById(Long id) throws InternalException;

}
