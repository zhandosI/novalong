package kz.teamvictus.novalong.rest.model.entity.dish;


import kz.teamvictus.novalong.rest.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "dish_types")
@Where(clause = "active = 1")
@Getter
@Setter
@ToString
public class DishType extends BaseEntity {

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "active")
    private boolean active;

}
