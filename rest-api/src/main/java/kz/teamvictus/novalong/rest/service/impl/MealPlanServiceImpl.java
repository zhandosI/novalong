package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.Program;
import kz.teamvictus.novalong.rest.model.entity.ProgramDishType;
import kz.teamvictus.novalong.rest.model.entity.User;
import kz.teamvictus.novalong.rest.model.entity.dish.joined.DishTypeDishesJoined;
import kz.teamvictus.novalong.rest.model.entity.dish.joined.DishUnitTypeJoined;
import kz.teamvictus.novalong.rest.model.entity.ration.UserDailyRation;
import kz.teamvictus.novalong.rest.model.entity.ration.UserRation;
import kz.teamvictus.novalong.rest.model.entity.ration.joined.RationContainer;
import kz.teamvictus.novalong.rest.model.entity.ration.joined.UserDailyRationJoined;
import kz.teamvictus.novalong.rest.model.entity.ration.joined.UserRationJoined;
import kz.teamvictus.novalong.rest.model.entity.ration.joined.UserRationObjectJoined;
import kz.teamvictus.novalong.rest.service.*;
import kz.teamvictus.novalong.rest.tool.MealPlanGenerationTool;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.LackOfDataException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import kz.teamvictus.novalong.util.CalculationUtil;
import kz.teamvictus.novalong.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MealPlanServiceImpl implements MealPlanService {

    private final UserRationService userRationService;
    private final UserDailyRationService dailyRationService;
    private final UserService userService;
    private final ProgramService programService;
    private final DishLightService dishLightService;

    @Autowired
    public MealPlanServiceImpl(UserRationService userRationService,
                               UserDailyRationService dailyRationService,
                               UserService userService,
                               ProgramService programService,
                               DishLightService dishLightService) {
        this.userRationService = userRationService;
        this.dailyRationService = dailyRationService;
        this.userService = userService;
        this.programService = programService;
        this.dishLightService = dishLightService;
    }

    @Override
    public UserRationJoined rationByParams(Long userId, Map<String, String> params) throws InternalException, ResourceNotFoundException {
        try {
            UserRation ration = userRationService.findByUser(userId);
            User user = userService.findProxyById(userId);
            Program program = programService.findByPurpose(user.getPurpose());

            if (program == null)
                throw new LackOfDataException("There is no program for user's " + user.getId() + " purpose: " + user.getPurpose().getCode(), ErrorCode.REQUIRED_PARAMS_NOT_FOUND);

            if (ration != null && !new Date().before(ration.getEndDate())) {
                this.userRationService.delete(ration);
                ration = this.createRation(user, program, DateUtil.getDaysNumTillWeekEnd());
            }
            else if (ration == null) {
                ration = this.createRation(user, program, DateUtil.getDaysNumTillWeekEnd());
            }

            RationContainer dailyJoined = null;
            int day = DateUtil.getTodayDay();
            float calsNeeded = (float) userService.userNeededCalories(user, program.getCaloriesDifferenceCoefficient(), 1);

            boolean regenerate = false;
            params_check: if (params != null && !params.isEmpty()) {
                if (params.get("regenerate") != null)
                    regenerate = Boolean.parseBoolean(params.get("regenerate"));

                if (params.get("day") != null && params.get("dish_type_id") != null && params.get("dish_id") != null) {
                    // TODO: regenerate dish
                    break params_check;
                }

                /* **************** Generate dishes by dishType **************** */
                if (params.get("day") != null && params.get("dish_type_id") != null) {
                    String dayCode = params.get("day").trim().toUpperCase();
                    Long dishTypeId = Long.parseLong(params.get("dish_type_id"));

                    ProgramDishType pdt = program.getProgramDishTypes().stream()
                            .filter(t -> t.getDishType().getId().equals(dishTypeId))
                            .findFirst()
                            .orElse(null);

                    day = DateUtil.getDayByCode(dayCode);
                    dailyJoined = this.generateByDishType(ration.getId(), pdt, calsNeeded, day, regenerate);
                    break params_check;
                }

                /* **************** Generate dishes by day **************** */
                if (params.get("day") != null) {
                    String dayCode = params.get("day").trim().toUpperCase();

                    if (!DateUtil.dayCodeExists(dayCode))
                        throw new ResourceNotFoundException("There is no day with code: " + dayCode, ErrorCode.RESOURCE_NOT_FOUND);

                    day = DateUtil.getDayByCode(dayCode);
                    dailyJoined = this.generateByDay(ration.getId(), program.getProgramDishTypes(), calsNeeded, day, regenerate);
                    break params_check;
                }

                /* **************** Generate for today by default **************** */
                if (params.get("day") == null && params.get("dish_type_id") == null && params.get("dish_id") == null)
                    dailyJoined = this.generateByDay(ration.getId(), program.getProgramDishTypes(), calsNeeded, day, regenerate);
            }

            UserRationObjectJoined rationObject = new UserRationObjectJoined(
                    userId, program.getId(),
                    ration.getStartDate(),
                    ration.getEndDate(),
                    0F,
                    CalculationUtil.roundToInt(calsNeeded),
                    CalculationUtil.roundToInt(calsNeeded));

            if ((dailyJoined instanceof DishTypeDishesJoined && ((DishTypeDishesJoined) dailyJoined).getMeals().isEmpty()) ||
                (dailyJoined instanceof UserDailyRationJoined && ((UserDailyRationJoined) dailyJoined).getDailyMeals().isEmpty()))
                return rationObject;

            float dayCals = (float) this.dailyRationService.dailyCalories(ration.getId(), DateUtil.getDayCodeOfWeek(day));

            rationObject.setCalories(CalculationUtil.roundToInt(dayCals));
            rationObject.setCaloriesDiff(CalculationUtil.roundToInt(dayCals - calsNeeded));
            rationObject.setRation(dailyJoined);

            return rationObject;
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    private UserRation createRation(User user, Program program, int daysNum) throws InternalException {
        try {
            double calsNeeded = userService.userNeededCalories(user, program.getCaloriesDifferenceCoefficient(), daysNum);
            return userRationService.save(user.getId(), program.getId(), daysNum, (float) calsNeeded);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private UserDailyRationJoined generateByDay(Long rationId, Set<ProgramDishType> pdt, double calsNeeded, int day, boolean regenerate) throws InternalException, ResourceNotFoundException {
        try {
            String dayCode = DateUtil.getDayCodeOfWeek(day);
            UserDailyRationJoined ration = new UserDailyRationJoined();
            Set<DishTypeDishesJoined> dishTypeDishes = new LinkedHashSet<>();
            ration.setDayCode(dayCode);

            if (regenerate) {
                this.dailyRationService.deleteAll(rationId, dayCode);
                this.dailyRationService.save(MealPlanGenerationTool.generate(rationId, pdt, calsNeeded, day, 1));
            }

            // Sorting pdt's by Id
            List<ProgramDishType> sortedPdts = new ArrayList<>(pdt);
            sortedPdts.sort(Comparator.comparing(ProgramDishType::getDishTypeId));

            float prots = 0, fats = 0, carbs = 0;
            for (ProgramDishType type: sortedPdts) {
                List<UserDailyRation> rationsList = this.dailyRationService.findAll(rationId, type.getDishType().getId(), dayCode);

                if (rationsList != null && !rationsList.isEmpty()) {
                    Set<DishUnitTypeJoined> meals = new HashSet<>();
                    float dishTypeCals = 0;

                    for (UserDailyRation daily: rationsList) {
                        prots += daily.getProtein();
                        fats += daily.getFat();
                        carbs += daily.getCarbohydrate();

                        float dishCals = daily.getProtein() * 4 + daily.getCarbohydrate() * 4 + daily.getFat() * 9;
                        dishTypeCals += dishCals;

                        meals.add(new DishUnitTypeJoined(
                                CalculationUtil.roundToInt(dishCals),
                                CalculationUtil.roundToOnePoint(daily.getProtein()),
                                CalculationUtil.roundToOnePoint(daily.getCarbohydrate()),
                                CalculationUtil.roundToOnePoint(daily.getFat()),
                                this.dishLightService.findById(daily.getDishId()),
                                daily.getUnitType(),
                                daily.getUnitAmount())
                        );
                    }

                    dishTypeDishes.add(new DishTypeDishesJoined(CalculationUtil.roundToInt(dishTypeCals), type.getDishType(), meals));
                }
            }

            ration.setProteins(CalculationUtil.roundToOnePoint(prots));
            ration.setFats(CalculationUtil.roundToOnePoint(fats));
            ration.setCarbohydrates(CalculationUtil.roundToOnePoint(carbs));
            ration.setCalories(CalculationUtil.roundToInt(prots * 4 + fats * 9 + carbs * 4));
            ration.setDailyMeals(dishTypeDishes);

            return ration;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private DishTypeDishesJoined generateByDishType(Long rationId, ProgramDishType pdt, double calsNeeded, int day, boolean regenerate) throws InternalException, ResourceNotFoundException {
        try {
            String dayCode = DateUtil.getDayCodeOfWeek(day);
            Set<DishUnitTypeJoined> meals = new HashSet<>();

            if (regenerate) {
                this.dailyRationService.deleteAll(rationId, dayCode, pdt.getDishType().getId());
                this.dailyRationService.save(MealPlanGenerationTool.generate(rationId, Collections.singleton(pdt), calsNeeded, day, 1));
            }

            float dishTypeCals = 0;
            List<UserDailyRation> rationsList = this.dailyRationService.findAll(rationId, pdt.getDishType().getId(), dayCode);
            if (rationsList != null && !rationsList.isEmpty()) {

                for (UserDailyRation daily: rationsList) {
                    float dishCals = daily.getProtein() * 4 + daily.getCarbohydrate() * 4 + daily.getFat() * 9;
                    dishTypeCals += dishCals;

                    meals.add(new DishUnitTypeJoined(
                            CalculationUtil.roundToInt(dishCals),
                            CalculationUtil.roundToOnePoint(daily.getProtein()),
                            CalculationUtil.roundToOnePoint(daily.getCarbohydrate()),
                            CalculationUtil.roundToOnePoint(daily.getFat()),
                            this.dishLightService.findById(daily.getDishId()),
                            daily.getUnitType(),
                            daily.getUnitAmount())
                    );
                }
            }

            return new DishTypeDishesJoined(dishTypeCals, pdt.getDishType(), meals);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private DishUnitTypeJoined generateByDish() {
        try {
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

}
