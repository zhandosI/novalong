package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.UnitType;
import kz.teamvictus.novalong.rest.repository.UnitTypeRepository;
import kz.teamvictus.novalong.rest.service.UnitTypeService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnitTypeServiceImpl implements UnitTypeService {

    private UnitTypeRepository unitTypeRepository;

    @Autowired
    public void setUnitTypeRepository(UnitTypeRepository unitTypeRepository) {
        this.unitTypeRepository = unitTypeRepository;
    }

    @Override
    public UnitType findByCode(String code) throws InternalException {
        try {
            return unitTypeRepository.findByCode(code);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public UnitType findByCode(String code, boolean isDish) throws InternalException {
        try {
            return unitTypeRepository.findByCodeAndIsDish(code, isDish);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<UnitType> findAll() throws InternalException {
        try {
            return unitTypeRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<UnitType> findActive() throws InternalException {
        try {
            return unitTypeRepository.findUnitTypesByActive(true);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<UnitType> findAllIngredient() throws InternalException {
        try {
            return unitTypeRepository.findAllByIsDish(false);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<UnitType> findAllDish() throws InternalException {
        try {
            return unitTypeRepository.findAllByIsDish(true);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public UnitType findById(Long id) throws InternalException {
        try {
            try {
                UnitType unitType = unitTypeRepository.findOne(id);

                if (unitType == null)
                    throw new ResourceNotFoundException("There is no unitType with id: " + id, ErrorCode.RESOURCE_NOT_FOUND);

                return unitType;
            } catch (ResourceNotFoundException e) {
                throw e;
            } catch (Exception e) {
                throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
            }
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public UnitType save(UnitType unitType) throws InternalException {
        try {
            if (unitType.getName() == null)
                throw new InternalException("There is no unitType code " + unitType.getName(), ErrorCode.EMPTY_CODE);

            unitType.setIsDish(unitType.getIsDish() != null ? unitType.getIsDish() : false);
            unitType.setActive(true);

            return unitTypeRepository.save(unitType);
        } catch (InternalException ie) {
            throw ie;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public UnitType update(Long unitTypeId, UnitType unitType) throws InternalException, ResourceNotFoundException {
        try {
            UnitType unitTypeToUpdate = unitTypeRepository.getOne(unitTypeId);

            if (unitTypeToUpdate.getId() == null)
                throw new ResourceNotFoundException("There is no unitType with id: " + unitTypeId, ErrorCode.RESOURCE_NOT_FOUND);

            unitTypeToUpdate.setName(unitType.getName());
            unitTypeToUpdate.setDataTypeId(unitType.getDataTypeId());
            unitTypeToUpdate.setIsDish(unitType.getIsDish());

            unitTypeRepository.save(unitTypeToUpdate);

            return unitTypeRepository.findOne(unitTypeId);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void toggle(Long unitTypeId) throws InternalException, ResourceNotFoundException {
        try {
            UnitType unitType = unitTypeRepository.getOne(unitTypeId);

            if (unitType.getId() == null)
                throw new ResourceNotFoundException("There is no unitType with id: " + unitTypeId, ErrorCode.RESOURCE_NOT_FOUND);

            unitType.setActive(!unitType.isActive());

            unitTypeRepository.save(unitType);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
