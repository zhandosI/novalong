package kz.teamvictus.novalong.rest.controller.endpoint.catalog;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.rest.service.GenderService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.util.mapper.impl.GenderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/genders")
public class GenderRestController extends BaseController {

    private final GenderMapper genderMapper;
    private final GenderService genderService;

    @Autowired
    public GenderRestController(GenderMapper genderMapper,
                                GenderService genderService) {
        this.genderMapper = genderMapper;
        this.genderService = genderService;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAllGenders() throws InternalException {
        return buildResponse(success(genderMapper.toDto(genderService.findAll())), HttpStatus.OK);
    }

}
