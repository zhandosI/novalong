package kz.teamvictus.novalong.rest.controller.endpoint.catalog;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.rest.model.dto.UnitTypeDto;
import kz.teamvictus.novalong.rest.model.entity.UnitType;
import kz.teamvictus.novalong.rest.service.UnitDataTypeService;
import kz.teamvictus.novalong.rest.service.UnitTypeService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.util.mapper.impl.UnitTypeMapper;
import kz.teamvictus.novalong.util.mapper.impl.UnitDataTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/unit_types")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class UnitTypeRestController extends BaseController {

    private final UnitTypeService unitDataTypeDishTypeService;
    private final UnitDataTypeService unitDataTypeService;
    private final UnitTypeMapper unitDataTypeDishTypeMapper;
    private final UnitDataTypeMapper unitDataTypeMapper;

    @Autowired
    public UnitTypeRestController(UnitTypeService unitDataTypeDishTypeService,
                                  UnitDataTypeService unitDataTypeService,
                                  UnitTypeMapper unitDataTypeDishTypeMapper,
                                  UnitDataTypeMapper unitDataTypeMapper) {
        this.unitDataTypeDishTypeService = unitDataTypeDishTypeService;
        this.unitDataTypeService = unitDataTypeService;
        this.unitDataTypeDishTypeMapper = unitDataTypeDishTypeMapper;
        this.unitDataTypeMapper = unitDataTypeMapper;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getUnitTypes() throws InternalException {
//        return buildResponse(success(unitDataTypeDishTypeMapper.toDto(unitDataTypeDishTypeService.findAll())), HttpStatus.OK);
        return buildResponse(success(unitDataTypeDishTypeService.findAll()), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getUnitType(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
//        return buildResponse(success(unitDataTypeDishTypeMapper.toDto(unitDataTypeDishTypeService.findById(id))), HttpStatus.OK);
        return buildResponse(success(unitDataTypeDishTypeService.findById(id)), HttpStatus.OK);
    }

    @GetMapping(value = "dataType", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getUnitDataTypes() throws InternalException {
        return buildResponse(success(unitDataTypeMapper.toDto(unitDataTypeService.findAll())), HttpStatus.OK);
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveUnitType(@RequestBody UnitType unitDataTypeDishTypeDto) throws InternalException {
        UnitType newUnitType = unitDataTypeDishTypeService.save(unitDataTypeDishTypeDto);
//        UnitType newUnitType = unitDataTypeDishTypeService.save(unitDataTypeDishTypeMapper.toEntity(unitDataTypeDishTypeDto));

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(buildLocationUri(newUnitType.getId(), "/{id}"));

        return buildResponse(success(unitDataTypeDishTypeMapper.toDto(newUnitType)), headers, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> toggleUnitType(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        unitDataTypeDishTypeService.toggle(id);

        return buildResponse(success(), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> updateUnitType(@RequestBody UnitType unitDataTypeDishType,
                                        @PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
//        UnitType updatedUnitType = unitDataTypeDishTypeService.update(id, unitDataTypeDishTypeMapper.toEntity(unitDataTypeDishTypeDto));
        UnitType updatedUnitType = unitDataTypeDishTypeService.update(id, unitDataTypeDishType);

        return buildResponse(success(unitDataTypeDishTypeMapper.toDto(updatedUnitType)), HttpStatus.OK);
    }

}
