package kz.teamvictus.novalong.rest.model.entity.dish;

import kz.teamvictus.novalong.util.CalculationUtil;
import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;

/**
 * Entity class for table dishes,
 * containing only database mandatory fields except date_added
 *
 * @see kz.teamvictus.novalong.rest.model.entity.dish.BaseDish
 */
@Entity
@Table(name = "dishes")
@Where(clause = "active = 1")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
public class DishMandatory extends BaseDish {

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "name")
    @NonNull
    private String name;

    @Column(name = "weight")
    @NonNull
    private Float weight;

    @Column(name = "protein")
    @NonNull
    private Float protein;

    @Column(name = "fat")
    @NonNull
    private Float fat;

    @Column(name = "carbohydrate")
    @NonNull
    private Float carbohydrate;

    @Column(name = "active")
    @NonNull
    private Boolean active;

    @Transient
    private transient Float calories;

    @PostLoad
    public void calculateAge() {
        this.calories = CalculationUtil.roundToTwoPoints(
                (getProtein()* 4 + getFat() * 9 + getCarbohydrate() * 4)*100 / getWeight());
    }

}
