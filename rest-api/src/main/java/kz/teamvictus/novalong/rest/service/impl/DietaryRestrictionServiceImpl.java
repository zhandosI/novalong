package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.DietaryRestriction;
import kz.teamvictus.novalong.rest.repository.DietaryRestrictionRepository;
import kz.teamvictus.novalong.rest.service.DietaryRestrictionService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DietaryRestrictionServiceImpl implements DietaryRestrictionService {

    private DietaryRestrictionRepository dietaryRestrictionRepository;

    @Autowired
    public void setDietaryRestrictionRepository(DietaryRestrictionRepository dietaryRestrictionRepository) {
        this.dietaryRestrictionRepository = dietaryRestrictionRepository;
    }

    @Override
    public List<DietaryRestriction> findAll() throws InternalException {
        try {
            return dietaryRestrictionRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<DietaryRestriction> findActive() throws InternalException {
        try {
            return dietaryRestrictionRepository.findDietaryRestrictionsByActive(true);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public DietaryRestriction findById(Long id) throws InternalException {
        try {
            try {
                DietaryRestriction dietaryRestriction = dietaryRestrictionRepository.findOne(id);

                if (dietaryRestriction == null)
                    throw new ResourceNotFoundException("There is no dietaryRestriction with id: " + id, ErrorCode.RESOURCE_NOT_FOUND);

                return dietaryRestriction;
            } catch (ResourceNotFoundException e) {
                throw e;
            } catch (Exception e) {
                throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
            }
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public DietaryRestriction save(DietaryRestriction dietaryRestriction) throws InternalException {
        try {
            if (dietaryRestriction.getCode() == null)
                throw new InternalException("There is no dietaryRestriction code " + dietaryRestriction.getCode(), ErrorCode.EMPTY_CODE);

            dietaryRestriction.setActive(true);

            return dietaryRestrictionRepository.save(dietaryRestriction);
        } catch (InternalException ie) {
            throw ie;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public DietaryRestriction update(Long dietaryRestrictionId, DietaryRestriction dietaryRestriction) throws InternalException, ResourceNotFoundException {
        try {
            DietaryRestriction dietaryRestrictionToUpdate = dietaryRestrictionRepository.getOne(dietaryRestrictionId);

            if (dietaryRestrictionToUpdate.getId() == null)
                throw new ResourceNotFoundException("There is no dietaryRestriction with id: " + dietaryRestrictionId, ErrorCode.RESOURCE_NOT_FOUND);

            dietaryRestrictionToUpdate.setCode(dietaryRestriction.getCode());
            dietaryRestrictionToUpdate.setName(dietaryRestriction.getName());

            dietaryRestrictionRepository.save(dietaryRestrictionToUpdate);

            return dietaryRestrictionRepository.findOne(dietaryRestrictionId);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void toggle(Long dietaryRestrictionId) throws InternalException, ResourceNotFoundException {
        try {
            DietaryRestriction dietaryRestriction = dietaryRestrictionRepository.getOne(dietaryRestrictionId);

            if (dietaryRestriction.getId() == null)
                throw new ResourceNotFoundException("There is no dietaryRestriction with id: " + dietaryRestrictionId, ErrorCode.RESOURCE_NOT_FOUND);

            dietaryRestriction.setActive(!dietaryRestriction.isActive());

            dietaryRestrictionRepository.save(dietaryRestriction);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
