package kz.teamvictus.novalong.rest.model.dto.dish.joined;

import kz.teamvictus.novalong.rest.model.dto.UnitTypeDto;

import kz.teamvictus.novalong.rest.model.dto.dish.DishDto;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DishUnitTypeJoinedDto {

    private Float calories;
    private Float proteins;
    private Float carbohydrates;
    private Float fats;
    private DishDto dish;
    private UnitTypeDto unitType;
    private Double unitAmount;

}
