package kz.teamvictus.novalong.rest.model.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PurposeDto {

    private Long id;
    private String code;
    private String name;
    private boolean active;

}
