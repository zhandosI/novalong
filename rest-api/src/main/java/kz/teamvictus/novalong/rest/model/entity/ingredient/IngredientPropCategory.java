package kz.teamvictus.novalong.rest.model.entity.ingredient;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import kz.teamvictus.novalong.rest.model.entity.UnitDataType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "ingredients_props_categories")
@Getter
@Setter
@ToString
public class IngredientPropCategory extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "ingredient_prop_id")
    private IngredientProp type;

    @ManyToOne
    @JoinColumn(name = "ingredient_category_id")
    private IngredientCategory category;

    @ManyToOne
    @JoinColumn(name = "unit_data_type_id")
    private UnitDataType unitDataType;

}
