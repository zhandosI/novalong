package kz.teamvictus.novalong.rest.repository.custom.impl;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientCategory;
import kz.teamvictus.novalong.rest.repository.custom.IngredientCategoryCustomRepository;
import kz.teamvictus.novalong.util.model.criteria.IngredientCategoryCriteria;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
public class IngredientCategoryCustomRepositoryImpl implements IngredientCategoryCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<IngredientCategory> findAllByCriteria(IngredientCategoryCriteria criteria) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<IngredientCategory> query = builder.createQuery(IngredientCategory.class);
        Root<IngredientCategory> root = query.from(IngredientCategory.class);

        List<Predicate> predicates = new ArrayList<>();

        // SEARCH BY QUERY
        if (criteria.getSearchQuery() != null) {
            final String PERCENT_SIGN = "%";
            predicates.add(builder.like(root.get("name"), PERCENT_SIGN + criteria.getSearchQuery() + PERCENT_SIGN));
        }

        // SEARCH BY NAME
        if (criteria.getName() != null)
            predicates.add(builder.equal(root.get("name"), criteria.getName()));

        // SEARCH BY PARENT CATEGORY ID
        if (criteria.getParentCategoryId() != null)
            predicates.add(builder.equal(root.get("parentCategoryId"), criteria.getParentCategoryId()));

        // SEARCH BY ACTIVE
        if (criteria.getActive() != null)
            predicates.add(builder.equal(root.get("active"), criteria.getActive()));

        query.select(root).where(predicates.toArray(new Predicate[]{}));

        TypedQuery<IngredientCategory> typedQuery = em.createQuery(query);

        if (criteria.getOffset() != null)
            typedQuery.setFirstResult(criteria.getOffset());

        if (criteria.getLimit() != null)
            typedQuery.setMaxResults(criteria.getLimit());

        return typedQuery.getResultList();
    }

}
