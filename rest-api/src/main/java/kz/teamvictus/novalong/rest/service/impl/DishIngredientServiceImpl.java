package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.dish.DishIngredient;
import kz.teamvictus.novalong.rest.repository.DishIngredientRepository;
import kz.teamvictus.novalong.rest.service.DishIngredientService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class DishIngredientServiceImpl implements DishIngredientService {

    private DishIngredientRepository dishIngredientRepository;

    @Autowired
    public void setDishIngredientRepository(DishIngredientRepository dishIngredientRepository) {
        this.dishIngredientRepository = dishIngredientRepository;
    }

    @Override
    public void save(Collection<DishIngredient> dishIngredients) throws InternalException {
        try {
            dishIngredientRepository.save(dishIngredients);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void delete(Collection<DishIngredient> dishIngredients) throws InternalException {
        try {
            dishIngredientRepository.delete(dishIngredients);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
