package kz.teamvictus.novalong.rest.repository.custom;

import kz.teamvictus.novalong.rest.model.entity.dish.Dish;
import kz.teamvictus.novalong.util.model.criteria.DishSearchCriteria;

import java.util.List;

public interface DishCustomRepository {

    List<Dish> findAllByCriteria(DishSearchCriteria dishCriteria);
    List<Dish> findRandomByDishType(Long dishTypeId, double calsNeeded, int dishesNum);
    List<Dish> findRandomByDishTypeAndCategories(Long dishTypeId, double calsNeeded, String categoryCodesParam, String ratiosParam);
    Long countAllByCriteria(DishSearchCriteria dishCriteria);

}
