package kz.teamvictus.novalong.rest.model.dto.ingredient;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class IngredientOptParamNameDto {

    private Long id;
    private String code;
    private String name;

}
