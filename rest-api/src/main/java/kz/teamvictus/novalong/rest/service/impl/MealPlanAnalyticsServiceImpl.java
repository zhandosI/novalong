package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.CookingType;
import kz.teamvictus.novalong.rest.model.entity.HealthFactor;
import kz.teamvictus.novalong.rest.model.entity.dish.Dish;
import kz.teamvictus.novalong.rest.model.entity.dish.DishIngredient;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientOptParamValue;
import kz.teamvictus.novalong.rest.model.entity.ration.UserDailyRation;
import kz.teamvictus.novalong.rest.model.entity.ration.UserRation;
import kz.teamvictus.novalong.rest.service.MealPlanAnalyticsService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MealPlanAnalyticsServiceImpl implements MealPlanAnalyticsService {

    // FIXME REFACTOR ALL HERE!
    // TODO:  Temporary solution for params map, without db storing, not working for list return
    private static Set<HealthFactor> healthParams;

    @Override
    public HealthFactor healthFactor(Dish dish) throws InternalException {
        try {
            Set<HealthFactor> healthParams = this.getHealthParams();

            if (!healthParams.isEmpty())
                healthParams.clear();

            double factor = this.dishHealthFactor(dish);
            return new HealthFactor(dish.getId(), this.healthColorCode(factor), factor, healthParams);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public HealthFactor healthFactor(UserRation ration) throws InternalException {
        try {
            // TODO: REWRITE! For demonstrative purpose only
            Set<HealthFactor> healthParams = this.getHealthParams();

            if (!healthParams.isEmpty())
                healthParams.clear();

            double dishFactorsSum = 0;
            double dishWeights = 0;

            double sugarFactor = 0;
            double saltFactor = 0;
            double fatsFactor = 0;
            double fatsSaturatedFactor = 0;
            double fatsTransFactor = 0;
            double cookingFactor = 0;

            double sugarTotal = 0;
            double saltTotal = 0;
            double fatsTotal = 0;
            double fatsSaturTotal = 0;
            double fatsTransTotal = 0;
            double cookingTotal = 0;

            // TODO: SHITTY!
            Set<HealthFactor> totalSet = new LinkedHashSet<>();

//            for (UserDailyRation daily: ration.getDailyRations()) {
//                // TODO: GRAMS unit type is hardcoded!
//                dishFactorsSum += 0
////                        this.dishHealthFactor(daily.getDish()) * daily.getUnitAmount()
//                ;
//                dishWeights += daily.getUnitAmount();
//
//                for (HealthFactor param: healthParams) {
//                    switch (param.getCode()) {
//                        case "SUGAR":
//                            sugarFactor += param.getFactor() * daily.getUnitAmount();
//
//                            if (param.getWeight() != null)
//                                sugarTotal += param.getWeight();
//
//                            break;
//                        case "SALT":
//                            saltFactor += param.getFactor() * daily.getUnitAmount();
//
//                            if (param.getWeight() != null)
//                                saltTotal += param.getWeight();
//
//                            break;
//                        case "FATS":
//                            fatsFactor += param.getFactor() * daily.getUnitAmount();
//
//                            if (param.getWeight() != null)
//                                fatsTotal += param.getWeight();
//
//                            break;
//                        case "FATS_SATURATED":
//                            fatsSaturatedFactor += param.getFactor() * daily.getUnitAmount();
//
//                            if (param.getWeight() != null)
//                                fatsSaturTotal += param.getWeight();
//
//                            break;
//                        case "FATS_TRANS":
//                            fatsTransFactor += param.getFactor() * daily.getUnitAmount();
//
//                            if (param.getWeight() != null)
//                                fatsTransTotal += param.getWeight();
//
//                            break;
//                        case "COOKING_TYPE":
//                            cookingFactor += param.getFactor() * daily.getUnitAmount();
//
//                            if (param.getWeight() != null)
//                                cookingTotal += param.getWeight();
//
//                            break;
//                    }
//                }
//                healthParams.clear();
//            }

            double factor = dishFactorsSum / dishWeights;

            double sugarTotalFactor = sugarFactor / dishWeights;
            double saltTotalFactor = saltFactor / dishWeights;
            double fatsTotalFactor = fatsFactor / dishWeights;
            double fatsSaturTotalFactor = fatsSaturatedFactor / dishWeights;
            double fatsTransTotalFactor = fatsTransFactor / dishWeights;
            double cookingTotalFactor = cookingFactor / dishWeights;

            totalSet.add(new HealthFactor("SUGAR", this.healthColorCode(sugarTotalFactor), sugarTotalFactor, sugarTotal));
            totalSet.add(new HealthFactor("SALT", this.healthColorCode(saltTotalFactor), saltTotalFactor, saltTotal));
            totalSet.add(new HealthFactor("FATS", this.healthColorCode(fatsTotalFactor), fatsTotalFactor, fatsTotal));
            totalSet.add(new HealthFactor("FATS_SATURATED", this.healthColorCode(fatsSaturTotalFactor), fatsSaturTotalFactor, fatsSaturTotal));
            totalSet.add(new HealthFactor("FATS_TRANS", this.healthColorCode(fatsTransTotalFactor), fatsTransTotalFactor, fatsTransTotal));
            totalSet.add(new HealthFactor("COOKING_TYPE", this.healthColorCode(cookingTotalFactor), cookingTotalFactor, cookingTotal));

            return new HealthFactor(this.healthColorCode(factor), factor, totalSet);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<HealthFactor> healthFactor(List<Dish> dishes) throws InternalException {
        try {
            List<HealthFactor> factors = new ArrayList<>();

            for (Dish dish: dishes)
               factors.add(this.healthFactor(dish));

            return factors;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    private double dishHealthFactor(Dish dish) {
        Set<HealthFactor> healthParams = this.getHealthParams();

        double total;
        double coef = 1;

        double sugarFactor = 1, saltFactor = 1, cookingTypeFactor = 1, fatsFactor = 1, saturatedFatsFactor = 1, transFatsFactor = 1;

        double sugar = 0, salt = 0, fats = 0, saturatedFats = 0, transFats = 0;

        for (DishIngredient di: dish.getDishIngredients()) {
            fats += di.getIngredient().getFat() * di.getWeight() / 100;

            // SALT count from dish
            if (di.getIngredient().getCode() != null) {
                switch (di.getIngredient().getCode()) {
                    case "SALT":
                        salt += di.getWeight();
                        break;
                }
            }

            // TODO: divided by 1000 is for mkg, map with unit types instead
            for (IngredientOptParamValue val: di.getIngredient().getOptionalParams()) {
                if (val.getParam().getCode().equals("SUGAR"))
                    sugar += di.getWeight() * val.getWeight() / 100;

                // NaCl for salt
                if (val.getParam().getCode().equals("SODIUM"))
                    salt += di.getWeight() * (val.getWeight() / 1000) / 100;

                // Saturated fats
                if (val.getParam().getCode().equals("FATS_SATURATED"))
                    saturatedFats += di.getWeight() * val.getWeight() / 100;

                // Saturated fats
                if (val.getParam().getCode().equals("FATS_TRANS"))
                    transFats += di.getWeight() * val.getWeight() / 100;
            }
        }

        double saltIn100g = salt * 100 / dish.getWeight();
        double sugarIn100g = sugar * 100 / dish.getWeight();
        double fatsIn100g = fats * 100 / dish.getWeight();
        double saturatedFatsIn100g = saturatedFats * 100 / dish.getWeight();
        double transFatsIn100g = transFats * 100 / dish.getWeight();

        /* ******************************************** */

        if (sugarIn100g >= 5 && sugarIn100g <= 22.5)
            sugarFactor = 1 - (sugarIn100g - 5) / (22.5 - 5);

        if (sugarIn100g > 22.5) {
            sugarFactor = 0;
            coef = 0.1;
        }

        /* ******************************************** */

        if (saltIn100g >= 0.3 && saltIn100g <= 1.5)
            saltFactor = 1 - (saltIn100g - 0.3) / (1.5 - 0.3);

        if (saltIn100g > 1.5) {
            saltFactor = 0;
            coef = 0.1;
        }

        /* ******************************************** */

        if (fatsIn100g >= 3 && fatsIn100g <= 17.5)
            fatsFactor = 1 - (fatsIn100g - 3) / (17.5 - 3);

        if (fatsIn100g > 17.5) {
            fatsFactor = 0;
            coef = 0.1;
        }

        /* ******************************************** */

        if (saturatedFatsIn100g >= 1.5 && saturatedFatsIn100g <= 5)
            saturatedFatsFactor = 1 - (saturatedFatsIn100g - 1.5) / (5 - 1.5);

        if (saturatedFatsIn100g > 5) {
            saturatedFatsFactor = 0;
            coef = 0.1;
        }

        /* ******************************************** */

        if (transFatsIn100g >= 1.5 && transFatsIn100g <= 5)
            transFatsFactor = 1 - (transFatsIn100g - 1.5) / (5 - 1.5);

        if (transFatsIn100g > 5) {
            transFatsFactor = 0;
            coef = 0.1;
        }

        // COOKING TYPE
        if (dish.getCookingTypes() != null && !dish.getCookingTypes().isEmpty()) {
            cookingTypeFactor = 0;

            for (CookingType c: dish.getCookingTypes()) {
                cookingTypeFactor += c.getColorCode().getFactor();

                if (c.getColorCode().getFactor() == 0)
                    coef = 0.1;
            }

            cookingTypeFactor /= dish.getCookingTypes().size();
        }

        total = sugarFactor + saltFactor + cookingTypeFactor + fatsFactor + saturatedFatsFactor + transFatsFactor;

        healthParams.add(new HealthFactor("SUGAR", this.healthColorCode(sugarFactor), sugarFactor, sugar));
        healthParams.add(new HealthFactor("SALT", this.healthColorCode(saltFactor), saltFactor, salt));
        healthParams.add(new HealthFactor("FATS", this.healthColorCode(fatsFactor), fatsFactor, fats));
        healthParams.add(new HealthFactor("FATS_SATURATED", this.healthColorCode(saturatedFatsFactor), saturatedFatsFactor, saturatedFats));
        healthParams.add(new HealthFactor("FATS_TRANS", this.healthColorCode(transFatsFactor), transFatsFactor, transFats));
        healthParams.add(new HealthFactor("COOKING_TYPE", this.healthColorCode(cookingTypeFactor), cookingTypeFactor));

        return (total / 6) * coef;
    }

    private String healthColorCode(double healthFactor) {
        int red = 0;
        int green = 0;
        int blue = 0;

        if (healthFactor == 1)
            return this.hexFromRGB(0, 255, 0);

        if (healthFactor == 0)
            return this.hexFromRGB(155, 0, 0);

        if (healthFactor == 0.5)
            return this.hexFromRGB(255, 255, 0);

        if (healthFactor >= 0.1 && healthFactor < 0.5) {
//            double roundedHealthFactor = Math.round(healthFactor * 10d) / 10d;
//            red = 255;
//            green = Math.toIntExact(Math.round(25 * roundedHealthFactor * 10));
            
            long rounded = Math.round(healthFactor * 255 / 0.5);
            red = 255;
            green = Math.toIntExact(rounded);
        }

        if (healthFactor > 0 && healthFactor < 0.1) {
            double flooredHealthFactor = Math.floor(healthFactor * 100d) / 100d;
            red = 155 + Math.toIntExact(Math.round((255 - 155) * flooredHealthFactor * 10));
        }

        if (healthFactor > 0.5 && healthFactor < 1) {
//            double roundedHealthFactor = Math.round(healthFactor * 10d) / 10d;
//            green = 255;
//            red = Math.toIntExact(Math.round(25 * roundedHealthFactor * 10));
            
            long rounded = Math.round((healthFactor - 0.5) * 255 / 0.5);
            green = 255;
            red = 255 - Math.toIntExact(rounded);
        }

        return this.hexFromRGB(red, green, blue);
    }

    private String hexFromRGB(int red, int green, int blue) {
        return String.format("#%02X%02X%02X", red, green, blue);
    }


    private Set<HealthFactor> getHealthParams() {
        if (healthParams == null) {
            synchronized (MealPlanAnalyticsServiceImpl.class) {
                if (healthParams == null)
                    healthParams = new LinkedHashSet<>();
            }
        }

        return healthParams;
    }

}
