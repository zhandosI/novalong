package kz.teamvictus.novalong.rest.model.entity.ingredient;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "ingredients_ingredients_props")
@Getter
@Setter
@ToString
public class IngredientPropMap extends BaseEntity {

    @Column(name = "ingredient_id")
    private Long ingredientId;

    @ManyToOne
    @JoinColumn(name = "ingredient_props_category_id")
    private IngredientPropCategory property;

    @Column(name = "value")
    private String value;

}
