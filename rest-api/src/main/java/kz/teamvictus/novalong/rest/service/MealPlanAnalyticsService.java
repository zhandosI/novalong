package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.HealthFactor;
import kz.teamvictus.novalong.rest.model.entity.dish.Dish;
import kz.teamvictus.novalong.rest.model.entity.ration.UserRation;
import kz.teamvictus.novalong.security.exceptions.InternalException;

import java.util.List;

public interface MealPlanAnalyticsService {

    HealthFactor healthFactor(Dish dish) throws InternalException;
    HealthFactor healthFactor(UserRation ration) throws InternalException;
    List<HealthFactor> healthFactor(List<Dish> dishes) throws InternalException;

}
