package kz.teamvictus.novalong.rest.model.dto.ingredient;

import kz.teamvictus.novalong.rest.model.dto.alternative.AlternativeCategoryDto;
import kz.teamvictus.novalong.rest.model.dto.alternative.AlternativeRelationDto;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Getter
@Setter
@ToString
public class IngredientAlternativesJoinedDto {

    private AlternativeCategoryDto category;
    private AlternativeRelationDto relation;
    private Set<IngredientDto> ingredients;

}
