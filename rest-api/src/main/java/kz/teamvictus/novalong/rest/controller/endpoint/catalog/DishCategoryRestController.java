package kz.teamvictus.novalong.rest.controller.endpoint.catalog;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.rest.model.dto.dish.DishCategoryDto;
import kz.teamvictus.novalong.rest.model.entity.dish.DishCategory;
import kz.teamvictus.novalong.rest.service.DishCategoryService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.util.mapper.impl.DishCategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/dish_categories")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class DishCategoryRestController extends BaseController {

    private final DishCategoryService alternativeRelationService;
    private final DishCategoryMapper alternativeRelationMapper;

    @Autowired
    public DishCategoryRestController(DishCategoryService alternativeRelationService,
                                      DishCategoryMapper alternativeRelationMapper) {
        this.alternativeRelationService = alternativeRelationService;
        this.alternativeRelationMapper = alternativeRelationMapper;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getDishCategorys() throws InternalException {
        return buildResponse(success(alternativeRelationMapper.toDto(alternativeRelationService.findAll())), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getDishCategory(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        return buildResponse(success(alternativeRelationMapper.toDto(alternativeRelationService.findById(id))), HttpStatus.OK);
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveDishCategory(@RequestBody DishCategoryDto alternativeRelationDto) throws InternalException {
        DishCategory newDishCategory = alternativeRelationService.save(alternativeRelationMapper.toEntity(alternativeRelationDto));

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(buildLocationUri(newDishCategory.getId(), "/{id}"));

        return buildResponse(success(alternativeRelationMapper.toDto(newDishCategory)), headers, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> toggleDishCategory(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        alternativeRelationService.toggle(id);

        return buildResponse(success(), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> updateDishCategory(@RequestBody DishCategoryDto alternativeRelationDto,
                                        @PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        DishCategory updatedDishCategory = alternativeRelationService.update(id, alternativeRelationMapper.toEntity(alternativeRelationDto));

        return buildResponse(success(alternativeRelationMapper.toDto(updatedDishCategory)), HttpStatus.OK);
    }

}
