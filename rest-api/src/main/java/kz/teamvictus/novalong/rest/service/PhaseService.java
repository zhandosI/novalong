package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.Phase;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;

import java.util.List;

public interface PhaseService {

    Phase findById(Long id) throws InternalException;
    Phase findByPriority(int priority) throws InternalException;

    List<Phase> findAll() throws InternalException;
    List<Phase> findActive() throws InternalException;

    Phase save(Phase phase) throws InternalException;
    Phase update(Long phaseId, Phase phase) throws InternalException, ResourceNotFoundException;
    void toggle(Long phaseId) throws InternalException, ResourceNotFoundException;
}
