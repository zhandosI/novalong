package kz.teamvictus.novalong.rest.model.entity.ration.joined;

import lombok.*;

import java.util.Set;

@Getter
@Setter
@ToString
public class UserRationArrayJoined extends UserRationJoined {

    private Set<RationContainer> ration;

}
