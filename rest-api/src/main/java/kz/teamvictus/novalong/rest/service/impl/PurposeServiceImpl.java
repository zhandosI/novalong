package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.Purpose;
import kz.teamvictus.novalong.rest.repository.PurposeRepository;
import kz.teamvictus.novalong.rest.service.PurposeService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PurposeServiceImpl implements PurposeService {

    private PurposeRepository purposeRepository;

    @Autowired
    public void setPurposeRepository(PurposeRepository purposeRepository) {
        this.purposeRepository = purposeRepository;
    }

    @Override
    public List<Purpose> findAll() throws InternalException {
        try {
            return purposeRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<Purpose> findActive() throws InternalException {
        try {
            return purposeRepository.findPurposesByActive(true);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public Purpose findById(Long id) throws InternalException {
        try {
            try {
                Purpose purpose = purposeRepository.findOne(id);

                if (purpose == null)
                    throw new ResourceNotFoundException("There is no purpose with id: " + id, ErrorCode.RESOURCE_NOT_FOUND);

                return purpose;
            } catch (ResourceNotFoundException e) {
                throw e;
            } catch (Exception e) {
                throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
            }
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public Purpose save(Purpose purpose) throws InternalException {
        try {
            if (purpose.getCode() == null)
                throw new InternalException("There is no purpose code " + purpose.getName(), ErrorCode.EMPTY_CODE);

            purpose.setActive(true);

            return purposeRepository.save(purpose);
        } catch (InternalException ie) {
            throw ie;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public Purpose update(Long purposeId, Purpose purpose) throws InternalException, ResourceNotFoundException {
        try {
            Purpose purposeToUpdate = purposeRepository.getOne(purposeId);

            if (purposeToUpdate.getId() == null)
                throw new ResourceNotFoundException("There is no purpose with id: " + purposeId, ErrorCode.RESOURCE_NOT_FOUND);

            purposeToUpdate.setName(purpose.getName());

            purposeRepository.save(purposeToUpdate);

            return purposeRepository.findOne(purposeId);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void toggle(Long purposeId) throws InternalException, ResourceNotFoundException {
        try {
            Purpose purpose = purposeRepository.getOne(purposeId);

            if (purpose.getId() == null)
                throw new ResourceNotFoundException("There is no purpose with id: " + purposeId, ErrorCode.RESOURCE_NOT_FOUND);

            purpose.setActive(!purpose.isActive());

            purposeRepository.save(purpose);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
