package kz.teamvictus.novalong.rest.model.entity;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "genders")
@Getter
@Setter
@ToString
public class Gender extends BaseEntity {

    @Column(name = "code")
    private String code;

    @Column(name = "name")
    private String name;

}
