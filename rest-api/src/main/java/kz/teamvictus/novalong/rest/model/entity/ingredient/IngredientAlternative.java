package kz.teamvictus.novalong.rest.model.entity.ingredient;


import kz.teamvictus.novalong.rest.model.BaseEntity;
import kz.teamvictus.novalong.rest.model.entity.alternative.AlternativeCategory;
import kz.teamvictus.novalong.rest.model.entity.alternative.AlternativeRelation;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "ingredients_alternatives")
@Getter
@Setter
@ToString
public class IngredientAlternative extends BaseEntity {

    @Column(name = "ingredient_id")
    private Long ingredientId;

    @ManyToOne
    @JoinColumn(name = "alternative_ingredient_id")
    private Ingredient alternativeIngredient;

    @ManyToOne
    @JoinColumn(name = "alternative_category_id")
    private AlternativeCategory category;

    @ManyToOne
    @JoinColumn(name = "alternative_relation_id")
    private AlternativeRelation relation;

}
