package kz.teamvictus.novalong.rest.model.entity.ingredient;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "ingredients")
@SecondaryTable(name = "ingredients_codes", pkJoinColumns = @PrimaryKeyJoinColumn(name = "ingredient_id"))
@Where(clause = "active = 1")
@Getter
@Setter
@ToString
public class Ingredient extends BaseEntity {

    @Column(name = "code", table = "ingredients_codes")
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "protein")
    private float protein;

    @Column(name = "fat")
    private float fat;

    @Column(name = "carbohydrate")
    private float carbohydrate;

    @Column(name = "kilocalories")
    private float kilocalories;

    @Column(name = "glycemic_index")
    private float glycemicIndex;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private IngredientCategory category;

    @Column(name = "is_allergic")
    private boolean isAllergic;

    @OneToMany(mappedBy = "ingredientId")
    private Set<IngredientUnitType> unitTypes;

    @ToString.Exclude
    @OneToMany(mappedBy = "ingredientId", fetch = FetchType.LAZY)
    private Set<IngredientOptParamValue> optionalParams;

    @ToString.Exclude
    @OneToMany(mappedBy = "ingredientId")
    private Set<IngredientAlternative> alternatives;

    @ToString.Exclude
    @OneToMany(mappedBy = "ingredientId")
    private Set<IngredientCookingType> cookingTypes;

    @Column(name = "active")
    private boolean active;

}
