package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.ColorCode;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ColorCodeRepository extends JpaRepository<ColorCode, Long> {

}
