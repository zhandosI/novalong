package kz.teamvictus.novalong.rest.model.dto.ingredient;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class IngredientOptParamValueDto {

    Long id;
    private IngredientOptParamNameDto param;
    private double weight;

}
