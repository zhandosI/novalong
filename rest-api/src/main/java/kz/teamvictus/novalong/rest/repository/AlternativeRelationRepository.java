package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.alternative.AlternativeRelation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AlternativeRelationRepository extends JpaRepository<AlternativeRelation, Long> {

    List<AlternativeRelation> findAlternativeRelationsByActive(Boolean isActive);
}
