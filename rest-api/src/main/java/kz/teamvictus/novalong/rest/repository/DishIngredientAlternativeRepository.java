package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.dish.DishIngredientAlternative;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DishIngredientAlternativeRepository extends JpaRepository<DishIngredientAlternative, Long> {

    List<DishIngredientAlternative> findAllByDishId(Long dishId);

}
