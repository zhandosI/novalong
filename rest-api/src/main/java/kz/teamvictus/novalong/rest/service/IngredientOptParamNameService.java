package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientOptParamName;
import kz.teamvictus.novalong.security.exceptions.InternalException;

import java.util.List;

public interface IngredientOptParamNameService {

    IngredientOptParamName findByCode(String code) throws InternalException;
    List<IngredientOptParamName> findAll() throws InternalException;

}
