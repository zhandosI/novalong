package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.Program;
import kz.teamvictus.novalong.rest.model.entity.Purpose;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProgramRepository extends JpaRepository<Program, Long> {

    List<Program> findProgramsByActive(Boolean isActive);
    Program findByPurpose(Purpose purpose);

}
