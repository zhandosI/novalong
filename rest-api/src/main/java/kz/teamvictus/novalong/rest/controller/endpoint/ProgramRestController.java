package kz.teamvictus.novalong.rest.controller.endpoint;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.rest.model.dto.ProgramDto;
import kz.teamvictus.novalong.rest.model.entity.Program;
import kz.teamvictus.novalong.rest.service.ProgramService;
import kz.teamvictus.novalong.rest.service.PurposeService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.util.mapper.impl.ProgramMapper;
import kz.teamvictus.novalong.util.mapper.impl.PurposeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/programs")
public class ProgramRestController extends BaseController {

    private final ProgramService programService;
    private final PurposeService purposeService;
    private final ProgramMapper programMapper;
    private final PurposeMapper purposeMapper;

    @Autowired
    public ProgramRestController(ProgramService programService,
                                 PurposeService purposeService,
                                 ProgramMapper programMapper,
                                 PurposeMapper purposeMapper) {
        this.programService = programService;
        this.purposeService = purposeService;
        this.programMapper = programMapper;
        this.purposeMapper = purposeMapper;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getPrograms() throws InternalException {
//        return buildResponse(success(programMapper.toDto(programService.findAll())), HttpStatus.OK);
        return buildResponse(success(programService.findAll()), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getProgram(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
//        return buildResponse(success(programMapper.toDto(programService.findById(id))), HttpStatus.OK);
        return buildResponse(success(programService.findById(id)), HttpStatus.OK);
    }

    @GetMapping(value = "purpose", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getProgramPurposes() throws InternalException {
        return buildResponse(success(purposeMapper.toDto(purposeService.findActive())), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveProgram(@RequestBody ProgramDto programDto) throws InternalException {
        Program newProgram = programService.save(programMapper.toEntity(programDto));

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(buildLocationUri(newProgram.getId(), "/{id}"));

        return buildResponse(success(programMapper.toDto(newProgram)), headers, HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> toggleProgram(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        programService.toggle(id);

        return buildResponse(success(), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> updateProgram(@RequestBody Program program,
                                           @PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {

//        Program updatedProgram = programService.update(id, programMapper.toEntity(programDto));
        Program updatedProgram = programService.update(id, program);

        return buildResponse(success(programMapper.toDto(updatedProgram)), HttpStatus.OK);
    }
}
