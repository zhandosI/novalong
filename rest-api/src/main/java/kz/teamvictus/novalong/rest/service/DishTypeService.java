package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.dish.DishType;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;

import java.util.List;

public interface DishTypeService {

    DishType findByName(String name) throws InternalException;
    List<DishType> findAll() throws InternalException;
    List<DishType> findActive() throws InternalException;
    DishType findById(Long id) throws InternalException;

    DishType save(DishType dishType) throws InternalException;
    DishType update(Long dishTypeId, DishType dishType)
            throws InternalException, ResourceNotFoundException;
    void toggle(Long dishTypeId) throws InternalException, ResourceNotFoundException;

}
