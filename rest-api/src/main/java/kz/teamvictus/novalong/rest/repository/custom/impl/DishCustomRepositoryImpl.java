package kz.teamvictus.novalong.rest.repository.custom.impl;

import kz.teamvictus.novalong.rest.model.entity.dish.Dish;
import kz.teamvictus.novalong.rest.model.entity.dish.DishCategory;
import kz.teamvictus.novalong.rest.model.entity.dish.DishType;
import kz.teamvictus.novalong.rest.repository.custom.DishCustomRepository;
import kz.teamvictus.novalong.util.model.criteria.DishSearchCriteria;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Repository
@Slf4j
public class DishCustomRepositoryImpl implements DishCustomRepository {

    /** Stored procedures names **/
    private static final String RANDOM_DISHES_BY_DISH_TYPE = "RANDOM_DISHES_BY_DISH_TYPE";
    private static final String RANDOM_DISHES_BY_DISH_TYPE_CATEGORIES = "RANDOM_DISHES_BY_DISH_TYPE_CATEGORIES";

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Dish> findAllByCriteria(DishSearchCriteria dishCriteria) {
        return this.criteriaQuery(dishCriteria);
    }

    @Override
    public List<Dish> findRandomByDishType(Long dishTypeId, double calsNeeded, int dishesNum) {
        try {
            StoredProcedureQuery query = em.createStoredProcedureQuery(RANDOM_DISHES_BY_DISH_TYPE);

            query.registerStoredProcedureParameter(1, Long.class, ParameterMode.IN);
            query.registerStoredProcedureParameter(2, Integer.class, ParameterMode.IN);
            query.registerStoredProcedureParameter(3, Double.class, ParameterMode.IN);
            query.setParameter(1, dishTypeId);
            query.setParameter(2, dishesNum);
            query.setParameter(3, calsNeeded);
            query.execute();

            return this.dishesFromResultSet(query.getResultList());
        } catch (Exception e) {
            // TODO: Vergiß du nicht, diese Ausnahme (exception) Logger später zu implementieren!
            throw e;
        }
    }

    @Override
    public List<Dish> findRandomByDishTypeAndCategories(Long dishTypeId, double calsNeeded, String categoryCodesParam, String ratiosParam) {
        try {
            StoredProcedureQuery query = em.createStoredProcedureQuery(RANDOM_DISHES_BY_DISH_TYPE_CATEGORIES);

            query.registerStoredProcedureParameter(1, Long.class, ParameterMode.IN);
            query.registerStoredProcedureParameter(2, String.class, ParameterMode.IN);
            query.registerStoredProcedureParameter(3, String.class, ParameterMode.IN);
            query.registerStoredProcedureParameter(4, Double.class, ParameterMode.IN);
            query.setParameter(1, dishTypeId);
            query.setParameter(2, categoryCodesParam);
            query.setParameter(3, ratiosParam);
            query.setParameter(4, calsNeeded);
            query.execute();

            return this.dishesFromResultSet(query.getResultList());
        } catch (Exception e) {
            // TODO: Vergiß du nicht, diese Ausnahme (exception) Logger später zu implementieren!
            throw e;
        }
    }

    @Override
    public Long countAllByCriteria(DishSearchCriteria dishCriteria) {
        return this.criteriaCount(dishCriteria);
    }

    private List<Dish> criteriaQuery(DishSearchCriteria dishCriteria) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Dish> query = builder.createQuery(Dish.class);
        Root<Dish> root = query.from(Dish.class);

        List<Predicate> predicates = new ArrayList<>();

        // SEARCH BY QUERY
        if (dishCriteria.getSearchQuery() != null) {
            final String PERCENT_SIGN = "%";

            Predicate name = builder.like(root.get("name"), PERCENT_SIGN + dishCriteria.getSearchQuery() + PERCENT_SIGN);
            Predicate description = builder.like(root.get("description"), PERCENT_SIGN + dishCriteria.getSearchQuery() + PERCENT_SIGN);
            Predicate recipeDescription = builder.like(root.get("recipeDescription"), PERCENT_SIGN + dishCriteria.getSearchQuery() + PERCENT_SIGN);

            predicates.add(builder.or(name, description, recipeDescription));
        }

        // SEARCH BY DISH TYPES
        if (dishCriteria.getDishTypes() != null && !dishCriteria.getDishTypes().isEmpty()) {
            for (DishType type: dishCriteria.getDishTypes())
                predicates.add(builder.isMember(type, root.get("dishTypes")));
        }

        // SEARCH BY CATEGORY
        if (dishCriteria.getCategories() != null) {
            for(DishCategory category: dishCriteria.getCategories()){
                predicates.add(builder.isMember(category, root.<Set<DishCategory>>get("categories")));
            }
        }

        // ORDER BY
        if (dishCriteria.getSort() != null) {
            if(dishCriteria.getSort().equals("date_added_old"))
                query.orderBy(builder.asc(root.get("dateAdded")));

            else if(dishCriteria.getSort().equals("date_added_new"))
                query.orderBy(builder.desc(root.get("dateAdded")));
        } else {
            query.orderBy(builder.desc(root.get("dateAdded")));
        }

        predicates.add(builder.equal(root.get("active"), true));

        query.select(root).where(predicates.toArray(new Predicate[]{}));

        TypedQuery<Dish> typedQuery = em.createQuery(query);

        if (dishCriteria.getOffset() != null)
            typedQuery.setFirstResult(dishCriteria.getOffset());

        if (dishCriteria.getLimit() != null)
            typedQuery.setMaxResults(dishCriteria.getLimit());

        return typedQuery.getResultList();
    }

    private Long criteriaCount(DishSearchCriteria dishCriteria)  {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<Dish> root = query.from(Dish.class);

        List<Predicate> predicates = new ArrayList<>();

        // SEARCH BY DISH TYPES
        if (dishCriteria.getDishTypes() != null && !dishCriteria.getDishTypes().isEmpty()) {
            for (DishType type: dishCriteria.getDishTypes())
                predicates.add(builder.isMember(type, root.get("dishTypes")));
        }

        // SEARCH BY CATEGORY
        if (dishCriteria.getCategories() != null) {
            for(DishCategory category: dishCriteria.getCategories()){
                predicates.add(builder.isMember(category, root.<Set<DishCategory>>get("categories")));
            }
        }

        predicates.add(builder.equal(root.get("active"), true));

        query.select(builder.count(root)).where(predicates.toArray(new Predicate[]{}));

        return em.createQuery(query).getSingleResult();
    }

    private List<Dish> dishesFromResultSet(List<Object[]> resultSet) {
        List<Dish> dishes = new ArrayList<>();

        for (Object[] obj: resultSet) {
            Dish dish = new Dish();
            dish.setId(((BigInteger) obj[0]).longValue());
            dish.setName((String) obj[1]);
            dish.setWeight((float) obj[2]);
            dish.setProtein((float) obj[3]);
            dish.setFat((float) obj[4]);
            dish.setCarbohydrate((float) obj[5]);

            dishes.add(dish);
        }

        return dishes;
    }
}
