package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.ration.UserDailyRation;
import kz.teamvictus.novalong.security.exceptions.InternalException;

import java.util.List;


public interface UserDailyRationService {

    List<UserDailyRation> findAll(Long userRationId, Long dishTypeId) throws InternalException;
    List<UserDailyRation> findAll(Long userRationId, String dayCode) throws InternalException;
    List<UserDailyRation> findAll(Long userRationId, Long dishTypeId, String dayCode) throws InternalException;

    void save(UserDailyRation dailyRation) throws InternalException;
    void save(Iterable<UserDailyRation> dailyRations) throws InternalException;

    void delete(Iterable<UserDailyRation> dailyRations) throws InternalException;
    void delete(Long userRationId) throws InternalException;

    void deleteAll(Long rationId, String dayCode) throws InternalException;
    void deleteAll(Long rationId, String dayCode, Long dishTypeId) throws InternalException;

    double dailyCalories(Long rationId, String dayCode) throws InternalException;

    boolean exists(Long userId, String dayCode) throws InternalException;

}
