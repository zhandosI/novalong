package kz.teamvictus.novalong.rest.model.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ActivityLevelDto {

    private Long id;
    private String code;
    private String name;
    private String description;
    private double coefficient;
    private boolean active;

}
