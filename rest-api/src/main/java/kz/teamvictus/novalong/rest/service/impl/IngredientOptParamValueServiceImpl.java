package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientOptParamValue;
import kz.teamvictus.novalong.rest.repository.IngredientOptParamValueRepository;
import kz.teamvictus.novalong.rest.service.IngredientOptParamValueService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class IngredientOptParamValueServiceImpl implements IngredientOptParamValueService {

    private IngredientOptParamValueRepository optParamRepository;

    @Autowired
    public void setOptParamRepository(IngredientOptParamValueRepository optParamRepository) {
        this.optParamRepository = optParamRepository;
    }

    @Override
    public List<IngredientOptParamValue> findByIngredient(Long ingredientId) throws InternalException {
        try {
            return optParamRepository.findAllByIngredientId(ingredientId);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void save(IngredientOptParamValue optParam, Long ingredientId) throws InternalException {
        try {
            optParam.setIngredientId(ingredientId);
            optParamRepository.save(optParam);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void save(Iterable<IngredientOptParamValue> optParams, Long ingredientId) throws InternalException {
        try {
            for (IngredientOptParamValue val: optParams)
                val.setIngredientId(ingredientId);

            optParamRepository.save(optParams);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void update(IngredientOptParamValue paramValue, Long paramValueId) throws InternalException, ResourceNotFoundException {
        try {
            IngredientOptParamValue paramValueToUpdate = this.optParamRepository.getOne(paramValueId);

            if (paramValueToUpdate.getId() == null)
                throw new ResourceNotFoundException("There is no param with id: " + paramValueId, ErrorCode.RESOURCE_NOT_FOUND);

            this.update(paramValueToUpdate, paramValue);
            this.optParamRepository.save(paramValueToUpdate);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void update(Set<IngredientOptParamValue> paramValues, Long ingredientId) throws InternalException {
        try {
            Set<String> codes = new HashSet<>(paramValues.size());
            Set<IngredientOptParamValue> toUpdate = new HashSet<>();

            for (IngredientOptParamValue p: paramValues) {
                final String code = p.getParam().getCode();
                codes.add(code);

                IngredientOptParamValue iParam = this.optParamRepository.findByIngredientIdAndParamCode(ingredientId, code);

                if (iParam == null)
                    iParam = new IngredientOptParamValue(ingredientId, p.getParam(), p.getWeight());
                else
                    iParam.setWeight(p.getWeight());

                toUpdate.add(iParam);
            }

            List<IngredientOptParamValue> toDelete = this.optParamRepository.findAllByNotExistingParam(ingredientId, codes);

            this.optParamRepository.delete(toDelete);
            this.optParamRepository.save(toUpdate);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void delete(Long paramValueId) throws InternalException, ResourceNotFoundException {
        try {
            IngredientOptParamValue param = this.optParamRepository.getOne(paramValueId);

            if (param.getId() == null)
                throw new ResourceNotFoundException("There is no param with id: " + paramValueId, ErrorCode.RESOURCE_NOT_FOUND);

            this.optParamRepository.delete(paramValueId);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    private void update(IngredientOptParamValue paramValueToUpdate, IngredientOptParamValue paramValue) {
        paramValueToUpdate.setParam(paramValue.getParam());
        paramValueToUpdate.setWeight(paramValue.getWeight());
    }

}
