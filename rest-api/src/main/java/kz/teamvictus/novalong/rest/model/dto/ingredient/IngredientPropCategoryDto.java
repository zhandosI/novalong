package kz.teamvictus.novalong.rest.model.dto.ingredient;

import kz.teamvictus.novalong.rest.model.dto.UnitDataTypeDto;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class IngredientPropCategoryDto {

    private Long id;
    private IngredientPropDto type;
    private IngredientCategoryDto category;
    private UnitDataTypeDto unitDataType;

}
