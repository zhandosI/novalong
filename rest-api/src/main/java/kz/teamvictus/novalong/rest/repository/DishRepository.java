package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.dish.Dish;
import kz.teamvictus.novalong.rest.model.entity.dish.DishCategory;
import kz.teamvictus.novalong.rest.model.entity.dish.DishType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DishRepository extends JpaRepository<Dish, Long> {

    List<Dish> findAllByOrderByDateAddedDesc();
    List<Dish> findAllByDishTypesContains(DishType dishType);
    List<Dish> findAllByDishTypesContainsAndCategoriesContains(DishType dishType, DishCategory category);

}
