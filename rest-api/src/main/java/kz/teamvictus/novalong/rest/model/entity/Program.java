package kz.teamvictus.novalong.rest.model.entity;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "programs")
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Program extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "purpose_id")
    private Long purposeId;

    @ManyToOne
    @JoinColumn(name = "purpose_id", insertable = false, updatable = false)
    private Purpose purpose;

    @Column(name = "calories_difference_coefficient")
    private double caloriesDifferenceCoefficient;

    @OneToMany(mappedBy = "programId")
    @OrderBy(value = "generationOrder ASC")
    private Set<ProgramDishType> programDishTypes;

    @Column(name = "active")
    private boolean active;

}
