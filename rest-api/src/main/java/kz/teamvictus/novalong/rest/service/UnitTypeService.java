package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.UnitType;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;

import java.util.List;

public interface UnitTypeService {

    UnitType findByCode(String code) throws InternalException;
    UnitType findByCode(String code, boolean isDish) throws InternalException;

    List<UnitType> findAll() throws InternalException;
    List<UnitType> findActive() throws InternalException;
    List<UnitType> findAllIngredient() throws InternalException;
    List<UnitType> findAllDish() throws InternalException;
    UnitType findById(Long id) throws InternalException;

    UnitType save(UnitType unitType) throws InternalException;
    UnitType update(Long unitTypeId, UnitType unitType)
            throws InternalException, ResourceNotFoundException;
    void toggle(Long unitTypeId) throws InternalException, ResourceNotFoundException;
}
