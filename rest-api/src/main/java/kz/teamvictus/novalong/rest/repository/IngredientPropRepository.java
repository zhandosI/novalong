package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientProp;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IngredientPropRepository extends JpaRepository<IngredientProp, Long> {

    List<IngredientProp> findIngredientPropsByActive(Boolean isActive);
}
