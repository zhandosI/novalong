package kz.teamvictus.novalong.rest.controller.endpoint.catalog;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.rest.model.dto.alternative.AlternativeRelationDto;
import kz.teamvictus.novalong.rest.model.entity.alternative.AlternativeRelation;
import kz.teamvictus.novalong.rest.service.AlternativeRelationService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.util.mapper.impl.AlternativeRelationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/alternatives_relations")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class AlternativeRelationRestController extends BaseController {

    private final AlternativeRelationService alternativeRelationService;
    private final AlternativeRelationMapper alternativeRelationMapper;

    @Autowired
    public AlternativeRelationRestController(AlternativeRelationService alternativeRelationService,
                                             AlternativeRelationMapper alternativeRelationMapper) {
        this.alternativeRelationService = alternativeRelationService;
        this.alternativeRelationMapper = alternativeRelationMapper;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAlternativeRelations() throws InternalException {
        return buildResponse(success(alternativeRelationMapper.toDto(alternativeRelationService.findAll())), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAlternativeRelation(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        return buildResponse(success(alternativeRelationMapper.toDto(alternativeRelationService.findById(id))), HttpStatus.OK);
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveAlternativeRelation(@RequestBody AlternativeRelationDto alternativeRelationDto) throws InternalException {
        AlternativeRelation newAlternativeRelation = alternativeRelationService.save(alternativeRelationMapper.toEntity(alternativeRelationDto));

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(buildLocationUri(newAlternativeRelation.getId(), "/{id}"));

        return buildResponse(success(alternativeRelationMapper.toDto(newAlternativeRelation)), headers, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> toggleAlternativeRelation(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        alternativeRelationService.toggle(id);

        return buildResponse(success(), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> updateAlternativeRelation(@RequestBody AlternativeRelationDto alternativeRelationDto,
                                        @PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        AlternativeRelation updatedAlternativeRelation = alternativeRelationService.update(id, alternativeRelationMapper.toEntity(alternativeRelationDto));

        return buildResponse(success(alternativeRelationMapper.toDto(updatedAlternativeRelation)), HttpStatus.OK);
    }

}
