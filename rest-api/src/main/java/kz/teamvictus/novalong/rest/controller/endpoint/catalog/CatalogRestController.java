package kz.teamvictus.novalong.rest.controller.endpoint.catalog;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.rest.model.dto.CatalogDto;
import kz.teamvictus.novalong.rest.model.entity.Catalog;
import kz.teamvictus.novalong.rest.service.CatalogService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.util.mapper.impl.CatalogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/catalogs")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class CatalogRestController extends BaseController {

    private final CatalogService catalogService;
    private final CatalogMapper catalogMapper;

    @Autowired
    public CatalogRestController(CatalogService catalogService,
                                 CatalogMapper catalogMapper) {
        this.catalogService = catalogService;
        this.catalogMapper = catalogMapper;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getCatalogs() throws InternalException {
        return buildResponse(success(catalogMapper.toDto(catalogService.findActive())), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getCatalog(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        return buildResponse(success(catalogMapper.toDto(catalogService.findById(id))), HttpStatus.OK);
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveCatalog(@RequestBody CatalogDto catalogDto) throws InternalException {
        Catalog newCatalog = catalogService.save(catalogMapper.toEntity(catalogDto));

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(buildLocationUri(newCatalog.getId(), "/{id}"));

        return buildResponse(success(catalogMapper.toDto(newCatalog)), headers, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> updateCatalog(@RequestBody CatalogDto catalogDto,
                                        @PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        Catalog updatedCatalog = catalogService.update(id, catalogMapper.toEntity(catalogDto));

        return buildResponse(success(catalogMapper.toDto(updatedCatalog)), HttpStatus.OK);
    }

}
