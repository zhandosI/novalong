package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.HealthFactor;
import kz.teamvictus.novalong.rest.model.entity.dish.Dish;
import kz.teamvictus.novalong.rest.model.entity.dish.DishCategory;
import kz.teamvictus.novalong.rest.model.entity.dish.DishType;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface DishService {

    Dish findById(Long id) throws InternalException, ResourceNotFoundException;

    List<Dish> findAll() throws InternalException;
    List<Dish> findAll(DishType dishType) throws InternalException;
    List<Dish> findAll(DishType dishType, DishCategory category) throws InternalException;
    List<Dish> findAll(Map<String, String> params) throws InternalException, ResourceNotFoundException;

    List<Dish> findRandom(Long dishTypeId, double calsNeeded, int dishesNum) throws InternalException;
    List<Dish> findRandom(Long dishTypeId, double calsNeeded, String[] categoryCodes, Double[] ratios) throws InternalException;

    Dish save(Dish dish) throws InternalException;
    Dish update(Long dishId, Dish dish) throws InternalException, ResourceNotFoundException;
    void delete(Long dishId) throws InternalException, ResourceNotFoundException;

    long count(Map<String, String> params) throws InternalException, ResourceNotFoundException;

    HealthFactor healthFactor(Long dishId) throws InternalException, ResourceNotFoundException;
    List<HealthFactor> healthFactor(Map<String, String> params) throws InternalException;

    Map<Long, String> findAllPhotos(Map<String, String> params) throws InternalException;
    String findPhoto(Long id) throws InternalException;
    void savePhoto(MultipartFile file, Long id) throws InternalException;

    /**
     * Creates Dishes from all ingredients in category with id:
     * @param ingredientCategoryId - category id.
     * @param unitTypeCode - is needed for dish weight evaluation.
     */
    void createFromIngredients(Long ingredientCategoryId, String unitTypeCode) throws InternalException, ResourceNotFoundException;
}
