package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.ProgramDishType;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;

import java.util.List;

public interface ProgramDishTypeService {

    List<ProgramDishType> findAllByProgram(Long programId) throws InternalException;
    List<ProgramDishType> findAll() throws InternalException;
    List<ProgramDishType> findActive() throws InternalException;
    ProgramDishType findById(Long id) throws InternalException;

    ProgramDishType save(ProgramDishType programDishType) throws InternalException;
    ProgramDishType update(Long programDishTypeId, ProgramDishType programDishType)
            throws InternalException, ResourceNotFoundException;
    void toggle(Long programDishTypeId) throws InternalException, ResourceNotFoundException;

}
