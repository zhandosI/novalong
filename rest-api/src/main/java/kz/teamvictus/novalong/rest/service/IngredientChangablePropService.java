package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientChangableProp;
import kz.teamvictus.novalong.security.exceptions.InternalException;

import java.util.List;

public interface IngredientChangablePropService {

    List<IngredientChangableProp> findAll() throws InternalException;

}
