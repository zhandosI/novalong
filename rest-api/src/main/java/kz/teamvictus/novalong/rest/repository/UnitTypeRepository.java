package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.UnitType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UnitTypeRepository extends JpaRepository<UnitType, Long> {

    UnitType findByCode(String code);
    UnitType findByCodeAndIsDish(String code, boolean dish);
    List<UnitType> findAllByIsDish(boolean dish);
    List<UnitType> findUnitTypesByActive(Boolean isActive);

}
