package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.Gender;
import kz.teamvictus.novalong.rest.repository.GenderRepository;
import kz.teamvictus.novalong.rest.service.GenderService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GenderServiceImpl implements GenderService {

    private GenderRepository genderRepository;

    @Autowired
    public void setGenderRepository(GenderRepository genderRepository) {
        this.genderRepository = genderRepository;
    }

    @Override
    public Gender findById(Long genderId) throws InternalException {
        try {
            return genderRepository.findOne(genderId);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<Gender> findAll() throws InternalException {
        try {
            return genderRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

}
