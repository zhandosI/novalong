package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.dish.DishMandatory;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;

import java.util.List;

public interface DishMandatoryService {

    List<DishMandatory> findDishReplaceOptions(Long curDishId, Long dishTypeId) throws InternalException, ResourceNotFoundException;
    List<DishMandatory> findUserDishReplaceOptions(Long userId, Long curDishId, Long dishTypeId) throws InternalException, ResourceNotFoundException;

}
