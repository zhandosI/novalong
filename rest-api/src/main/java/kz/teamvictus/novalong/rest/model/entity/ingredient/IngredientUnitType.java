package kz.teamvictus.novalong.rest.model.entity.ingredient;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import kz.teamvictus.novalong.rest.model.entity.UnitType;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "ingredients_unit_types")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class IngredientUnitType extends BaseEntity {

    @Column(name = "ingredient_id")
    private Long ingredientId;

    @ManyToOne
    @JoinColumn(name = "unit_type_id")
    private UnitType type;

    @Column(name = "weight")
    private float weight;

}


