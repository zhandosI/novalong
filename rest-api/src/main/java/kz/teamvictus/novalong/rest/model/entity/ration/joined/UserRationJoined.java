package kz.teamvictus.novalong.rest.model.entity.ration.joined;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class UserRationJoined {

    private Long userId;
    private Long programId;
    private Date startDate;
    private Date endDate;
    private Float calories;
    private Float caloriesNeeded;
    private Float caloriesDiff;

}
