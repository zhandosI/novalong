package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.WeekDay;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WeekDayRepository extends JpaRepository<WeekDay, Long> {

    WeekDay findByCode(String code);

}
