package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.dish.DishType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DishTypeRepository extends JpaRepository<DishType, Long> {

    List<DishType> findDishTypesByActive(Boolean isActive);
    DishType findByName(String name);

}
