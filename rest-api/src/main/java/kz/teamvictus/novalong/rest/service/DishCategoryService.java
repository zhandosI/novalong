package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.dish.DishCategory;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;

import java.util.List;

public interface DishCategoryService {

    DishCategory findByName(String name) throws InternalException;
    List<DishCategory> findAll() throws InternalException;
    List<DishCategory> findActive() throws InternalException;
    DishCategory findById(Long id) throws InternalException;

    DishCategory save(DishCategory dishCategory) throws InternalException;
    DishCategory update(Long dishCategoryId, DishCategory dishCategory)
            throws InternalException, ResourceNotFoundException;
    void toggle(Long dishCategoryId) throws InternalException, ResourceNotFoundException;
}
