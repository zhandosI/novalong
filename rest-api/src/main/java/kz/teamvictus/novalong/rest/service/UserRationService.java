package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.ration.UserRation;
import kz.teamvictus.novalong.security.exceptions.InternalException;

public interface UserRationService {

    Long findIdByUser(Long userId) throws InternalException;
    UserRation findByUser(Long userId) throws InternalException;
    UserRation save(Long userId, Long programId, int daysNum, Float totalCaloriesAmount) throws InternalException;
    void delete(UserRation ration) throws InternalException;

}
