package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.CookingType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CookingTypeRepository extends JpaRepository<CookingType, Long> {

    List<CookingType> findCookingTypesByActive(Boolean isActive);
}
