package kz.teamvictus.novalong.rest.model.entity.ingredient;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import kz.teamvictus.novalong.rest.model.entity.CookingType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "ingredients_cooking_types")
@Getter
@Setter
@ToString
public class IngredientCookingType extends BaseEntity {

    @Column(name = "ingredient_id")
    private Long ingredientId;

    @ManyToOne
    @JoinColumn(name = "cooking_type_id")
    private CookingType cookingType;

    @ManyToOne
    @JoinColumn(name = "ingredients_changable_prop_id")
    private IngredientChangableProp changableProp;

    @Column(name = "difference_coefficient")
    private double differenceCoefficient;

}
