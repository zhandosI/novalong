package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.dish.DishIngredientAlternative;
import kz.teamvictus.novalong.rest.repository.DishIngredientAlternativeRepository;
import kz.teamvictus.novalong.rest.service.DishIngredientAlternativeService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.UniqueConstraintViolationException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DishIngredientAlternativeServiceImpl implements DishIngredientAlternativeService {

    private DishIngredientAlternativeRepository alternativeRepository;

    @Autowired
    public void setAlternativeRepository(DishIngredientAlternativeRepository alternativeRepository) {
        this.alternativeRepository = alternativeRepository;
    }

    @Override
    public List<DishIngredientAlternative> findAllByDish(Long dishId) throws InternalException {
        try {
            return alternativeRepository.findAllByDishId(dishId);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void save(List<DishIngredientAlternative> alternatives, Long dishId) throws InternalException, UniqueConstraintViolationException {
        try {
            alternatives.forEach(a -> a.setDishId(dishId));
            alternativeRepository.save(alternatives);
        } catch (DataIntegrityViolationException e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                final String errorMsg = "The dish with id: " + dishId + ", " +
                        "has already got this alternative in this category";

                throw new UniqueConstraintViolationException(errorMsg, ErrorCode.UNIQUE_RESOURCE_CONFLICT);
            }
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
