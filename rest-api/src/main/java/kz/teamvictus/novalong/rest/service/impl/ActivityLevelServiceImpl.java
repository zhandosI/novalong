package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.ActivityLevel;
import kz.teamvictus.novalong.rest.repository.ActivityLevelRepository;
import kz.teamvictus.novalong.rest.service.ActivityLevelService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityLevelServiceImpl implements ActivityLevelService {

    private ActivityLevelRepository activityLevelRepository;

    @Autowired
    public void setActivityLevelRepository(ActivityLevelRepository activityLevelRepository) {
        this.activityLevelRepository = activityLevelRepository;
    }

    @Override
    public ActivityLevel findById(Long id) throws InternalException {
        try {
            try {
                ActivityLevel activityLevel = activityLevelRepository.findOne(id);

                if (activityLevel == null)
                    throw new ResourceNotFoundException("There is no activityLevel with id: " + id, ErrorCode.RESOURCE_NOT_FOUND);

                return activityLevel;
            } catch (ResourceNotFoundException e) {
                throw e;
            } catch (Exception e) {
                throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
            }
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<ActivityLevel> findAll() throws InternalException {
        try {
            return activityLevelRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<ActivityLevel> findActive() throws InternalException {
        try {
            return activityLevelRepository.findActivityLevelsByActive(true);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ActivityLevel save(ActivityLevel activityLevel) throws InternalException {
        try {
            if (activityLevel.getCode() == null)
                throw new InternalException("There is no activityLevel code " + activityLevel.getName(), ErrorCode.EMPTY_CODE);

            activityLevel.setActive(true);

            return activityLevelRepository.save(activityLevel);
        } catch (InternalException ie) {
            throw ie;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ActivityLevel update(Long activityLevelId, ActivityLevel activityLevel) throws InternalException, ResourceNotFoundException {
        try {
            ActivityLevel activityLevelToUpdate = activityLevelRepository.getOne(activityLevelId);

            if (activityLevelToUpdate.getId() == null)
                throw new ResourceNotFoundException("There is no activityLevel with id: " + activityLevelId, ErrorCode.RESOURCE_NOT_FOUND);

            activityLevelToUpdate.setCode(activityLevel.getCode());
            activityLevelToUpdate.setName(activityLevel.getName());
            activityLevelToUpdate.setDescription(activityLevel.getDescription());
            activityLevelToUpdate.setCoefficient(activityLevel.getCoefficient());

            activityLevelRepository.save(activityLevelToUpdate);

            return activityLevelRepository.findOne(activityLevelId);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void toggle(Long activityLevelId) throws InternalException, ResourceNotFoundException {
        try {
            ActivityLevel activityLevel = activityLevelRepository.getOne(activityLevelId);

            if (activityLevel.getId() == null)
                throw new ResourceNotFoundException("There is no activityLevel with id: " + activityLevelId, ErrorCode.RESOURCE_NOT_FOUND);

            activityLevel.setActive(!activityLevel.isActive());

            activityLevelRepository.save(activityLevel);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
