package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.dish.Dish;
import kz.teamvictus.novalong.rest.model.entity.ingredient.*;
import kz.teamvictus.novalong.rest.repository.IngredientRepository;
import kz.teamvictus.novalong.rest.repository.custom.IngredientCustomRepository;
import kz.teamvictus.novalong.rest.service.*;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.LackOfDataException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import kz.teamvictus.novalong.util.StringUtil;
import kz.teamvictus.novalong.util.model.criteria.IngredientSearchCriteria;
import kz.teamvictus.novalong.util.model.SearchParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class IngredientServiceImpl implements IngredientService {

    private final IngredientRepository ingredientRepository;
    private final IngredientCustomRepository ingredientCustomRepository;

    private final UnitTypeService unitTypeService;
    private final IngredientUnitTypeService ingredientUnitTypeService;
    private final IngredientCategoryService ingredientCategoryService;
    private final IngredientOptParamValueService optParamValueService;
    private final IngredientOptParamNameService optParamNameService;
    private final DishService dishService;

    @Autowired
    public IngredientServiceImpl(IngredientRepository ingredientRepository,
                                 IngredientCustomRepository ingredientCustomRepository,
                                 UnitTypeService unitTypeService,
                                 IngredientUnitTypeService ingredientUnitTypeService,
                                 IngredientCategoryService ingredientCategoryService,
                                 IngredientOptParamValueService optParamValueService,
                                 IngredientOptParamNameService optParamNameService,
                                 @Lazy DishService dishService) {
        this.ingredientRepository = ingredientRepository;
        this.ingredientCustomRepository = ingredientCustomRepository;
        this.unitTypeService = unitTypeService;
        this.ingredientUnitTypeService = ingredientUnitTypeService;
        this.ingredientCategoryService = ingredientCategoryService;
        this.optParamValueService = optParamValueService;
        this.optParamNameService = optParamNameService;
        this.dishService = dishService;
    }

    @Override
    public Ingredient findById(Long id) throws InternalException, ResourceNotFoundException {
        try {
            Ingredient ingredient = ingredientRepository.findOne(id);

            if (ingredient == null)
                throw new ResourceNotFoundException("There is no ingredient with id: " + id, ErrorCode.RESOURCE_NOT_FOUND);

            return ingredient;
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public Ingredient findByCode(String code) throws InternalException, ResourceNotFoundException {
        try {
            Ingredient ingredient = ingredientRepository.findByCode(code);

            if (ingredient == null)
                throw new ResourceNotFoundException("There is no ingredient with code: " + code, ErrorCode.RESOURCE_NOT_FOUND);

            return ingredient;
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<Ingredient> findAll() throws InternalException {
        try {
            return ingredientRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<Ingredient> findAll(Map<String, String> params) throws InternalException {
        try {
            if (!params.isEmpty()) {
                List<Ingredient> ingredients = ingredientCustomRepository.findAllByCriteria(this.parseParams(params));

                if (ingredients == null || ingredients.isEmpty())
                    return Collections.emptyList();

                return ingredients;
            }

            return ingredientRepository.findAll();
        } catch (Exception e) {
            e.printStackTrace();
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<Ingredient> findAll(IngredientCategory category) throws InternalException {
        try {
            return ingredientRepository.findAllByCategory(category);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<Ingredient> findAll(Long ingredientCategoryId, String unitTypeCode) throws InternalException {
        try {
            return ingredientRepository.findAllByCategoryAndUnitType(ingredientCategoryId, unitTypeCode);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public Ingredient save(Ingredient ingredient) throws InternalException {
        try {
            ingredient.setActive(true);
            ingredient.setKilocalories(this.kilocaloriesAmount(ingredient));

            Ingredient newIngredient = ingredientRepository.save(ingredient);

            /* Ingredient unit types save */
            IngredientUnitType gramsUnitType = new IngredientUnitType(
                    newIngredient.getId(),
                    unitTypeService.findByCode("GRAMS"),
                    1F
            );

            if (ingredient.getUnitTypes() == null)
                ingredient.setUnitTypes(new HashSet<>(Collections.singleton(gramsUnitType)));
            else
                ingredient.getUnitTypes().add(gramsUnitType);

            ingredientUnitTypeService.save(ingredient.getUnitTypes(), newIngredient.getId());

            /* Ingredient optional params save */
            if (ingredient.getOptionalParams() != null)
                optParamValueService.save(ingredient.getOptionalParams(), newIngredient.getId());

            return newIngredient;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public Ingredient save(Ingredient ingredient, Boolean isDish) throws InternalException, LackOfDataException {
        try {
            if (isDish != null && isDish) {
                if (ingredient.getUnitTypes() == null || ingredient.getUnitTypes().isEmpty())
                    throw new LackOfDataException("Unit types are empty. At least one unit type required to calculate the dish weight", ErrorCode.REQUIRED_PARAMS_NOT_FOUND);

                // TODO: PIECES unit type hardcoded. Überdenken Sie den Ansatz später
                final String unitTypeCode = "PIECES";
                IngredientUnitType unitType = null;

                for (IngredientUnitType ut: ingredient.getUnitTypes()) {
                    if (ut.getType() == null)
                        throw new LackOfDataException("There is no type property of unit type specified", ErrorCode.REQUIRED_PARAMS_NOT_FOUND);

                    if (ut.getType().getCode() == null)
                        throw new LackOfDataException("There is no code property of unit type specified", ErrorCode.REQUIRED_PARAMS_NOT_FOUND);

                    if (ut.getType().getCode().equals(unitTypeCode))
                        unitType = ut;
                }

                if (unitType == null)
                    throw new LackOfDataException("There is no " + unitTypeCode + " unit type to calculate the dish weight", ErrorCode.REQUIRED_PARAMS_NOT_FOUND);

                float dishWeight = unitType.getWeight();

                Dish dish = new Dish();
                dish.setName(ingredient.getName());
                dish.setProtein(ingredient.getProtein() * (dishWeight / 100));  // Proteins in dishWeight grams
                dish.setCarbohydrate(ingredient.getCarbohydrate() * (dishWeight / 100));
                dish.setFat(ingredient.getFat() * (dishWeight / 100));
                dish.setWeight(dishWeight);

                dishService.save(dish);
            }

            return this.save(ingredient);
        } catch (LackOfDataException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void update(List<Ingredient> ingredients) throws InternalException {
        try {
            if (ingredients != null && !ingredients.isEmpty())
                ingredientRepository.save(ingredients);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public Ingredient update(Long ingredientId, Ingredient ingredient) throws InternalException, ResourceNotFoundException {
        try {
            Ingredient ingredientToUpdate = ingredientRepository.getOne(ingredientId);

            if (ingredientToUpdate.getId() == null)
                throw new ResourceNotFoundException("There is no ingredient with id: " + ingredientId, ErrorCode.RESOURCE_NOT_FOUND);

            this.update(ingredientToUpdate, ingredient);
            ingredientToUpdate.setUnitTypes(null);

            return ingredientRepository.save(ingredientToUpdate);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void delete(Long ingredientId) throws InternalException, ResourceNotFoundException {
        try {
            Ingredient ingredient = ingredientRepository.getOne(ingredientId);

            if (ingredient.getId() == null)
                throw new ResourceNotFoundException("There is no ingredient with id: " + ingredientId, ErrorCode.RESOURCE_NOT_FOUND);

            ingredient.setActive(false);
            ingredientRepository.save(ingredient);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public long count(Map<String, String> params) throws InternalException {
        try {
            return ingredientCustomRepository.countAllByCriteria(this.parseParams(params));
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public float kilocaloriesAmount(Ingredient ingredient) throws InternalException {
        try {
            if (ingredient.getKilocalories() == 0)
                return ingredient.getProtein() * 4 + ingredient.getCarbohydrate() * 4 + ingredient.getFat() * 9;

            return ingredient.getKilocalories();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void loadFromCsv(List parsedCsv) throws InternalException {
        try {
            for (Object obj : parsedCsv) {
                Map entry = (Map) obj;

                // Skip the empty names
                if (entry.get("NAME") == null || entry.get("NAME").equals(""))
                    continue;

                System.out.println("Ingredient " + entry.get("NAME") + " with ID " + entry.get("ID"));

                Ingredient ingredient = this.parseParamsToIngredient(entry);
                ingredient.setActive(true);


                idIf: if (entry.get("ID") == null || entry.get("ID").equals("")) {
                    if (this.ingredientRepository.existsByNameAndActive(ingredient.getName(), true)) {
                        ingredient.setId(this.ingredientRepository.findIdByNameAndActive(ingredient.getName(), true));
                        break idIf;
                    }

                    ingredient = this.ingredientRepository.save(ingredient);
                }
                else
                    ingredient.setId(Long.parseLong((String) entry.get("ID")));

                ingredient.setUnitTypes(this.parseParamsToUnitTypes(entry, ingredient.getId()));
                ingredient.setOptionalParams(this.parseParamsToOptParams(entry, ingredient.getId()));

                this.update(ingredientRepository.getOne(ingredient.getId()), ingredient);
            }
        } catch (Exception e) {
                throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    private void update(Ingredient ingredientToUpdate, Ingredient ingredient) throws InternalException, ResourceNotFoundException {
        ingredientToUpdate.setName(ingredient.getName());
        ingredientToUpdate.setAllergic(ingredient.isAllergic());
        ingredientToUpdate.setProtein(ingredient.getProtein());
        ingredientToUpdate.setFat(ingredient.getFat());
        ingredientToUpdate.setCarbohydrate(ingredient.getCarbohydrate());
        ingredientToUpdate.setGlycemicIndex(ingredient.getGlycemicIndex());
        ingredientToUpdate.setCategory(ingredientCategoryService.findById(ingredient.getCategory().getId()));
        ingredientToUpdate.setKilocalories(this.kilocaloriesAmount(ingredient));

        // Unit types update
        if (ingredient.getUnitTypes() != null && !ingredient.getUnitTypes().isEmpty())
            this.ingredientUnitTypeService.update(ingredient.getUnitTypes(), ingredientToUpdate.getId());

        // Optional params update
        if (ingredient.getOptionalParams() != null && !ingredient.getOptionalParams().isEmpty())
            this.optParamValueService.update(ingredient.getOptionalParams(), ingredientToUpdate.getId());
    }

    private IngredientSearchCriteria parseParams(Map<String,String> params) {
        IngredientSearchCriteria criteria = new IngredientSearchCriteria();

        for (Map.Entry<String, String> entry: params.entrySet()) {
            switch (entry.getKey()) {
                case SearchParams.QUERY:
                    criteria.setSearchQuery(entry.getValue());
                    break;

                case SearchParams.CATEGORY:
                    criteria.setCategory(Integer.parseInt(entry.getValue()));
                    break;

                case SearchParams.NAME:
                    criteria.setName(entry.getValue());
                    break;

                case SearchParams.LIMIT:
                    criteria.setLimit(Integer.parseInt(entry.getValue()));
                    break;

                case SearchParams.OFFSET:
                    criteria.setOffset(Integer.parseInt(entry.getValue()));
                    break;

                case SearchParams.SORT:
                    criteria.setSort(String.valueOf(entry.getValue()));
                    break;

                case SearchParams.COUNT:
                    criteria.setCount(true);
                    break;
            }
        }

        return criteria;
    }

    private Ingredient parseParamsToIngredient(Map<String,String> params) throws ResourceNotFoundException, InternalException {
        Ingredient ingredient = new Ingredient();

        for (Map.Entry<String, String> entry: params.entrySet()) {
            switch (entry.getKey()) {
                case "CATEGORY_ID":
                    ingredient.setCategory(ingredientCategoryService.findById(Long.parseLong(entry.getValue())));
                    break;

                case "NAME":
                    ingredient.setName(entry.getValue());
                    break;

                case "PROTEIN":
                    if (entry.getValue() == null || entry.getValue().equals(""))
                        ingredient.setProtein(0);
                    else
                        ingredient.setProtein(Float.parseFloat(entry.getValue()));
                    break;

                case "FAT":
                    if (entry.getValue() == null || entry.getValue().equals(""))
                        ingredient.setFat(0);
                    else
                        ingredient.setFat(Float.parseFloat(entry.getValue()));
                    break;

                case "CARBOHYDRATE":
                    if (entry.getValue() == null || entry.getValue().equals(""))
                        ingredient.setCarbohydrate(0);
                    else
                        ingredient.setCarbohydrate(Float.parseFloat(entry.getValue()));
                    break;

                case "IS_ALLERGIC":
                    ingredient.setAllergic(entry.getValue().equalsIgnoreCase("Аллерген"));
                    break;
            }
        }

        return ingredient;
    }

    private Set<IngredientUnitType> parseParamsToUnitTypes(Map<String, String> params, Long ingredientId) throws InternalException {
        Set<IngredientUnitType> unitTypes = new HashSet<>();

        for (Map.Entry<String, String> entry: params.entrySet())
            if (entry.getKey().startsWith("UNIT_TYPE_") && !(entry.getValue() == null || entry.getValue().equals("") || !StringUtil.isDouble(entry.getValue()))) {
                String unitCode = entry.getKey().split("UNIT_TYPE_")[1];

                unitTypes.add(
                        new IngredientUnitType(ingredientId,
                        unitTypeService.findByCode(unitCode),
                        Float.parseFloat(entry.getValue()))
                );
            }
        unitTypes.add(new IngredientUnitType(ingredientId, unitTypeService.findByCode("GRAMS"),1F));

        return unitTypes;
    }

    private Set<IngredientOptParamValue> parseParamsToOptParams(Map<String, String> params, Long ingredientId) throws InternalException {
        Set<IngredientOptParamValue> vals = new HashSet<>();

        for (Map.Entry<String, String> entry: params.entrySet())
            if (entry.getKey().startsWith("OPT_PARAM_") && !(entry.getValue() == null || entry.getValue().equals("") || !StringUtil.isDouble(entry.getValue()))) {
                String paramCode = entry.getKey().split("OPT_PARAM_")[1];

                vals.add(
                        new IngredientOptParamValue(ingredientId,
                        this.optParamNameService.findByCode(paramCode),
                        Double.parseDouble(entry.getValue()))
                );
            }

        return vals;
    }
}
