package kz.teamvictus.novalong.rest.model.dto.ingredient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import kz.teamvictus.novalong.rest.model.dto.alternative.AlternativeCategoryDto;
import kz.teamvictus.novalong.rest.model.dto.alternative.AlternativeRelationDto;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class IngredientAlternativeDto {

    @JsonIgnoreProperties("alternatives")
    @JsonProperty("ingredient")
    private IngredientDto alternativeIngredient;
    private AlternativeCategoryDto category;
    private AlternativeRelationDto relation;

}
