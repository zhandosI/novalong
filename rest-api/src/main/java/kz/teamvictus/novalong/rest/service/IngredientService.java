package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.ingredient.Ingredient;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientCategory;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.LackOfDataException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;

import java.util.List;
import java.util.Map;

public interface IngredientService {

    Ingredient findById(Long id) throws InternalException, ResourceNotFoundException;
    Ingredient findByCode(String code) throws InternalException, ResourceNotFoundException;

    List<Ingredient> findAll() throws InternalException;
    List<Ingredient> findAll(Map<String, String> params) throws InternalException;
    List<Ingredient> findAll(IngredientCategory category) throws InternalException;
    List<Ingredient> findAll(Long ingredientCategoryId, String unitTypeCode) throws InternalException;

    Ingredient save(Ingredient ingredient) throws InternalException;
    Ingredient save(Ingredient ingredient, Boolean isDish) throws InternalException, LackOfDataException;

    Ingredient update(Long ingredientId, Ingredient ingredient) throws InternalException, ResourceNotFoundException;
    void update(List<Ingredient> ingredients) throws InternalException;

    void delete(Long ingredientId) throws InternalException, ResourceNotFoundException;

    long count(Map<String, String> params) throws InternalException, ResourceNotFoundException;
    float kilocaloriesAmount(Ingredient ingredient) throws InternalException;

    void loadFromCsv(List parsedCsv) throws InternalException;

}
