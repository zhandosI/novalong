package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientPropMap;
import kz.teamvictus.novalong.security.exceptions.InternalException;

import java.util.List;

public interface IngredientPropMapService {

    List<IngredientPropMap> findAll() throws InternalException;
    List<IngredientPropMap> findAllByIngredient(Long ingredientId) throws InternalException;
    void save(List<IngredientPropMap> properties, Long ingredientId) throws InternalException;

}
