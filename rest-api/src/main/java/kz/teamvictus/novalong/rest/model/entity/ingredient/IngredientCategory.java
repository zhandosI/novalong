package kz.teamvictus.novalong.rest.model.entity.ingredient;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "ingredients_categories")
@SecondaryTable(name = "ingredients_categories_codes", pkJoinColumns = @PrimaryKeyJoinColumn(name = "category_id"))
@Getter
@Setter
@ToString(exclude = {"ingredientProperties", "parentCategory"})
public class IngredientCategory extends BaseEntity {

    @Column(name = "code", table = "ingredients_categories_codes")
    private String code;

    @Column(name = "name")
    private String name;

    @Column(name = "parent_category_id")
    private Long parentCategoryId;

    @ManyToOne
    @JoinColumn(name = "parent_category_id", insertable = false, updatable = false)
    private IngredientCategory parentCategory;

    @Column(name = "active")
    private boolean active;

    @Column(name = "is_allergic")
    private boolean allergic;

    @OneToMany
    @JoinColumn(name = "parent_category_id")
    private Set<IngredientCategory> childCategories;

    @OneToMany(mappedBy = "category")
    private Set<IngredientPropCategory> ingredientProperties;

    @Transient
    private transient boolean hasChildCategories;

    @PostLoad
    private void setHasChildCategories() {
        this.hasChildCategories = !this.childCategories.isEmpty();
    }

}
