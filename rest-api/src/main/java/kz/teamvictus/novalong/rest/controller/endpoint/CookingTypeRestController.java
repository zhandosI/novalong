package kz.teamvictus.novalong.rest.controller.endpoint;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.rest.model.dto.CookingTypeDto;
import kz.teamvictus.novalong.rest.model.entity.CookingType;
import kz.teamvictus.novalong.rest.service.ColorCodeService;
import kz.teamvictus.novalong.rest.service.CookingTypeService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.util.mapper.impl.ColorCodeMapper;
import kz.teamvictus.novalong.util.mapper.impl.CookingTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/cookingtypes")
public class CookingTypeRestController extends BaseController {

    private final CookingTypeMapper cookingTypeMapper;
    private final ColorCodeMapper colorCodeMapper;
    private final CookingTypeService cookingTypeService;
    private final ColorCodeService colorCodeService;

    @Autowired
    public CookingTypeRestController(CookingTypeMapper cookingTypeMapper,
                                     ColorCodeMapper colorCodeMapper,
                                     CookingTypeService cookingTypeService,
                                     ColorCodeService colorCodeService) {
        this.cookingTypeMapper = cookingTypeMapper;
        this.colorCodeMapper = colorCodeMapper;
        this.cookingTypeService = cookingTypeService;
        this.colorCodeService = colorCodeService;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAllCookingTypes() throws InternalException {
        return buildResponse(success(cookingTypeService.findAll()), HttpStatus.OK);
//        return buildResponse(success(cookingTypeMapper.toDto(cookingTypeService.findAll())), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getCookingType(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
//        return buildResponse(success(cookingTypeMapper.toDto(cookingTypeService.findById(id))), HttpStatus.OK);
        return buildResponse(success(cookingTypeService.findById(id)), HttpStatus.OK);
    }

    @GetMapping(value = "colorCode", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getUnitDataTypes() throws InternalException {
        return buildResponse(success(colorCodeMapper.toDto(colorCodeService.findAll())), HttpStatus.OK);
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveCookingType(@RequestBody CookingType cookingTypeDtoDto) throws InternalException {
//        CookingType newCookingType = cookingTypeService.save(cookingTypeMapper.toEntity(cookingTypeDtoDto));
        CookingType newCookingType = cookingTypeService.save(cookingTypeDtoDto);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(buildLocationUri(newCookingType.getId(), "/{id}"));

        return buildResponse(success(cookingTypeMapper.toDto(newCookingType)), headers, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> toggleCookingType(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        cookingTypeService.toggle(id);

        return buildResponse(success(), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> updateCookingType(@RequestBody CookingType cookingTypeDto,
                                            @PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
//        CookingType updatedCookingType = cookingTypeService.update(id, cookingTypeMapper.toEntity(cookingTypeDtoDto));
        CookingType updatedCookingType = cookingTypeService.update(id, cookingTypeDto);

        return buildResponse(success(cookingTypeMapper.toDto(updatedCookingType)), HttpStatus.OK);
    }
}
