package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientUnitType;
import kz.teamvictus.novalong.rest.repository.IngredientUnitTypeRepository;
import kz.teamvictus.novalong.rest.service.IngredientUnitTypeService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class IngredientUnitTypeServiceImpl implements IngredientUnitTypeService {

    private IngredientUnitTypeRepository unitTypeRepository;

    @Autowired
    public void setUnitTypeRepository(IngredientUnitTypeRepository unitTypeRepository) {
        this.unitTypeRepository = unitTypeRepository;
    }

    @Override
    public void save(Set<IngredientUnitType> unitTypes, Long ingredientId) throws InternalException {
        try {
            unitTypes.forEach(u -> u.setIngredientId(ingredientId));
            unitTypeRepository.save(unitTypes);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void update(Set<IngredientUnitType> unitTypes, Long ingredientId) throws InternalException {
        try {
            Set<String> codes = new HashSet<>(unitTypes.size());
            Set<IngredientUnitType> toUpdate = new HashSet<>();

            for (IngredientUnitType u: unitTypes) {
                final String uCode = u.getType().getCode();
                codes.add(uCode);

                IngredientUnitType iUnitType = this.unitTypeRepository.findByIngredientIdAndTypeCode(ingredientId, uCode);

                if (iUnitType == null)
                    iUnitType = new IngredientUnitType(ingredientId, u.getType(), u.getWeight());
                else
                    iUnitType.setWeight(u.getWeight());

                toUpdate.add(iUnitType);
            }

            List<IngredientUnitType> toDelete = this.unitTypeRepository.findAllByNotExistingType(ingredientId, codes);

            this.unitTypeRepository.delete(toDelete);
            this.unitTypeRepository.save(toUpdate);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void delete(Set<IngredientUnitType> unitTypes) throws InternalException {
        try {
            unitTypeRepository.delete(unitTypes);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
