package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.WeekDay;
import kz.teamvictus.novalong.rest.repository.WeekDayRepository;
import kz.teamvictus.novalong.rest.service.WeekDayService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WeekDayServiceImpl implements WeekDayService {

    private WeekDayRepository weekDayRepository;

    @Autowired
    public void setWeekDayRepository(WeekDayRepository weekDayRepository) {
        this.weekDayRepository = weekDayRepository;
    }

    @Override
    public WeekDay findByCode(String code) throws InternalException {
        try {
            return weekDayRepository.findByCode(code);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<WeekDay> findAll() throws InternalException {
        try {
            return weekDayRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

}
