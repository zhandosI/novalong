package kz.teamvictus.novalong.rest.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.Set;

@Getter
@Setter
@ToString
public class UserDto {

    private Long id;
    private String username;
    private String email;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String socialMediaId;

    private GenderDto gender;
    private int age;
    private Date birthDate;
    private double height;
    private double weight;
    private PurposeDto purpose;
    private Set<DietaryRestrictionDto> restrictions;
    private ActivityLevelDto activityLevel;
    private RoleDto role;
    private PhaseDto currentPhase;

}
