package kz.teamvictus.novalong.rest.model.entity.ingredient;

import kz.teamvictus.novalong.rest.model.entity.CookingType;
import lombok.*;

import java.util.Set;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class IngredientCookingTypeJoined {

    private CookingType cookingType;
    private Set<IngredientChangablePropDifferenceJoined> props;

}
