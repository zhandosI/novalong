package kz.teamvictus.novalong.rest.model.entity.ingredient;

import kz.teamvictus.novalong.rest.model.entity.alternative.AlternativeCategory;
import kz.teamvictus.novalong.rest.model.entity.alternative.AlternativeRelation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class IngredientAlternativesJoined {

    private AlternativeCategory category;
    private AlternativeRelation relation;
    private Set<Ingredient> ingredients;

}
