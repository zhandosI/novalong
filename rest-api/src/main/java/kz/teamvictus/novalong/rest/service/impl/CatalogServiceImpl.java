package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.Catalog;
import kz.teamvictus.novalong.rest.repository.CatalogRepository;
import kz.teamvictus.novalong.rest.service.CatalogService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CatalogServiceImpl implements CatalogService {

    private CatalogRepository catalogRepository;

    @Autowired
    public void setCatalogRepository(CatalogRepository catalogRepository) {
        this.catalogRepository = catalogRepository;
    }

    @Override
    public Catalog findById(Long id) throws InternalException {
        try {
            try {
                Catalog catalog = catalogRepository.findOne(id);

                if (catalog == null)
                    throw new ResourceNotFoundException("There is no catalog with id: " + id, ErrorCode.RESOURCE_NOT_FOUND);

                return catalog;
            } catch (ResourceNotFoundException e) {
                throw e;
            } catch (Exception e) {
                throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
            }
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<Catalog> findAll() throws InternalException {
        try {
            return catalogRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<Catalog> findActive() throws InternalException {
        try {
            return catalogRepository.findCatalogsByActive(true);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public Catalog save(Catalog catalog) throws InternalException {
        try {
            if (catalog.getName() == null)
                throw new InternalException("There is no catalog name " + catalog.getName(), ErrorCode.EMPTY_CODE);

            catalog.setActive(true);

            return catalogRepository.save(catalog);
        } catch (InternalException ie) {
            throw ie;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public Catalog update(Long catalogId, Catalog catalog) throws InternalException, ResourceNotFoundException {
        try {
            Catalog catalogToUpdate = catalogRepository.getOne(catalogId);

            if (catalogToUpdate.getId() == null)
                throw new ResourceNotFoundException("There is no catalog with id: " + catalogId, ErrorCode.RESOURCE_NOT_FOUND);

            catalogToUpdate.setDescription(catalog.getDescription());
//            catalogToUpdate.setJsonTemplate(catalog.getJsonTemplate());

            catalogRepository.save(catalogToUpdate);

            return catalogRepository.findOne(catalogId);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
