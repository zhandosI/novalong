package kz.teamvictus.novalong.rest.model.entity.ingredient;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "ingredients_opt_param_values")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class IngredientOptParamValue extends BaseEntity {

    @Column(name = "ingredient_id")
    private Long ingredientId;

    @ManyToOne
    @JoinColumn(name = "opt_param_name_id")
    private IngredientOptParamName param;

    @Column(name = "weight")
    private double weight;

}
