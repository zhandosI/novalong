package kz.teamvictus.novalong.rest.controller.endpoint.catalog;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.rest.model.dto.PurposeDto;
import kz.teamvictus.novalong.rest.model.entity.Purpose;
import kz.teamvictus.novalong.rest.service.PurposeService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.util.mapper.impl.PurposeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/purposes")
public class PurposeRestController extends BaseController {

    private final PurposeService purposeService;
    private final PurposeMapper purposeMapper;

    @Autowired
    public PurposeRestController(PurposeService purposeService,
                                 PurposeMapper purposeMapper) {
        this.purposeService = purposeService;
        this.purposeMapper = purposeMapper;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getPurposes() throws InternalException {
        return buildResponse(success(purposeMapper.toDto(purposeService.findAll())), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getPurpose(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        return buildResponse(success(purposeMapper.toDto(purposeService.findById(id))), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> savePurpose(@RequestBody PurposeDto purposeDto) throws InternalException {
        Purpose newPurpose = purposeService.save(purposeMapper.toEntity(purposeDto));

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(buildLocationUri(newPurpose.getId(), "/{id}"));

        return buildResponse(success(purposeMapper.toDto(newPurpose)), headers, HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> togglePurpose(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        purposeService.toggle(id);

        return buildResponse(success(), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> updatePurpose(@RequestBody PurposeDto purposeDto,
                                        @PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        Purpose updatedPurpose = purposeService.update(id, purposeMapper.toEntity(purposeDto));

        return buildResponse(success(purposeMapper.toDto(updatedPurpose)), HttpStatus.OK);
    }

}
