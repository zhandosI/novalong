package kz.teamvictus.novalong.rest.model.entity.dish.joined;


import kz.teamvictus.novalong.rest.model.entity.UnitType;
import kz.teamvictus.novalong.rest.model.entity.dish.BaseDish;
import kz.teamvictus.novalong.rest.model.entity.ration.joined.RationContainer;
import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DishUnitTypeJoined extends RationContainer {

    private Float calories;
    private Float proteins;
    private Float carbohydrates;
    private Float fats;
    private BaseDish dish;
    private UnitType unitType;
    private Double unitAmount;

}
