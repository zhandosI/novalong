package kz.teamvictus.novalong.rest.model.entity.ingredient;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "ingredients_props")
@Getter
@Setter
@ToString
public class IngredientProp extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "active")
    private boolean active;
}
