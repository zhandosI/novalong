package kz.teamvictus.novalong.rest.model.entity.ingredient;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class IngredientChangablePropDifferenceJoined {

    private IngredientChangableProp changableProp;
    private double differenceCoefficient;

}
