package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.ration.UserRation;
import kz.teamvictus.novalong.rest.repository.UserRationRepository;
import kz.teamvictus.novalong.rest.service.UserRationService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;

@Service
public class UserRationServiceImpl implements UserRationService {

    private UserRationRepository userRationRepository;

    @Autowired
    public void setUserRationRepository(UserRationRepository userRationRepository) {
        this.userRationRepository = userRationRepository;
    }

    @Override
    public Long findIdByUser(Long userId) throws InternalException {
        try {
            return userRationRepository.findIdByUserId(userId);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public UserRation findByUser(Long userId) throws InternalException {
        try {
            return userRationRepository.findByUserId(userId);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public UserRation save(Long userId, Long programId, int daysNum, Float totalCaloriesAmount) throws InternalException {
        try {
            UserRation ration = new UserRation();
            ration.setUserId(userId);
            ration.setProgramId(programId);
            ration.setTotalCaloriesNeeded(totalCaloriesAmount);

            Date today = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(today);
            calendar.add(Calendar.DAY_OF_YEAR, daysNum);

            ration.setStartDate(today);
            ration.setEndDate(calendar.getTime());
            ration.setActive(true);

            return userRationRepository.save(ration);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void delete(UserRation ration) throws InternalException {
        try {
            ration.setActive(false);
            this.userRationRepository.save(ration);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
