package kz.teamvictus.novalong.rest.model.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PhaseDto {

    private Long id;
    private String name;
    private Integer priority;
    private float proteinsPercentage;
    private float carbsPercentage;
    private float fatsPercentage;
    private boolean active;

}
