package kz.teamvictus.novalong.rest.model.dto.ingredient;

import kz.teamvictus.novalong.rest.model.dto.CookingTypeDto;
import lombok.*;

import java.util.Set;

@Getter
@Setter
@ToString
public class IngredientCookingTypeJoinedDto {

    private CookingTypeDto cookingType;
    private Set<IngredientChangablePropDifferenceJoinedDto> props;

}
