package kz.teamvictus.novalong.rest.model.entity.dish.joined;

import kz.teamvictus.novalong.rest.model.entity.dish.DishType;
import kz.teamvictus.novalong.rest.model.entity.ration.joined.RationContainer;
import lombok.*;

import java.util.Set;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DishTypeDishesJoined extends RationContainer {

    private Float calories;
    private DishType dishType;
    private Set<DishUnitTypeJoined> meals;

}
