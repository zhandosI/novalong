package kz.teamvictus.novalong.rest.controller.endpoint;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.rest.model.dto.dish.DishDto;
import kz.teamvictus.novalong.rest.model.dto.dish.DishIngredientAlternativeDto;
import kz.teamvictus.novalong.rest.model.entity.dish.Dish;
import kz.teamvictus.novalong.rest.service.*;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.exceptions.UniqueConstraintViolationException;
import kz.teamvictus.novalong.util.mapper.impl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/dishes")
public class DishRestController extends BaseController {

    private final DishMapper dishMapper;
    private final DishMandatoryMapper dishMandatoryMapper;
    private final DishTypeMapper dishTypeMapper;
    private final DishIngredientAlternativeMapper alternativeMapper;
    private final DishCategoryMapper categoryMapper;
    private final UnitTypeMapper unitTypeMapper;

    private final DishService dishService;
    private final DishMandatoryService dishMandatoryService;
    private final DishIngredientAlternativeService alternativeService;
    private final DishTypeService dishTypeService;
    private final DishCategoryService categoryService;
    private final UnitTypeService unitTypeService;

    @Autowired
    public DishRestController(DishMapper dishMapper,
                              DishMandatoryMapper dishMandatoryMapper,
                              DishTypeMapper dishTypeMapper,
                              DishIngredientAlternativeMapper alternativeMapper,
                              DishCategoryMapper categoryMapper,
                              UnitTypeMapper unitTypeMapper,
                              DishTypeService dishTypeService,
                              DishService dishService,
                              DishMandatoryService dishMandatoryService,
                              DishIngredientAlternativeService alternativeService,
                              DishCategoryService categoryService,
                              UnitTypeService unitTypeService) {
        this.dishMapper = dishMapper;
        this.dishMandatoryMapper = dishMandatoryMapper;
        this.dishTypeMapper = dishTypeMapper;
        this.alternativeMapper = alternativeMapper;
        this.categoryMapper = categoryMapper;
        this.unitTypeMapper = unitTypeMapper;
        this.dishService = dishService;
        this.dishMandatoryService = dishMandatoryService;
        this.alternativeService = alternativeService;
        this.dishTypeService = dishTypeService;
        this.categoryService = categoryService;
        this.unitTypeService = unitTypeService;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAllDishes(@RequestParam Map<String, String> params,
                                          @RequestParam(name = "count", required = false) String count,
                                          @RequestParam(name = "health_factor", required = false) String healthFactor) throws InternalException, ResourceNotFoundException {
        if (count != null)
            return buildResponse(success(dishService.count(params)), HttpStatus.OK);

        if (healthFactor != null)
            return buildResponse(success(dishService.healthFactor(params)), HttpStatus.OK);

        return buildResponse(success(dishMapper.toDto(dishService.findAll(params)), dishService.count(params), params), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getDish(@PathVariable("id") Long id,
                                     @RequestParam(name = "health_factor", required = false) String healthFactor) throws InternalException, ResourceNotFoundException {
        if (healthFactor != null)
            return buildResponse(success(dishService.healthFactor(id)), HttpStatus.OK);

        return buildResponse(success(dishMapper.toDto(dishService.findById(id))), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveDish(@RequestBody DishDto dish,
                                      @RequestParam(name = "ingredient_category_id", required = false) Long ingredientCategoryId,
                                      @RequestParam(name = "unit_type_code", required = false) String unitTypeCode) throws InternalException, ResourceNotFoundException {
        if (ingredientCategoryId != null && unitTypeCode != null) {
            dishService.createFromIngredients(ingredientCategoryId, unitTypeCode);
            return buildResponse(success(), HttpStatus.CREATED);
        }

        Dish newDish = dishService.save(dishMapper.toEntity(dish));

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(buildLocationUri(newDish.getId(), "/{id}"));

        return buildResponse(success(dishMapper.toDto(newDish)), headers, HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> removeDish(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        dishService.delete(id);
        return buildResponse(success(), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> updateDish(@RequestBody DishDto dish,
                                        @PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        Dish updatedDish = dishService.update(id, dishMapper.toEntity(dish));
        return buildResponse(success(dishMapper.toDto(updatedDish)), HttpStatus.OK);
    }

    @GetMapping(value = "/types", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAllDishesTypes() throws InternalException {
        return buildResponse(success(dishTypeMapper.toDto(dishTypeService.findAll())), HttpStatus.OK);
    }

    @GetMapping(value = "/categories", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAllDishesCategories() throws InternalException {
        return buildResponse(success(categoryMapper.toDto(categoryService.findAll())), HttpStatus.OK);
    }

    @GetMapping(value = "/unit_types", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAllDishUnitTypes() throws InternalException {
        return buildResponse(success(unitTypeMapper.toDto(unitTypeService.findAllDish())), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/ingredients/alternatives", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getDishIngredientAlternatives(@PathVariable("id") Long id) throws InternalException {
        return buildResponse(success(alternativeMapper.toDto(alternativeService.findAllByDish(id))), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/{id}/ingredients/alternatives", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveDishIngredientAlternatives(@RequestBody List<DishIngredientAlternativeDto> alternatives,
                                                            @PathVariable("id") Long id) throws InternalException, UniqueConstraintViolationException {
        alternativeService.save(alternativeMapper.toEntity(alternatives), id);
        return buildResponse(success(), HttpStatus.CREATED);
    }

    @GetMapping(value = "/photos", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAllDishPhotos(@RequestParam Map<String, String> params) throws InternalException {
        // TODO: Ersetzen Sie die Map Rückkehr durch die List!
        return buildResponse(success(dishService.findAllPhotos(params)), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/photos", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> uploadDishPhoto(@Valid @RequestParam("uploadFile") MultipartFile file,
                                             @Valid @RequestParam("dishId") Long dishId) throws InternalException {
        dishService.savePhoto(file, dishId);
        return buildResponse(success(), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/photos", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getDishPhoto(@PathVariable("id") Long id) throws InternalException {
        return buildResponse(success((Object) dishService.findPhoto(id)), HttpStatus.OK);
    }

    @GetMapping(value = "/replace/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getDishReplaceOptions(@PathVariable("id") Long dishId,
                                          @RequestParam(name = "dish_type_id", required = false) Long dishTypeId)
            throws InternalException, ResourceNotFoundException {

        return buildResponse(
                success(dishMandatoryMapper.toDto(dishMandatoryService.findDishReplaceOptions(dishId, dishTypeId))),
                HttpStatus.OK);
    }
}
