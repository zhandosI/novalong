package kz.teamvictus.novalong.rest.model.dto.alternative;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AlternativeRelationDto {

    private Long id;
    private String code;
    private boolean active;

}
