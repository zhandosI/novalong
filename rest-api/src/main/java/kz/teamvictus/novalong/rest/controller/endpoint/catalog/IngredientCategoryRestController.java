package kz.teamvictus.novalong.rest.controller.endpoint.catalog;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.rest.model.dto.ingredient.IngredientCategoryDto;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientCategory;
import kz.teamvictus.novalong.rest.service.IngredientCategoryService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.util.mapper.impl.IngredientCategoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/ingredients_categories")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class IngredientCategoryRestController extends BaseController {

    private final IngredientCategoryService alternativeRelationService;
    private final IngredientCategoryMapper alternativeRelationMapper;

    @Autowired
    public IngredientCategoryRestController(IngredientCategoryService alternativeRelationService,
                                            IngredientCategoryMapper alternativeRelationMapper) {
        this.alternativeRelationService = alternativeRelationService;
        this.alternativeRelationMapper = alternativeRelationMapper;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getIngredientCategories() throws InternalException {
        return buildResponse(success(alternativeRelationMapper.toDto(alternativeRelationService.findAll())), HttpStatus.OK);
    }

    @GetMapping(value = "parentCategory", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getIngredientCategoryParentCategories() throws InternalException {
        return buildResponse(success(alternativeRelationMapper.toDto(alternativeRelationService.findAll())), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getIngredientCategory(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        return buildResponse(success(alternativeRelationMapper.toDto(alternativeRelationService.findById(id))), HttpStatus.OK);
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveIngredientCategory(@RequestBody IngredientCategoryDto alternativeRelationDto) throws InternalException {
        IngredientCategory newIngredientCategory = alternativeRelationService.save(alternativeRelationMapper.toEntity(alternativeRelationDto));

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(buildLocationUri(newIngredientCategory.getId(), "/{id}"));

        return buildResponse(success(alternativeRelationMapper.toDto(newIngredientCategory)), headers, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> toggleIngredientCategory(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        alternativeRelationService.toggle(id);

        return buildResponse(success(), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> updateIngredientCategory(@RequestBody IngredientCategoryDto alternativeRelationDto,
                                        @PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        IngredientCategory updatedIngredientCategory = alternativeRelationService.update(id, alternativeRelationMapper.toEntity(alternativeRelationDto));

        return buildResponse(success(alternativeRelationMapper.toDto(updatedIngredientCategory)), HttpStatus.OK);
    }

}
