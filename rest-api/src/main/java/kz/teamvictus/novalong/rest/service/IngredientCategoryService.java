package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientCategory;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;

import java.util.List;
import java.util.Map;

public interface IngredientCategoryService {

    IngredientCategory findById(Long categoryId) throws InternalException, ResourceNotFoundException;
    IngredientCategory findByCode(String code) throws InternalException;
    IngredientCategory findByName(String name) throws InternalException;

    List<IngredientCategory> findAll() throws InternalException;
    List<IngredientCategory> findAll(Long parentCategoryId) throws InternalException;
    List<IngredientCategory> findAll(Map<String, String> params) throws InternalException;
    List<IngredientCategory> findChild(Long parentCategoryId) throws InternalException;

    IngredientCategory save(IngredientCategory category) throws InternalException;
    IngredientCategory update(Long categoryId, IngredientCategory category) throws InternalException, ResourceNotFoundException;
    void toggle(Long categoryId) throws InternalException, ResourceNotFoundException;

}
