package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientOptParamName;
import kz.teamvictus.novalong.rest.repository.IngredientOptParamNameRepository;
import kz.teamvictus.novalong.rest.service.IngredientOptParamNameService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientOptParamNameServiceImpl implements IngredientOptParamNameService {

    private IngredientOptParamNameRepository optParamNameRepository;

    @Autowired
    public void setOptParamNameRepository(IngredientOptParamNameRepository optParamNameRepository) {
        this.optParamNameRepository = optParamNameRepository;
    }

    @Override
    public IngredientOptParamName findByCode(String code) throws InternalException {
        try {
            return optParamNameRepository.findByCode(code);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<IngredientOptParamName> findAll() throws InternalException {
        try {
            return optParamNameRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

}
