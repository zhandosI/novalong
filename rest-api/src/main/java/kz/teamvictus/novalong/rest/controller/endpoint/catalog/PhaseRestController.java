package kz.teamvictus.novalong.rest.controller.endpoint.catalog;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.rest.model.dto.PhaseDto;
import kz.teamvictus.novalong.rest.model.entity.Phase;
import kz.teamvictus.novalong.rest.service.PhaseService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.util.mapper.impl.PhaseMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/phases")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class PhaseRestController extends BaseController {

    private final PhaseService phaseService;
    private final PhaseMapper phaseMapper;

    @Autowired
    public PhaseRestController(PhaseService phaseService,
                               PhaseMapper phaseMapper) {
        this.phaseService = phaseService;
        this.phaseMapper = phaseMapper;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getPhases() throws InternalException {
        return buildResponse(success(phaseMapper.toDto(phaseService.findAll())), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getPhase(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        return buildResponse(success(phaseMapper.toDto(phaseService.findById(id))), HttpStatus.OK);
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> savePhase(@RequestBody PhaseDto phaseDto) throws InternalException {
        Phase newPhase = phaseService.save(phaseMapper.toEntity(phaseDto));

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(buildLocationUri(newPhase.getId(), "/{id}"));

        return buildResponse(success(phaseMapper.toDto(newPhase)), headers, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> togglePhase(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        phaseService.toggle(id);

        return buildResponse(success(), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> updatePhase(@RequestBody PhaseDto phaseDto,
                                        @PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        Phase updatedPhase = phaseService.update(id, phaseMapper.toEntity(phaseDto));

        return buildResponse(success(phaseMapper.toDto(updatedPhase)), HttpStatus.OK);
    }

}
