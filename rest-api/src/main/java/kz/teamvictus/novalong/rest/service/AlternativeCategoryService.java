package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.alternative.AlternativeCategory;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;

import java.util.List;

public interface AlternativeCategoryService {

    List<AlternativeCategory> findAll() throws InternalException;
    List<AlternativeCategory> findActive() throws InternalException;
    AlternativeCategory findById(Long id) throws InternalException;

    AlternativeCategory save(AlternativeCategory alternativeCategory) throws InternalException;
    AlternativeCategory update(Long alternativeCategoryId, AlternativeCategory alternativeCategory)
            throws InternalException, ResourceNotFoundException;
    void toggle(Long alternativeCategoryId) throws InternalException, ResourceNotFoundException;
}
