package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.dish.DishLight;
import kz.teamvictus.novalong.rest.repository.DishLightRepository;
import kz.teamvictus.novalong.rest.service.DishLightService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DishLightServiceImpl implements DishLightService {

    private DishLightRepository dishLightRepository;

    @Autowired
    public void setDishLightRepository(DishLightRepository dishLightRepository) {
        this.dishLightRepository = dishLightRepository;
    }

    @Override
    public DishLight findById(Long dishId) throws InternalException, ResourceNotFoundException {
        try {
            DishLight dish = this.dishLightRepository.findOne(dishId);

            if (dish == null)
                throw new ResourceNotFoundException("There is no dish with id: " + dishId, ErrorCode.RESOURCE_NOT_FOUND);

            return dish;
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

}
