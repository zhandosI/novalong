package kz.teamvictus.novalong.rest.tool;

import kz.teamvictus.novalong.rest.model.entity.ProgramDishType;
import kz.teamvictus.novalong.rest.model.entity.dish.Dish;
import kz.teamvictus.novalong.rest.model.entity.dish.DishType;
import kz.teamvictus.novalong.rest.model.entity.ration.UserDailyRation;
import kz.teamvictus.novalong.rest.service.DishService;
import kz.teamvictus.novalong.rest.service.locator.ServiceLocator;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.util.CalculationUtil;
import kz.teamvictus.novalong.util.DateUtil;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Meal Plan generation algorithm
 *
 * @author Sanzhar Kudaibergen
 * @version 1.1
 */

public class MealPlanGenerationTool {

    /**
     * Iterates over each day passed to generate a meal plan by each dish types
     *
     * @param rationId - user ration id
     * @param pdt - dish types to iterate through
     * @param calsNeeded - total calories for each day
     * @param day - num of day to start from
     * @param daysNum - total number of days generation
     * @return Set of user_daily_ration table entities
     * @throws InternalException - thrown from spring services
     */
    public static Set<UserDailyRation> generate(Long rationId, Set<ProgramDishType> pdt, double calsNeeded, int day, int daysNum) throws InternalException {
        try {
            Set<UserDailyRation> rations = new HashSet<>();

            for (int i = 0; i < daysNum; i++) {
                rations.addAll(bumpDishes(rationId, day, pdt, calsNeeded));
                day = (day % 7) + 1;
            }

            return rations;
        } catch (Exception e) {
            // TODO: Vergessen Sie nicht, diese Ausnahme zu protokollieren!
            e.printStackTrace();
            throw e;
        }
    }

    private static Set<UserDailyRation> bumpDishes(Long rationId, int todayDay, Set<ProgramDishType> programDishTypes, double totalCalsNeeded) throws InternalException {
        Set<UserDailyRation> rations = new HashSet<>();

        // TODO: NOT FINISHED!!
        for (ProgramDishType pdt: programDishTypes) {
            Set<UserDailyRation> dishTypeRations = randomDishes(rationId, todayDay, pdt.getDishType(), pdt.getCaloriesPercentage() * totalCalsNeeded);

            if (dishTypeRations != null)
                rations.addAll(dishTypeRations);
        }
        return rations;
    }

    private static Set<UserDailyRation> randomDishes(Long rationId, int todayDay, DishType type, double calsNeeded) throws InternalException {
        DishService dishService = ServiceLocator.getDishService();

        Set<UserDailyRation> rations = new HashSet<>();
        List<Dish> dishes;
        String[] categoriesArr = null;
        Double[] ratiosArr = null;

        // TODO: Ratios are hardcoded! randomize later
        // TODO: Dish can be repeated! Keep track of them - passing list of ids onto procedure can fit here.
        switch (type.getCode()) {
            case "LUNCH":
                // Random boolean
                if (Math.random() < 0.5) {
                    categoriesArr = new String[]{"MEAT_MEAL", "GARNISH", "SALAD"};
                    ratiosArr = new Double[]{0.4, 0.4, 0.2};
                } else {
                    categoriesArr = new String[]{"PRIMARY_MEAL", "SALAD"};
                    ratiosArr = new Double[]{0.6, 0.4};
                }
                break;

            case "DINNER":
                // Random boolean
                if (Math.random() < 0.5) {
                    categoriesArr = new String[]{"MEAT_MEAL", "VEGETABLE_GARNISH"};
                    ratiosArr = new Double[]{0.5, 0.5};
                } else {
                    categoriesArr = new String[]{"MEAT_MEAL", "SALAD"};
                    ratiosArr = new Double[]{0.6, 0.4};
                }
                break;
        }

        if (categoriesArr != null)
            dishes = dishService.findRandom(type.getId(), calsNeeded, categoriesArr, ratiosArr);
        else {
            // random dish number from 1 to 3 inclusive
            int dishNum = new Random().nextInt(3) + 1;
            dishes = dishService.findRandom(type.getId(), calsNeeded, dishNum);
        }

        for (Dish dish: dishes)
            rations.add(createDailyRation(rationId, type.getId(), DateUtil.getDayCodeOfWeek(todayDay), dish));

        return rations;
    }

    private static UserDailyRation createDailyRation(Long rationId, Long dishTypeId, String dayCode, Dish dish) throws InternalException {
        UserDailyRation dailyRation = new UserDailyRation();
        dailyRation.setUserRationId(rationId);
        dailyRation.setDayCode(dayCode);
        dailyRation.setDishTypeId(dishTypeId);
        dailyRation.setDishId(dish.getId());
        dailyRation.setUnitType(ServiceLocator.getUnitTypeService().findByCode("GRAMS_DISH", true));
        dailyRation.setProtein(CalculationUtil.roundToTwoPoints(dish.getProtein()));
        dailyRation.setFat(CalculationUtil.roundToTwoPoints(dish.getFat()));
        dailyRation.setCarbohydrate(CalculationUtil.roundToTwoPoints(dish.getCarbohydrate()));
        dailyRation.setUnitAmount(CalculationUtil.roundToTwoPoints(dish.getWeight()));
        dailyRation.setActive(true);
        return dailyRation;
    }

}
