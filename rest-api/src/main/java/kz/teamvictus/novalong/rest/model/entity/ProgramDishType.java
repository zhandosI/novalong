package kz.teamvictus.novalong.rest.model.entity;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import kz.teamvictus.novalong.rest.model.entity.dish.DishType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "programs_dish_types")
@Getter
@Setter
@ToString
public class ProgramDishType extends BaseEntity {

    @Column(name = "program_id")
    private Long programId;

    @Column(name = "dish_type_id")
    private Long dishTypeId;

    @ManyToOne
    @JoinColumn(name = "dish_type_id", insertable = false, updatable = false)
    private DishType dishType;

    @Column(name = "calories_percentage")
    private double caloriesPercentage;

    @Column(name = "generation_order")
    private int generationOrder;

    @Column(name = "active")
    private boolean active;

}
