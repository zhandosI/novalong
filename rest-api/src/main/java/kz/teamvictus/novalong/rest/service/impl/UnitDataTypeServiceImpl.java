package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.UnitDataType;
import kz.teamvictus.novalong.rest.repository.UnitDataTypeRepository;
import kz.teamvictus.novalong.rest.service.UnitDataTypeService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnitDataTypeServiceImpl implements UnitDataTypeService {

    private UnitDataTypeRepository unitDataTypeRepository;

    @Autowired
    public void setUnitDataTypeRepository(UnitDataTypeRepository unitDataTypeRepository) {
        this.unitDataTypeRepository = unitDataTypeRepository;
    }

    @Override
    public List<UnitDataType> findAll() throws InternalException {
        try {
            return unitDataTypeRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public UnitDataType findById(Long id) throws InternalException {
        try {
            try {
                UnitDataType unitDataType = unitDataTypeRepository.findOne(id);

                if (unitDataType == null)
                    throw new ResourceNotFoundException("There is no unitDataType with id: " + id, ErrorCode.RESOURCE_NOT_FOUND);

                return unitDataType;
            } catch (ResourceNotFoundException e) {
                throw e;
            } catch (Exception e) {
                throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
            }
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
