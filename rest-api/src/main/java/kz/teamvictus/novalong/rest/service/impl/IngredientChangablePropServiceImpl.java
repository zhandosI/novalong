package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientChangableProp;
import kz.teamvictus.novalong.rest.repository.IngredientChangablePropRepository;
import kz.teamvictus.novalong.rest.service.IngredientChangablePropService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientChangablePropServiceImpl implements IngredientChangablePropService {

    private IngredientChangablePropRepository propRepository;

    @Autowired
    public void setPropRepository(IngredientChangablePropRepository propRepository) {
        this.propRepository = propRepository;
    }

    @Override
    public List<IngredientChangableProp> findAll() throws InternalException {
        try {
            return propRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
