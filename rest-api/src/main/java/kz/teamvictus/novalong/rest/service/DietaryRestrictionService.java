package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.DietaryRestriction;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;

import java.util.List;

public interface DietaryRestrictionService {

    List<DietaryRestriction> findAll() throws InternalException;
    List<DietaryRestriction> findActive() throws InternalException;
    DietaryRestriction findById(Long id) throws InternalException;

    DietaryRestriction save(DietaryRestriction dietaryRestriction) throws InternalException;
    DietaryRestriction update(Long dietaryRestrictionId, DietaryRestriction dietaryRestriction)
            throws InternalException, ResourceNotFoundException;
    void toggle(Long dietaryRestrictionId) throws InternalException, ResourceNotFoundException;

}
