package kz.teamvictus.novalong.rest.model.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HealthFactor {

    private Long dishId;
    private String code;
    private String colorHex;
    private Double factor;
    private Double weight;
    private Set<HealthFactor> params;

    public HealthFactor(Long dishId, String colorHex, Double factor, Set<HealthFactor> params) {
        this.dishId = dishId;
        this.colorHex = colorHex;
        this.factor = factor;
        this.params = params;
    }

    public HealthFactor(String colorHex, Double factor, Set<HealthFactor> params) {
        this.colorHex = colorHex;
        this.factor = factor;
        this.params = params;
    }

    public HealthFactor(String code, String colorHex, Double factor) {
        this.code = code;
        this.colorHex = colorHex;
        this.factor = factor;
    }

    public HealthFactor(String code, String colorHex, Double factor, Double weight) {
        this.code = code;
        this.colorHex = colorHex;
        this.factor = factor;
        this.weight = weight;
    }
}
