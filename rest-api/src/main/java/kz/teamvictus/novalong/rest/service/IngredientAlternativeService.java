package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientAlternative;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientAlternativesJoined;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.UniqueConstraintViolationException;

import java.util.List;

public interface IngredientAlternativeService {

    List<IngredientAlternative> findAllByIngredient(Long id) throws InternalException;
    List<IngredientAlternativesJoined> findAllByIngredientJoinedByCategory(Long id) throws InternalException;

    void save(IngredientAlternative alternative, Long ingredientId) throws InternalException, UniqueConstraintViolationException;
    void save(List<IngredientAlternative> alternatives, Long ingredientId) throws InternalException, UniqueConstraintViolationException;

}
