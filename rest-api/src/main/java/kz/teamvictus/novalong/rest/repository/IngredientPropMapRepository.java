package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientPropMap;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IngredientPropMapRepository extends JpaRepository<IngredientPropMap, Long> {

    List<IngredientPropMap> findAllByIngredientId(Long ingredientId);

}
