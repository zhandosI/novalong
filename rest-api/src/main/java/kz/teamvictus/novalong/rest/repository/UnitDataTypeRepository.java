package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.UnitDataType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UnitDataTypeRepository extends JpaRepository<UnitDataType, Long> {

}
