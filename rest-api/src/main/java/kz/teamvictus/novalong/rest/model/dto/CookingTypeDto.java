package kz.teamvictus.novalong.rest.model.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CookingTypeDto {

    private Long id;
    private String name;
    private Long colorCodeId;
    private ColorCodeDto colorCode;
    private boolean outOfMealPlan;
    private boolean active;

}
