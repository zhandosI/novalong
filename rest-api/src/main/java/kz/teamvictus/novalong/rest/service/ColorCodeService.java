package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.ColorCode;
import kz.teamvictus.novalong.security.exceptions.InternalException;

import java.util.List;

public interface ColorCodeService {

    List<ColorCode> findAll() throws InternalException;
    ColorCode findById(Long id) throws InternalException;

}
