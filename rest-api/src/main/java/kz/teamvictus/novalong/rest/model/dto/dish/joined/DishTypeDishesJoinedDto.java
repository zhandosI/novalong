package kz.teamvictus.novalong.rest.model.dto.dish.joined;

import kz.teamvictus.novalong.rest.model.dto.dish.DishTypeDto;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Getter
@Setter
@ToString
public class DishTypeDishesJoinedDto {

    private Float calories;
    private DishTypeDto dishType;
    private Set<DishUnitTypeJoinedDto> meals;

}
