package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.alternative.AlternativeCategory;
import kz.teamvictus.novalong.rest.repository.AlternativeCategoryRepository;
import kz.teamvictus.novalong.rest.service.AlternativeCategoryService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AlternativeCategoryServiceImpl implements AlternativeCategoryService {

    private AlternativeCategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(AlternativeCategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<AlternativeCategory> findAll() throws InternalException {
        try {
            return categoryRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<AlternativeCategory> findActive() throws InternalException {
        try {
            return categoryRepository.findAlternativeCategoriesByActive(true);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public AlternativeCategory findById(Long id) throws InternalException {
        try {
            try {
                AlternativeCategory alternativeCategory = categoryRepository.findOne(id);

                if (alternativeCategory == null)
                    throw new ResourceNotFoundException("There is no alternativeCategory with id: " + id, ErrorCode.RESOURCE_NOT_FOUND);

                return alternativeCategory;
            } catch (ResourceNotFoundException e) {
                throw e;
            } catch (Exception e) {
                throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
            }
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public AlternativeCategory save(AlternativeCategory alternativeCategory) throws InternalException {
        try {
            if (alternativeCategory.getCode() == null)
                throw new InternalException("There is no alternativeCategory code " + alternativeCategory.getCode(), ErrorCode.EMPTY_CODE);

            alternativeCategory.setActive(true);

            return categoryRepository.save(alternativeCategory);
        } catch (InternalException ie) {
            throw ie;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public AlternativeCategory update(Long alternativeCategoryId, AlternativeCategory alternativeCategory) throws InternalException, ResourceNotFoundException {
        try {
            AlternativeCategory alternativeCategoryToUpdate = categoryRepository.getOne(alternativeCategoryId);

            if (alternativeCategoryToUpdate.getId() == null)
                throw new ResourceNotFoundException("There is no alternativeCategory with id: " + alternativeCategoryId, ErrorCode.RESOURCE_NOT_FOUND);

            alternativeCategoryToUpdate.setCode(alternativeCategory.getCode());

            categoryRepository.save(alternativeCategoryToUpdate);

            return categoryRepository.findOne(alternativeCategoryId);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void toggle(Long alternativeCategoryId) throws InternalException, ResourceNotFoundException {
        try {
            AlternativeCategory alternativeCategory = categoryRepository.getOne(alternativeCategoryId);

            if (alternativeCategory.getId() == null)
                throw new ResourceNotFoundException("There is no alternativeCategory with id: " + alternativeCategoryId, ErrorCode.RESOURCE_NOT_FOUND);

            alternativeCategory.setActive(!alternativeCategory.isActive());

            categoryRepository.save(alternativeCategory);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
