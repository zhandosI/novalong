package kz.teamvictus.novalong.rest.service.locator;

import kz.teamvictus.novalong.rest.service.DishService;
import kz.teamvictus.novalong.rest.service.UnitTypeService;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * Service Locator implementation class for inversion of control
 * An alternative approach for dependency injection
 *
 * @author Sanzhar Kudaibergen
 */
@Component
public class ServiceLocator implements ApplicationContextAware {

    private static ApplicationContext context;

    private static final String DISH_SERVICE = "dishServiceImpl";
    private static final String UNIT_TYPE_SERVICE = "unitTypeServiceImpl";

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    public static DishService getDishService() {
        return (DishService) context.getBean(DISH_SERVICE);
    }

    public static UnitTypeService getUnitTypeService() {
        return (UnitTypeService) context.getBean(UNIT_TYPE_SERVICE);
    }

}
