package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.Role;
import kz.teamvictus.novalong.security.exceptions.InternalException;

public interface RoleService {

    Role findById(Long id) throws InternalException;
    Role findByName(String roleName) throws InternalException;

}
