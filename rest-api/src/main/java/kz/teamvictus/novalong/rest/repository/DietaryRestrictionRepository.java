package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.DietaryRestriction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DietaryRestrictionRepository extends JpaRepository<DietaryRestriction, Long> {

    List<DietaryRestriction> findDietaryRestrictionsByActive(Boolean isActive);
}
