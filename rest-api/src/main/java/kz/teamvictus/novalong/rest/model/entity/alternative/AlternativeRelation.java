package kz.teamvictus.novalong.rest.model.entity.alternative;


import kz.teamvictus.novalong.rest.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "alternatives_relations")
@Getter
@Setter
@ToString(exclude = "inverseRelation")
public class AlternativeRelation extends BaseEntity {

    @Column(name = "code")
    private String code;

    @OneToOne
    @JoinColumn(name = "pair_id")
    private AlternativeRelation inverseRelation;

    @Column(name = "active")
    private boolean active;

}
