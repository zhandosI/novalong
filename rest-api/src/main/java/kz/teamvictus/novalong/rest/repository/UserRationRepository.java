package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.ration.UserRation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRationRepository extends JpaRepository<UserRation, Long> {

    @Query("SELECT r.id FROM UserRation r WHERE r.userId = ?1 AND r.active = true")
    Long findIdByUserId(Long userId);
    UserRation findByUserId(Long userId);

}
