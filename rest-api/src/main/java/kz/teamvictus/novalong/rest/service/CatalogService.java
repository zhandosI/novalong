package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.Catalog;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;

import java.util.List;

public interface CatalogService {

    Catalog findById(Long id) throws InternalException;
    List<Catalog> findAll() throws InternalException;
    List<Catalog> findActive() throws InternalException;

    Catalog save(Catalog catalog) throws InternalException;
    Catalog update(Long catalogId, Catalog catalog) throws InternalException, ResourceNotFoundException;
}
