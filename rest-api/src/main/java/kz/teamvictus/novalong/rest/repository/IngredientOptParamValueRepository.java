package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientOptParamValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface IngredientOptParamValueRepository extends JpaRepository<IngredientOptParamValue, Long> {

    @Query("SELECT v FROM IngredientOptParamValue v INNER JOIN v.param n WHERE v.ingredientId = ?1 AND n.code = ?2")
    IngredientOptParamValue findByIngredientIdAndParamCode(Long ingredientId, String code);

    @Query("SELECT v FROM IngredientOptParamValue v INNER JOIN v.param n WHERE v.ingredientId = ?1 AND n.code NOT IN ?2")
    List<IngredientOptParamValue> findAllByNotExistingParam(Long ingredientId, Set<String> codes);

    List<IngredientOptParamValue> findAllByIngredientId(Long ingredientId);

}
