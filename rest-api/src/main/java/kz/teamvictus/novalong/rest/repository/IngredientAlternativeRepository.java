package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.alternative.AlternativeCategory;
import kz.teamvictus.novalong.rest.model.entity.alternative.AlternativeRelation;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientAlternative;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IngredientAlternativeRepository extends JpaRepository<IngredientAlternative, Long> {

    List<IngredientAlternative> findAllByIngredientId(Long id);
    List<IngredientAlternative> findAllByIngredientIdAndCategoryAndRelation(Long ingredientId,
                                                                            AlternativeCategory category,
                                                                            AlternativeRelation relation);

}
