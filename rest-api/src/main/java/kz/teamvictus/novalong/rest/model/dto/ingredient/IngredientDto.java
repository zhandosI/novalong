package kz.teamvictus.novalong.rest.model.dto.ingredient;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Getter
@Setter
@ToString
public class IngredientDto {

    private Long id;
    private String code;
    private String name;
    private float protein;
    private float fat;
    private float carbohydrate;
    private float kilocalories;
    private float glycemicIndex;
    private IngredientCategoryDto category;
    private boolean allergic;
    private Set<IngredientUnitTypeDto> unitTypes;
    private Set<IngredientCookingTypeDto> cookingTypes;
    private boolean active;

}
