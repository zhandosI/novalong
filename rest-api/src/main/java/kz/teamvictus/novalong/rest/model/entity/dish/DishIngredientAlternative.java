package kz.teamvictus.novalong.rest.model.entity.dish;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import kz.teamvictus.novalong.rest.model.entity.alternative.AlternativeCategory;
import kz.teamvictus.novalong.rest.model.entity.alternative.AlternativeRelation;
import kz.teamvictus.novalong.rest.model.entity.ingredient.Ingredient;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "dish_ingredients_alternatives")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DishIngredientAlternative extends BaseEntity {

    @Column(name = "dish_id")
    private Long dishId;

    @ManyToOne
    @JoinColumn(name = "ingredient_id")
    private Ingredient ingredient;

    @ManyToOne
    @JoinColumn(name = "alternative_ingredient_id")
    private Ingredient alternativeIngredient;

    @ManyToOne
    @JoinColumn(name = "alternative_category_id")
    private AlternativeCategory category;

    @ManyToOne
    @JoinColumn(name = "alternative_relation_id")
    private AlternativeRelation relation;

}
