package kz.teamvictus.novalong.rest.controller.endpoint.catalog;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.rest.model.dto.ingredient.IngredientPropDto;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientProp;
import kz.teamvictus.novalong.rest.service.IngredientPropService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.util.mapper.impl.IngredientPropMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/ingredients_props")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class IngredientPropRestController extends BaseController {

    private final IngredientPropService alternativeRelationService;
    private final IngredientPropMapper alternativeRelationMapper;

    @Autowired
    public IngredientPropRestController(IngredientPropService alternativeRelationService,
                                        IngredientPropMapper alternativeRelationMapper) {
        this.alternativeRelationService = alternativeRelationService;
        this.alternativeRelationMapper = alternativeRelationMapper;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getIngredientProps() throws InternalException {
        return buildResponse(success(alternativeRelationMapper.toDto(alternativeRelationService.findAll())), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getIngredientProp(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        return buildResponse(success(alternativeRelationMapper.toDto(alternativeRelationService.findById(id))), HttpStatus.OK);
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveIngredientProp(@RequestBody IngredientPropDto alternativeRelationDto) throws InternalException {
        IngredientProp newIngredientProp = alternativeRelationService.save(alternativeRelationMapper.toEntity(alternativeRelationDto));

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(buildLocationUri(newIngredientProp.getId(), "/{id}"));

        return buildResponse(success(alternativeRelationMapper.toDto(newIngredientProp)), headers, HttpStatus.CREATED);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> toggleIngredientProp(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        alternativeRelationService.toggle(id);

        return buildResponse(success(), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> updateIngredientProp(@RequestBody IngredientPropDto alternativeRelationDto,
                                        @PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        IngredientProp updatedIngredientProp = alternativeRelationService.update(id, alternativeRelationMapper.toEntity(alternativeRelationDto));

        return buildResponse(success(alternativeRelationMapper.toDto(updatedIngredientProp)), HttpStatus.OK);
    }

}
