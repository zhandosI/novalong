package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.dish.DishCategory;
import kz.teamvictus.novalong.rest.repository.DishCategoryRepository;
import kz.teamvictus.novalong.rest.service.DishCategoryService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DishCategoryServiceImpl implements DishCategoryService {

    private DishCategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(DishCategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public DishCategory findByName(String name) throws InternalException {
        try {
            return categoryRepository.findByName(name);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<DishCategory> findAll() throws InternalException {
        try {
            return categoryRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<DishCategory> findActive() throws InternalException {
        try {
            return categoryRepository.findDishCategoriesByActive(true);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public DishCategory findById(Long id) throws InternalException {
        try {
            try {
                DishCategory dishCategory = categoryRepository.findOne(id);

                if (dishCategory == null)
                    throw new ResourceNotFoundException("There is no dishCategory with id: " + id, ErrorCode.RESOURCE_NOT_FOUND);

                return dishCategory;
            } catch (ResourceNotFoundException e) {
                throw e;
            } catch (Exception e) {
                throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
            }
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public DishCategory save(DishCategory dishCategory) throws InternalException {
        try {
            if (dishCategory.getName() == null)
                throw new InternalException("There is no dishCategory code " + dishCategory.getName(), ErrorCode.EMPTY_CODE);

            dishCategory.setActive(true);

            return categoryRepository.save(dishCategory);
        } catch (InternalException ie) {
            throw ie;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public DishCategory update(Long dishCategoryId, DishCategory dishCategory) throws InternalException, ResourceNotFoundException {
        try {
            DishCategory dishCategoryToUpdate = categoryRepository.getOne(dishCategoryId);

            if (dishCategoryToUpdate.getId() == null)
                throw new ResourceNotFoundException("There is no dishCategory with id: " + dishCategoryId, ErrorCode.RESOURCE_NOT_FOUND);

            dishCategoryToUpdate.setName(dishCategory.getName());

            categoryRepository.save(dishCategoryToUpdate);

            return categoryRepository.findOne(dishCategoryId);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void toggle(Long dishCategoryId) throws InternalException, ResourceNotFoundException {
        try {
            DishCategory dishCategory = categoryRepository.getOne(dishCategoryId);

            if (dishCategory.getId() == null)
                throw new ResourceNotFoundException("There is no dishCategory with id: " + dishCategoryId, ErrorCode.RESOURCE_NOT_FOUND);

            dishCategory.setActive(!dishCategory.isActive());

            categoryRepository.save(dishCategory);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
