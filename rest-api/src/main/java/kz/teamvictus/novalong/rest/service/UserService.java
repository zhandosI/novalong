package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.User;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.exceptions.UniqueConstraintViolationException;

import java.util.List;

public interface UserService {

    User findById(Long id) throws InternalException, ResourceNotFoundException;
    User findProxyById(Long id) throws InternalException, ResourceNotFoundException;
    User findByUsername(String username) throws InternalException;
    User findBySocialMediaId(String socialMediaId) throws InternalException;
    List<User> findAll() throws InternalException;
    User save(User user) throws InternalException, UniqueConstraintViolationException;
    User update(Long userId, User user) throws InternalException, UniqueConstraintViolationException;
    double userNeededCalories(User user, double diffCoef, int daysNum) throws InternalException;

}
