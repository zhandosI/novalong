package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.Phase;
import kz.teamvictus.novalong.rest.repository.PhaseRepository;
import kz.teamvictus.novalong.rest.service.PhaseService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhaseServiceImpl implements PhaseService {

    private PhaseRepository phaseRepository;

    @Autowired
    public void setPhaseRepository(PhaseRepository phaseRepository) {
        this.phaseRepository = phaseRepository;
    }

    @Override
    public List<Phase> findAll() throws InternalException {
        try {
            return phaseRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<Phase> findActive() throws InternalException {
        try {
            return phaseRepository.findPhasesByActive(true);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public Phase findById(Long id) throws InternalException {
        try {
            try {
                Phase phase = phaseRepository.findOne(id);

                if (phase == null)
                    throw new ResourceNotFoundException("There is no phase with id: " + id, ErrorCode.RESOURCE_NOT_FOUND);

                return phase;
            } catch (ResourceNotFoundException e) {
                throw e;
            } catch (Exception e) {
                throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
            }
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public Phase findByPriority(int priority) throws InternalException {
        try {
            return phaseRepository.findByPriority(priority);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public Phase save(Phase phase) throws InternalException {
        try {
            if (phase.getName() == null)
                throw new InternalException("There is no phase code " + phase.getName(), ErrorCode.EMPTY_CODE);

            phase.setActive(true);

            return phaseRepository.save(phase);
        } catch (InternalException ie) {
            throw ie;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public Phase update(Long phaseId, Phase phase) throws InternalException, ResourceNotFoundException {
        try {
            Phase phaseToUpdate = phaseRepository.getOne(phaseId);

            if (phaseToUpdate.getId() == null)
                throw new ResourceNotFoundException("There is no phase with id: " + phaseId, ErrorCode.RESOURCE_NOT_FOUND);

            phaseToUpdate.setName(phase.getName());

            phaseRepository.save(phaseToUpdate);

            return phaseRepository.findOne(phaseId);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void toggle(Long phaseId) throws InternalException, ResourceNotFoundException {
        try {
            Phase phase = phaseRepository.getOne(phaseId);

            if (phase.getId() == null)
                throw new ResourceNotFoundException("There is no phase with id: " + phaseId, ErrorCode.RESOURCE_NOT_FOUND);

            phase.setActive(!phase.isActive());

            phaseRepository.save(phase);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
