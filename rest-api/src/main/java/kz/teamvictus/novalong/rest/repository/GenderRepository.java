package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.Gender;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenderRepository extends JpaRepository<Gender, Long> {
}
