package kz.teamvictus.novalong.rest.model.dto.dish;

import kz.teamvictus.novalong.rest.model.dto.UnitTypeDto;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DishUnitTypeDto {

    private UnitTypeDto type;
    private float weight;

}
