package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IngredientCategoryRepository extends JpaRepository<IngredientCategory, Long> {

    IngredientCategory findByName(String name);
    IngredientCategory findByCode(String code);
    List<IngredientCategory> findAllByParentCategoryId(Long parentCategoryId);

}
