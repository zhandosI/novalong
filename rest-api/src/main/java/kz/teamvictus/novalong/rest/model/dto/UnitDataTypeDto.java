package kz.teamvictus.novalong.rest.model.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UnitDataTypeDto {

    private Long id;
    private String name;
    private String dataTypeCode;

}
