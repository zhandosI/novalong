package kz.teamvictus.novalong.rest.repository.custom;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientCategory;
import kz.teamvictus.novalong.util.model.criteria.IngredientCategoryCriteria;

import java.util.List;

public interface IngredientCategoryCustomRepository {

    List<IngredientCategory> findAllByCriteria(IngredientCategoryCriteria criteria);

}
