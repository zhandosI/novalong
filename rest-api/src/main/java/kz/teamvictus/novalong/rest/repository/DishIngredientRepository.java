package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.dish.DishIngredient;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DishIngredientRepository extends JpaRepository<DishIngredient, Long> {
}
