package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.dish.DishUnitType;
import kz.teamvictus.novalong.security.exceptions.InternalException;

public interface DishUnitTypeService {

    void save(DishUnitType unitType) throws InternalException;
    void save(Iterable<DishUnitType> unitTypes, Long dishId) throws InternalException;
    void delete(Iterable<DishUnitType> unitTypes) throws InternalException;

}
