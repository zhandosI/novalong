package kz.teamvictus.novalong.rest.controller.endpoint;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.rest.service.AlternativeCategoryService;
import kz.teamvictus.novalong.rest.service.AlternativeRelationService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.util.mapper.impl.AlternativeCategoryMapper;
import kz.teamvictus.novalong.util.mapper.impl.AlternativeRelationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/v1/alternatives")
public class AlternativeRestController extends BaseController {

    private final AlternativeCategoryMapper categoryMapper;
    private final AlternativeRelationMapper relationMapper;

    private final AlternativeCategoryService categoryService;
    private final AlternativeRelationService relationService;

    @Autowired
    public AlternativeRestController(AlternativeCategoryMapper categoryMapper,
                                     AlternativeRelationMapper relationMapper,
                                     AlternativeCategoryService categoryService,
                                     AlternativeRelationService relationService) {
        this.categoryMapper = categoryMapper;
        this.relationMapper = relationMapper;
        this.categoryService = categoryService;
        this.relationService = relationService;
    }

    @GetMapping(value = "/categories", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAllCategories() throws InternalException {
        return buildResponse(success(categoryMapper.toDto(categoryService.findAll())), HttpStatus.OK);
    }

    @GetMapping(value = "/relations", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAllRelations() throws InternalException {
        return buildResponse(success(relationMapper.toDto(relationService.findAll())), HttpStatus.OK);
    }
}
