package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.ration.joined.UserRationJoined;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;

import java.util.Map;

public interface MealPlanService {

    UserRationJoined rationByParams(Long userId, Map<String, String> params) throws InternalException, ResourceNotFoundException;

}
