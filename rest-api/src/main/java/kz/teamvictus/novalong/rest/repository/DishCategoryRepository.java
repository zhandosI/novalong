package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.dish.DishCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DishCategoryRepository extends JpaRepository<DishCategory, Long> {

    @Query(value = "select DISTINCT ddc.dish_category_id from dishes_dish_categories ddc where ddc.dish_id=:dishId",
            nativeQuery = true)
    List<Long> findAllIdsByDishId(@Param("dishId") Long dishId);
    List<DishCategory> findDishCategoriesByActive(Boolean isActive);
    DishCategory findByName(String name);

}
