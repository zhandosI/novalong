package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientUnitType;
import kz.teamvictus.novalong.security.exceptions.InternalException;

import java.util.Set;

public interface IngredientUnitTypeService {

    void save(Set<IngredientUnitType> unitTypes, Long ingredientId) throws InternalException;
    void update(Set<IngredientUnitType> unitTypes, Long ingredientId) throws InternalException;
    void delete(Set<IngredientUnitType> unitTypes) throws InternalException;

}
