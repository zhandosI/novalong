package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.Catalog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CatalogRepository extends JpaRepository<Catalog, Long> {

    Catalog findCatalogByName(String name);
    List<Catalog> findCatalogsByActive(Boolean isActive);
}
