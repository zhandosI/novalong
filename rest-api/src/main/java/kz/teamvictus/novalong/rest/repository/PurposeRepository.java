package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.Purpose;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PurposeRepository extends JpaRepository<Purpose, Long> {

    List<Purpose> findPurposesByActive(Boolean isActive);
}
