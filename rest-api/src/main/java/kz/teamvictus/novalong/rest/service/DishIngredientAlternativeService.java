package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.dish.DishIngredientAlternative;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.UniqueConstraintViolationException;

import java.util.List;

public interface DishIngredientAlternativeService {

    List<DishIngredientAlternative> findAllByDish(Long dishId) throws InternalException;
    void save(List<DishIngredientAlternative> alternatives, Long dishId) throws InternalException, UniqueConstraintViolationException;

}
