package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.User;
import kz.teamvictus.novalong.rest.repository.UserRepository;
import kz.teamvictus.novalong.rest.service.*;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.exceptions.UniqueConstraintViolationException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import kz.teamvictus.novalong.util.DateUtil;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;
    private final PhaseService phaseService;

    public UserServiceImpl(UserRepository userRepository,
                           RoleService roleService,
                           PasswordEncoder passwordEncoder,
                           PhaseService phaseService) {
        this.userRepository = userRepository;
        this.roleService = roleService;
        this.passwordEncoder = passwordEncoder;
        this.phaseService = phaseService;
    }

    @Override
    public User findById(Long id) throws InternalException, ResourceNotFoundException {
        try {
            User user = userRepository.findOne(id);

            if (user == null)
                throw new ResourceNotFoundException("There is no user with id: " + id, ErrorCode.RESOURCE_NOT_FOUND);

            return user;
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public User findProxyById(Long id) throws InternalException, ResourceNotFoundException {
        try {
            User user = userRepository.getOne(id);

            if (user.getId() == null)
                throw new ResourceNotFoundException("There is no user with id: " + id, ErrorCode.RESOURCE_NOT_FOUND);

            return user;
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public User findByUsername(String username) throws InternalException {
        try {
            return userRepository.findByUsername(username);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public User findBySocialMediaId(String socialMediaId) throws InternalException {
        try {
            return userRepository.findBySocialMediaId(socialMediaId);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<User> findAll() throws InternalException {
        try {
            return userRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public User save(User user) throws InternalException, UniqueConstraintViolationException {
        try {
            if (userRepository.existsByUsername(user.getUsername()))
                throw new UniqueConstraintViolationException("This username is already in use", ErrorCode.UNIQUE_RESOURCE_CONFLICT);

            if (userRepository.existsByEmail(user.getEmail()))
                throw new UniqueConstraintViolationException("This email is already in use", ErrorCode.UNIQUE_RESOURCE_CONFLICT);

            user.setActive(true);
            user.setRole(roleService.findByName("ROLE_USER"));
            user.setCurrentPhase(phaseService.findByPriority(1));
            user.setPhaseUpdateDate(DateUtil.getDateFromToday(7));

            if (user.getPassword() != null)
                user.setPassword(passwordEncoder.encode(user.getPassword()));

            return userRepository.save(user);
        } catch (DataIntegrityViolationException e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                throw new UniqueConstraintViolationException(e.getMessage(), ErrorCode.UNIQUE_RESOURCE_CONFLICT);
            }

            throw e;
        } catch (UniqueConstraintViolationException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public User update(Long userId, User user) throws InternalException, UniqueConstraintViolationException {
        try {
            User userToUpdate = this.userRepository.getOne(userId);

            if (userToUpdate.getId() == null)
                throw new ResourceNotFoundException("There is no user with id: " + userId, ErrorCode.RESOURCE_NOT_FOUND);

            this.update(userToUpdate, user);

            return userRepository.save(userToUpdate);
        } catch (DataIntegrityViolationException e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                throw new UniqueConstraintViolationException(e.getMessage(), ErrorCode.UNIQUE_RESOURCE_CONFLICT);
            }

            throw e;
        } catch (UniqueConstraintViolationException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public double userNeededCalories(User user, double diffCoef, int daysNum) throws InternalException {
        try {
            // Harris – Benedicts formula  (weight - kg, height - cm, age - years)
            double bmr = (user.getWeight() * 10) + (user.getHeight() * 6.25) - (user.getAge() * 5);

            if (user.getGender().getCode().equals("MALE"))
                bmr += 5;
            else
                bmr -= 161;

            // Total calories user needs according to particular program
            // BMR * Activity * program difference coef (e.g. 0.8 for weight loose)
            return bmr * user.getActivityLevel().getCoefficient() * diffCoef * daysNum;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    private void update(User userToUpdate, User user) throws UniqueConstraintViolationException {
        if (user.getUsername() != null && !user.getUsername().equals(userToUpdate.getUsername())) {
            if (userRepository.existsByUsername(user.getUsername()))
                throw new UniqueConstraintViolationException("This username is already in use", ErrorCode.UNIQUE_RESOURCE_CONFLICT);

            userToUpdate.setUsername(user.getUsername());
        }

        if (user.getEmail() != null && !user.getEmail().equals(userToUpdate.getEmail())) {
            if (userRepository.existsByEmail(user.getEmail()))
                throw new UniqueConstraintViolationException("This email is already in use", ErrorCode.UNIQUE_RESOURCE_CONFLICT);

            userToUpdate.setEmail(user.getEmail());
        }

        if (user.getActivityLevel() != null)
            userToUpdate.setActivityLevel(user.getActivityLevel());

        if (user.getGender() != null)
            userToUpdate.setGender(user.getGender());

        if (user.getWeight() != 0)
            userToUpdate.setWeight(user.getWeight());

        if (user.getHeight() != 0)
            userToUpdate.setHeight(user.getHeight());

        if (user.getBirthDate() != null)
            userToUpdate.setBirthDate(user.getBirthDate());

        if (user.getRestrictions() != null)
            userToUpdate.setRestrictions(user.getRestrictions());
    }
}
