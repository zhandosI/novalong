package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.dish.DishLight;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DishLightRepository extends JpaRepository<DishLight, Long> {
}
