package kz.teamvictus.novalong.rest.controller.endpoint;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.rest.service.IngredientService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.util.CsvUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/api/v1/files")
public class FileRestController extends BaseController {

    private final IngredientService ingredientService;

    @Autowired
    public FileRestController(IngredientService ingredientService) {
        this.ingredientService = ingredientService;
    }

    @PostMapping(value = "/ingredients/csv", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> uploadIngredientsCsv(@RequestParam(name = "ingredients") MultipartFile file) throws InternalException, IOException {
        ingredientService.loadFromCsv(CsvUtils.read(Object.class, file.getInputStream()));
        return buildResponse(success(), HttpStatus.OK);
    }

}
