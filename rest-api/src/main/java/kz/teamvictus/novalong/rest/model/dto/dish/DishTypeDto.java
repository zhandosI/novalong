package kz.teamvictus.novalong.rest.model.dto.dish;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DishTypeDto {

    private Long id;
    private String code;
    private String name;
    private boolean active;

}
