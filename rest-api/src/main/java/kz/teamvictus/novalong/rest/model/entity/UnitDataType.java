package kz.teamvictus.novalong.rest.model.entity;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "unit_data_types")
@Getter
@Setter
@ToString
public class UnitDataType extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "data_type_code")
    private String dataTypeCode;

}
