package kz.teamvictus.novalong.rest.model.entity.alternative;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "alternatives_categories")
@Getter
@Setter
@ToString
public class AlternativeCategory extends BaseEntity {

    @Column(name = "code")
    private String code;

    @Column(name = "active")
    private boolean active;
}
