package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.Program;
import kz.teamvictus.novalong.rest.model.entity.Purpose;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;

import java.util.List;

public interface ProgramService {

    Program findById(Long programId) throws InternalException, ResourceNotFoundException;
    Program findByPurpose(Purpose purpose) throws InternalException;
    List<Program> findAll() throws InternalException;
    List<Program> findActive() throws InternalException;

    Program save(Program program) throws InternalException;
    Program update(Long programId, Program program)
            throws InternalException, ResourceNotFoundException;
    void toggle(Long programId) throws InternalException, ResourceNotFoundException;
}
