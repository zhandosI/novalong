package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.dish.DishType;
import kz.teamvictus.novalong.rest.repository.DishTypeRepository;
import kz.teamvictus.novalong.rest.service.DishTypeService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DishTypeServiceImpl implements DishTypeService {

    private DishTypeRepository dishTypeRepository;

    @Autowired
    public void setDishTypeRepository(DishTypeRepository dishTypeRepository) {
        this.dishTypeRepository = dishTypeRepository;
    }

    @Override
    public DishType findByName(String name) throws InternalException {
        try {
            return dishTypeRepository.findByName(name);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<DishType> findAll() throws InternalException {
        try {
            return dishTypeRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<DishType> findActive() throws InternalException {
        try {
            return dishTypeRepository.findDishTypesByActive(true);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public DishType findById(Long id) throws InternalException {
        try {
            try {
                DishType dishType = dishTypeRepository.findOne(id);

                if (dishType == null)
                    throw new ResourceNotFoundException("There is no dishType with id: " + id, ErrorCode.RESOURCE_NOT_FOUND);

                return dishType;
            } catch (ResourceNotFoundException e) {
                throw e;
            } catch (Exception e) {
                throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
            }
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public DishType save(DishType dishType) throws InternalException {
        try {
            if (dishType.getName() == null)
                throw new InternalException("There is no dishType code " + dishType.getName(), ErrorCode.EMPTY_CODE);

            dishType.setActive(true);

            return dishTypeRepository.save(dishType);
        } catch (InternalException ie) {
            throw ie;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public DishType update(Long dishTypeId, DishType dishType) throws InternalException, ResourceNotFoundException {
        try {
            DishType dishTypeToUpdate = dishTypeRepository.getOne(dishTypeId);

            if (dishTypeToUpdate.getId() == null)
                throw new ResourceNotFoundException("There is no dishType with id: " + dishTypeId, ErrorCode.RESOURCE_NOT_FOUND);

            dishTypeToUpdate.setName(dishType.getName());

            dishTypeRepository.save(dishTypeToUpdate);

            return dishTypeRepository.findOne(dishTypeId);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void toggle(Long dishTypeId) throws InternalException, ResourceNotFoundException {
        try {
            DishType dishType = dishTypeRepository.getOne(dishTypeId);

            if (dishType.getId() == null)
                throw new ResourceNotFoundException("There is no dishType with id: " + dishTypeId, ErrorCode.RESOURCE_NOT_FOUND);

            dishType.setActive(!dishType.isActive());

            dishTypeRepository.save(dishType);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
