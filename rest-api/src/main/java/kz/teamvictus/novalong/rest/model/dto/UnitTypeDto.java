package kz.teamvictus.novalong.rest.model.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UnitTypeDto {

    private Long id;
    private String code;
    private String name;
    private Long dataTypeId;
    private UnitDataTypeDto dataType;
    private boolean active;

}
