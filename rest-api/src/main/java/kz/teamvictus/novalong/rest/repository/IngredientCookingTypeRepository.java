package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.CookingType;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientCookingType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IngredientCookingTypeRepository extends JpaRepository<IngredientCookingType, Long> {

    List<IngredientCookingType> findAllByIngredientId(Long ingredientId);
    List<IngredientCookingType> findAllByIngredientIdAndCookingType(Long ingredientId, CookingType cookingType);

}
