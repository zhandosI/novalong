package kz.teamvictus.novalong.rest.model.dto.ingredient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientCategory;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Getter
@Setter
@ToString
public class IngredientCategoryDto {

    private Long id;
    private String code;
    private String name;
    @JsonIgnoreProperties("parentCategory")
    private IngredientCategoryDto parentCategory;
    private Long parentCategoryId;

    @JsonIgnoreProperties("category")
    private Set<IngredientPropCategoryDto> ingredientProperties;
    private boolean hasChildCategories;
    private Boolean active;
    private boolean allergic;

}
