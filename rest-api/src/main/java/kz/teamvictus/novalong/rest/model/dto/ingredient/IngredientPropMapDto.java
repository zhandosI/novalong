package kz.teamvictus.novalong.rest.model.dto.ingredient;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class IngredientPropMapDto {

    private Long ingredientId;
    private IngredientPropCategoryDto property;
    private String value;

}
