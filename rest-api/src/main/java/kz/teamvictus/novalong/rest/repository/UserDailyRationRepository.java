package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.ration.UserDailyRation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserDailyRationRepository extends JpaRepository<UserDailyRation, Long> {

    List<UserDailyRation> findAllByUserRationId(Long rationId);
    List<UserDailyRation> findAllByUserRationIdAndDishTypeIdAndActive(Long rationId, Long dishTypeId, boolean active);
    List<UserDailyRation> findAllByUserRationIdAndDishTypeIdAndDayCodeAndActive(Long rationId, Long dishTypeId, String dayCode, boolean active);
    List<UserDailyRation> findAllByUserRationIdAndDayCodeAndActiveOrderByDishTypeId(Long rationId, String dayCode, boolean active);

    boolean existsAllByUserRationIdAndDayCodeAndActive(Long rationId, String dayCode, boolean active);

}
