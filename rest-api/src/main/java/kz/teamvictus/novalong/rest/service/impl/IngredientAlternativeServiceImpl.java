package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.alternative.AlternativeCategory;
import kz.teamvictus.novalong.rest.model.entity.alternative.AlternativeRelation;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientAlternative;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientAlternativesJoined;
import kz.teamvictus.novalong.rest.repository.IngredientAlternativeRepository;
import kz.teamvictus.novalong.rest.service.AlternativeCategoryService;
import kz.teamvictus.novalong.rest.service.AlternativeRelationService;
import kz.teamvictus.novalong.rest.service.IngredientAlternativeService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.UniqueConstraintViolationException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class IngredientAlternativeServiceImpl implements IngredientAlternativeService {

    private final IngredientAlternativeRepository alternativeRepository;
    private final AlternativeCategoryService categoryService;
    private final AlternativeRelationService relationService;

    @Autowired
    public IngredientAlternativeServiceImpl(IngredientAlternativeRepository alternativeRepository,
                                            AlternativeCategoryService categoryService,
                                            AlternativeRelationService relationService) {
        this.alternativeRepository = alternativeRepository;
        this.categoryService = categoryService;
        this.relationService = relationService;
    }

    @Override
    public List<IngredientAlternative> findAllByIngredient(Long id) throws InternalException {
        try {
            return alternativeRepository.findAllByIngredientId(id);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    /**
     * Shitty solution for joined response model
     *
     * TODO: Refactor later!
     */
    @Override
    public List<IngredientAlternativesJoined> findAllByIngredientJoinedByCategory(Long id) throws InternalException {
        try {
            List<IngredientAlternativesJoined> alternativesJoined = new ArrayList<>();

            for (AlternativeCategory category: categoryService.findAll()) {

                for (AlternativeRelation relation: relationService.findAll()) {
                    List<IngredientAlternative> alternatives =
                            alternativeRepository.findAllByIngredientIdAndCategoryAndRelation(id, category, relation);

                    alternativesJoined.add(new IngredientAlternativesJoined(category, relation,
                            alternatives.stream().map(IngredientAlternative::getAlternativeIngredient).collect(Collectors.toSet())));
                }
            }

            return alternativesJoined;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void save(IngredientAlternative alternative, Long ingredientId) throws InternalException, UniqueConstraintViolationException {
        try {
            alternative.setIngredientId(ingredientId);
            alternativeRepository.save(alternative);
        } catch (DataIntegrityViolationException e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                final String errorMsg = "The ingredient with id: " + ingredientId + ", " +
                                        "has already got this alternative with id: " + alternative.getAlternativeIngredient().getId() + ", " +
                                        "in this category with id: " + alternative.getCategory().getId();

                throw new UniqueConstraintViolationException(errorMsg, ErrorCode.UNIQUE_RESOURCE_CONFLICT);
            }
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void save(List<IngredientAlternative> alternatives, Long ingredientId) throws InternalException, UniqueConstraintViolationException {
        try {
            alternatives.forEach(a -> a.setIngredientId(ingredientId));
            alternativeRepository.save(alternatives);
        } catch (DataIntegrityViolationException e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                final String errorMsg = "The ingredient with id: " + ingredientId + ", " +
                                        "has already got this alternatives in this category";

                throw new UniqueConstraintViolationException(errorMsg, ErrorCode.UNIQUE_RESOURCE_CONFLICT);
            }
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
