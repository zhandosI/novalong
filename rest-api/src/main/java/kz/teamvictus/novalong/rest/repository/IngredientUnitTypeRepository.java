package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientUnitType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface IngredientUnitTypeRepository extends JpaRepository<IngredientUnitType, Long> {

    @Query("SELECT iu FROM IngredientUnitType iu INNER JOIN iu.type u WHERE iu.ingredientId = ?1 AND u.code = ?2")
    IngredientUnitType findByIngredientIdAndTypeCode(Long ingredientId, String code);

    @Query("SELECT iu FROM IngredientUnitType iu INNER JOIN iu.type u WHERE iu.ingredientId = ?1 AND u.code NOT IN ?2")
    List<IngredientUnitType> findAllByNotExistingType(Long ingredientId, Set<String> codes);

}
