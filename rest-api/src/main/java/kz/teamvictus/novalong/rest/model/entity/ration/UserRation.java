package kz.teamvictus.novalong.rest.model.entity.ration;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user_rations")
@Where(clause = "active = 1")
@Getter
@Setter
@ToString
public class UserRation extends BaseEntity {

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "program_id")
    private Long programId;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "total_calories_needed")
    private Float totalCaloriesNeeded;

    @Column(name = "active")
    private boolean active;

}
