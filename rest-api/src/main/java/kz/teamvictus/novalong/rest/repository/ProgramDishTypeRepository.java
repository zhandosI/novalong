package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.ProgramDishType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProgramDishTypeRepository extends JpaRepository<ProgramDishType, Long> {

    List<ProgramDishType> findAllByProgramIdOrderByDishTypeId(Long programId);
    List<ProgramDishType> findProgramDishTypesByActive(Boolean isActive);

}
