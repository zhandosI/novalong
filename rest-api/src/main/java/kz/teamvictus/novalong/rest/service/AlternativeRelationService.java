package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.alternative.AlternativeRelation;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;

import java.util.List;

public interface AlternativeRelationService {

    List<AlternativeRelation> findAll() throws InternalException;
    List<AlternativeRelation> findActive() throws InternalException;
    AlternativeRelation findById(Long id) throws InternalException;

    AlternativeRelation save(AlternativeRelation alternativeRelation) throws InternalException;
    AlternativeRelation update(Long alternativeRelationId, AlternativeRelation alternativeRelation)
            throws InternalException, ResourceNotFoundException;
    void toggle(Long alternativeRelationId) throws InternalException, ResourceNotFoundException;

}
