package kz.teamvictus.novalong.rest.repository.custom.impl;

import kz.teamvictus.novalong.rest.repository.custom.UserDailyRationCustomRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class UserDailyRationCustomRepositoryImpl implements UserDailyRationCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public double dailyCalories(Long rationId, String dayCode) {
        try {
            final String sql = "select sum(protein * 4 + carbohydrate * 4 + fat * 9) as calories from user_daily_rations dr\n" +
                    "where dr.user_ration_id = " + rationId + " and\n" +
                    "      dr.day_code = \'"+ dayCode +"\' and\n" +
                    "      dr.active = 1";

            Object o = em.createNativeQuery(sql).getSingleResult();
            return o == null ? 0 : (double) o;
        } catch (Exception e) {
            // TODO: Vergessen Sie nicht, diese Ausnahme zu protokollieren!
            e.printStackTrace();
            throw e;

        }
    }

    @Override
    @Transactional
    public void deactivateAll(Long rationId, String dayCode) {
        try {
            final String sql = "UPDATE user_daily_rations r " +
                               "SET r.active = false " +
                               "WHERE r.user_ration_id = " + rationId + " AND " +
                                     "r.day_code = '" + dayCode + "'";

            em.createNativeQuery(sql).executeUpdate();
        } catch (Exception e) {
            // TODO: Vergessen Sie nicht, diese Ausnahme zu protokollieren!
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    @Transactional
    public void deactivateAll(Long rationId, String dayCode, Long dishTypeId) {
        try {
            final String sql = "UPDATE user_daily_rations r " +
                    "SET r.active = false " +
                    "WHERE r.user_ration_id = " + rationId + " AND " +
                    "r.day_code = '" + dayCode + "' AND " +
                    "r.dish_type_id = " + dishTypeId;

            em.createNativeQuery(sql).executeUpdate();
        } catch (Exception e) {
            // TODO: Vergessen Sie nicht, diese Ausnahme zu protokollieren!
            e.printStackTrace();
            throw e;
        }
    }

}
