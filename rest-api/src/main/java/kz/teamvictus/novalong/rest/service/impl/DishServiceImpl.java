package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.HealthFactor;
import kz.teamvictus.novalong.rest.model.entity.dish.*;
import kz.teamvictus.novalong.rest.model.entity.ingredient.Ingredient;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientAlternative;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientUnitType;
import kz.teamvictus.novalong.rest.repository.DishMandatoryRepository;
import kz.teamvictus.novalong.rest.repository.DishRepository;
import kz.teamvictus.novalong.rest.repository.custom.DishCustomRepository;
import kz.teamvictus.novalong.rest.service.*;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import kz.teamvictus.novalong.util.CalculationUtil;
import kz.teamvictus.novalong.util.model.criteria.DishSearchCriteria;
import kz.teamvictus.novalong.util.model.SearchParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class DishServiceImpl implements DishService {

    private final DishRepository dishRepository;
    private final DishCustomRepository dishCustomRepository;
    private final DishMandatoryRepository dishMandatoryRepository;

    private final IngredientService ingredientService;
    private final DishIngredientService dishIngredientService;
    private final DishCategoryService dishCategoryService;
    private final DishIngredientAlternativeService ingredientAlternativeService;
    private final DishUnitTypeService dishUnitTypeService;
    private final DishTypeService dishTypeService;
    private final MealPlanAnalyticsService analyticsService;

    @Autowired
    public DishServiceImpl(DishRepository dishRepository,
                           DishCustomRepository dishCustomRepository,
                           DishMandatoryRepository dishMandatoryRepository,
                           IngredientService ingredientService,
                           DishIngredientService dishIngredientService,
                           DishCategoryService dishCategoryService,
                           DishIngredientAlternativeService ingredientAlternativeService,
                           DishUnitTypeService dishUnitTypeService,
                           DishTypeService dishTypeService,
                           MealPlanAnalyticsService analyticsService) {
        this.dishRepository = dishRepository;
        this.dishCustomRepository = dishCustomRepository;
        this.dishMandatoryRepository = dishMandatoryRepository;
        this.ingredientService = ingredientService;
        this.dishIngredientService = dishIngredientService;
        this.dishCategoryService = dishCategoryService;
        this.ingredientAlternativeService = ingredientAlternativeService;
        this.dishUnitTypeService = dishUnitTypeService;
        this.dishTypeService = dishTypeService;
        this.analyticsService = analyticsService;
    }

    @Override
    public Dish findById(Long id) throws InternalException, ResourceNotFoundException {
        try {
            Dish dish = dishRepository.findOne(id);

            if (dish == null)
                throw new ResourceNotFoundException("There is no dish with id: " + id, ErrorCode.RESOURCE_NOT_FOUND);

            return dish;
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<Dish> findAll() throws InternalException {
        try {
            return dishRepository.findAllByOrderByDateAddedDesc();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<Dish> findAll(DishType type) throws InternalException {
        try {
            return dishRepository.findAllByDishTypesContains(type);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<Dish> findAll(DishType type, DishCategory category) throws InternalException {
        try {
            return dishRepository.findAllByDishTypesContainsAndCategoriesContains(type, category);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<Dish> findAll(Map<String, String> params) throws InternalException, ResourceNotFoundException {
        try {
            if (!params.isEmpty()) {
                List<Dish> dishes = dishCustomRepository.findAllByCriteria(this.parseParams(params));

                if (dishes == null || dishes.isEmpty())
                    throw new ResourceNotFoundException("There is no such dishes", ErrorCode.RESOURCE_NOT_FOUND);

                return dishes;
            }

            return dishRepository.findAllByOrderByDateAddedDesc();
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<Dish> findRandom(Long dishTypeId, double calsNeeded, int dishesNum) throws InternalException {
        try {
            return dishCustomRepository.findRandomByDishType(dishTypeId, calsNeeded, dishesNum);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<Dish> findRandom(Long dishTypeId, double calsNeeded, String[] categoryCodes, Double[] ratios) throws InternalException {
        try {
            String categoryCodesStr = Arrays.stream(categoryCodes).map(Object::toString).collect(Collectors.joining(","));
            String ratiosStr = Arrays.stream(ratios).map(String::valueOf).collect(Collectors.joining(","));

            return dishCustomRepository.findRandomByDishTypeAndCategories(dishTypeId, calsNeeded, categoryCodesStr, ratiosStr);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public Dish save(Dish dish) throws InternalException {
        try {
            dish.setActive(true);
            Dish newDish = dishRepository.save(dish);

            if (dish.getUnitTypes() != null)
                this.dishUnitTypeService.save(dish.getUnitTypes(), newDish.getId());

            // This loop saves all the ingredients with counted weight according to unit type
            // and sums the total dish weight and macronutrients
            if (dish.getDishIngredients() != null)  {
                this.saveDishIngredients(dish.getDishIngredients(), newDish);

                // This loop saves all the alternatives of ingredients to particular dish
                List<DishIngredientAlternative> alternatives = new ArrayList<>();
                for (DishIngredient dishIngredient: dish.getDishIngredients()) {

                    for (IngredientAlternative alternative: ingredientService.findById(dishIngredient.getIngredient().getId()).getAlternatives()) {
                        alternatives.add(new DishIngredientAlternative(
                                newDish.getId(),
                                dishIngredient.getIngredient(),
                                alternative.getAlternativeIngredient(),
                                alternative.getCategory(),
                                alternative.getRelation()
                        ));
                    }
                }
                ingredientAlternativeService.save(alternatives, dish.getId());
            }

            return newDish;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public Dish update(Long dishId, Dish dish) throws InternalException, ResourceNotFoundException {
        try {
            Dish dishToUpdate = dishRepository.getOne(dishId);

            if (dishToUpdate.getId() == null)
                throw new ResourceNotFoundException("There is no dish with id: " + dishId, ErrorCode.RESOURCE_NOT_FOUND);

            this.update(dishToUpdate, dish);
            return dishRepository.findOne(dishId);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void delete(Long dishId) throws InternalException, ResourceNotFoundException {
        try {
            Dish dish = dishRepository.getOne(dishId);

            if (dish.getId() == null)
                throw new ResourceNotFoundException("There is no dish with id: " + dishId, ErrorCode.RESOURCE_NOT_FOUND);

            dish.setActive(false);
            dishRepository.save(dish);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public long count(Map<String, String> params) throws InternalException {
        try {
            return dishCustomRepository.countAllByCriteria(this.parseParams(params));
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public HealthFactor healthFactor(Long dishId) throws InternalException, ResourceNotFoundException {
        try {
            Dish dish = this.dishRepository.getOne(dishId);

            if (dish.getId() == null)
                throw new ResourceNotFoundException("There is no dish with id: " + dishId, ErrorCode.RESOURCE_NOT_FOUND);

            return analyticsService.healthFactor(dish);
        } catch (ResourceNotFoundException e) {
            throw e;
        }  catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<HealthFactor> healthFactor(Map<String, String> params) throws InternalException {
        try {
            List<Dish> dishes = this.findAll(params);

            if (dishes.isEmpty())
                return Collections.emptyList();

            return analyticsService.healthFactor(dishes);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public Map<Long, String> findAllPhotos(Map<String, String> params) throws InternalException {
        try {
            Map<Long, String> map = new HashMap<>();

            for(Dish dish:  dishCustomRepository.findAllByCriteria(this.parseParams(params))){
                map.put(dish.getId(), dish.getPhotoBase64());
            }

            return map;
        }  catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public String findPhoto(Long id) throws InternalException {
        try {
            return  dishRepository.getOne(id).getPhotoBase64();
        }  catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void savePhoto(MultipartFile file, Long id) throws InternalException {
        try {
            if (file == null)
                throw new InternalException("File does not exist", ErrorCode.FILE_NOT_FOUND);

            if (file.getBytes().length >= 1500000)
                throw new InternalException("File size must not exceed 1mb", ErrorCode.TOO_LARGE_FILE_SIZE);

            Dish dishToUpdate = dishRepository.getOne(id);
            dishToUpdate.setPhoto(file.getBytes());

            dishRepository.save(dishToUpdate);
        } catch (InternalException e) {
            throw e;
        }  catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void createFromIngredients(Long ingredientCategoryId, String unitTypeCode) throws InternalException, ResourceNotFoundException {
        try {
            final String uCodeTrimmed = unitTypeCode.trim().toUpperCase();
            List<Ingredient> ingredients = this.ingredientService.findAll(ingredientCategoryId, uCodeTrimmed);

            if (ingredients == null || ingredients.isEmpty()) {
                final String msg = "There is no ingredients with category Id: " + ingredientCategoryId + " and unitType: " + uCodeTrimmed;
                throw new ResourceNotFoundException(msg ,ErrorCode.RESOURCE_NOT_FOUND);
            }

            List<InternalException> caughtExceptions = new ArrayList<>();

            ingredients.forEach(i -> i.getUnitTypes().stream()
                .filter(iut -> iut.getType().getCode().equals(uCodeTrimmed))
                .findFirst()
                .ifPresent(iUnitType -> {
                    float weight = iUnitType.getWeight();
                    Long newDishId = this.dishMandatoryRepository.save(new DishMandatory(
                            i.getName(),
                            weight,
                            CalculationUtil.roundToTwoPoints((i.getProtein() / 100) * weight),
                            CalculationUtil.roundToTwoPoints((i.getFat() / 100) * weight),
                            CalculationUtil.roundToTwoPoints((i.getCarbohydrate() / 100) * weight),
                            true)).getId();
                    try {
                        this.dishUnitTypeService.save(new DishUnitType(newDishId, iUnitType.getType(), weight));
                    } catch (InternalException e) {
                        caughtExceptions.add(e);
                    }
                }));

            if (!caughtExceptions.isEmpty())
                throw caughtExceptions.get(0);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    private DishSearchCriteria parseParams(Map<String,String> params) throws InternalException {
        DishSearchCriteria criteria = new DishSearchCriteria();

        for (Map.Entry<String, String> entry: params.entrySet()) {
            switch (entry.getKey()) {
                case SearchParams.CATEGORIES:
                    Set<DishCategory> categories = new HashSet<>();
                    String[] categoriesArr = entry.getValue().split(",");
                    for (String categoryName : categoriesArr) {
                        categories.add(dishCategoryService.findByName(categoryName));
                    }

                    criteria.setCategories(categories);
                    break;

                case SearchParams.QUERY:
                    criteria.setSearchQuery(entry.getValue());
                    break;

                case SearchParams.DISH_TYPES:
                    String[] dishTypeNames = entry.getValue().split(",");

                    Set<DishType> dishTypes = new HashSet<>();
                    for (String dishTypeName : dishTypeNames)
                        dishTypes.add(dishTypeService.findByName(dishTypeName));

                    criteria.setDishTypes(dishTypes);
                    break;

                case SearchParams.SORT:
                    criteria.setSort(entry.getValue());
                    break;

                case SearchParams.LIMIT:
                    criteria.setLimit(Integer.parseInt(entry.getValue()));
                    break;

                case SearchParams.OFFSET:
                    criteria.setOffset(Integer.parseInt(entry.getValue()));
                    break;

                case SearchParams.COUNT:
                    criteria.setCount(true);
                    break;
            }
        }

        return criteria;
    }

    private void update(Dish dishToUpdate, Dish dish) throws InternalException {
        dishToUpdate.setName(dish.getName());
        dishToUpdate.setDescription(dish.getDescription());
        dishToUpdate.setRecipeDescription(dish.getRecipeDescription());
        dishToUpdate.setCategories(dish.getCategories());
        dishToUpdate.setDishTypes(dish.getDishTypes());
        dishToUpdate.setProtein(dish.getProtein());
        dishToUpdate.setCarbohydrate(dish.getCarbohydrate());
        dishToUpdate.setFat(dish.getFat());
        dishToUpdate.setWeight(dish.getWeight());
        dishToUpdate.setRecipeLink(dish.getRecipeLink());
        dishToUpdate.setCookingTypes(dish.getCookingTypes());

        if (dishToUpdate.getDishIngredients() != null && !dishToUpdate.getDishIngredients().isEmpty())
            dishIngredientService.delete(dishToUpdate.getDishIngredients());

        this.saveDishIngredients(dish.getDishIngredients(), dishToUpdate);
        dishToUpdate.setDishIngredients(null);

        if (dish.getUnitTypes() != null && !dish.getUnitTypes().isEmpty()) {
            if (dishToUpdate.getUnitTypes() != null && !dishToUpdate.getUnitTypes().isEmpty())
                dishUnitTypeService.delete(dishToUpdate.getUnitTypes());

            this.dishUnitTypeService.save(dish.getUnitTypes(), dishToUpdate.getId());
            dishToUpdate.setUnitTypes(null);
        }

        dishRepository.save(dishToUpdate);
    }

    private void update(Long dishId, float weight, float prots, float fats, float carbs) {
        Dish dish = dishRepository.getOne(dishId);
        dish.setWeight(weight);
        dish.setProtein(prots);
        dish.setCarbohydrate(carbs);
        dish.setFat(fats);
    }

    private void saveDishIngredients(Set<DishIngredient> dishIngredients, Dish newDish) throws InternalException {
        float dishWeight = 0, dishProts = 0, dishCarbs = 0, dishFats = 0;

        for (DishIngredient dishIngredient: dishIngredients) {
            for (IngredientUnitType ingredientUnitType : dishIngredient.getIngredient().getUnitTypes()) {
                if (dishIngredient.getUnitType().getCode().equals(ingredientUnitType.getType().getCode())) {
                    float totalWeight = (float) (ingredientUnitType.getWeight() * dishIngredient.getUnitAmount());
                    dishIngredient.setWeight(totalWeight);
                }
            }
            // Dish total weight
            dishWeight += dishIngredient.getWeight();

            // Dish total macronutrients rounded to 1 decimal point
            dishProts += dishIngredient.getIngredient().getProtein() / 100 * dishIngredient.getWeight();
            dishCarbs += dishIngredient.getIngredient().getCarbohydrate() / 100 * dishIngredient.getWeight();
            dishFats += dishIngredient.getIngredient().getFat() / 100 * dishIngredient.getWeight();

            dishIngredient.setDishId(newDish.getId());
        }
        this.update(
                newDish.getId(),
                CalculationUtil.roundToTwoPoints(dishWeight),
                CalculationUtil.roundToTwoPoints(dishProts),
                CalculationUtil.roundToTwoPoints(dishFats),
                CalculationUtil.roundToTwoPoints(dishCarbs)
        );
        dishIngredientService.save(dishIngredients);
    }

}
