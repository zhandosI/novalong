package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.WeekDay;
import kz.teamvictus.novalong.security.exceptions.InternalException;

import java.util.List;

public interface WeekDayService {

    WeekDay findByCode(String code) throws InternalException;
    List<WeekDay> findAll() throws InternalException;

}
