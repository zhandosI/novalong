package kz.teamvictus.novalong.rest.model.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Getter
@Setter
@ToString
public class ProgramDto {

    private Long id;
    private String name;
    private PurposeDto purpose;
    private Long purposeId;
    private double caloriesDifferenceCoefficient;
    private boolean active;
    @JsonIgnoreProperties("program")
    private Set<ProgramDishTypeDto> programDishTypes;

}
