package kz.teamvictus.novalong.rest.model.entity.dish;

import kz.teamvictus.novalong.rest.model.entity.CookingType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Base64;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "dishes")
@Where(clause = "active = 1 and user_id is null")
@Getter
@Setter
@ToString
public class Dish extends BaseDish {

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    @Lob
    private String description;

    @Column(name = "recipe_description")
    @Lob
    private String recipeDescription;

    @Column(name = "date_added")
    private Date dateAdded;

    @ManyToMany
    @Where(clause = "active = 1")
    @JoinTable(name = "dishes_dish_types",
            joinColumns = @JoinColumn(name = "dish_id"),
     inverseJoinColumns = @JoinColumn(name = "dish_type_id"))
    private Set<DishType> dishTypes;

    @ManyToMany
    @Where(clause = "active = 1")
    @JoinTable(name = "dishes_dish_categories",
            joinColumns = @JoinColumn(name = "dish_id"),
     inverseJoinColumns = @JoinColumn(name = "dish_category_id"))
    private Set<DishCategory> categories;

    @ManyToMany
    @JoinTable(name = "dishes_cooking_types",
            joinColumns = @JoinColumn(name = "dish_id"),
     inverseJoinColumns = @JoinColumn(name = "cooking_type_id"))
    private Set<CookingType> cookingTypes;

    @OneToMany(mappedBy = "dishId")
    private Set<DishIngredient> dishIngredients;

    @Column(name = "weight")
    private Float weight;

    @Column(name = "protein")
    private Float protein;

    @Column(name = "fat")
    private Float fat;

    @Column(name = "carbohydrate")
    private Float carbohydrate;

    @OneToMany(mappedBy = "dishId", fetch = FetchType.EAGER)
    private Set<DishUnitType> unitTypes;

    @Column(name = "recipe_link")
    private String recipeLink;

    @Column(name = "photo")
    @Lob
    private byte[] photo;

    @Transient
    private String photoBase64;

    @Column(name = "active")
    private Boolean active;

    @PostLoad
    public void setPhotoBase64() {
        if(this.photo != null)
            this.photoBase64 = Base64.getEncoder().encodeToString(this.photo);
    }

}
