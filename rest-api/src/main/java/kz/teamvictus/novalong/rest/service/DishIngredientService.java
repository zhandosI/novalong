package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.dish.DishIngredient;
import kz.teamvictus.novalong.security.exceptions.InternalException;

import java.util.Collection;

public interface DishIngredientService {

    void save(Collection<DishIngredient> dishIngredients) throws InternalException;
    void delete(Collection<DishIngredient> dishIngredients) throws InternalException;

}
