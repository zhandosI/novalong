package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.ration.UserDailyRation;
import kz.teamvictus.novalong.rest.repository.UserDailyRationRepository;
import kz.teamvictus.novalong.rest.repository.custom.UserDailyRationCustomRepository;
import kz.teamvictus.novalong.rest.service.UserDailyRationService;
import kz.teamvictus.novalong.rest.service.UserRationService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserDailyRationServiceImpl implements UserDailyRationService {

    private final UserDailyRationRepository dailyRationRepository;
    private final UserDailyRationCustomRepository dailyRationCustomRepository;
    private final UserRationService userRationService;

    @Autowired
    public UserDailyRationServiceImpl(UserDailyRationRepository dailyRationRepository,
                                      UserDailyRationCustomRepository dailyRationCustomRepository,
                                      UserRationService userRationService) {
        this.dailyRationRepository = dailyRationRepository;
        this.dailyRationCustomRepository = dailyRationCustomRepository;
        this.userRationService = userRationService;
    }

    @Override
    public List<UserDailyRation> findAll(Long userRationId, Long dishTypeId) throws InternalException {
        try {
            return dailyRationRepository.findAllByUserRationIdAndDishTypeIdAndActive(userRationId, dishTypeId, true);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<UserDailyRation> findAll(Long userRationId, String dayCode) throws InternalException {
        try {
            return dailyRationRepository.findAllByUserRationIdAndDayCodeAndActiveOrderByDishTypeId(userRationId, dayCode, true);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<UserDailyRation> findAll(Long userRationId, Long dishTypeId, String dayCode) throws InternalException {
        try {
            return dailyRationRepository.findAllByUserRationIdAndDishTypeIdAndDayCodeAndActive(userRationId, dishTypeId, dayCode, true);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void save(UserDailyRation dailyRation) throws InternalException {
        try {
            dailyRationRepository.save(dailyRation);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void save(Iterable<UserDailyRation> dailyRations) throws InternalException {
        try {
            dailyRationRepository.save(dailyRations);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void delete(Iterable<UserDailyRation> dailyRations) throws InternalException {
        try {
            dailyRationRepository.delete(dailyRations);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void delete(Long userRationId) throws InternalException {
        try {
            dailyRationRepository.delete(dailyRationRepository.findAllByUserRationId(userRationId));
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void deleteAll(Long rationId, String dayCode) throws InternalException {
        try {
            dailyRationCustomRepository.deactivateAll(rationId, dayCode);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void deleteAll(Long rationId, String dayCode, Long dishTypeId) throws InternalException {
        try {
            dailyRationCustomRepository.deactivateAll(rationId, dayCode, dishTypeId);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public double dailyCalories(Long rationId, String dayCode) throws InternalException {
        try {
            return dailyRationCustomRepository.dailyCalories(rationId, dayCode);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public boolean exists(Long userId, String dayCode) throws InternalException {
        try {
            Long rationId = this.userRationService.findIdByUser(userId);

            if (rationId == null)
                return false;

            return this.dailyRationRepository.existsAllByUserRationIdAndDayCodeAndActive(rationId, dayCode, true);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

}
