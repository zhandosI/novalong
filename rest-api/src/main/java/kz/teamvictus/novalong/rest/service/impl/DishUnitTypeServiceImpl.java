package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.dish.DishUnitType;
import kz.teamvictus.novalong.rest.repository.DishUnitTypeRepository;
import kz.teamvictus.novalong.rest.service.DishUnitTypeService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DishUnitTypeServiceImpl implements DishUnitTypeService {

    private DishUnitTypeRepository unitTypeRepository;

    @Autowired
    public void setUnitTypeRepository(DishUnitTypeRepository unitTypeRepository) {
        this.unitTypeRepository = unitTypeRepository;
    }

    @Override
    public void save(DishUnitType unitType) throws InternalException {
        try {
            this.unitTypeRepository.save(unitType);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void save(Iterable<DishUnitType> unitTypes, Long dishId) throws InternalException {
        try {
            unitTypes.forEach(u -> u.setDishId(dishId));
            this.unitTypeRepository.save(unitTypes);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void delete(Iterable<DishUnitType> unitTypes) throws InternalException {
        try {
            this.unitTypeRepository.delete(unitTypes);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

}
