package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.alternative.AlternativeRelation;
import kz.teamvictus.novalong.rest.repository.AlternativeRelationRepository;
import kz.teamvictus.novalong.rest.service.AlternativeRelationService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AlternativeRelationServiceImpl implements AlternativeRelationService {

    private AlternativeRelationRepository relationRepository;

    @Autowired
    public void setRelationRepository(AlternativeRelationRepository relationRepository) {
        this.relationRepository = relationRepository;
    }

    @Override
    public List<AlternativeRelation> findAll() throws InternalException {
        try {
            return relationRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<AlternativeRelation> findActive() throws InternalException {
        try {
            return relationRepository.findAlternativeRelationsByActive(true);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public AlternativeRelation findById(Long id) throws InternalException {
        try {
            try {
                AlternativeRelation alternativeRelation = relationRepository.findOne(id);

                if (alternativeRelation == null)
                    throw new ResourceNotFoundException("There is no alternativeRelation with id: " + id, ErrorCode.RESOURCE_NOT_FOUND);

                return alternativeRelation;
            } catch (ResourceNotFoundException e) {
                throw e;
            } catch (Exception e) {
                throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
            }
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public AlternativeRelation save(AlternativeRelation alternativeRelation) throws InternalException {
        try {
            if (alternativeRelation.getCode() == null)
                throw new InternalException("There is no alternativeRelation code " + alternativeRelation.getCode(), ErrorCode.EMPTY_CODE);

            alternativeRelation.setActive(true);

            return relationRepository.save(alternativeRelation);
        } catch (InternalException ie) {
            throw ie;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public AlternativeRelation update(Long alternativeRelationId, AlternativeRelation alternativeRelation) throws InternalException, ResourceNotFoundException {
        try {
            AlternativeRelation alternativeRelationToUpdate = relationRepository.getOne(alternativeRelationId);

            if (alternativeRelationToUpdate.getId() == null)
                throw new ResourceNotFoundException("There is no alternativeRelation with id: " + alternativeRelationId, ErrorCode.RESOURCE_NOT_FOUND);

            alternativeRelationToUpdate.setCode(alternativeRelation.getCode());

            relationRepository.save(alternativeRelationToUpdate);

            return relationRepository.findOne(alternativeRelationId);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void toggle(Long alternativeRelationId) throws InternalException, ResourceNotFoundException {
        try {
            AlternativeRelation alternativeRelation = relationRepository.getOne(alternativeRelationId);

            if (alternativeRelation.getId() == null)
                throw new ResourceNotFoundException("There is no alternativeRelation with id: " + alternativeRelationId, ErrorCode.RESOURCE_NOT_FOUND);

            alternativeRelation.setActive(!alternativeRelation.isActive());

            relationRepository.save(alternativeRelation);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
