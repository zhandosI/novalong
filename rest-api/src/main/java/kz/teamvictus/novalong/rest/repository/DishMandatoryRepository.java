package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.dish.DishMandatory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DishMandatoryRepository extends JpaRepository<DishMandatory, Long> {

    @Query(value = "select DISTINCT d.* from dishes d\n" +
            "            inner join dishes_dish_types ddt on d.id = ddt.dish_id\n" +
            "            inner join dishes_cooking_types dct on d.id = dct.dish_id\n" +
            "            inner join dishes_dish_categories ddc on d.id = ddc.dish_id\n" +
            "            inner join cooking_types ct on dct.cooking_type_id = ct.id\n" +
            "            where d.user_id is null and d.id <> :curDishId " +
            "                  and (:dishTypeId is null OR ddt.dish_type_id = :dishTypeId) and\n" +
            "                  ct.out_of_meal_plan = 0 and (ddc.dish_category_id in :dishCategoryIds OR :dishCategoryIds is null)",
            nativeQuery = true)
    List<DishMandatory> findDishReplaceOptions(@Param("curDishId") Long curDishId,
                                               @Param("dishTypeId") Long dishTypeId,
                                               @Param("dishCategoryIds") List<Long> dishCategoryIds);

    @Query(value = "select DISTINCT d.* from dishes d\n" +
            "            inner join dishes_dish_types ddt on d.id = ddt.dish_id\n" +
            "            where d.user_id = :userId and d.id <> :curDishId " +
            "                  and (:dishTypeId is null OR ddt.dish_type_id = :dishTypeId)",
            nativeQuery = true) // TODO check for dish category and cooking type ???
    List<DishMandatory> findUserDishReplaceOptions(@Param("userId") Long userId,
                                                   @Param("curDishId") Long curDishId,
                                                   @Param("dishTypeId") Long dishTypeId);
}
