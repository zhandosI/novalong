package kz.teamvictus.novalong.rest.model.entity.dish;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Lightened Entity class for table dishes,
 * containing only name and id
 */
@Entity
@Table(name = "dishes")
@Where(clause = "active = 1 and user_id is null")
@Getter
@Setter
@ToString
public class DishLight extends BaseDish {

    @Column(name = "name")
    private String name;

}
