package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.ingredient.Ingredient;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IngredientRepository extends JpaRepository<Ingredient, Long> {

    Ingredient findByCode(String code);
    List<Ingredient> findAllByCategory(IngredientCategory category);
    boolean existsByNameAndActive(String name, boolean active);

    @Query("SELECT i.id FROM Ingredient i WHERE i.name = ?1 AND i.active = ?2")
    Long findIdByNameAndActive(String name, boolean active);

    @Query(value = "SELECT * FROM ingredients i " +
            "inner join ingredients_unit_types iut on i.id = iut.ingredient_id\n" +
            "inner join unit_types ut on iut.unit_type_id = ut.id\n" +
            "where i.category_id =:categoryId and ut.code = :unitTypeCode", nativeQuery = true)
    List<Ingredient> findAllByCategoryAndUnitType(@Param("categoryId") Long categoryId, @Param("unitTypeCode") String unitTypeCode);

}
