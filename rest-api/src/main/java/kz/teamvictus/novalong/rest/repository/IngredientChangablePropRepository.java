package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientChangableProp;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IngredientChangablePropRepository extends JpaRepository<IngredientChangableProp, Long> {
}
