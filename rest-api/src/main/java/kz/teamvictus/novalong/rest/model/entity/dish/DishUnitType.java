package kz.teamvictus.novalong.rest.model.entity.dish;

import kz.teamvictus.novalong.rest.model.BaseEntity;
import kz.teamvictus.novalong.rest.model.entity.UnitType;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "dish_unit_types")
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class DishUnitType extends BaseEntity {

    @Column(name = "dish_id")
    private Long dishId;

    @ManyToOne
    @JoinColumn(name = "unit_type_id")
    private UnitType type;

    @Column(name = "weight")
    private float weight;

}
