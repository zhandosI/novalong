package kz.teamvictus.novalong.rest.controller;

import kz.teamvictus.novalong.security.model.response.*;
import kz.teamvictus.novalong.util.SerializationUtil;
import kz.teamvictus.novalong.util.UriUtil;
import kz.teamvictus.novalong.util.model.SearchParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Map;

@Slf4j
public class BaseController {

    private static final String SUCCESS = "success";
    private static final String ERROR = "error";

    protected ResponseEntity<?> buildResponse(Object data, HttpStatus status) {
        return new ResponseEntity<>(data, status);
    }

    protected ResponseEntity<?> buildResponse(HttpHeaders headers, HttpStatus status) {
        return new ResponseEntity<>(headers, status);
    }

    protected ResponseEntity<?> buildResponse(Object data, HttpHeaders headers, HttpStatus status) {
        return new ResponseEntity<>(data, headers, status);
    }

    protected URI buildLocationUri(Long id, String path) {
        return ServletUriComponentsBuilder
                .fromCurrentRequest().path(path)
                .buildAndExpand(id).toUri();
    }

    protected ResponseArray<?> success(List<?> data) {
        return new ResponseArray<>(SUCCESS, new ItemsResponse(data.size()), data);
    }

    protected ResponseObject<?> success(Object data) {
        return new ResponseObject<>(SUCCESS, new ItemsResponse(1), new LinksResponse.Builder(UriUtil.buildRequestUri()).build(), data);
    }

    protected ResponseObject<?> success(Object data, LinksResponse links) {
        return new ResponseObject<>(SUCCESS, new ItemsResponse(1), links, data);
    }

    protected ResponseArray<?> success(long count) {
        return new ResponseArray<>(SUCCESS, new ItemsResponse(count), null);
    }

    protected ResponseArray<?> success(List<?> data, long count, Map<String, String> params) {
        ItemsResponse items = new ItemsResponse(count);
        LinksResponse links =  new LinksResponse.Builder(UriUtil.buildRequestUri()).build();

        if (params.containsKey(SearchParams.LIMIT))
            items.setLimit(Integer.parseInt(params.get(SearchParams.LIMIT)));

        if (params.containsKey(SearchParams.OFFSET)) {
            int offset = Integer.parseInt(params.get(SearchParams.OFFSET));
            items.setOffset(offset);

            if (params.containsKey(SearchParams.LIMIT)) {
                int limit = Integer.parseInt(params.get(SearchParams.LIMIT));
                MultiValueMap<String, String> paginationParams = new LinkedMultiValueMap<>();

                paginationParams.add(SearchParams.LIMIT, String.valueOf(limit));
                paginationParams.add(SearchParams.OFFSET, String.valueOf(offset + limit));

                links.setNext(UriUtil.buildRequestUri(paginationParams));
            }
        }

        return new ResponseArray<>(SUCCESS, items, links, data);
    }

    protected ResponseAuth success(String token) {
        return new ResponseAuth(SUCCESS, token);
    }

    protected ResponseArray<?> success() {
        return new ResponseArray<>(SUCCESS, null);
    }

    protected ResponseError error(Exception e, Enum errorCode, String message) {
        log.error(SerializationUtil.getExceptionString(e));
        return new ResponseError(ERROR, errorCode.toString(), message);
    }

}
