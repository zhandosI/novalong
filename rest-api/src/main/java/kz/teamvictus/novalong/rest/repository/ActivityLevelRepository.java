package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.ActivityLevel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ActivityLevelRepository extends JpaRepository<ActivityLevel, Long> {

    List<ActivityLevel> findActivityLevelsByActive(Boolean isActive);
}
