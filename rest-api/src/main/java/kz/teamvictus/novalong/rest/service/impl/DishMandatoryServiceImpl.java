package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.dish.DishMandatory;
import kz.teamvictus.novalong.rest.repository.DishCategoryRepository;
import kz.teamvictus.novalong.rest.repository.DishMandatoryRepository;
import kz.teamvictus.novalong.rest.service.DishMandatoryService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DishMandatoryServiceImpl implements DishMandatoryService {

    private DishMandatoryRepository dishMandatoryRepository;
    private DishCategoryRepository dishCategoryRepository;

    @Autowired
    public void setDishMandatoryRepository(DishMandatoryRepository dishMandatoryRepository,
                                           DishCategoryRepository dishCategoryRepository) {
        this.dishMandatoryRepository = dishMandatoryRepository;
        this.dishCategoryRepository = dishCategoryRepository;
    }

    @Override
    public List<DishMandatory> findDishReplaceOptions(Long curDishId, Long dishTypeId) throws InternalException, ResourceNotFoundException {
        try {
            DishMandatory dish = this.dishMandatoryRepository.findOne(curDishId);

            if (dish == null)
                throw new ResourceNotFoundException("There is no dish with id: " + curDishId, ErrorCode.RESOURCE_NOT_FOUND);

            List<Long> dishCategoryIds = dishCategoryRepository.findAllIdsByDishId(dish.getId());


            return dishMandatoryRepository.findDishReplaceOptions(curDishId, dishTypeId, dishCategoryIds);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<DishMandatory> findUserDishReplaceOptions(Long userId, Long curDishId, Long dishTypeId) throws InternalException, ResourceNotFoundException {
        try {
            DishMandatory dish = this.dishMandatoryRepository.findOne(curDishId);

            if (dish == null)
                throw new ResourceNotFoundException("There is no dish with id: " + curDishId, ErrorCode.RESOURCE_NOT_FOUND);

            return dishMandatoryRepository.findUserDishReplaceOptions(userId, curDishId, dishTypeId);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
