package kz.teamvictus.novalong.rest.model.dto.dish;

import kz.teamvictus.novalong.rest.model.dto.CookingTypeDto;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.Set;

@Getter
@Setter
@ToString
public class DishMandatoryDto {

    private Long id;
    private Long userId;
    private String name;
    private Double weight;
    private Float protein;
    private Float fat;
    private Float carbohydrate;
    private Float calories;
    private Boolean active;

}
