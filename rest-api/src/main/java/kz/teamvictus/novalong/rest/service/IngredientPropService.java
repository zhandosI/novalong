package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientProp;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;

import java.util.List;

public interface IngredientPropService {

    List<IngredientProp> findAll() throws InternalException;
    List<IngredientProp> findActive() throws InternalException;
    IngredientProp findById(Long id) throws InternalException;

    IngredientProp save(IngredientProp ingredientProp) throws InternalException;
    IngredientProp update(Long ingredientPropId, IngredientProp ingredientProp)
            throws InternalException, ResourceNotFoundException;
    void toggle(Long ingredientPropId) throws InternalException, ResourceNotFoundException;
}
