package kz.teamvictus.novalong.rest.controller.endpoint;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.rest.model.dto.UserDto;
import kz.teamvictus.novalong.rest.model.entity.User;
import kz.teamvictus.novalong.rest.service.*;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.exceptions.UniqueConstraintViolationException;
import kz.teamvictus.novalong.security.model.response.LinksResponse;
import kz.teamvictus.novalong.security.service.TokenService;
import kz.teamvictus.novalong.util.mapper.impl.DishMandatoryMapper;
import kz.teamvictus.novalong.util.DateUtil;
import kz.teamvictus.novalong.util.UriUtil;
import kz.teamvictus.novalong.util.mapper.impl.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/v1/users")
public class UserRestController extends BaseController {

    private final UserMapper userMapper;
    private final DishMandatoryMapper dishMandatoryMapper;
    private final UserService userService;
    private final MealPlanService mealPlanService;
    private final UserDailyRationService dailyRationService;
    private final DishMandatoryService dishMandatoryService;
    private final TokenService tokenService;

    @Autowired
    public UserRestController(UserMapper userMapper,
                              DishMandatoryMapper dishMandatoryMapper,
                              UserService userService,
                              MealPlanService mealPlanService,
                              DishMandatoryService dishMandatoryService,
                              UserDailyRationService dailyRationService,
                              TokenService tokenService) {
        this.userMapper = userMapper;
        this.dishMandatoryMapper = dishMandatoryMapper;
        this.userService = userService;
        this.mealPlanService = mealPlanService;
        this.dishMandatoryService = dishMandatoryService;
        this.dailyRationService = dailyRationService;
        this.tokenService = tokenService;
    }

    @GetMapping(value = "/{id}",  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getUser(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        return buildResponse(success(userMapper.toDto(userService.findById(id))), HttpStatus.OK);
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAllUsers() throws InternalException {
        return buildResponse(success(userMapper.toDto(userService.findAll())), HttpStatus.OK);
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveUser(@RequestBody UserDto user) throws InternalException, UniqueConstraintViolationException {
        User newUser = userService.save(userMapper.toEntity(user));

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(buildLocationUri(newUser.getId(), "/{id}"));

        return buildResponse(success(tokenService.generateToken(newUser)), headers, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveUser(@RequestBody UserDto user,
                                      @PathVariable("id") Long userId) throws InternalException, UniqueConstraintViolationException {
        User updatedUser = userService.update(userId, userMapper.toEntity(user));
        return buildResponse(success(userMapper.toDto(updatedUser)), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/rations",  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getUserRations(@RequestParam Map<String, String> params,
                                            @PathVariable("id") Long userId) throws InternalException, ResourceNotFoundException {
        MultiValueMap<String, String> nextParams = new LinkedMultiValueMap<>(),
                                      prevParams = new LinkedMultiValueMap<>();
        String nextDayCode = DateUtil.getNextDayCode(params.get("day")),
               prevDayCode = DateUtil.getPrevDayCode(params.get("day"));

        nextParams.add("day", nextDayCode);
        prevParams.add("day", prevDayCode);
        nextParams.add("regenerate", String.valueOf(!dailyRationService.exists(userId, nextDayCode)));
        prevParams.add("regenerate", String.valueOf(!dailyRationService.exists(userId, prevDayCode)));

        LinksResponse links = new LinksResponse.Builder(UriUtil.buildRequestUri())
                .withNext(UriUtil.buildRequestUri(nextParams))
                .withPrev(UriUtil.buildRequestUri(prevParams))
                .build();

        return buildResponse(success(this.mealPlanService.rationByParams(userId, params), links), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}/dishes/replace/{dish_id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getUserDishReplaceOptions(@PathVariable("id") Long id,
                                          @PathVariable("dish_id") Long dishId,
                                          @RequestParam(name = "dish_type_id", required = false) Long dishTypeId)
            throws InternalException, ResourceNotFoundException {

        return buildResponse(
                success(dishMandatoryMapper.toDto(dishMandatoryService.findUserDishReplaceOptions(id, dishId, dishTypeId))),
                HttpStatus.OK);
    }
}
