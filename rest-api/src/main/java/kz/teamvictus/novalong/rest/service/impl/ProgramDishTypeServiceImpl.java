package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.ProgramDishType;
import kz.teamvictus.novalong.rest.repository.ProgramDishTypeRepository;
import kz.teamvictus.novalong.rest.service.ProgramDishTypeService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProgramDishTypeServiceImpl implements ProgramDishTypeService {

    private ProgramDishTypeRepository programDishTypeRepository;

    @Autowired
    public void setProgramDishTypeRepository(ProgramDishTypeRepository programDishTypeRepository) {
        this.programDishTypeRepository = programDishTypeRepository;
    }

    @Override
    public List<ProgramDishType> findAllByProgram(Long programId) throws InternalException {
        try {
            return programDishTypeRepository.findAllByProgramIdOrderByDishTypeId(programId);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<ProgramDishType> findAll() throws InternalException {
        try {
            return programDishTypeRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<ProgramDishType> findActive() throws InternalException {
        try {
            return programDishTypeRepository.findProgramDishTypesByActive(true);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ProgramDishType findById(Long id) throws InternalException {
        try {
            try {
                ProgramDishType programDishType = programDishTypeRepository.findOne(id);

                if (programDishType == null)
                    throw new ResourceNotFoundException("There is no programDishType with id: " + id, ErrorCode.RESOURCE_NOT_FOUND);

                return programDishType;
            } catch (ResourceNotFoundException e) {
                throw e;
            } catch (Exception e) {
                throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
            }
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ProgramDishType save(ProgramDishType programDishType) throws InternalException {
        try {
            programDishType.setActive(true);

            return programDishTypeRepository.save(programDishType);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public ProgramDishType update(Long programDishTypeId, ProgramDishType programDishType) throws InternalException, ResourceNotFoundException {
        try {
            ProgramDishType programDishTypeToUpdate = programDishTypeRepository.getOne(programDishTypeId);

            if (programDishTypeToUpdate.getId() == null)
                throw new ResourceNotFoundException("There is no programDishType with id: " + programDishTypeId, ErrorCode.RESOURCE_NOT_FOUND);

            programDishTypeToUpdate.setProgramId(programDishType.getProgramId());
            programDishTypeToUpdate.setDishTypeId(programDishType.getDishTypeId());
            programDishTypeToUpdate.setCaloriesPercentage(programDishType.getCaloriesPercentage());

            programDishTypeRepository.save(programDishTypeToUpdate);

            return programDishTypeRepository.findOne(programDishTypeId);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void toggle(Long programDishTypeId) throws InternalException, ResourceNotFoundException {
        try {
            ProgramDishType programDishType = programDishTypeRepository.getOne(programDishTypeId);

            if (programDishType.getId() == null)
                throw new ResourceNotFoundException("There is no programDishType with id: " + programDishTypeId, ErrorCode.RESOURCE_NOT_FOUND);

            programDishType.setActive(!programDishType.isActive());

            programDishTypeRepository.save(programDishType);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
