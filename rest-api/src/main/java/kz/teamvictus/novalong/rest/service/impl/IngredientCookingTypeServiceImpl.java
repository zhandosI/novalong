package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.CookingType;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientChangablePropDifferenceJoined;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientCookingType;
import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientCookingTypeJoined;
import kz.teamvictus.novalong.rest.repository.IngredientCookingTypeRepository;
import kz.teamvictus.novalong.rest.service.CookingTypeService;
import kz.teamvictus.novalong.rest.service.IngredientCookingTypeService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.UniqueConstraintViolationException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Service
public class IngredientCookingTypeServiceImpl implements IngredientCookingTypeService {

    private final IngredientCookingTypeRepository ingredientCookingTypeRepository;
    private final CookingTypeService cookingTypeService;

    @Autowired
    public IngredientCookingTypeServiceImpl(IngredientCookingTypeRepository ingredientCookingTypeRepository,
                                            CookingTypeService cookingTypeService) {
        this.ingredientCookingTypeRepository = ingredientCookingTypeRepository;
        this.cookingTypeService = cookingTypeService;
    }

    @Override
    public List<IngredientCookingType> findAllByIngredient(Long ingredientId) throws InternalException {
        try {
            return ingredientCookingTypeRepository.findAllByIngredientId(ingredientId);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    /**
     * TODO: Mappings like this could be done better, later...
     */
    @Override
    public List<IngredientCookingTypeJoined> findAllByIngredientJoined(Long ingredientId) throws InternalException {
        try {
            List<IngredientCookingTypeJoined> ingredientCookingTypeJoined = new ArrayList<>();

            for (CookingType cookingType: cookingTypeService.findAll()) {
                List<IngredientCookingType> ingredientCookingTypes = ingredientCookingTypeRepository.findAllByIngredientIdAndCookingType(ingredientId, cookingType);
                Set<IngredientChangablePropDifferenceJoined> changablePropDifferenceSet = new LinkedHashSet<>();

                if (!ingredientCookingTypes.isEmpty()) {
                    for (IngredientCookingType ingredientCookingType: ingredientCookingTypes) {
                        IngredientChangablePropDifferenceJoined changablePropDifference = new IngredientChangablePropDifferenceJoined(
                                ingredientCookingType.getChangableProp(),
                                ingredientCookingType.getDifferenceCoefficient());

                        changablePropDifferenceSet.add(changablePropDifference);
                    }

                    ingredientCookingTypeJoined.add(new IngredientCookingTypeJoined(cookingType, changablePropDifferenceSet));
                }
            }

            return ingredientCookingTypeJoined;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void save(IngredientCookingType cookingType, Long ingredientId) throws InternalException, UniqueConstraintViolationException {
        try {
            cookingType.setIngredientId(ingredientId);
            ingredientCookingTypeRepository.save(cookingType);
        } catch (DataIntegrityViolationException e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                final String errorMsg = "The ingredient with id: " + ingredientId + ", " +
                        "has already got this property for this cooking type";

                throw new UniqueConstraintViolationException(errorMsg, ErrorCode.UNIQUE_RESOURCE_CONFLICT);
            }
        } catch ( Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void save(List<IngredientCookingType> cookingTypes, Long ingredientId) throws InternalException, UniqueConstraintViolationException {
        try {
            cookingTypes.forEach(type -> type.setIngredientId(ingredientId));
            ingredientCookingTypeRepository.save(cookingTypes);
        } catch (DataIntegrityViolationException e) {
            if (e.getCause() instanceof ConstraintViolationException) {
                final String errorMsg = "The ingredient with id: " + ingredientId + ", " +
                        "has already got this properties for this cooking types";

                throw new UniqueConstraintViolationException(errorMsg, ErrorCode.UNIQUE_RESOURCE_CONFLICT);
            }
        } catch ( Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
