package kz.teamvictus.novalong.rest.repository.custom.impl;

import kz.teamvictus.novalong.rest.model.entity.ingredient.Ingredient;
import kz.teamvictus.novalong.rest.repository.custom.IngredientCustomRepository;
import kz.teamvictus.novalong.util.model.criteria.IngredientSearchCriteria;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class IngredientCustomRepositoryImpl implements IngredientCustomRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Ingredient> findAllByCriteria(IngredientSearchCriteria ingredientCriteria) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Ingredient> query = builder.createQuery(Ingredient.class);
        Root<Ingredient> root = query.from(Ingredient.class);

        List<Predicate> predicates = new ArrayList<>();

        // SEARCH BY NAME CONTAINING
        if (ingredientCriteria.getName() != null) {
            final String PERCENT_SIGN = "%";
            predicates.add(builder.like(root.get("name"), PERCENT_SIGN + ingredientCriteria.getName() + PERCENT_SIGN));
        }

        // SEARCH BY CATEGORY
        if (ingredientCriteria.getCategory() != null) {
            predicates.add(builder.equal(root.join("category").get("id"), ingredientCriteria.getCategory()));
        }

        // ORDER BY
        String sort = ingredientCriteria.getSort();

        if (sort != null && !sort.isEmpty()) {
            String[] attributes = sort.split(","); // +name,-category.name,+glycemicIndex
            List<Order> orders = new ArrayList<>();

            for (String attr: attributes) {
                String attrSubstr = attr.substring(1); // ex: [+name | -name] => [name]
                Expression<?> expression = null;

                if (Arrays.asList(new String[]{"id", "name", "glycemicIndex"}).contains(attrSubstr)) {
                    expression = root.get(attrSubstr);
                } else if (attrSubstr.equals("category.name")) {
                    expression = root.join("category").get("name");
                }

                if (expression != null) {
                    orders.add(attr.startsWith("-") ? builder.desc(expression) : builder.asc(expression));
                }
            }

            if (!orders.isEmpty())
                query.orderBy(orders);
        }

        predicates.add(builder.equal(root.get("active"), true));

        query.select(root).where(predicates.toArray(new Predicate[]{}));

        TypedQuery<Ingredient> typedQuery = em.createQuery(query);

        if (ingredientCriteria.getOffset() != null)
            typedQuery.setFirstResult(ingredientCriteria.getOffset());

        if (ingredientCriteria.getLimit() != null)
            typedQuery.setMaxResults(ingredientCriteria.getLimit());

        return typedQuery.getResultList();
    }

    @Override
    public Long countAllByCriteria(IngredientSearchCriteria ingredientCriteria) {
        return this.criteriaCount(ingredientCriteria);
    }

    private Long criteriaCount(IngredientSearchCriteria ingredientCriteria)  {

        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<Ingredient> root = query.from(Ingredient.class);

        List<Predicate> predicates = new ArrayList<>();

        // SEARCH BY NAME CONTAINING
        if (ingredientCriteria.getName() != null) {
            final String PERCENT_SIGN = "%";
            predicates.add(builder.like(root.get("name"), PERCENT_SIGN + ingredientCriteria.getName() + PERCENT_SIGN));
        }

        // SEARCH BY CATEGORY
        if (ingredientCriteria.getCategory() != null) {
            predicates.add(builder.equal(root.join("category").get("id"), ingredientCriteria.getCategory()));
        }

        predicates.add(builder.equal(root.get("active"), true));

        query.select(builder.count(root)).where(predicates.toArray(new Predicate[]{}));

        return em.createQuery(query).getSingleResult();
    }

}
