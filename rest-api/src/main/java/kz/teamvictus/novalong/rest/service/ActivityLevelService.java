package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.ActivityLevel;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;

import java.util.List;

public interface ActivityLevelService {

    ActivityLevel findById(Long id) throws InternalException;
    List<ActivityLevel> findAll() throws InternalException;
    List<ActivityLevel> findActive() throws InternalException;

    ActivityLevel save(ActivityLevel activityLevel) throws InternalException;
    ActivityLevel update(Long activityLevelId, ActivityLevel activityLevel) throws InternalException, ResourceNotFoundException;
    void toggle(Long activityLevelId) throws InternalException, ResourceNotFoundException;
}
