package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientOptParamName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IngredientOptParamNameRepository extends JpaRepository<IngredientOptParamName, Long> {

    IngredientOptParamName findByCode(String code);

}
