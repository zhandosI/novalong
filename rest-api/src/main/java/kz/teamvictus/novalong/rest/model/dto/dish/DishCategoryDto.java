package kz.teamvictus.novalong.rest.model.dto.dish;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DishCategoryDto {

    private Long id;
    private String name;
    private boolean active;

}
