package kz.teamvictus.novalong.rest.controller.endpoint.catalog;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.rest.model.dto.ActivityLevelDto;
import kz.teamvictus.novalong.rest.model.entity.ActivityLevel;
import kz.teamvictus.novalong.rest.service.ActivityLevelService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.util.mapper.impl.ActivityLevelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/activity_levels")
public class ActivityLevelRestController extends BaseController {

    private final ActivityLevelService activityLevelService;
    private final ActivityLevelMapper activityLevelMapper;

    @Autowired
    public ActivityLevelRestController(ActivityLevelService activityLevelService,
                                       ActivityLevelMapper activityLevelMapper) {
        this.activityLevelService = activityLevelService;
        this.activityLevelMapper = activityLevelMapper;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getActivityLevels() throws InternalException {
        return buildResponse(success(activityLevelMapper.toDto(activityLevelService.findAll())), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getActivityLevel(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        return buildResponse(success(activityLevelMapper.toDto(activityLevelService.findById(id))), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveActivityLevel(@RequestBody ActivityLevelDto activityLevelDto) throws InternalException {
        ActivityLevel newActivityLevel = activityLevelService.save(activityLevelMapper.toEntity(activityLevelDto));

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(buildLocationUri(newActivityLevel.getId(), "/{id}"));

        return buildResponse(success(activityLevelMapper.toDto(newActivityLevel)), headers, HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> toggleActivityLevel(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        activityLevelService.toggle(id);

        return buildResponse(success(), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> updateActivityLevel(@RequestBody ActivityLevelDto activityLevelDto,
                                        @PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        ActivityLevel updatedActivityLevel = activityLevelService.update(id, activityLevelMapper.toEntity(activityLevelDto));

        return buildResponse(success(activityLevelMapper.toDto(updatedActivityLevel)), HttpStatus.OK);
    }

}
