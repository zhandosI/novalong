package kz.teamvictus.novalong.rest.model.dto.dish;

import kz.teamvictus.novalong.rest.model.dto.UnitTypeDto;
import kz.teamvictus.novalong.rest.model.dto.ingredient.IngredientDto;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DishIngredientDto {

    private IngredientDto ingredient;
    private float weight;
    private UnitTypeDto unitType;
    private double unitAmount;

}
