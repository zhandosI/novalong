package kz.teamvictus.novalong.rest.controller.endpoint;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.rest.model.dto.ingredient.*;
import kz.teamvictus.novalong.rest.model.entity.ingredient.Ingredient;
import kz.teamvictus.novalong.rest.service.*;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.LackOfDataException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.exceptions.UniqueConstraintViolationException;
import kz.teamvictus.novalong.util.mapper.impl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/ingredients")
public class IngredientRestController extends BaseController {

    private final IngredientMapper ingredientMapper;
    private final IngredientCategoryMapper categoryMapper;
    private final UnitTypeMapper unitTypeMapper;
    private final IngredientAlternativeMapper alternativeMapper;
    private final IngredientPropertiesMapper propertiesMapper;
    private final IngredientChangablePropMapper changablePropMapper;
    private final IngredientCookingTypeMapper cookingTypeMapper;
    private final IngredientOptParamNameMapper optParamNameMapper;
    private final IngredientOptParamValueMapper optParamValueMapper;

    private final IngredientService ingredientService;
    private final IngredientAlternativeService alternativeService;
    private final IngredientCategoryService categoryService;
    private final UnitTypeService unitTypeService;
    private final IngredientPropMapService propMapService;
    private final IngredientChangablePropService changablePropService;
    private final IngredientCookingTypeService cookingTypeService;
    private final IngredientOptParamNameService optParamNameService;
    private final IngredientOptParamValueService optParamValueService;

    @Autowired
    public IngredientRestController(IngredientMapper ingredientMapper,
                                    IngredientCategoryMapper categoryMapper,
                                    UnitTypeMapper unitTypeMapper,
                                    IngredientAlternativeMapper alternativeMapper,
                                    IngredientPropertiesMapper propertiesMapper,
                                    IngredientChangablePropMapper changablePropMapper,
                                    IngredientCookingTypeMapper cookingTypeMapper,
                                    IngredientOptParamNameMapper optParamNameMapper,
                                    IngredientOptParamValueMapper optParamValueMapper,
                                    IngredientService ingredientService,
                                    IngredientAlternativeService alternativeService,
                                    IngredientCategoryService categoryService,
                                    UnitTypeService unitTypeService,
                                    IngredientPropMapService propMapService,
                                    IngredientChangablePropService changablePropService,
                                    IngredientCookingTypeService cookingTypeService,
                                    IngredientOptParamNameService optParamNameService,
                                    IngredientOptParamValueService optParamValueService) {
        this.ingredientMapper = ingredientMapper;
        this.categoryMapper = categoryMapper;
        this.unitTypeMapper = unitTypeMapper;
        this.alternativeMapper = alternativeMapper;
        this.propertiesMapper = propertiesMapper;
        this.changablePropMapper = changablePropMapper;
        this.cookingTypeMapper = cookingTypeMapper;
        this.optParamNameMapper = optParamNameMapper;
        this.optParamValueMapper = optParamValueMapper;
        this.ingredientService = ingredientService;
        this.alternativeService = alternativeService;
        this.categoryService = categoryService;
        this.unitTypeService = unitTypeService;
        this.propMapService = propMapService;
        this.changablePropService = changablePropService;
        this.cookingTypeService = cookingTypeService;
        this.optParamNameService = optParamNameService;
        this.optParamValueService = optParamValueService;
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAllIngredients(@RequestParam Map<String, String> params) throws InternalException, ResourceNotFoundException {
        return buildResponse(success(ingredientMapper.toDto(ingredientService.findAll(params)), ingredientService.count(params), params), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getIngredient(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        return buildResponse(success(ingredientMapper.toDto(ingredientService.findById(id))), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveIngredient(@RequestBody IngredientDto ingredient,
                                            @RequestParam(name = "is_dish", required = false) Boolean isDish) throws InternalException, LackOfDataException {
        Ingredient newIngredient = ingredientService.save(ingredientMapper.toEntity(ingredient), isDish);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(buildLocationUri(newIngredient.getId(), "/{id}"));

        return buildResponse(success(ingredientMapper.toDto(newIngredient)), headers, HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> updateIngredient(@RequestBody IngredientDto ingredient,
                                              @PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        Ingredient updatedIngredient = ingredientService.update(id, ingredientMapper.toEntity(ingredient));
        return buildResponse(success(ingredientMapper.toDto(updatedIngredient)), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> removeIngredient(@PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        ingredientService.delete(id);
        return buildResponse(success(), HttpStatus.OK);
    }

    /******************** CATEGORIES ********************/

    @GetMapping(value = "/categories", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAllIngredientsCategories(@RequestParam Map<String, String> params) throws InternalException {
        return buildResponse(success(categoryMapper.toDto(categoryService.findAll(params))), HttpStatus.OK);
    }

    @GetMapping(value = "/categories/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getIngredientCategory(@RequestParam(name = "children", required = false) String children,
                                                   @PathVariable("id") Long id) throws InternalException, ResourceNotFoundException {
        if (children != null)
            return buildResponse(success(categoryMapper.toDto(categoryService.findChild(id))), HttpStatus.OK);

        return buildResponse(success(categoryMapper.toDto(categoryService.findById(id))), HttpStatus.OK);
    }

    /******************** UNIT TYPES  ********************/

    @GetMapping(value = "/unit_types", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAllIngredientsUnitTypes() throws InternalException {
        return buildResponse(success(unitTypeMapper.toDto(unitTypeService.findAllIngredient())), HttpStatus.OK);
    }

    /******************** CHANGABLE PROPS BY CATEGORY  ********************/

    @GetMapping(value = "/changable_props", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAllIngredientsChangableProps() throws InternalException {
        return buildResponse(success(changablePropMapper.toDto(changablePropService.findAll())), HttpStatus.OK);
    }

    /******************** OPTIONAL PROPS ********************/

    @GetMapping(value = "/{id}/optional_props", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getIngredientOptionalProps(@PathVariable("id") Long id) throws InternalException {
        return buildResponse(success(optParamValueMapper.toDto(optParamValueService.findByIngredient(id))), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/{id}/optional_props", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveIngredientOptionalProps(@RequestBody List<IngredientOptParamValueDto> optParams,
                                                         @PathVariable("id") Long id) throws InternalException {
        optParamValueService.save(optParamValueMapper.toEntity(optParams), id);
        return buildResponse(success(), HttpStatus.CREATED);
    }

    @GetMapping(value = "/optional_props", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getAllIngredientsOptionalProps() throws InternalException {
        return buildResponse(success(optParamNameMapper.toDto(optParamNameService.findAll())), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/optional_props/{propId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> updateIngredientOptionalProp(@RequestBody IngredientOptParamValueDto optParam,
                                                          @PathVariable("propId") Long propId) throws InternalException, ResourceNotFoundException {
        optParamValueService.update(optParamValueMapper.toEntity(optParam), propId);
        return buildResponse(success(), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/optional_props/{propId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> deleteIngredientOptionalProp(@PathVariable("propId") Long propId) throws InternalException, ResourceNotFoundException {
        optParamValueService.delete(propId);
        return buildResponse(success(), HttpStatus.OK);
    }

    /******************** ALTERNATIVES ********************/

    @GetMapping(value = "/{id}/alternatives", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getIngredientAlternatives(@RequestParam(name = "joined", required = false) boolean joined,
                                                       @PathVariable("id") Long id) throws InternalException {
        List<?> data;
        if (joined) data = alternativeMapper.toDtoJoined(alternativeService.findAllByIngredientJoinedByCategory(id));
        else data = alternativeMapper.toDto(alternativeService.findAllByIngredient(id));

        return buildResponse(success(data), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/{id}/alternatives", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveIngredientAlternative(@RequestBody List<IngredientAlternativeDto> alternatives,
                                                       @PathVariable("id") Long id) throws InternalException, UniqueConstraintViolationException {
        alternativeService.save(alternativeMapper.toEntity(alternatives), id);
        return buildResponse(success(), HttpStatus.CREATED);
    }

    /******************** PROPERTIES BY CATEGORY ********************/

    @GetMapping(value = "/{id}/properties", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getIngredientProperties(@PathVariable("id") Long id) throws InternalException {
        return buildResponse(success(propertiesMapper.toDto(propMapService.findAllByIngredient(id))), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/{id}/properties", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveIngredientProperties(@RequestBody List<IngredientPropMapDto> properties,
                                                      @PathVariable("id") Long id) throws InternalException {
        propMapService.save(propertiesMapper.toEntity(properties), id);
        return buildResponse(success(), HttpStatus.CREATED);
    }

    /******************** COOKING TYPES ********************/

    @GetMapping(value = "/{id}/cookingtypes", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> getIngredientCookingTypes(@RequestParam(name = "joined", required = false) boolean joined,
                                                       @PathVariable("id") Long id) throws InternalException {
        List<?> data;
        if (joined) data = cookingTypeMapper.toDtoJoined(cookingTypeService.findAllByIngredientJoined(id));
        else data = cookingTypeMapper.toDto(cookingTypeService.findAllByIngredient(id));

        return buildResponse(success(data), HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/{id}/cookingtypes", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> saveIngredientCookingTypes(@RequestBody List<IngredientCookingTypeDto> cookingType,
                                                        @PathVariable("id") Long id) throws InternalException, UniqueConstraintViolationException {
        cookingTypeService.save(cookingTypeMapper.toEntity(cookingType), id);
        return buildResponse(success(), HttpStatus.CREATED);
    }
}
