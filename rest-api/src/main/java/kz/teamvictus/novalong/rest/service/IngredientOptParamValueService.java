package kz.teamvictus.novalong.rest.service;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientOptParamValue;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;

import java.util.List;
import java.util.Set;

public interface IngredientOptParamValueService {

    List<IngredientOptParamValue> findByIngredient(Long ingredientId) throws InternalException;

    void save(IngredientOptParamValue optParam, Long ingredientId) throws InternalException;
    void save(Iterable<IngredientOptParamValue> optParams, Long ingredientId) throws InternalException;

    void update(IngredientOptParamValue paramValue, Long paramValueId) throws InternalException, ResourceNotFoundException;
    void update(Set<IngredientOptParamValue> paramValues, Long ingredientId) throws InternalException;

    void delete(Long paramValueId) throws InternalException, ResourceNotFoundException;

}
