package kz.teamvictus.novalong.rest.service.impl;

import kz.teamvictus.novalong.rest.model.entity.ingredient.IngredientProp;
import kz.teamvictus.novalong.rest.repository.IngredientPropRepository;
import kz.teamvictus.novalong.rest.service.IngredientPropService;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.exceptions.ResourceNotFoundException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IngredientPropServiceImpl implements IngredientPropService {

    private IngredientPropRepository ingredientPropRepository;

    @Autowired
    public void setIngredientPropRepository(IngredientPropRepository ingredientPropRepository) {
        this.ingredientPropRepository = ingredientPropRepository;
    }

    @Override
    public List<IngredientProp> findAll() throws InternalException {
        try {
            return ingredientPropRepository.findAll();
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public List<IngredientProp> findActive() throws InternalException {
        try {
            return ingredientPropRepository.findIngredientPropsByActive(true);
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public IngredientProp findById(Long id) throws InternalException {
        try {
            try {
                IngredientProp ingredientProp = ingredientPropRepository.findOne(id);

                if (ingredientProp == null)
                    throw new ResourceNotFoundException("There is no ingredientProp with id: " + id, ErrorCode.RESOURCE_NOT_FOUND);

                return ingredientProp;
            } catch (ResourceNotFoundException e) {
                throw e;
            } catch (Exception e) {
                throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
            }
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public IngredientProp save(IngredientProp ingredientProp) throws InternalException {
        try {
            if (ingredientProp.getName() == null)
                throw new InternalException("There is no ingredientProp code " + ingredientProp.getName(), ErrorCode.EMPTY_CODE);

            ingredientProp.setActive(true);

            return ingredientPropRepository.save(ingredientProp);
        } catch (InternalException ie) {
            throw ie;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public IngredientProp update(Long ingredientPropId, IngredientProp ingredientProp) throws InternalException, ResourceNotFoundException {
        try {
            IngredientProp ingredientPropToUpdate = ingredientPropRepository.getOne(ingredientPropId);

            if (ingredientPropToUpdate.getId() == null)
                throw new ResourceNotFoundException("There is no ingredientProp with id: " + ingredientPropId, ErrorCode.RESOURCE_NOT_FOUND);

            ingredientPropToUpdate.setName(ingredientProp.getName());

            ingredientPropRepository.save(ingredientPropToUpdate);

            return ingredientPropRepository.findOne(ingredientPropId);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }

    @Override
    public void toggle(Long ingredientPropId) throws InternalException, ResourceNotFoundException {
        try {
            IngredientProp ingredientProp = ingredientPropRepository.getOne(ingredientPropId);

            if (ingredientProp.getId() == null)
                throw new ResourceNotFoundException("There is no ingredientProp with id: " + ingredientPropId, ErrorCode.RESOURCE_NOT_FOUND);

            ingredientProp.setActive(!ingredientProp.isActive());

            ingredientPropRepository.save(ingredientProp);
        } catch (ResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalException(e.getMessage(), ErrorCode.SYSTEM_ERROR);
        }
    }
}
