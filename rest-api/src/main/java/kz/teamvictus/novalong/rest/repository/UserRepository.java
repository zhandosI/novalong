package kz.teamvictus.novalong.rest.repository;

import kz.teamvictus.novalong.rest.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);
    User findBySocialMediaId(String socialMediaId);

    boolean existsByUsername(String username);
    boolean existsByEmail(String email);

}
