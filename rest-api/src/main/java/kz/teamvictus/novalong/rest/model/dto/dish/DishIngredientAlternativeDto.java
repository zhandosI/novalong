package kz.teamvictus.novalong.rest.model.dto.dish;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import kz.teamvictus.novalong.rest.model.dto.alternative.AlternativeCategoryDto;
import kz.teamvictus.novalong.rest.model.dto.alternative.AlternativeRelationDto;
import kz.teamvictus.novalong.rest.model.dto.ingredient.IngredientDto;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DishIngredientAlternativeDto {

    @JsonIgnoreProperties("alternatives")
    private IngredientDto ingredient;

    @JsonIgnoreProperties("alternatives")
    private IngredientDto alternativeIngredient;

    private AlternativeCategoryDto category;
    private AlternativeRelationDto relation;

}
