package kz.teamvictus.novalong.rest.model.entity.ration.joined;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class UserRationObjectJoined extends UserRationJoined {

    private RationContainer ration;

    public UserRationObjectJoined(RationContainer ration,
                                  Long userId,
                                  Long programId,
                                  Date startDate,
                                  Date endDate,
                                  Float calories,
                                  Float caloriesNeeded,
                                  Float caloriesDiff) {
        super(userId, programId, startDate, endDate, calories, caloriesNeeded, caloriesDiff);
        this.ration = ration;
    }

    public UserRationObjectJoined(Long userId,
                                  Long programId,
                                  Date startDate,
                                  Date endDate,
                                  Float calories,
                                  Float caloriesNeeded,
                                  Float caloriesDiff) {
        super(userId, programId, startDate, endDate, calories, caloriesNeeded, caloriesDiff);
    }

}
