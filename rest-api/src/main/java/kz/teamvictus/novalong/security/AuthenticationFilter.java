package kz.teamvictus.novalong.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import kz.teamvictus.novalong.security.model.response.ResponseError;
import kz.teamvictus.novalong.security.service.TokenService;
import kz.teamvictus.novalong.security.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthenticationFilter extends OncePerRequestFilter {

    private final UserDetailsServiceImpl userDetailsService;
    private final TokenService tokenService;

    @Value("${security.jwt.secret}")
    private String secret;

    public AuthenticationFilter(UserDetailsServiceImpl userDetailsService, TokenService tokenService) {
        this.userDetailsService = userDetailsService;
        this.tokenService = tokenService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        final String header = request.getHeader("Authorization");

        String username = null;
        String token = null;

        if (header != null && header.startsWith("Bearer ")) {
            token = header.substring("Bearer".length()).trim();

            try {
                username = tokenService.getUsernameFromToken(token);
            } catch (SignatureException | ExpiredJwtException | MalformedJwtException e) {
                sendJwtErrorResponse(e, response);
                return;
            }
        }

        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);

            if (tokenService.validateToken(token, userDetails)) {
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }

        chain.doFilter(request, response);
    }

    private void sendJwtErrorResponse(Exception e, HttpServletResponse servletResponse) throws IOException {
        final String error = "ERROR";
        int status;
        ResponseError errorRes;

        if (e instanceof ExpiredJwtException) {
            status = HttpStatus.UNAUTHORIZED.value();
            errorRes = new ResponseError(error, ErrorCode.EXPIRED_TOKEN.toString(), e.getMessage());
        }
        else if (e instanceof SignatureException) {
            status = HttpStatus.UNAUTHORIZED.value();
            errorRes = new ResponseError(error, ErrorCode.INVALID_TOKEN.toString(), e.getMessage());
        }
        else {
            status = HttpStatus.BAD_REQUEST.value();
            errorRes = new ResponseError(error, ErrorCode.INVALID_TOKEN.toString(), e.getMessage());
        }

        servletResponse.setStatus(status);
        servletResponse.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        servletResponse.getWriter().write(new ObjectMapper().writeValueAsString(errorRes));
    }
}
