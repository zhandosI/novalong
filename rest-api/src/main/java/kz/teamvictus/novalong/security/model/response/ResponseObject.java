package kz.teamvictus.novalong.security.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResponseObject<T> {

    @JsonProperty("status")
    private String status;

    @JsonProperty("items")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private ItemsResponse items;

    @JsonProperty("links")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LinksResponse links;

    @JsonProperty("data")
    private T data;

}
