package kz.teamvictus.novalong.security.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UniqueConstraintViolationException extends Exception {

    private String message;
    private Enum errorCode;

}
