package kz.teamvictus.novalong.security.controller;

import kz.teamvictus.novalong.rest.model.entity.User;
import kz.teamvictus.novalong.rest.service.UserService;
import kz.teamvictus.novalong.security.exceptions.AuthException;
import kz.teamvictus.novalong.security.exceptions.InternalException;
import kz.teamvictus.novalong.security.model.ErrorCode;
import kz.teamvictus.novalong.security.model.response.ResponseAuth;
import kz.teamvictus.novalong.security.model.UserCredentials;
import kz.teamvictus.novalong.security.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
public class AuthController {

    private static final String SUCCESS = "success";

    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final TokenService tokenService;

    @Autowired
    public AuthController(AuthenticationManager authenticationManager, UserService userService, TokenService tokenService) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
        this.tokenService = tokenService;
    }

    @PostMapping(value = "/api/auth",  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ResponseAuth> auth(@RequestBody final UserCredentials credentials) throws AuthException, InternalException {
        this.authenticate(credentials.getUsername(), credentials.getPassword());
        User user = userService.findByUsername(credentials.getUsername());

        return ResponseEntity.ok(new ResponseAuth(SUCCESS, tokenService.generateToken(user)));
    }

    @GetMapping(value = "/api/auth",  produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ResponseAuth> checkSocialAuth(@RequestParam(name = "facebook_id") String facebookId) throws InternalException, AuthException {
        User user = userService.findBySocialMediaId(facebookId);

        if (user == null)
            throw new AuthException("There is no user with this Facebook ID: " + facebookId, ErrorCode.RESOURCE_NOT_FOUND);

        return ResponseEntity.ok(new ResponseAuth(SUCCESS, tokenService.generateToken(user)));
    }

    private void authenticate(String username, String password) throws AuthException {
        try {
            Objects.requireNonNull(username);
            Objects.requireNonNull(password);

            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (NullPointerException | DisabledException | BadCredentialsException e) {
            throw new AuthException(e.getMessage(), ErrorCode.AUTH_ERROR);
        }
    }

}
