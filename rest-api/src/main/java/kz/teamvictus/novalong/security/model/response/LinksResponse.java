package kz.teamvictus.novalong.security.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * A class for representing links object
 * <tt>HATEOAS</tt>
 *
 * @author Sanzhar Kudaibergen
 */

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LinksResponse {

    @JsonProperty("self")
    private String self;

    @JsonProperty("next")
    private String next;

    @JsonProperty("prev")
    private String prev;

    public static class Builder {

        private String self;
        private String next;
        private String prev;

        public Builder(String self) {
            this.self = self;
        }

        public Builder withNext(String next) {
            this.next = next;
            return this;
        }

        public Builder withPrev(String prev) {
            this.prev = prev;
            return this;
        }

        public LinksResponse build() {
            LinksResponse res = new LinksResponse();
            res.self = this.self;
            res.next = this.next;
            res.prev = this.prev;

            return res;
        }

    }

}
