package kz.teamvictus.novalong.security.controller;

import kz.teamvictus.novalong.rest.controller.BaseController;
import kz.teamvictus.novalong.security.exceptions.*;
import kz.teamvictus.novalong.security.model.ErrorCode;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice
@RestController
public class ErrorController extends BaseController {

    @ExceptionHandler(InternalException.class)
    public ResponseEntity<?> internalException(InternalException e) {
        return buildResponse(error(e, e.getErrorCode(), e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(AuthException.class)
    public ResponseEntity<?> authException(AuthException e) {
        return buildResponse(error(e, e.getErrorCode(), e.getMessage()), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<?> resourceNotFoundException(ResourceNotFoundException e) {
        return buildResponse(error(e, e.getErrorCode(), e.getMessage()), HttpStatus.OK);
    }

    @ExceptionHandler(LackOfDataException.class)
    public ResponseEntity<?> LackOfDataException(LackOfDataException e) {
        return buildResponse(error(e, e.getErrorCode(), e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UniqueConstraintViolationException.class)
    public ResponseEntity<?> uniqueConstraintViolationException(UniqueConstraintViolationException e) {
        return buildResponse(error(e, e.getErrorCode(), e.getMessage()), HttpStatus.CONFLICT);
    }

    /** Spring Security exception thrown by @PreAuthorize role check **/
    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<?> accessDeniedException(AccessDeniedException e) {
        return buildResponse(error(e, ErrorCode.ACCESS_DENIED, e.getMessage()), HttpStatus.FORBIDDEN);
    }

}
