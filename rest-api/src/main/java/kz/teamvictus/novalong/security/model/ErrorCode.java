package kz.teamvictus.novalong.security.model;

public enum ErrorCode {
    SYSTEM_ERROR,
    AUTH_ERROR,
    ACCESS_DENIED,
    UNIQUE_RESOURCE_CONFLICT,
    RESOURCE_NOT_FOUND,
    INVALID_TOKEN,
    EXPIRED_TOKEN,
    EMPTY_CODE,
    FILE_NOT_FOUND,
    TOO_LARGE_FILE_SIZE,
    REQUIRED_PARAMS_NOT_FOUND
}
