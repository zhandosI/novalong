package kz.teamvictus.novalong.security.exceptions;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InternalException extends Exception {

    private String message;
    private Enum errorCode;

    public InternalException(String message, Enum errorCode) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public InternalException() {
    }
}