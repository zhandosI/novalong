package kz.teamvictus.novalong.security.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class ResponseAuth {

    @JsonProperty("status")
    private String status;

    @JsonProperty("token")
    private String token;

}
