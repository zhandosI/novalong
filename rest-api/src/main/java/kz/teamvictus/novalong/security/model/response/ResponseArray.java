package kz.teamvictus.novalong.security.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ResponseArray<T> {

    @JsonProperty("status")
    private String status;

    @JsonProperty("items")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private ItemsResponse items;

    @JsonProperty("links")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LinksResponse links;

    @JsonProperty("data")
    private List<T> data;

    public ResponseArray(String status, List<T> data) {
        this.status = status;
        this.data = data;
    }

    public ResponseArray(String status, ItemsResponse items, List<T> data) {
        this.status = status;
        this.items = items;
        this.data = data;
    }

}
