INSERT INTO unit_types (code, name, unit_data_type_id, is_dish) VALUE ('GRAMS_DISH', 'Грамм', 1, 1);

ALTER TABLE user_daily_rations ADD unit_type_id bigint NOT NULL;
ALTER TABLE user_daily_rations ADD unit_amount double NOT NULL;

ALTER TABLE user_daily_rations
ADD CONSTRAINT user_daily_rations_unit_types_id_fk
FOREIGN KEY (unit_type_id) REFERENCES unit_types (id);