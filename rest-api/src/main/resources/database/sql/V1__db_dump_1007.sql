-- MySQL dump 10.13  Distrib 5.7.22-ndb-7.6.6, for Linux (x86_64)
--
-- Host: localhost    Database: novalong_db
-- ------------------------------------------------------
-- Server version	5.7.22-ndb-7.6.6

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alternatives_categories`
--

DROP TABLE IF EXISTS `alternatives_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alternatives_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alternatives_categories_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alternatives_categories`
--

LOCK TABLES `alternatives_categories` WRITE;
/*!40000 ALTER TABLE `alternatives_categories` DISABLE KEYS */;
INSERT INTO `alternatives_categories` VALUES (1,'PRICE'),(2,'HEALTH');
/*!40000 ALTER TABLE `alternatives_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alternatives_relations`
--

DROP TABLE IF EXISTS `alternatives_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alternatives_relations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `pair_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alternatives_relations_id_uindex` (`id`),
  KEY `alternatives_relations_alternatives_relations_id_fk` (`pair_id`),
  CONSTRAINT `alternatives_relations_alternatives_relations_id_fk` FOREIGN KEY (`pair_id`) REFERENCES `alternatives_relations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alternatives_relations`
--

LOCK TABLES `alternatives_relations` WRITE;
/*!40000 ALTER TABLE `alternatives_relations` DISABLE KEYS */;
INSERT INTO `alternatives_relations` VALUES (1,'LESS',2),(2,'MORE',1);
/*!40000 ALTER TABLE `alternatives_relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dish_ingredients_alternatives`
--

DROP TABLE IF EXISTS `dish_ingredients_alternatives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dish_ingredients_alternatives` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dish_id` bigint(20) NOT NULL,
  `ingredient_id` bigint(20) NOT NULL,
  `alternative_ingredient_id` bigint(20) NOT NULL,
  `alternative_category_id` bigint(20) NOT NULL,
  `alternative_relation_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dish_ingredients_alternatives_id_uindex` (`id`),
  UNIQUE KEY `dish_id_ingredient_id_alternative_i_id_category_id_uindex` (`dish_id`,`ingredient_id`,`alternative_ingredient_id`,`alternative_category_id`),
  KEY `dish_ingredients_alternatives_ingredients_id_fk` (`ingredient_id`),
  KEY `dish_ingredients_alternatives_alternatives_relations_id_fk` (`alternative_relation_id`),
  KEY `dish_ingredients_alternatives_alternatives_categories_id_fk` (`alternative_category_id`),
  KEY `dish_ingredients_alternatives_ingredients_id_fk_2` (`alternative_ingredient_id`),
  CONSTRAINT `dish_ingredients_alternatives_alternatives_categories_id_fk` FOREIGN KEY (`alternative_category_id`) REFERENCES `alternatives_categories` (`id`),
  CONSTRAINT `dish_ingredients_alternatives_alternatives_relations_id_fk` FOREIGN KEY (`alternative_relation_id`) REFERENCES `alternatives_relations` (`id`),
  CONSTRAINT `dish_ingredients_alternatives_dishes_id_fk` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`),
  CONSTRAINT `dish_ingredients_alternatives_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `dish_ingredients_alternatives_ingredients_id_fk_2` FOREIGN KEY (`alternative_ingredient_id`) REFERENCES `ingredients` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dish_ingredients_alternatives`
--

LOCK TABLES `dish_ingredients_alternatives` WRITE;
/*!40000 ALTER TABLE `dish_ingredients_alternatives` DISABLE KEYS */;
INSERT INTO `dish_ingredients_alternatives` VALUES (1,1,5,3,2,1),(3,1,5,7,2,1),(4,16,3,2,2,2),(5,16,3,18,1,2),(6,17,18,13,1,2),(7,17,18,15,1,2),(8,17,18,19,2,1),(9,17,18,2,1,2),(10,17,18,6,2,2),(11,17,18,13,2,2),(12,17,18,6,1,2),(13,17,18,3,1,1),(14,17,18,17,1,1),(15,17,18,1,1,1),(16,17,18,20,1,1),(17,17,17,18,1,2),(18,18,20,13,1,2),(19,18,18,13,1,2),(20,18,18,6,1,2),(21,18,18,3,1,1),(22,18,18,20,1,1),(23,18,18,19,2,1),(24,18,18,6,2,2),(25,18,18,2,1,2),(26,18,18,15,1,2),(27,18,18,17,1,1),(28,18,18,13,2,2),(29,18,18,1,1,1);
/*!40000 ALTER TABLE `dish_ingredients_alternatives` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dish_types`
--

DROP TABLE IF EXISTS `dish_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dish_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dish_type_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dish_types`
--

LOCK TABLES `dish_types` WRITE;
/*!40000 ALTER TABLE `dish_types` DISABLE KEYS */;
INSERT INTO `dish_types` VALUES (1,'Breakfast'),(2,'Dinner'),(3,'Lunch');
/*!40000 ALTER TABLE `dish_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dishes`
--

DROP TABLE IF EXISTS `dishes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dishes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dishes_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dishes`
--

LOCK TABLES `dishes` WRITE;
/*!40000 ALTER TABLE `dishes` DISABLE KEYS */;
INSERT INTO `dishes` VALUES (1,'Beef under the sour cream','Delicious staff........Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla consequat mi id quam dapibus fringilla. Sed pellentesque nisi sed urna dignissim, quis pharetra risus ullamcorper. Donec scelerisque, massa a sagittis laoreet, elit ligula tempor felis, eu consectetur massa mi vel lectus. Etiam non mattis orci, et iaculis leo.','2018-06-27 05:22:16'),(2,'Plain sandwich','nisi sed urna dignissim, quis pharetra risus ullamcorper. Donec scelerisque, massa a sagittis laoreet, elit ligula tempor felis, eu consectetur massa mi vel lectus. Etiam non mattis orci, et iaculis leo.','2018-07-03 07:25:04'),(3,'Tyrkey sandwich','nisi sed urna dignissim, quis pharetra risus ullamcorper. Donec scelerisque, massa a sagittis laoreet, elit ligula tempor felis, eu consectetur massa mi vel lectus. Etiam non mattis orci, et iaculis leo.','2018-07-03 07:27:12'),(4,'Plov','dwqdqwwqdwqdqwdwqying to be required is 100% missing on an incremental watch build.\nDoesn\'t happen all the time.\n\nFor instance, using reactjs key mirror, on a list of constants, I\'d require(\'react/lib/keyMirror\'). On first build, no problem. On watch build, keyMirror module ','2018-07-03 12:11:12'),(5,'Test dish','sdsadsadsdsad','2018-07-04 07:17:01'),(6,'dadrarararar','rara','2018-07-06 10:28:11'),(7,'dadrarararar','dsa','2018-07-06 12:18:15'),(8,'ice cream','tasty...','2018-07-08 18:11:03'),(9,'Feature','dsadsadsd','2018-07-08 18:13:39'),(10,'axaxa','rerere','2018-07-08 18:37:25'),(11,'tte','test','2018-07-08 18:38:27'),(12,'ew','ew','2018-07-08 18:39:18'),(13,'rere','rere','2018-07-08 18:41:26'),(14,'rere','rere','2018-07-08 18:43:10'),(15,'rere','rere','2018-07-08 18:43:19'),(16,'rere','rere','2018-07-08 18:52:27'),(17,'ffff','fdsfsdf','2018-07-09 05:20:38'),(18,'Bug','rrrr','2018-07-09 10:18:52');
/*!40000 ALTER TABLE `dishes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dishes_dish_types`
--

DROP TABLE IF EXISTS `dishes_dish_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dishes_dish_types` (
  `dish_id` bigint(20) NOT NULL,
  `dish_type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`dish_id`,`dish_type_id`),
  UNIQUE KEY `dishes_id_dish_types_id_uindex` (`dish_id`,`dish_type_id`),
  KEY `dishes_dish_types_dish_types_id_fk` (`dish_type_id`),
  CONSTRAINT `dishes_dish_types_dish_types_id_fk` FOREIGN KEY (`dish_type_id`) REFERENCES `dish_types` (`id`),
  CONSTRAINT `dishes_dish_types_dishes_id_fk` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dishes_dish_types`
--

LOCK TABLES `dishes_dish_types` WRITE;
/*!40000 ALTER TABLE `dishes_dish_types` DISABLE KEYS */;
INSERT INTO `dishes_dish_types` VALUES (5,1),(6,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(1,2),(4,2),(5,2),(6,2),(7,2),(12,2),(13,2),(14,2),(15,2),(16,2),(17,2),(18,2),(2,3),(3,3),(4,3),(5,3),(8,3);
/*!40000 ALTER TABLE `dishes_dish_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dishes_ingredients`
--

DROP TABLE IF EXISTS `dishes_ingredients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dishes_ingredients` (
  `dish_id` bigint(20) NOT NULL,
  `ingredient_id` bigint(20) NOT NULL,
  PRIMARY KEY (`dish_id`,`ingredient_id`),
  KEY `dishes_ingredients_ingredients_id_fk` (`ingredient_id`),
  CONSTRAINT `dishes_ingredients_dishes_id_fk` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`),
  CONSTRAINT `dishes_ingredients_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dishes_ingredients`
--

LOCK TABLES `dishes_ingredients` WRITE;
/*!40000 ALTER TABLE `dishes_ingredients` DISABLE KEYS */;
INSERT INTO `dishes_ingredients` VALUES (1,1),(9,3),(10,3),(11,3),(12,3),(13,3),(14,3),(15,3),(16,3),(1,5),(1,6),(1,9),(9,13),(8,15),(17,17),(17,18),(18,18),(18,20),(3,24),(3,29),(3,30),(10,45),(8,48);
/*!40000 ALTER TABLE `dishes_ingredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients`
--

DROP TABLE IF EXISTS `ingredients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `protein` float NOT NULL,
  `fat` float NOT NULL,
  `carbohydrate` float NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `is_allergic` tinyint(4) NOT NULL DEFAULT '0',
  `glycemic_index` float NOT NULL DEFAULT '0',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_id_uindex` (`id`),
  KEY `ingredients_ingredients_category_id_fk` (`category_id`),
  CONSTRAINT `ingredients_ingredients_category_id_fk` FOREIGN KEY (`category_id`) REFERENCES `ingredients_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients`
--

LOCK TABLES `ingredients` WRITE;
/*!40000 ALTER TABLE `ingredients` DISABLE KEYS */;
INSERT INTO `ingredients` VALUES (1,'Beef',18.9,12.4,0,7,0,0,1),(2,'Corn flour',7.2,1.5,70.2,1,0,0,1),(3,'Wheat flour',9.2,1.2,74.9,1,1,0,1),(4,'Sour cream 10%',3,10,2,2,1,0,1),(5,'Sour cream 15%',2.6,15,3,2,1,0,1),(6,'Cherry tomato',0.8,0.1,2.8,5,0,0,1),(7,'Champignon mushroom',4.3,1,0.1,4,0,0,1),(8,'Limon',0.9,0.1,3,6,0,0,1),(9,'Vinegar',0,0,2.3,3,0,0,1),(10,'Mutton',15.6,16.3,0,7,0,0,1),(11,'Rice flour',7.4,0.6,82,1,0,0,1),(12,'Sour cream 20%',3.2,20,2.8,2,1,0,1),(13,'Wine vinegar',0,0,2.1,3,0,0,1),(14,'Pork',16,21.6,0,7,0,0,1),(15,'Banana',2.5,0.4,25.1,1,0,0,1),(16,'fdfdf',54,565,0,1,1,0,0),(17,'Rice flour',7.72,0.5,74.2,1,0,0,1),(18,'Almond flour',7.72,0.5,74.2,1,0,0,1),(19,'Soy flour',6.2,0.5,74.5,1,0,0,1),(20,'Honey agaric',18.9,12.4,0,4,0,0,1),(21,'Honey agaric',18.9,12.4,0,4,0,0,0),(22,'Honey agaric',18.9,12.4,0,4,0,0,0),(23,'Chicken',25,34,7,7,0,15,1),(24,'Tyrkey',26,19,4,7,1,15,1),(25,'Mandarin',4,2,25,6,1,52,1),(26,'Avokado',7,1,14,5,0,24,1),(27,'Avokado',7,1,14,5,0,24,0),(28,'avocado',14,1,24,5,0,1,0),(29,'test',1,1,1,4,0,1,0),(30,'test',1,1,1,4,0,1,0),(31,'test',1,1,1,4,0,1,0),(32,'Pamelo',7,2,19,6,1,28,1),(33,'test test',18.9,12.4,0,4,0,0,0),(34,'test test',18.9,12.4,0,4,0,0,0),(35,'test e23e3es',18.9,12.4,0,4,0,0,0),(36,'Bug',3,332,42,4,1,32,0),(37,'Bug',32,2,2,7,1,43,0),(38,'ewq',4324,443,432,8,0,432,0),(39,'ds',32,32,32,8,0,32,0),(40,'dw',32,320,32,8,0,43,0),(41,'dsa',321,312,3213,4,1,3123,0),(42,'tetrtretrrterter',4324,423,32423,4,0,432,0),(43,'rrr',321,312,32,7,1,312,0),(44,'ttt',43,2,43,8,0,3,0),(45,'Feature',4,432,432,1,1,423,0),(46,'sooooo',43,43,43,4,0,44,0),(47,'neeeeeee',2,21,210,7,0,2,0),(48,'foobarrrrr',645,546,54,7,0,654,0),(49,'testttt',3230,32,32,7,0,32,0),(50,'Whole milk',18,3.2,2,14,1,21,1),(51,'Skim milk',19,0,1,14,1,14,1),(52,'ggtgtgt',312,312,321,4,0,32,1),(53,'Super fatty milk',17,20,1,14,1,17,1),(54,'Sour cream 20%',210,2,202,2,1,2,1);
/*!40000 ALTER TABLE `ingredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_alternatives`
--

DROP TABLE IF EXISTS `ingredients_alternatives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_alternatives` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ingredient_id` bigint(20) DEFAULT NULL,
  `alternative_ingredient_id` bigint(20) DEFAULT NULL,
  `alternative_category_id` bigint(20) DEFAULT NULL,
  `alternative_relation_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_alternatives_id_uindex` (`id`),
  UNIQUE KEY `ingredient_id_alternative_i_id_cateory_id_uindex` (`ingredient_id`,`alternative_ingredient_id`,`alternative_category_id`),
  KEY `ingredients_alternatives_ingredients_id_fk_2` (`alternative_ingredient_id`),
  KEY `ingredients_alternatives_alternatives_categories_id_fk` (`alternative_category_id`),
  KEY `ingredients_alternatives_alternatives_relations_id_fk` (`alternative_relation_id`),
  CONSTRAINT `ingredients_alternatives_alternatives_categories_id_fk` FOREIGN KEY (`alternative_category_id`) REFERENCES `alternatives_categories` (`id`),
  CONSTRAINT `ingredients_alternatives_alternatives_relations_id_fk` FOREIGN KEY (`alternative_relation_id`) REFERENCES `alternatives_relations` (`id`),
  CONSTRAINT `ingredients_alternatives_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `ingredients_alternatives_ingredients_id_fk_2` FOREIGN KEY (`alternative_ingredient_id`) REFERENCES `ingredients` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_alternatives`
--

LOCK TABLES `ingredients_alternatives` WRITE;
/*!40000 ALTER TABLE `ingredients_alternatives` DISABLE KEYS */;
INSERT INTO `ingredients_alternatives` VALUES (1,3,2,2,2),(2,17,18,1,2),(3,3,18,1,2),(4,18,3,1,1),(8,18,17,1,1),(9,18,19,2,1),(11,15,13,2,1),(13,18,6,2,2),(24,18,6,1,2),(26,5,4,1,1),(27,5,12,1,2),(28,45,13,1,2),(29,45,7,1,2),(30,47,13,1,2),(35,47,13,2,1),(39,18,13,1,2),(41,18,13,2,2),(43,18,1,1,1),(44,18,15,1,2),(45,18,20,1,1),(46,18,2,1,2),(47,1,2,1,1),(48,1,1,1,1),(49,1,19,2,1),(50,20,13,1,2),(51,15,3,1,2),(52,49,3,1,2),(53,49,5,2,1),(54,52,13,1,1),(55,52,50,2,1);
/*!40000 ALTER TABLE `ingredients_alternatives` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_categories`
--

DROP TABLE IF EXISTS `ingredients_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_category_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_category_id_uindex` (`id`),
  KEY `ingredients_categories_ingredients_categories_id_fk` (`parent_category_id`),
  CONSTRAINT `ingredients_categories_ingredients_categories_id_fk` FOREIGN KEY (`parent_category_id`) REFERENCES `ingredients_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_categories`
--

LOCK TABLES `ingredients_categories` WRITE;
/*!40000 ALTER TABLE `ingredients_categories` DISABLE KEYS */;
INSERT INTO `ingredients_categories` VALUES (1,'Flour and baking mixes',9),(2,'Sour creams',11),(3,'Vinegars',10),(4,'Mushrooms',NULL),(5,'Tomatoes and cucumbers',13),(6,'Citrus',12),(7,'Meat',NULL),(8,'Ingredients for baking',9),(9,'Grocery',NULL),(10,'Sauces and Oils',NULL),(11,'Dairy produce',NULL),(12,'Fruits and berries',NULL),(13,'Vegetables',NULL),(14,'Milk',11);
/*!40000 ALTER TABLE `ingredients_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_ingredients_props`
--

DROP TABLE IF EXISTS `ingredients_ingredients_props`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_ingredients_props` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ingredient_id` bigint(20) NOT NULL,
  `ingredient_props_category_id` bigint(20) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_ingredients_props_id_uindex` (`id`),
  KEY `ingredients_ingredients_props_ingredients_props_categories_id_fk` (`ingredient_props_category_id`),
  KEY `ingredients_ingredients_props_ingredients_id_fk` (`ingredient_id`),
  CONSTRAINT `ingredients_ingredients_props_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `ingredients_ingredients_props_ingredients_props_categories_id_fk` FOREIGN KEY (`ingredient_props_category_id`) REFERENCES `ingredients_props_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_ingredients_props`
--

LOCK TABLES `ingredients_ingredients_props` WRITE;
/*!40000 ALTER TABLE `ingredients_ingredients_props` DISABLE KEYS */;
INSERT INTO `ingredients_ingredients_props` VALUES (1,50,1,'3.2'),(2,51,1,'0'),(3,6,5,'October - December'),(4,53,3,'20'),(5,54,4,'20');
/*!40000 ALTER TABLE `ingredients_ingredients_props` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_props`
--

DROP TABLE IF EXISTS `ingredients_props`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_props` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_properties_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_props`
--

LOCK TABLES `ingredients_props` WRITE;
/*!40000 ALTER TABLE `ingredients_props` DISABLE KEYS */;
INSERT INTO `ingredients_props` VALUES (1,'Fat content'),(2,'Seasonality');
/*!40000 ALTER TABLE `ingredients_props` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_props_categories`
--

DROP TABLE IF EXISTS `ingredients_props_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_props_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ingredient_prop_id` bigint(20) NOT NULL,
  `ingredient_category_id` bigint(20) NOT NULL,
  `unit_data_type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_props_categories_map_id_uindex` (`id`),
  UNIQUE KEY `props_id_category_id_uindex` (`ingredient_prop_id`,`ingredient_category_id`),
  KEY `ingredients_props_categories_map_ingredients_categories_id_fk` (`ingredient_category_id`),
  KEY `ingredients_props_categories_map_unit_data_types_id_fk` (`unit_data_type_id`),
  CONSTRAINT `ingredients_props_categories_map_ingredients_categories_id_fk` FOREIGN KEY (`ingredient_category_id`) REFERENCES `ingredients_categories` (`id`),
  CONSTRAINT `ingredients_props_categories_map_ingredients_props_id_fk` FOREIGN KEY (`ingredient_prop_id`) REFERENCES `ingredients_props` (`id`),
  CONSTRAINT `ingredients_props_categories_map_unit_data_types_id_fk` FOREIGN KEY (`unit_data_type_id`) REFERENCES `unit_data_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_props_categories`
--

LOCK TABLES `ingredients_props_categories` WRITE;
/*!40000 ALTER TABLE `ingredients_props_categories` DISABLE KEYS */;
INSERT INTO `ingredients_props_categories` VALUES (1,1,11,2),(2,2,13,3),(3,1,14,2),(4,1,2,2),(5,2,5,3);
/*!40000 ALTER TABLE `ingredients_props_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_unit_types`
--

DROP TABLE IF EXISTS `ingredients_unit_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_unit_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ingredient_id` bigint(20) NOT NULL,
  `unit_type_id` bigint(20) NOT NULL,
  `weight` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_unit_types_id_uindex` (`id`),
  UNIQUE KEY `ingredients_unit_types_u` (`ingredient_id`,`unit_type_id`),
  KEY `ingredients_unit_types_unit_types_id_fk` (`unit_type_id`),
  CONSTRAINT `ingredients_unit_types_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `ingredients_unit_types_unit_types_id_fk` FOREIGN KEY (`unit_type_id`) REFERENCES `unit_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_unit_types`
--

LOCK TABLES `ingredients_unit_types` WRITE;
/*!40000 ALTER TABLE `ingredients_unit_types` DISABLE KEYS */;
INSERT INTO `ingredients_unit_types` VALUES (1,2,1,25),(2,2,2,8),(3,18,2,12),(4,18,1,22),(5,19,1,19),(6,19,2,9),(7,20,3,34),(8,21,3,34),(9,22,3,34),(10,23,2,17),(11,23,3,14),(12,24,3,15),(13,25,2,22),(14,31,1,11),(15,32,3,17),(16,48,1,222),(17,49,1,2323),(18,52,1,32);
/*!40000 ALTER TABLE `ingredients_unit_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_USER');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unit_data_types`
--

DROP TABLE IF EXISTS `unit_data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unit_data_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `data_type_code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unit_data_types_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unit_data_types`
--

LOCK TABLES `unit_data_types` WRITE;
/*!40000 ALTER TABLE `unit_data_types` DISABLE KEYS */;
INSERT INTO `unit_data_types` VALUES (1,'Grams','FLOAT'),(2,'Percents','FLOAT'),(3,'Months','STRING');
/*!40000 ALTER TABLE `unit_data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unit_types`
--

DROP TABLE IF EXISTS `unit_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unit_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `unit_data_type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unit_types_id_uindex` (`id`),
  KEY `unit_types_unit_data_types_id_fk` (`unit_data_type_id`),
  CONSTRAINT `unit_types_unit_data_types_id_fk` FOREIGN KEY (`unit_data_type_id`) REFERENCES `unit_data_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unit_types`
--

LOCK TABLES `unit_types` WRITE;
/*!40000 ALTER TABLE `unit_types` DISABLE KEYS */;
INSERT INTO `unit_types` VALUES (1,'tablespoon',1),(2,'teaspoon',1),(3,'handful',1);
/*!40000 ALTER TABLE `unit_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_id_uindex` (`id`),
  KEY `users_roles_id_fk` (`role_id`),
  CONSTRAINT `users_roles_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'tyrion','$2a$10$LTu15SRR31VNZ3P8OtAf1exnKzZ41TparWDh4gR79dPM/eqIbdNZm',1),(2,'rhaegar','rrr',2),(3,'aegon','$2a$10$LTu15SRR31VNZ3P8OtAf1exnKzZ41TparWDh4gR79dPM/eqIbdNZm',1),(4,'sansa','$2a$10$dCcpU10OFTP9CBb8fbFqYuy1Gu39QQlUM7sKWNzDpz1KP94W2USAa',2),(5,'arya','$2a$10$VI56Jp0VR6hEBSNp1QkxN.VI6LqAMQNVubkpWONZpu2ONEwRBA0Ne',2),(6,'sersey','$2a$10$UjKxdt0kmA85x8MuWAzrHu2S/2IAoKkWVLr8XexRdh4UEsiAmB9cm',2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-10 12:14:07
