ALTER TABLE novalong_db.`activity_levels` ADD COLUMN `active` TINYINT(1) DEFAULT 1;
ALTER TABLE novalong_db.`alternatives_categories` ADD COLUMN `active` TINYINT(1) DEFAULT 1;
ALTER TABLE novalong_db.`alternatives_relations` ADD COLUMN `active` TINYINT(1) DEFAULT 1;
ALTER TABLE novalong_db.`dietary_restrictions` ADD COLUMN `active` TINYINT(1) DEFAULT 1;
ALTER TABLE novalong_db.`dish_categories` ADD COLUMN `active` TINYINT(1) DEFAULT 1;
ALTER TABLE novalong_db.`dish_types` ADD COLUMN `active` TINYINT(1) DEFAULT 1;
ALTER TABLE novalong_db.`ingredients_categories` ADD COLUMN `active` TINYINT(1) DEFAULT 1;
ALTER TABLE novalong_db.`ingredients_props` ADD COLUMN `active` TINYINT(1) DEFAULT 1;
ALTER TABLE novalong_db.`phases` ADD COLUMN `active` TINYINT(1) DEFAULT 1;
ALTER TABLE novalong_db.`programs` ADD COLUMN `active` TINYINT(1) DEFAULT 1;
ALTER TABLE novalong_db.`programs_dish_types` ADD COLUMN `active` TINYINT(1) DEFAULT 1;
ALTER TABLE novalong_db.`purposes` ADD COLUMN `active` TINYINT(1) DEFAULT 1;
ALTER TABLE novalong_db.`unit_types` ADD COLUMN `active` TINYINT(1) DEFAULT 1;

DROP TABLE IF EXISTS novalong_db.catalog;
-- auto-generated definition
CREATE TABLE novalong_db.catalog
(
  id      INT AUTO_INCREMENT PRIMARY KEY,
  name          VARCHAR(255)            NOT NULL,
  description   VARCHAR(255)            NOT NULL,
  json_template TEXT                    NOT NULL,
  active   TINYINT(1) DEFAULT '1'  NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE novalong_db.dish_types ADD code varchar(40) NOT NULL AFTER id;

UPDATE novalong_db.dish_types SET code = 'BREAKFAST' WHERE name = 'Breakfast';
UPDATE novalong_db.dish_types SET code = 'SECONDBREAKFAST' WHERE name = 'Second breakfast';
UPDATE novalong_db.dish_types SET code = 'LUNCH' WHERE name = 'Lunch';
UPDATE novalong_db.dish_types SET code = 'SNACKS' WHERE name = 'Snacks';
UPDATE novalong_db.dish_types SET code = 'DINNER' WHERE name = 'Dinner';
UPDATE novalong_db.dish_types SET code = 'SECONDDINNER' WHERE name = 'Second dinner';

ALTER TABLE novalong_db.dish_types ADD CONSTRAINT dish_code_uidx UNIQUE (code);


INSERT INTO novalong_db.catalog(name, description, json_template) VALUES
  ('activity_levels', 'Виды активности человека', '{ "id": -1, "code": "code", "name": "name", "description": "description", "coefficient": -1.01, "active": true }'),
  ('alternatives_categories', 'Категории для выбора альтернатив к ингредиентам', '{ "id": -1, "code": "code", "active": true }'),
  ('alternatives_relations', 'Виды связи по альтернативам', '{ "id": -1, "code": "code", "active": true }'),
  ('dietary_restrictions', 'Ограничения по диете', '{ "id": -1, "code": "code", "name": "name", "active": true }'),
  ('dish_categories', 'Категории блюд', '{ "id": -1, "name": "name", "active": true }'),
  ('dish_types', 'Виды приемов пищи', '{ "id": -1, "code": "code", "name": "name", "active": true }'),
  ('ingredients_categories', 'Категории ингредиентов', '{ "id": -1, "name": "name", "parentCategoryId": -1, "active": true }'),
  ('ingredients_props', 'Свойства ингредиентов', '{ "id": -1, "name": "name", "active": true }'),
  ('phases', 'Фазы', '{ "id": -1, "name": "name", "priority": -1, "proteinsPercentage": -1, "carbsPercentage": -1, "fatsPercentage": -1, "active": true }'),
  ('programs', 'Программы', '{ "id": -1, "name": "name", "purposeId": -1, "caloriesDifferenceCoefficient": -1, "active": true }'),
  ('programs_dish_types', 'Приемы пищи по программам', '{ "id": -1, "programId": -1, "dishTypeId": -1, "caloriesPercentage": -1, "active": true }'),
  ('purposes', 'Цели', '{ "id": -1, "code": "code", "name": "name", "active": true }'),
  ('unit_types', 'Единицы измерения', '{ "id": -1, "code": "code", "name": "name", "dataTypeId": -1, "isDish": true, "active": true }');