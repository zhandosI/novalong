
INSERT INTO novalong_db.catalog(name, description, json_template) VALUES
  ('cookingtypes', 'Способы готовки', '{ "id": -1, "name": "name", "colorCodeId": -1, "active": true }');

ALTER TABLE novalong_db.`cooking_types` ADD COLUMN `active` TINYINT(1) DEFAULT 1;

ALTER TABLE novalong_db.`color_codes` ADD COLUMN `name` VARCHAR(25) NOT NULL AFTER `code`;

UPDATE novalong_db.color_codes cc set cc.name='Красный' where cc.code='RED';
UPDATE novalong_db.color_codes cc set cc.name='Желтый' where cc.code='YELLOW';
UPDATE novalong_db.color_codes cc set cc.name='Зеленый' where cc.code='GREEN';

ALTER TABLE novalong_db.color_codes DROP hex_value;