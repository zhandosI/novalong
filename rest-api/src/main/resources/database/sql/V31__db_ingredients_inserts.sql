
# INGREDIENTS OPT PARAM NAMES

DROP TABLE IF EXISTS `ingredients_opt_param_values`;
DROP TABLE IF EXISTS `ingredients_opt_param_names`;

CREATE TABLE `ingredients_opt_param_names` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(40) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_opt_param_names_id_uindex` (`id`),
  UNIQUE KEY `ingredients_opt_param_names_code_uindex` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `ingredients_opt_param_values` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `opt_param_name_id` bigint(20) NOT NULL,
  `ingredient_id` bigint(20) NOT NULL,
  `weight` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_opt_param_values_id_uindex` (`id`),
  UNIQUE KEY `ingredients_opt_param_name_id_ingredient_id_uindex` (`opt_param_name_id`,`ingredient_id`),
  KEY `ingredients_opt_param_values_ingredients_id_fk` (`ingredient_id`),
  CONSTRAINT `ingredients_opt_param_values_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `ingredients_opt_param_values_ingredients_opt_param_names_id_fk` FOREIGN KEY (`opt_param_name_id`) REFERENCES `ingredients_opt_param_names` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `ingredients_opt_param_names` (code, name) VALUES
  ('WATER','Вода (г)'),
  ('ASH','Зола (г)'),
  ('SUGAR','Сахар (г)'),
  ('FIBER','Клетчатка (г)'),
  ('STARCH','Крахмал'),
  ('CHOLESTEROL','Холестерин (мг)'),
  ('FATS_TRANS','Трансжиры (г)'),
  ('VITAMIN_A','Витамин А (мкг)'),
  ('BETA_CAROTENE','Бета-каротин (мкг)'),
  ('ALPHA_CAROTENE','Альфа-каротин (мкг)'),
  ('VITAMIN_D','Витамин D (мкг)'),
  ('VITAMIN_E','Витамин Е (мг)'),
  ('VITAMIN_K','Витамин К (мкг)'),
  ('VITAMIN_C','Витамин С (мг)'),
  ('VITAMIN_B1','Витамин В1 (мг)'),
  ('VITAMIN_B2','Витамин В2 (мг)'),
  ('VITAMIN_B3','Витамин В3 (мг)'),
  ('VITAMIN_B4','Витамин В4 (холин) (мг)'),
  ('VITAMIN_B5','Витамин В5 (мг)'),
  ('VITAMIN_B6','Витамин В6 (мг)'),
  ('VITAMIN_B9','Витамин В9 (мкг)'),
  ('VITAMIN_B12','Витамин В12 (мкг)'),
  ('CALCIUM','Кальций (мг)'),
  ('FERRUM','Железо (мг)'),
  ('MAGNESIUM','Магний (мг)'),
  ('PHOSPHORUS','Фосфор (мг)'),
  ('POTASSIUM','Калий (мг)'),
  ('SODIUM','Натрий (мг)'),
  ('ZINC','Цинк (мг)'),
  ('COPPER','Медь (мг)'),
  ('MANGANESE','Марганец (мг)'),
  ('SELENIUM','Селен (мкг)'),
  ('FLUORINE','Фтор (мкг)'),
  ('FATS_MONOUNSATURATED','Жирные кислоты, мононенасыщенные (г)'),
  ('FATS_POLYUNSATURATED','Жирные кислоты, полиненасыщенные (г)'),
  ('FATS_SATURATED','Жирные кислоты, насыщенные (г)'),
  ('CAFFEINE','Кофеин');

INSERT INTO ingredients_categories (name, parent_category_id, is_allergic, active) VALUES
  ('Грибы сушёные', 44, 0, 1),
  ('Шоколад', 72, 1, 1),
  ('Свинина (постная)', 78, 0, 1),
  ('Говядина (постная)', 78, 0, 1),
  ('Говядина', 79, 0, 1),
  ('Гусь', 77, 0, 1),
  ('Свинина', 79, 0, 1),
  ('Утка', 79, 0, 1),
  ('Высокобелковые продукты', 1, 0, 1),
  ('Фастфуд', 1, 0, 1);