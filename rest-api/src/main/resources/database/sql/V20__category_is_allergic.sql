ALTER TABLE ingredients_categories
  ADD is_allergic TINYINT(1) DEFAULT 0 NOT NULL AFTER parent_category_id;