-- 17.09
-- MySQL dump 10.13  Distrib 5.7.22, for macos10.13 (x86_64)
--
-- Host: localhost    Database: novalong_db
-- ------------------------------------------------------
-- Server version	5.7.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `social_media_id` varchar(255) DEFAULT NULL,
  `gender_id` bigint(20) NOT NULL,
  `height` double NOT NULL,
  `weight` double NOT NULL,
  `birth_date` date NOT NULL,
  `purpose_id` bigint(20) DEFAULT NULL,
  `activity_level_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) NOT NULL,
  `current_phase_id` bigint(20) NOT NULL,
  `phase_update_date` date NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_id_uindex` (`id`),
  UNIQUE KEY `username_uindex` (`username`),
  UNIQUE KEY `users_email_uindex` (`email`),
  UNIQUE KEY `users_social_media_id_uindex` (`social_media_id`),
  KEY `users_roles_id_fk` (`role_id`),
  KEY `users_genders_id_fk` (`gender_id`),
  KEY `users_purposes_id_fk` (`purpose_id`),
  KEY `users_activity_levels_id_fk` (`activity_level_id`),
  KEY `users_phases_id_fk` (`current_phase_id`),
  CONSTRAINT `users_activity_levels_id_fk` FOREIGN KEY (`activity_level_id`) REFERENCES `activity_levels` (`id`),
  CONSTRAINT `users_genders_id_fk` FOREIGN KEY (`gender_id`) REFERENCES `genders` (`id`),
  CONSTRAINT `users_phases_id_fk` FOREIGN KEY (`current_phase_id`) REFERENCES `phases` (`id`),
  CONSTRAINT `users_purposes_id_fk` FOREIGN KEY (`purpose_id`) REFERENCES `purposes` (`id`),
  CONSTRAINT `users_roles_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'tyrion','tyrion@tyrion','$2a$10$LTu15SRR31VNZ3P8OtAf1exnKzZ41TparWDh4gR79dPM/eqIbdNZm',NULL,1,120,41,'1982-09-10',3,3,1,1,'2018-07-16',1),(2,'rhaegar','rhaegar@rhaegar','$2a$10$dCcpU10OFTP9CBb8fbFqYuy1Gu39QQlUM7sKWNzDpz1KP94W2USAa',NULL,1,187,82,'1982-09-10',3,3,2,1,'2018-07-16',0),(3,'aegon','aegon@aegon','$2a$10$LTu15SRR31VNZ3P8OtAf1exnKzZ41TparWDh4gR79dPM/eqIbdNZm','valid_id',1,190,81,'1982-09-10',3,1,2,1,'2018-07-16',1),(4,'sansa','sansa@sansa','$2a$10$dCcpU10OFTP9CBb8fbFqYuy1Gu39QQlUM7sKWNzDpz1KP94W2USAa',NULL,2,164,47,'1982-09-10',3,2,2,1,'2018-07-16',1),(5,'arya','arya@arya','$2a$10$VI56Jp0VR6hEBSNp1QkxN.VI6LqAMQNVubkpWONZpu2ONEwRBA0Ne',NULL,2,153,42,'1982-09-10',3,3,2,1,'2018-07-16',1),(6,'sersey','sersey@sersey','$2a$10$UjKxdt0kmA85x8MuWAzrHu2S/2IAoKkWVLr8XexRdh4UEsiAmB9cm',NULL,2,170,51,'1982-09-10',3,1,2,1,'2018-07-16',1),(7,'lianna','lianna@lianna',NULL,NULL,2,153,42,'1982-09-10',3,3,2,1,'2018-07-16',1),(8,'test','test@test',NULL,NULL,1,153,42,'1982-09-10',3,3,2,1,'2018-07-16',1),(9,'john','john@john',NULL,NULL,1,164,55,'1982-09-10',3,3,2,1,'2018-07-16',1),(10,'newUser','new@user',NULL,NULL,1,161,40,'1982-09-10',3,1,2,1,'2018-09-25',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-18 16:03:58
