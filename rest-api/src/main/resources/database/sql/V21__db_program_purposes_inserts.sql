ALTER TABLE programs_dish_types MODIFY active tinyint(1) NOT NULL DEFAULT '1';
ALTER TABLE programs MODIFY active tinyint(1) NOT NULL DEFAULT '1';

INSERT INTO purposes (code, name) VALUE ('MAINTENANCE', 'Weight maintenance');

UPDATE programs SET name = 'Novalong program', purpose_id = 3 WHERE id = 1;
UPDATE programs SET active = 0 WHERE id = 2;


-- Redump with no unique index on ration, dish type and dish

DROP TABLE IF EXISTS `user_daily_rations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_daily_rations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_ration_id` bigint(20) NOT NULL,
  `dish_type_id` bigint(20) NOT NULL,
  `dish_id` bigint(20) NOT NULL,
  `day_code` varchar(10) NOT NULL,
  `protein` float NOT NULL,
  `carbohydrate` float NOT NULL,
  `fat` bigint(20) NOT NULL,
  `unit_type_id` bigint(20) NOT NULL,
  `unit_amount` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_daily_rations_id_uindex` (`id`),
  UNIQUE KEY `ration_id_dish_type_id_dish_id_uindex` (`user_ration_id`,`dish_type_id`,`dish_id`),
  KEY `user_daily_rations_dish_types_id_fk` (`dish_type_id`),
  KEY `user_daily_rations_dishes_id_fk` (`dish_id`),
  KEY `user_daily_rations_unit_types_id_fk` (`unit_type_id`),
  CONSTRAINT `user_daily_rations_dish_types_id_fk` FOREIGN KEY (`dish_type_id`) REFERENCES `dish_types` (`id`),
  CONSTRAINT `user_daily_rations_dishes_id_fk` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`),
  CONSTRAINT `user_daily_rations_unit_types_id_fk` FOREIGN KEY (`unit_type_id`) REFERENCES `unit_types` (`id`),
  CONSTRAINT `user_daily_rations_user_rations_id_fk` FOREIGN KEY (`user_ration_id`) REFERENCES `user_rations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=369 DEFAULT CHARSET=utf8;