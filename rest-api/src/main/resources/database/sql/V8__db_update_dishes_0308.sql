ALTER TABLE dishes ADD recipe_description text NULL;
ALTER TABLE dishes MODIFY description text;
ALTER TABLE dishes
  MODIFY COLUMN recipe_description text AFTER description,
  MODIFY COLUMN dish_category_id bigint(20) NOT NULL AFTER carbohydrate;