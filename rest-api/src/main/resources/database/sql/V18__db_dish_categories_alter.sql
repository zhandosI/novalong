-- -------------------------------------- 24.08.18 ----------------------------------------

CREATE TABLE dishes_dish_categories
(
    dish_id bigint NOT NULL,
    dish_category_id bigint NOT NULL,
    CONSTRAINT dishes_dish_categories_dishes_id_fk
      FOREIGN KEY (dish_id) REFERENCES dishes (id),
    CONSTRAINT dishes_dish_categories_dish_categories_id_fk
      FOREIGN KEY (dish_category_id) REFERENCES dish_categories (id)
);
CREATE UNIQUE INDEX dishes_dish_categories_dish_id_dish_category_id_uindex
  ON dishes_dish_categories (dish_id, dish_category_id);

ALTER TABLE dishes_dish_categories ADD PRIMARY KEY (dish_id, dish_category_id);
-- -----------------------------------------------------------------------------------------------------------


-- --------- This loop inserts all dish_category ids with dish ids into a new mapper table ----------------
DROP PROCEDURE IF EXISTS POPULATE_DISHES_DISH_CATEGORIES;
DELIMITER ;;

CREATE PROCEDURE POPULATE_DISHES_DISH_CATEGORIES()
  BEGIN
    DECLARE i int DEFAULT 0;
    DECLARE n int DEFAULT 0;

    SELECT COUNT(*) FROM dishes INTO n;

    SET i = 0;
    WHILE i < n DO
      INSERT INTO dishes_dish_categories (dish_id, dish_category_id)
          SELECT d.id, d.dish_category_id FROM dishes AS d ORDER BY d.id LIMIT i, 1;

      SET i = i + 1;
    END WHILE;
  END ;;

CALL POPULATE_DISHES_DISH_CATEGORIES();
-- -----------------------------------------------------------------------------------------------------------

ALTER TABLE dishes DROP FOREIGN KEY dishes_dish_categories_id_fk;
DROP INDEX dishes_dish_categories_id_fk ON dishes;
ALTER TABLE dishes DROP dish_category_id;