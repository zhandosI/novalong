-- MySQL dump 10.13  Distrib 5.7.22, for macos10.13 (x86_64)
--
-- Host: localhost    Database: novalong_db
-- ------------------------------------------------------
-- Server version	5.7.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_levels`
--

DROP TABLE IF EXISTS `activity_levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_levels` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `coefficient` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `activity_levels_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_levels`
--

LOCK TABLES `activity_levels` WRITE;
/*!40000 ALTER TABLE `activity_levels` DISABLE KEYS */;
INSERT INTO `activity_levels` VALUES (1,'SEDENTARY','Sedentary or light activity','Office worker getting little or no exercise',1.53),(2,'ACTIVE','Active or moderately active','Construction worker or person running one hour daily',1.76),(3,'EXTREME','Vigorously active','Agricultural worker (non mechanized) or person swimming two hours daily	',2.25);
/*!40000 ALTER TABLE `activity_levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alternatives_categories`
--

DROP TABLE IF EXISTS `alternatives_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alternatives_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alternatives_categories_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alternatives_categories`
--

LOCK TABLES `alternatives_categories` WRITE;
/*!40000 ALTER TABLE `alternatives_categories` DISABLE KEYS */;
INSERT INTO `alternatives_categories` VALUES (1,'PRICE'),(2,'HEALTH');
/*!40000 ALTER TABLE `alternatives_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alternatives_relations`
--

DROP TABLE IF EXISTS `alternatives_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alternatives_relations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `pair_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alternatives_relations_id_uindex` (`id`),
  KEY `alternatives_relations_alternatives_relations_id_fk` (`pair_id`),
  CONSTRAINT `alternatives_relations_alternatives_relations_id_fk` FOREIGN KEY (`pair_id`) REFERENCES `alternatives_relations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alternatives_relations`
--

LOCK TABLES `alternatives_relations` WRITE;
/*!40000 ALTER TABLE `alternatives_relations` DISABLE KEYS */;
INSERT INTO `alternatives_relations` VALUES (1,'LESS',2),(2,'MORE',1);
/*!40000 ALTER TABLE `alternatives_relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cooking_types`
--

DROP TABLE IF EXISTS `cooking_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cooking_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cooking_types_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cooking_types`
--

LOCK TABLES `cooking_types` WRITE;
/*!40000 ALTER TABLE `cooking_types` DISABLE KEYS */;
INSERT INTO `cooking_types` VALUES (1,'Boiling'),(2,'Roasting');
/*!40000 ALTER TABLE `cooking_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dietary_restrictions`
--

DROP TABLE IF EXISTS `dietary_restrictions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dietary_restrictions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dietary_restrictions_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dietary_restrictions`
--

LOCK TABLES `dietary_restrictions` WRITE;
/*!40000 ALTER TABLE `dietary_restrictions` DISABLE KEYS */;
INSERT INTO `dietary_restrictions` VALUES (1,'DIABETES','Sugar diabetes'),(2,'PREGNANCY','Pregnancy'),(3,'HYPERTENSION','Hypertension'),(4,'GASTRITIS','Gastritis');
/*!40000 ALTER TABLE `dietary_restrictions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dish_categories`
--

DROP TABLE IF EXISTS `dish_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dish_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dish_categories_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dish_categories`
--

LOCK TABLES `dish_categories` WRITE;
/*!40000 ALTER TABLE `dish_categories` DISABLE KEYS */;
INSERT INTO `dish_categories` VALUES (1,'Garnishes'),(2,'Meats'),(3,'Cereals'),(4,'Floury'),(5,'Other');
/*!40000 ALTER TABLE `dish_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dish_ingredients_alternatives`
--

DROP TABLE IF EXISTS `dish_ingredients_alternatives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dish_ingredients_alternatives` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dish_id` bigint(20) NOT NULL,
  `ingredient_id` bigint(20) NOT NULL,
  `alternative_ingredient_id` bigint(20) NOT NULL,
  `alternative_category_id` bigint(20) NOT NULL,
  `alternative_relation_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dish_ingredients_alternatives_id_uindex` (`id`),
  UNIQUE KEY `dish_id_ingredient_id_alternative_i_id_category_id_uindex` (`dish_id`,`ingredient_id`,`alternative_ingredient_id`,`alternative_category_id`),
  KEY `dish_ingredients_alternatives_ingredients_id_fk` (`ingredient_id`),
  KEY `dish_ingredients_alternatives_alternatives_relations_id_fk` (`alternative_relation_id`),
  KEY `dish_ingredients_alternatives_alternatives_categories_id_fk` (`alternative_category_id`),
  KEY `dish_ingredients_alternatives_ingredients_id_fk_2` (`alternative_ingredient_id`),
  CONSTRAINT `dish_ingredients_alternatives_alternatives_categories_id_fk` FOREIGN KEY (`alternative_category_id`) REFERENCES `alternatives_categories` (`id`),
  CONSTRAINT `dish_ingredients_alternatives_alternatives_relations_id_fk` FOREIGN KEY (`alternative_relation_id`) REFERENCES `alternatives_relations` (`id`),
  CONSTRAINT `dish_ingredients_alternatives_dishes_id_fk` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`),
  CONSTRAINT `dish_ingredients_alternatives_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `dish_ingredients_alternatives_ingredients_id_fk_2` FOREIGN KEY (`alternative_ingredient_id`) REFERENCES `ingredients` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dish_ingredients_alternatives`
--

LOCK TABLES `dish_ingredients_alternatives` WRITE;
/*!40000 ALTER TABLE `dish_ingredients_alternatives` DISABLE KEYS */;
INSERT INTO `dish_ingredients_alternatives` VALUES (1,1,5,3,2,1),(3,1,5,7,2,1),(4,16,3,2,2,2),(5,16,3,18,1,2),(6,17,18,13,1,2),(7,17,18,15,1,2),(8,17,18,19,2,1),(9,17,18,2,1,2),(10,17,18,6,2,2),(11,17,18,13,2,2),(12,17,18,6,1,2),(13,17,18,3,1,1),(14,17,18,17,1,1),(15,17,18,1,1,1),(16,17,18,20,1,1),(17,17,17,18,1,2),(18,18,20,13,1,2),(19,18,18,13,1,2),(20,18,18,6,1,2),(21,18,18,3,1,1),(22,18,18,20,1,1),(23,18,18,19,2,1),(24,18,18,6,2,2),(25,18,18,2,1,2),(26,18,18,15,1,2),(27,18,18,17,1,1),(28,18,18,13,2,2),(29,18,18,1,1,1),(30,19,3,18,1,2),(31,19,3,2,2,2),(32,24,5,4,1,1),(33,24,5,12,1,2),(34,26,3,18,1,2),(35,26,3,2,2,2),(36,28,3,2,2,2),(37,28,3,18,1,2),(38,34,3,2,2,2),(39,34,3,18,1,2),(40,35,1,2,1,1),(41,35,1,1,1,1),(42,35,1,19,2,1),(43,35,3,2,2,2),(44,35,3,18,1,2),(45,36,20,13,1,2),(46,36,18,13,1,2),(47,36,18,13,2,2),(48,36,18,6,2,2),(49,36,18,17,1,1),(50,36,18,6,1,2),(51,36,18,2,1,2),(52,36,18,20,1,1),(53,36,18,19,2,1),(54,36,18,3,1,1),(55,36,18,15,1,2),(56,36,18,1,1,1),(57,37,3,2,2,2),(58,37,3,18,1,2),(59,38,5,4,1,1),(60,38,5,12,1,2);
/*!40000 ALTER TABLE `dish_ingredients_alternatives` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dish_types`
--

DROP TABLE IF EXISTS `dish_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dish_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dish_type_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dish_types`
--

LOCK TABLES `dish_types` WRITE;
/*!40000 ALTER TABLE `dish_types` DISABLE KEYS */;
INSERT INTO `dish_types` VALUES (1,'Breakfast'),(2,'Second breakfast'),(3,'Lunch'),(4,'Snacks'),(5,'Dinner'),(6,'Second dinner');
/*!40000 ALTER TABLE `dish_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dishes`
--

DROP TABLE IF EXISTS `dishes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dishes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `dish_category_id` bigint(20) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `weight` float NOT NULL,
  `protein` float NOT NULL,
  `fat` float NOT NULL,
  `carbohydrate` float NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `dishes_id_uindex` (`id`),
  KEY `dishes_dish_categories_id_fk` (`dish_category_id`),
  CONSTRAINT `dishes_dish_categories_id_fk` FOREIGN KEY (`dish_category_id`) REFERENCES `dish_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dishes`
--

LOCK TABLES `dishes` WRITE;
/*!40000 ALTER TABLE `dishes` DISABLE KEYS */;
INSERT INTO `dishes` VALUES (1,'Beef under the sour cream','Delicious staff........Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla consequat mi id quam dapibus fringilla. Sed pellentesque nisi sed urna dignissim, quis pharetra risus ullamcorper. Donec scelerisque, massa a sagittis laoreet, elit ligula tempor felis, eu consectetur massa mi vel lectus. Etiam non mattis orci, et iaculis leo.',3,'2018-06-27 05:22:16',124,54,65,4,1),(2,'Plain sandwich','nisi sed urna dignissim, quis pharetra risus ullamcorper. Donec scelerisque, massa a sagittis laoreet, elit ligula tempor felis, eu consectetur massa mi vel lectus. Etiam non mattis orci, et iaculis leo.',4,'2018-07-03 07:25:04',583,77,5,65,1),(3,'Tyrkey sandwich','nisi sed urna dignissim, quis pharetra risus ullamcorper. Donec scelerisque, massa a sagittis laoreet, elit ligula tempor felis, eu consectetur massa mi vel lectus. Etiam non mattis orci, et iaculis leo.',4,'2018-07-03 07:27:12',857,12,87,44,1),(4,'Plov','Required is 100% missing on an incremental watch build.\nDoesn\'t happen all the time.\n\nFor instance, using reactjs key mirror, on a list of constants, I\'d require(\'react/lib/keyMirror\'). On first build, no problem. On watch build, keyMirror module ',4,'2018-07-03 12:11:12',85,0,0,0,0),(5,'Test dish','sdsadsadsdsad',4,'2018-07-04 07:17:01',785,88,77,89,1),(6,'Besparmakkk','rara',4,'2018-07-06 10:28:11',5785,58,144,90,1),(7,'Kuirdakkkk','dsa',4,'2018-07-06 12:18:15',58,65,140,89,1),(8,'ice cream','tasty...',4,'2018-07-08 18:11:03',58,25,17,84,1),(9,'Feature','dsadsadsd',4,'2018-07-08 18:13:39',587,0,0,0,0),(10,'axaxa','rerere',4,'2018-07-08 18:37:25',885,0,0,0,0),(11,'tte','test',4,'2018-07-08 18:38:27',875,0,0,0,0),(12,'ew','ew',4,'2018-07-08 18:39:18',587,0,0,0,0),(13,'rere','rere',4,'2018-07-08 18:41:26',768,0,0,0,0),(14,'rere','rere',4,'2018-07-08 18:43:10',77,0,0,0,0),(15,'rere','rere',4,'2018-07-08 18:43:19',8,0,0,0,0),(16,'rere','rere',4,'2018-07-08 18:52:27',58,0,0,0,0),(17,'ffff','fdsfsdf',4,'2018-07-09 05:20:38',572,0,0,0,0),(18,'Bug','rrrr',4,'2018-07-09 10:18:52',25,9,17,3,1),(19,'tedED','test',4,'2018-07-10 06:43:48',12,0,0,0,0),(20,'rererererere','re',4,'2018-07-11 11:15:03',0,0,0,0,0),(21,'ew','w',4,'2018-07-11 11:17:18',0,0,0,0,0),(22,'ew','w',4,'2018-07-11 11:23:52',0,0,0,0,0),(23,'a','a',4,'2018-07-11 11:24:23',0,0,0,0,0),(24,'gg','g',4,'2018-07-11 11:45:06',0,0,0,0,0),(25,'gaaaa','g',4,'2018-07-11 11:46:19',0,0,0,0,0),(26,'32','32',4,'2018-07-11 15:27:43',0,0,0,0,0),(28,'fuuuuu','ew',4,'2018-07-11 15:51:20',0,94,57,12,1),(29,'test ddddd','ddd',4,'2018-07-12 06:17:42',499,0,0,0,0),(30,'test','te',4,'2018-07-12 06:20:42',34,0,0,0,0),(31,'testddd','dd',4,'2018-07-12 06:21:28',507,0,0,0,0),(32,'tw','we',4,'2018-07-12 06:27:32',507,0,0,0,0),(33,'TESTETETET','eu consectetur massa mi vel lectus. Etiam non mattis orci, et iaculis leo.',3,'2018-07-16 13:11:20',0,0,0,0,0),(34,'Featuredadad','dada',1,'2018-07-16 14:08:56',46,4.4,0.6,32.2,1),(35,'Beef sausage','ssss',2,'2018-07-16 14:13:18',556,99.9,62.8,40.4,1),(36,'SUPPPER DISH','untf8 ???????? ????',4,'2018-07-17 15:55:47',1096,96,58.1,210.7,1),(37,'Oatmeal','wee',3,'2018-07-17 16:00:13',245,20,3.3,177.6,1),(38,'test ','swsw',2,'2018-07-23 10:25:32',460,19.3,7.9,1.1,1);
/*!40000 ALTER TABLE `dishes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dishes_dish_types`
--

DROP TABLE IF EXISTS `dishes_dish_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dishes_dish_types` (
  `dish_id` bigint(20) NOT NULL,
  `dish_type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`dish_id`,`dish_type_id`),
  UNIQUE KEY `dishes_id_dish_types_id_uindex` (`dish_id`,`dish_type_id`),
  KEY `dishes_dish_types_dish_types_id_fk` (`dish_type_id`),
  CONSTRAINT `dishes_dish_types_dish_types_id_fk` FOREIGN KEY (`dish_type_id`) REFERENCES `dish_types` (`id`),
  CONSTRAINT `dishes_dish_types_dishes_id_fk` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dishes_dish_types`
--

LOCK TABLES `dishes_dish_types` WRITE;
/*!40000 ALTER TABLE `dishes_dish_types` DISABLE KEYS */;
INSERT INTO `dishes_dish_types` VALUES (5,1),(6,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(25,1),(26,1),(28,1),(29,1),(34,1),(37,1),(1,2),(4,2),(5,2),(6,2),(7,2),(12,2),(13,2),(14,2),(15,2),(16,2),(17,2),(18,2),(25,2),(30,2),(32,2),(33,2),(37,2),(2,3),(3,3),(4,3),(5,3),(8,3),(24,3),(31,3),(35,3),(38,3),(36,4),(38,4),(36,5);
/*!40000 ALTER TABLE `dishes_dish_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dishes_ingredients`
--

DROP TABLE IF EXISTS `dishes_ingredients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dishes_ingredients` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dish_id` bigint(20) NOT NULL,
  `ingredient_id` bigint(20) NOT NULL,
  `weight` float NOT NULL,
  `unit_type_id` bigint(20) NOT NULL,
  `unit_type_amount` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dishes_ingredients_id_uindex` (`id`),
  UNIQUE KEY `dish_id_ingr_id_weight_uindex` (`dish_id`,`ingredient_id`,`weight`),
  KEY `dishes_ingredients_ingredients_id_fk` (`ingredient_id`),
  KEY `dishes_ingredients_unit_types_id_fk` (`unit_type_id`),
  CONSTRAINT `dishes_ingredients_dishes_id_fk` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`),
  CONSTRAINT `dishes_ingredients_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `dishes_ingredients_unit_types_id_fk` FOREIGN KEY (`unit_type_id`) REFERENCES `unit_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dishes_ingredients`
--

LOCK TABLES `dishes_ingredients` WRITE;
/*!40000 ALTER TABLE `dishes_ingredients` DISABLE KEYS */;
INSERT INTO `dishes_ingredients` VALUES (1,1,1,542,2,0),(2,1,5,25,1,0),(3,1,6,0,2,0),(4,1,9,57254,1,0),(5,3,24,52,1,0),(6,3,29,524,2,0),(7,3,30,0,1,0),(8,8,15,0,1,0),(9,8,48,0,1,0),(10,9,3,0,1,0),(11,9,13,0,1,0),(12,10,3,0,1,0),(13,10,45,0,1,0),(14,11,3,0,1,0),(15,12,3,0,1,0),(16,13,3,0,1,0),(17,14,3,0,1,0),(18,15,3,0,1,0),(19,16,3,0,1,0),(20,17,17,0,1,0),(21,17,18,0,1,0),(22,18,18,0,1,0),(23,18,20,0,1,0),(24,19,3,0,1,0),(25,28,3,12,4,12),(26,28,50,15,4,15),(27,28,13,13,4,13),(28,32,55,432,4,432),(29,32,57,75,1,3),(30,34,3,43,4,43),(31,34,50,3,4,3),(32,35,1,502,4,502),(33,35,3,54,4,54),(34,36,56,74,4,74),(35,36,4,135,4,135),(36,36,6,45,4,45),(37,36,20,34,3,1),(38,36,7,588,4,588),(39,36,18,220,1,10),(40,37,2,125,1,5),(41,37,3,120,4,120),(42,38,7,436,4,436),(43,38,5,21,4,21),(44,38,4,3,4,3);
/*!40000 ALTER TABLE `dishes_ingredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genders`
--

DROP TABLE IF EXISTS `genders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `genders_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genders`
--

LOCK TABLES `genders` WRITE;
/*!40000 ALTER TABLE `genders` DISABLE KEYS */;
INSERT INTO `genders` VALUES (1,'MALE','Male'),(2,'FEMALE','Female');
/*!40000 ALTER TABLE `genders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients`
--

DROP TABLE IF EXISTS `ingredients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `protein` float NOT NULL,
  `fat` float NOT NULL,
  `carbohydrate` float NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `is_allergic` tinyint(4) NOT NULL DEFAULT '0',
  `glycemic_index` float NOT NULL DEFAULT '0',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_id_uindex` (`id`),
  KEY `ingredients_ingredients_category_id_fk` (`category_id`),
  CONSTRAINT `ingredients_ingredients_category_id_fk` FOREIGN KEY (`category_id`) REFERENCES `ingredients_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients`
--

LOCK TABLES `ingredients` WRITE;
/*!40000 ALTER TABLE `ingredients` DISABLE KEYS */;
INSERT INTO `ingredients` VALUES (1,'дрожжи',12.7,0,2.7,2,0,0,1),(2,'панировочные сухари',9.7,77.6,1.9,2,1,0,1),(3,'пектин',0,89.6,0,2,0,0,1),(4,'желатин',87.2,0.8,0.4,2,0,0,1),(5,'мак',17.5,2,47.5,2,0,0,1),(6,'краситель пищевой',0,0,0,2,0,0,1),(7,'лимонная кислота',0,0,0,2,0,0,1),(8,'сода и разрыхлитель',0,0,0,2,0,0,1),(9,'марципан',6.8,65.3,21.2,2,0,0,1),(10,'пшеничная мука',9.2,74.9,1.2,3,1,0,1),(11,'миндальная мука',20.1,13.5,39.5,3,1,0,1),(12,'кукурузная мука',7.2,70.2,1.5,3,1,0,1),(13,'ржаная мука',9,73,1,3,1,0,1),(14,'рисовая мука',7.4,82,0.6,3,1,0,1),(15,'амарантовая мука',8.9,61.7,1.7,3,1,0,1),(16,'соевая мука',36.5,17.9,18.6,3,1,0,1),(17,'льняная мука',36,9,10,3,1,0,1),(18,'гороховая цельносмолотая мука',21,49,2,3,1,0,1),(19,'овсяная мука',13,64.9,6.8,3,1,0,1),(20,'крахмал картофельный    ',0.1,79.6,0,3,0,0,1),(21,'чечевичная мука цельносмолотая',28,56,1,3,1,0,1),(22,'ячменная цельносмолотая мука',10,56.1,1.6,3,0,0,1),(23,'гречневая крупа',12.6,62.1,3.3,4,0,0,1),(24,'кукурузная крупа',8.3,75,1.2,4,0,0,1),(25,'манная крупа',10.3,67.4,1,4,1,0,1),(26,'пшеничная крупа',11.5,62,1.3,4,1,0,1),(27,'пшено',11.5,69.3,3.3,4,0,0,1),(28,'рис белый',6.7,78.9,0.7,4,0,0,1),(29,'рис бурый',7.4,72.9,1.8,4,0,0,1),(30,'киноа',14.1,57.2,6.1,4,0,0,1),(31,'перловая',9.3,73.7,1.1,4,0,0,1),(32,'ячневая',10.4,66.3,1.3,4,0,0,1),(33,'булгур',12.3,57.6,1.3,4,0,0,1),(34,'кускус',3.8,21.8,0.2,4,0,0,1),(35,'овсяные хлопья',12.3,59.5,6.1,5,1,0,1),(36,'хлопья 3 злака \"Царь\"',11.7,63.5,3,5,1,0,1),(37,'Хлопья 4 злака',11.5,63.8,3.5,5,1,0,1),(38,'хлопья овсяные',12.3,61.8,6.2,5,1,0,1),(39,'хлопья 5 злаков',12.15,61.3,4.8,5,1,0,1),(40,'хлопья 6 злаков',11.5,62.87,3.2,5,1,0,1),(41,'хлопья 7 злаков',10,67.4,2.3,5,1,0,1),(42,'горох',20.5,53.3,2,6,1,0,1),(43,'горох маш',23.5,46,2,6,1,0,1),(44,'нут',19,61,6,6,1,0,1),(45,'фасоль',20.9,64,1.8,6,1,0,1),(46,'чечевица красная',21.6,48,1.1,6,1,0,1),(47,'чечевица жёлтая',24,42.7,1.5,6,1,0,1),(48,'чечевица зеленая',24,42.7,1.5,6,1,0,1),(49,'сахар  ',0,99.7,0,7,0,0,1),(50,'сахарная пудра',0,99.8,0,7,0,0,1),(51,'сахар тростниковый',0,99.4,0,7,0,0,1),(52,'сахар ванильный',0,98.5,0,7,0,0,1),(53,'Мёд',0.8,81.5,0,7,1,0,1),(54,'сироп агавы',0,76,0.5,7,0,0,1),(55,'кленовый сироп',0,67.4,0.1,7,0,0,1),(56,'ксилит',0,97.9,0,7,0,0,1),(57,'вермишель',10.4,69.7,1.1,8,1,0,1),(58,'рожки',10.4,69.7,1.1,8,1,0,1),(59,'спагетти',10.4,69.7,1.1,8,1,0,1),(60,'фигурные макароны',10.4,69.7,1.1,8,1,0,1),(61,'пшеничная',12,65,1,9,1,0,1),(62,'гречневая',14.7,70.5,0.9,9,1,0,1),(63,'рисовая',7,79,0,9,1,0,1),(64,'фунчоза',0.7,84,0.5,8,0,0,1),(65,'перепел',21.8,0,4.5,11,0,0,1),(66,'гусь',15.2,0,39,11,0,0,1),(67,'суповой набор',16.5,0.4,12.6,12,0,0,1),(68,'бедра',21.3,0.1,11,12,0,0,1),(69,'крылышки',19.2,0,12.2,12,0,0,1),(70,'окорочка',16.8,0,10.2,12,0,0,1),(71,'куриное филе',23.1,0,1.2,12,0,0,1),(72,'цыпленок бройлерный',18.7,0.5,16.1,12,0,0,1),(73,'куриная печень',19.1,0.6,6.3,12,0,0,1),(74,'куриные желудочки',18.2,0.6,4.2,12,0,0,1),(75,'печень',19.5,0,22,13,0,0,1),(76,'бедро',15.7,0,8.9,13,0,0,1),(77,'голень',15.7,0,8.9,13,0,0,1),(78,'грудка',19.2,0,0.7,13,0,0,1),(79,'говядина     ',18.9,0,12.4,10,0,0,1),(80,'баранина   ',15.6,0,16.3,10,0,0,1),(81,'свинина',16,0,21.6,10,0,0,1),(82,'конина',20.2,0,7,10,0,0,1),(83,'кролик',21,0,8,10,0,0,1),(84,'Плавленный сыр',16.8,23.8,11.2,15,1,0,1),(85,'фетакса',8,1,25,16,1,0,1),(86,'тофу',8.1,0.6,4.2,16,1,0,1),(87,'брынза',14.6,0,25.5,16,1,0,1),(88,'сыр фета',17,1,16,16,1,0,1),(89,'сыр чечил',22.42,1.2,17.55,16,1,0,1),(90,'сыр моцарелла \"Galbani\"',13.3,20.2,13.1,16,1,0,1),(91,'сыр моцарелла \"агропродукт\" 40%',22.3,0,21.2,16,1,0,1),(92,'брынза 45%',12,3.7,16.7,16,1,0,1),(93,'молоко 1,5%',2.8,4.7,1.5,18,1,0,1),(94,'молоко 2,5%',2.8,4.7,2.5,18,1,0,1),(95,'молоко 3,2%',2.8,4.7,3.2,18,1,0,1),(96,'молоко 6%',2.9,4.69,6,18,1,0,1),(97,'молоко \"Шадринское\"',8,8.4,7.1,19,1,0,1),(98,'йогурт',2.8,14.5,2.4,14,1,0,1),(99,'ряженка 2,5%',2.9,4.2,2.5,20,1,0,1),(100,'ряженка 4%',2.8,4.2,4,20,1,0,1),(101,'кумыс',2.1,5,1.9,20,1,0,1),(102,'шубат',4.11,5.45,5.53,20,1,0,1),(103,'кефир 1%',2.8,4,1,20,1,0,1),(104,'кефир 2,5%',2.8,3.9,2.5,20,1,0,1),(105,'кефир 3,2%',2.8,4.1,3.2,20,1,0,1),(106,'творог 0%',16.5,1.3,0,21,1,0,1),(107,'творог 0,6%',18,1.8,0.6,21,1,0,1),(108,'творог 2%',18,3.3,2,21,1,0,1),(109,'творог 5%',17.2,1.8,5,21,1,0,1),(110,'творог 9%',16.7,2,9,21,1,0,1),(111,'творог 15%',15.99,1.28,12.5,21,1,0,1),(112,'иримшик',32.1,23.1,29.5,21,1,0,1),(113,'курт',25,2.7,16,21,1,0,1),(114,'сметана 10%',3,2.9,10,22,1,0,1),(115,'сметана 15%',2.6,3,15,22,1,0,1),(116,'сметана 20%',2.8,3.2,20,22,1,0,1),(117,'сливки 10%',3,4,10,23,1,0,1),(118,'сливки 20%',2.8,3.7,20,23,1,0,1),(119,'йогурт детский \"живой\" \"Амиран\" 2,8%',2.8,4.7,2.8,24,1,0,1),(120,'творог \"Амиран\" зернистый детский 9%',16,2,9,24,1,0,1),(121,'паста \"Амиран\" творожная детская 9%',18,2,9,24,1,0,1),(122,'продукт кисломолочный \"Амиран\" детский 3,5%',2.8,4.7,3.5,24,1,0,1),(123,'сыворотка молочная',0.8,3.2,0.2,24,1,0,1),(124,'кефир \"Амиран\" живой 2,5%',2.9,3.9,2.5,24,1,0,1),(125,'кефир \"Амиран\" живой 3,2%',2.8,3.6,3.2,24,1,0,1),(126,'молоко \"Амиран\" живое 2,5%',2.8,4.7,2.5,24,1,0,1),(127,'молоко \"Амиран\" живое 3,2%',2.8,4.7,3.2,24,1,0,1),(128,'простокваша \"Амиран\" кисломолочная 3,5%',2.8,4.7,3.5,24,1,0,1),(129,'сметана \"Амиран\" 20%',2.8,3.2,20,24,1,0,1),(130,'Творог \"Амиран\" живой 9%',16,2,9,24,1,0,1),(131,'Творог \"Амиран\" живой 0%',18,1.5,0.6,24,1,0,1),(132,'масло топленое \"Амиран\"',0,0,99,24,1,0,1),(133,'молоко питьевое детское и для приготовления каш \"Амиран\" 3,2%',2.8,4.7,3.2,24,1,0,1),(134,'масло облегченное 60%',1.3,1.7,60,25,1,0,1),(135,'масло сливочное 72,5%',1,1.4,72.5,25,1,0,1),(136,'масло сливочноее 82,5%',0.5,0.8,82.5,25,1,0,1),(137,'маргарин 80%',0.3,1,82,25,1,0,1),(138,'кокосовое молоко',1.8,2.7,14.9,26,1,0,1),(139,'миндальное молоко',3.6,5.6,11.1,26,1,0,1),(140,'соевое молоко',3.3,5.7,1.8,26,1,0,1),(141,'яйца куриные',12.71,0.71,10.91,27,1,0,1),(142,'яйцо гусиное',13.9,1.4,13.3,27,1,0,1),(143,'яйцо утиное',13.3,0.1,14.5,27,1,0,1),(144,'яйца перепелиные',11.92,0.58,13.08,27,1,0,1),(145,'арахис',26.3,9.2,45.2,29,1,0,1),(146,'грецкий орех',15.2,7,65.2,29,1,0,1),(147,'кедровые орешки',15.6,28.4,56,29,1,0,1),(148,'миндаль',18.6,16.2,57.7,29,1,0,1),(149,'кешью',18.5,22.5,48.5,29,1,0,1),(150,'фисташки',20,7,50,29,1,0,1),(151,'фундук',16.1,9.9,66.9,29,1,0,1),(152,'пекан',9.2,4.3,72,29,1,0,1),(153,'финики',2.5,69.2,0.5,30,0,0,1),(154,'алыча сушеная',2.3,50.2,0.5,30,0,0,1),(155,'изюм ',2.9,66,0.6,30,0,0,1),(156,'инжир сушеный',3.1,57.9,0.8,30,0,0,1),(157,'клюква сушеная',0.1,76.5,1.4,30,0,0,1),(158,'курага',5.2,51,0.3,30,0,0,1),(159,'пастила',0.5,80.8,0,30,0,0,1),(160,'ягоды годжи',11.1,53.4,2.6,30,0,0,1),(161,'чернослив',2.3,57.5,0.7,30,0,0,1),(162,'цукаты',2,71,1,30,0,0,1),(163,'яблоки сушеные',2.2,59,0.1,30,0,0,1),(164,'урюк сушеный',5,50.6,0.4,30,0,0,1),(165,'дыня сушеная',0.7,82.2,0.1,30,0,0,1),(166,'груша сушеная',2.3,62.6,0.6,30,0,0,1),(167,'персики сушеные',3,57.7,0.4,30,0,0,1),(168,'семечки подсолнечные нечищеные',20.7,3.4,52.9,31,0,0,1),(169,'семечки подсолнечные очищенные',20.7,10.5,59.9,31,0,0,1),(170,'семечки тыквенные нечищеные',11.92,6.5,20.08,31,0,0,1),(171,'семечки тыквенные очищенные',24.47,8.26,44.83,31,0,0,1),(172,'семена льна',18.3,28.9,42.2,31,0,0,1),(173,'семена чиа',16.5,42.1,30.7,31,0,0,1),(174,'кунжут семена',19.4,12.2,48.7,31,0,0,1),(175,'масло подсолнечное ',0,0,99.9,33,0,0,1),(176,'масло оливковое ',0,0,99.8,33,0,0,1),(177,'масло кукурузное',0,0,99.9,33,0,0,1),(178,'масло льняное ',0,0,99.8,33,0,0,1),(179,'масло ореховое ',0,0,100,33,1,0,1),(180,'масло горчичное',0,0,99.8,33,0,0,1),(181,'масло грецкого ореха',0,0,99.8,33,1,0,1),(182,'масло шиповника',0,0,99.8,33,0,0,1),(183,'масло кокосовое',0,0,99.9,33,0,0,1),(184,'масло облепиховое',0,0,99.5,33,0,0,1),(185,'масло хлопковое',0,0,99.7,33,0,0,1),(186,'масло кунжутное',0,0,99.9,33,1,0,1),(187,'масло соевое',0,0,99.9,33,1,0,1),(188,'масло из семян рыжика',0,0,99.8,33,0,0,1),(189,'масло из виноградных косточек',0,0,99.8,33,0,0,1),(190,'масло чесночное',0,0,99.8,33,0,0,1),(191,'масло абрикосовое нерафинированное',0,0,99.8,33,0,0,1),(192,'масло расторопши',0,0,99.8,33,0,0,1),(193,'масло конопляное нерафинированное',0,0,99.8,33,0,0,1),(194,'масло зародышей пшеницы',0,0,99.8,33,1,0,1),(195,'масло авокадо',0,0,99.8,33,0,0,1),(196,'масло тыквенное нерафинированное',0,0,99.8,33,0,0,1),(197,'масло острого перца чили',0,0,99.8,33,0,0,1),(198,'масло черного тмина',0,0,99.8,33,0,0,1),(199,'масло амарантовое',0,0,99.8,33,1,0,1),(200,'кетчуп',1.8,22.2,1,34,0,0,1),(201,'томатная паста',2.5,16.7,0.3,34,0,0,1),(202,'томатный соус',1.7,4.5,7.8,34,0,0,1),(203,'соевый соус',2.6,43,0,35,1,0,1),(204,'бальзамический соус',0.1,37.8,0,35,0,0,1),(205,'кисло-сладкий соус',0.3,39.7,0.8,35,0,0,1),(206,'острый соус томатный',2.5,21.8,0,35,0,0,1),(207,'классический майонез ',2.4,3.9,67,35,0,0,1),(208,'хрен',3.2,10.5,0.4,36,0,0,1),(209,'васаби',0,40,9,36,0,0,1),(210,'горчица',5.7,22,6.4,36,0,0,1),(211,'уксус столовый',0,2.3,0,37,0,0,1),(212,'майонез 67%',2.4,3.9,67,38,1,0,1),(213,'соус песто',5,6,45,39,0,0,1),(214,'батон нарезной',7.5,50.9,2.9,41,1,0,1),(215,'хлеб белый',8.1,48.8,1,41,1,0,1),(216,'хлеб бородинский',6.9,40.9,1.3,41,1,0,1),(217,'чиабатта классическая',7.7,47.8,3.8,41,1,0,1),(218,'хлеб ржаной зерновой',13,40,3,41,1,0,1),(219,'лаваш',7.9,47.6,1,41,1,0,1),(220,'шелпек',6,36,11,41,1,0,1),(221,'баурсаки',6.8,45.8,2.6,41,1,0,1),(222,'багет',7.5,51.4,2.9,41,1,0,1),(223,'хлебцы',11,58,2.7,40,1,0,1),(224,'сушки',11.3,70.5,4.4,43,1,0,1),(225,'баранки',16,70,1,43,1,0,1),(226,'булочки, сдобные и слоеные изделия',8.4,52.8,2.2,40,1,0,1),(227,'вешенки',2.5,6.5,0.3,45,0,0,1),(228,'шампиньоны',4.3,0.1,1,45,0,0,1),(229,'укроп',2.5,6.3,0.5,46,0,0,1),(230,'петрушка',3.7,7.6,0.4,46,0,0,1),(231,'лук зеленый',1.3,4.6,0,46,0,0,1),(232,'кинза',2.1,1.9,0.5,46,0,0,1),(233,'базилик ',2.5,4.3,0.6,46,0,0,1),(234,'джусай',1.4,8.2,0.2,46,0,0,1),(235,'мята',3.7,8,0.4,46,0,0,1),(236,'розмарин свежий',3.3,20.7,5.9,46,0,0,1),(237,'руккола',2.6,2.1,0.7,46,0,0,1),(238,'Салат айсберг',0.9,1.8,0.1,46,0,0,1),(239,'салат Радичио',1.5,3.3,0.2,46,0,0,1),(240,'салат ромэн',1.8,2.7,1.1,46,0,0,1),(241,'салат листовой',1.2,1.3,0.3,46,0,0,1),(242,'салат Лолло Росса',1.5,2,0.2,46,0,0,1),(243,'Сельдерей зелень',0.9,2.1,0.1,46,0,0,1),(244,'тимьян свежий',5.6,10.5,1.7,46,0,0,1),(245,'фенхель',1.2,7.3,0.2,46,0,0,1),(246,'чесночные стрелки',1.3,3.4,0.1,46,0,0,1),(247,'щавель',1.5,2.9,0.3,46,0,0,1),(248,'шпинат',2.9,2,0.3,46,0,0,1),(249,'виноград',0.6,16.8,0.2,47,0,0,1),(250,'апельсин',0.9,8.1,0.2,48,0,0,1),(251,'грейпфрут',0.7,6.5,0.2,48,0,0,1),(252,'лайм',0.9,3,0.1,48,0,0,1),(253,'лимон',0.9,3,0.1,48,0,0,1),(254,'мандарин',0.8,7.51,0.2,48,0,0,1),(255,'памело',0.6,6.7,0.2,48,0,0,1),(256,'авокадо',2,6,20,49,0,0,1),(257,'ананас',0.4,10.6,0.2,49,0,0,1),(258,'киви',0.5,5.15,0.3,49,0,0,1),(259,'кокос',1.7,3.1,16.75,49,0,0,1),(260,'папайя',0.6,9.5,0.1,49,0,0,1),(261,'манго',0.5,11.5,0.3,49,0,0,1),(262,'банан',1.5,21.8,0.2,49,0,0,1),(263,'яблоки',0.4,9.8,0.4,47,0,0,1),(264,'груши',0.4,10.9,0.3,47,0,0,1),(265,'абрикос',0.88,9,0.12,47,0,0,1),(266,'персик',0.91,11.31,0.11,47,0,0,1),(267,'слива',0.8,9.6,0.3,47,0,0,1),(268,'гранат',0.9,13.9,0,47,0,0,1),(269,'дыня',0.6,7.4,0.3,47,0,0,1),(270,'хурма',0.5,15.3,0.3,47,0,0,1),(271,'арбуз',0.6,5.8,0.1,47,0,0,1),(272,'черешня',1.1,11.5,0.4,50,0,0,1),(273,'асаи',0.4,12.6,0,50,0,0,1),(274,'инжир',0.7,13.7,0.2,50,0,0,1),(275,'вишня',0.8,11.3,0.5,50,0,0,1),(276,'клубника',0.8,7.5,0.4,50,0,0,1),(277,'малина   ',0.8,8.3,0.5,50,0,0,1),(278,'ежевика    ',2,6.4,0,50,0,0,1),(279,'ежемалина',1.5,7.7,0.3,50,0,0,1),(280,'голубика',1,8.2,0,50,0,0,1),(281,'смородина',0.6,7.7,0.2,50,0,0,1),(282,'жимолость',0,7.8,0,50,0,0,1),(283,'черника',1.1,7.6,0.4,50,0,0,1),(284,'шиповник',1.6,14,0,50,0,0,1),(285,'крыжовник',0.7,12,0.2,50,0,0,1),(286,'капуста белокочанная',1.8,4.7,0.1,53,0,0,1),(287,'капуста брокколи',3,5.2,0.4,53,0,0,1),(288,'капуста пекинская',1.2,2,0.2,53,0,0,1),(289,'капуста краснокочанная',0.8,7.6,0,53,0,0,1),(290,'капуста цветная',2.5,5.4,0.3,53,0,0,1),(291,'картофель',2,16.1,0.4,53,0,0,1),(292,'морковь',1.31,6.94,0.11,53,0,0,1),(293,'репа',1.5,6.2,0.1,53,0,0,1),(294,'редис',1.2,3.4,0.1,53,0,0,1),(295,'редька',1.9,6.7,0.2,53,0,0,1),(296,'редис дайкон',1.2,4.1,0,53,0,0,1),(297,'корень имбиря',1.8,15.8,0.8,53,0,0,1),(298,'топинамбур',2.1,12.8,0.1,53,0,0,1),(299,'корень сельдерея',1.3,6.5,0.3,53,0,0,1),(300,'свекла',1.5,8.8,0.1,53,0,0,1),(301,'лук репчатый',1.4,10.4,0,53,0,0,1),(302,'лук красный',1.4,9.1,0,53,0,0,1),(303,'лук-порей',2,8.2,0,53,0,0,1),(304,'чеснок ',6.5,30,0.5,53,0,0,1),(305,'перец болгарский',1.3,5.3,0,53,0,0,1),(306,'перец чили ',2,9.5,0.2,53,0,0,1),(307,'помидоры обычные',1.11,3.71,0.2,53,0,0,1),(308,'огурцы',0.8,2.8,0.1,53,0,0,1),(309,'помидоры черри',0.8,2.8,0.1,53,0,0,1),(310,'баклажаны ',1.2,4.5,0.1,53,0,0,1),(311,'кабачки',0.6,4.6,0.3,53,0,0,1),(312,'тыква',1.3,7.7,0.3,53,0,0,1),(313,'сельдерей (стебель)',0.9,2.1,0.1,53,0,0,1),(314,'спаржа (стебель)',1.9,3.1,0.1,53,0,0,1),(315,'фасоль свежая',2,3.6,0.2,53,1,0,1),(316,'патиссон   ',0.6,4.3,0.1,53,0,0,1),(317,'соя проросшая',34.9,17.3,17.3,53,1,0,1),(318,'огурцы маринованные',2.8,1.3,0,54,0,0,1),(319,'капуста квашеная',1.8,4.4,0.1,54,0,0,1),(320,'хлорелла',66.6,16.7,0.5,56,0,0,1),(321,'спирулина',4.2,9.9,2.5,56,0,0,1),(322,'отруби пшеничные',14.7,20.6,4.1,58,1,0,1),(323,'клетчатка',0.5,4,0.5,58,0,0,1),(324,'сазан филе',18.4,0,5.3,60,1,0,1),(325,'судак филе',19.2,0,0.7,60,1,0,1),(326,'анчоус',20.1,0,6.1,60,1,0,1),(327,'сёмга',21.6,0,6,60,1,0,1),(328,'минтай',15.9,0,0.9,60,1,0,1),(329,'дорадо',18,0,3,60,1,0,1),(330,'сибас',18,0,3,60,1,0,1),(331,'форель',19.2,0,2.1,60,1,0,1),(332,'скумбрия',18,0,13.2,60,1,0,1),(333,'лосось',19.8,0,6.3,60,1,0,1),(334,'мойва',13.4,0,11.5,60,1,0,1),(335,'треска',17.7,0,0.7,60,1,0,1),(336,'палтус',18.9,0,3,60,1,0,1),(337,'окунь',18.5,0,0.9,60,1,0,1),(338,'филе тилапии',20.1,0,1.7,60,1,0,1),(339,'филе пангасиуса',15,0,3,60,1,0,1),(340,'пикша',17.2,0,0.2,60,1,0,1),(341,'камбала',16.5,0,1.8,60,1,0,1),(342,'горбуша ',20.5,0,6.5,60,1,0,1),(343,'килька',17.1,0,7.6,60,1,0,1),(344,'скумбрия ',18,0,13.2,60,1,0,1),(345,'сельдь',16.3,0,10.7,60,1,0,1),(346,'креветки чищенные',22,0,1,61,1,0,1),(347,'кальмар    ',19,1.8,2.5,61,0,0,1),(348,'крабовые палочки',6,10,1,61,1,0,1),(349,'казы сырые',13.5,0.2,37.4,63,0,0,1),(350,'сосиски молочные',11,1.6,23.9,63,0,0,1),(351,'манты   ',2.2,11.2,0,64,0,0,1),(352,'кукуруза консервированная',3.6,9.8,0.1,65,0,0,1),(353,'горошек консервированный',6.7,17.4,0.3,65,0,0,1),(354,'морская капуста',0.8,0,5.1,70,0,0,1),(355,'соль',0,0,0,70,0,0,1);
/*!40000 ALTER TABLE `ingredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_alternatives`
--

DROP TABLE IF EXISTS `ingredients_alternatives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_alternatives` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ingredient_id` bigint(20) DEFAULT NULL,
  `alternative_ingredient_id` bigint(20) DEFAULT NULL,
  `alternative_category_id` bigint(20) DEFAULT NULL,
  `alternative_relation_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_alternatives_id_uindex` (`id`),
  UNIQUE KEY `ingredient_id_alternative_i_id_cateory_id_uindex` (`ingredient_id`,`alternative_ingredient_id`,`alternative_category_id`),
  KEY `ingredients_alternatives_ingredients_id_fk_2` (`alternative_ingredient_id`),
  KEY `ingredients_alternatives_alternatives_categories_id_fk` (`alternative_category_id`),
  KEY `ingredients_alternatives_alternatives_relations_id_fk` (`alternative_relation_id`),
  CONSTRAINT `ingredients_alternatives_alternatives_categories_id_fk` FOREIGN KEY (`alternative_category_id`) REFERENCES `alternatives_categories` (`id`),
  CONSTRAINT `ingredients_alternatives_alternatives_relations_id_fk` FOREIGN KEY (`alternative_relation_id`) REFERENCES `alternatives_relations` (`id`),
  CONSTRAINT `ingredients_alternatives_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `ingredients_alternatives_ingredients_id_fk_2` FOREIGN KEY (`alternative_ingredient_id`) REFERENCES `ingredients` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_alternatives`
--

LOCK TABLES `ingredients_alternatives` WRITE;
/*!40000 ALTER TABLE `ingredients_alternatives` DISABLE KEYS */;
INSERT INTO `ingredients_alternatives` VALUES (1,3,2,2,2),(2,17,18,1,2),(3,3,18,1,2),(4,18,3,1,1),(8,18,17,1,1),(9,18,19,2,1),(11,15,13,2,1),(13,18,6,2,2),(24,18,6,1,2),(26,5,4,1,1),(27,5,12,1,2),(28,45,13,1,2),(29,45,7,1,2),(30,47,13,1,2),(35,47,13,2,1),(39,18,13,1,2),(41,18,13,2,2),(43,18,1,1,1),(44,18,15,1,2),(45,18,20,1,1),(46,18,2,1,2),(47,1,2,1,1),(48,1,1,1,1),(49,1,19,2,1),(50,20,13,1,2),(51,15,3,1,2),(52,49,3,1,2),(53,49,5,2,1),(54,52,13,1,1),(55,52,50,2,1),(56,15,19,1,2),(57,15,18,2,1);
/*!40000 ALTER TABLE `ingredients_alternatives` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_categories`
--

DROP TABLE IF EXISTS `ingredients_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_category_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_category_id_uindex` (`id`),
  KEY `ingredients_categories_ingredients_categories_id_fk` (`parent_category_id`),
  CONSTRAINT `ingredients_categories_ingredients_categories_id_fk` FOREIGN KEY (`parent_category_id`) REFERENCES `ingredients_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_categories`
--

LOCK TABLES `ingredients_categories` WRITE;
/*!40000 ALTER TABLE `ingredients_categories` DISABLE KEYS */;
INSERT INTO `ingredients_categories` VALUES (1,'Бакалея',NULL),(2,'Ингредиенты для выпечки',1),(3,'Мука и смеси для выпечки',1),(4,'Крупы, бобы',1),(5,'Овсяные и злаковые хлопья, мюсли',4),(6,'Бобовые',4),(7,'Сахар и другие подсластители',1),(8,'Макароны высшего сорта',1),(9,'Лапша',8),(10,'Мясо',NULL),(11,'Птица',10),(12,'Курица',10),(13,'Индейка',12),(14,'Молочные продукты',NULL),(15,'Сырная лавка',14),(16,'Рассольный сыр',15),(17,'натуральные закваски',14),(18,'молоко',14),(19,'молоко сгущенное и концентрированное',18),(20,'кисломолочные продукты',14),(21,'творог и творожные изделия',14),(22,'сметана',14),(23,'сливки',14),(24,'Молочная продукция \"Амиран\"',14),(25,'сливочное масло, маргарин',14),(26,'растительное молоко',14),(27,'яйца',NULL),(28,'Орехи, сухофрукты, семена',NULL),(29,'орехи весовые',28),(30,'сухофрукты',28),(31,'семена',28),(32,'масла, соусы, заправки',NULL),(33,'растительное масло',32),(34,'кетчуп, томатная паста',32),(35,'соусы',32),(36,'хрен, горчица, уксус, маринад',32),(37,'уксус',32),(38,'майонез',32),(39,'прочие соусы и лимонады',32),(40,'хлеб, сдоба, хлебцы',NULL),(41,'хлеб',40),(42,'цельнозерновой хлеб',40),(43,'сушки и баранки',40),(44,'грибы',NULL),(45,'грибы свежие',44),(46,'зелень',NULL),(47,'фрукты, ягоды',NULL),(48,'цитрусовые',47),(49,'экзотические',47),(50,'ягоды свежие',47),(51,'фрукты и ягоды замороженные',47),(52,'овощи',NULL),(53,'овощи свежие',52),(54,'овощи маринованные',52),(55,'овощи замороженные',52),(56,'БАДы',NULL),(57,'диетическое питание',NULL),(58,'отруби и семена',57),(59,'рыба, морепродукты и икра',NULL),(60,'рыба',59),(61,'морепродукты',59),(62,'полуфабрикаты',NULL),(63,'мясные полуфабрикаты',62),(64,'пельмени, вареники, манты',62),(65,'консервы',NULL),(66,'грибные консервы',65),(67,'оливки и маслины',65),(68,'овощные консервы',65),(69,'чипсы, снэхи, сухарики',NULL),(70,'азиатская кухня',NULL);
/*!40000 ALTER TABLE `ingredients_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_changable_props`
--

DROP TABLE IF EXISTS `ingredients_changable_props`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_changable_props` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_changable_props_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_changable_props`
--

LOCK TABLES `ingredients_changable_props` WRITE;
/*!40000 ALTER TABLE `ingredients_changable_props` DISABLE KEYS */;
INSERT INTO `ingredients_changable_props` VALUES (1,'PROTEIN','Protein'),(2,'CARBOHYDRATE','Carbohydrate'),(3,'FAT','Fat'),(4,'WEIGHT','Weight'),(5,'VOLUME','Volume');
/*!40000 ALTER TABLE `ingredients_changable_props` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_cooking_types`
--

DROP TABLE IF EXISTS `ingredients_cooking_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_cooking_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ingredient_id` bigint(20) NOT NULL,
  `cooking_type_id` bigint(20) NOT NULL,
  `difference_coefficient` double NOT NULL,
  `ingredients_changable_prop_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_cooking_types_id_uindex` (`id`),
  UNIQUE KEY `ingr_id_cooking_type_id_prop_id_uindex` (`ingredient_id`,`cooking_type_id`,`ingredients_changable_prop_id`),
  KEY `ingredients_cooking_types_cooking_types_id_fk` (`cooking_type_id`),
  KEY `ingredients_cooking_types_ingredients_changable_props_id_fk` (`ingredients_changable_prop_id`),
  CONSTRAINT `ingredients_cooking_types_cooking_types_id_fk` FOREIGN KEY (`cooking_type_id`) REFERENCES `cooking_types` (`id`),
  CONSTRAINT `ingredients_cooking_types_ingredients_changable_props_id_fk` FOREIGN KEY (`ingredients_changable_prop_id`) REFERENCES `ingredients_changable_props` (`id`),
  CONSTRAINT `ingredients_cooking_types_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_cooking_types`
--

LOCK TABLES `ingredients_cooking_types` WRITE;
/*!40000 ALTER TABLE `ingredients_cooking_types` DISABLE KEYS */;
INSERT INTO `ingredients_cooking_types` VALUES (1,3,1,1.2,4),(3,3,1,1.2,3),(5,18,1,1.2,5),(6,18,1,0.95,4),(7,18,2,0.55,5),(17,18,2,0.43,2);
/*!40000 ALTER TABLE `ingredients_cooking_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_ingredients_props`
--

DROP TABLE IF EXISTS `ingredients_ingredients_props`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_ingredients_props` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ingredient_id` bigint(20) NOT NULL,
  `ingredient_props_category_id` bigint(20) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_ingredients_props_id_uindex` (`id`),
  KEY `ingredients_ingredients_props_ingredients_props_categories_id_fk` (`ingredient_props_category_id`),
  KEY `ingredients_ingredients_props_ingredients_id_fk` (`ingredient_id`),
  CONSTRAINT `ingredients_ingredients_props_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `ingredients_ingredients_props_ingredients_props_categories_id_fk` FOREIGN KEY (`ingredient_props_category_id`) REFERENCES `ingredients_props_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_ingredients_props`
--

LOCK TABLES `ingredients_ingredients_props` WRITE;
/*!40000 ALTER TABLE `ingredients_ingredients_props` DISABLE KEYS */;
INSERT INTO `ingredients_ingredients_props` VALUES (1,50,1,'3.2'),(2,51,1,'0'),(3,6,5,'October - December'),(4,53,3,'20'),(5,54,4,'20'),(6,56,5,'\"October\"');
/*!40000 ALTER TABLE `ingredients_ingredients_props` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_props`
--

DROP TABLE IF EXISTS `ingredients_props`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_props` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_properties_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_props`
--

LOCK TABLES `ingredients_props` WRITE;
/*!40000 ALTER TABLE `ingredients_props` DISABLE KEYS */;
INSERT INTO `ingredients_props` VALUES (1,'Жирность'),(2,'Сезонность');
/*!40000 ALTER TABLE `ingredients_props` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_props_categories`
--

DROP TABLE IF EXISTS `ingredients_props_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_props_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ingredient_prop_id` bigint(20) NOT NULL,
  `ingredient_category_id` bigint(20) NOT NULL,
  `unit_data_type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_props_categories_map_id_uindex` (`id`),
  UNIQUE KEY `props_id_category_id_uindex` (`ingredient_prop_id`,`ingredient_category_id`),
  KEY `ingredients_props_categories_map_ingredients_categories_id_fk` (`ingredient_category_id`),
  KEY `ingredients_props_categories_map_unit_data_types_id_fk` (`unit_data_type_id`),
  CONSTRAINT `ingredients_props_categories_map_ingredients_categories_id_fk` FOREIGN KEY (`ingredient_category_id`) REFERENCES `ingredients_categories` (`id`),
  CONSTRAINT `ingredients_props_categories_map_ingredients_props_id_fk` FOREIGN KEY (`ingredient_prop_id`) REFERENCES `ingredients_props` (`id`),
  CONSTRAINT `ingredients_props_categories_map_unit_data_types_id_fk` FOREIGN KEY (`unit_data_type_id`) REFERENCES `unit_data_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_props_categories`
--

LOCK TABLES `ingredients_props_categories` WRITE;
/*!40000 ALTER TABLE `ingredients_props_categories` DISABLE KEYS */;
INSERT INTO `ingredients_props_categories` VALUES (1,1,11,2),(2,2,13,3),(3,1,14,2),(4,1,2,2),(5,2,5,3);
/*!40000 ALTER TABLE `ingredients_props_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_unit_types`
--

DROP TABLE IF EXISTS `ingredients_unit_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_unit_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ingredient_id` bigint(20) NOT NULL,
  `unit_type_id` bigint(20) NOT NULL,
  `weight` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_unit_types_id_uindex` (`id`),
  UNIQUE KEY `ingredients_unit_types_u` (`ingredient_id`,`unit_type_id`),
  KEY `ingredients_unit_types_unit_types_id_fk` (`unit_type_id`),
  CONSTRAINT `ingredients_unit_types_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `ingredients_unit_types_unit_types_id_fk` FOREIGN KEY (`unit_type_id`) REFERENCES `unit_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_unit_types`
--

LOCK TABLES `ingredients_unit_types` WRITE;
/*!40000 ALTER TABLE `ingredients_unit_types` DISABLE KEYS */;
INSERT INTO `ingredients_unit_types` VALUES (54,1,2,7),(55,2,2,5),(56,3,2,7),(57,4,2,5),(58,5,2,5),(59,8,2,8),(60,9,2,7),(61,10,2,8),(62,11,2,7),(63,12,2,7),(64,13,2,7),(65,14,2,7),(66,15,2,10),(67,16,2,7),(68,17,2,7),(69,18,2,7),(70,19,2,7),(71,20,2,10),(72,21,2,10),(73,22,2,10),(74,23,2,8),(75,24,2,7),(76,25,2,10),(77,26,2,7),(78,28,2,7),(79,29,2,7),(80,30,2,7),(81,31,2,8),(82,32,2,7),(83,33,2,7),(84,34,2,7),(85,42,2,7),(86,43,2,7),(87,44,2,7),(88,45,2,7),(89,46,2,7),(90,47,2,7),(91,48,2,7),(92,49,2,10),(93,50,2,7),(94,51,2,10),(95,52,2,7),(96,53,2,8),(97,54,2,7),(98,55,2,7),(99,56,2,7),(100,57,2,7),(101,58,2,7),(102,59,2,7),(103,60,2,7),(104,61,2,7),(105,62,2,7),(106,63,2,7),(107,64,2,7),(108,99,2,7),(109,100,2,7),(110,101,2,7),(111,114,2,7),(112,115,2,7),(113,116,2,7),(114,134,2,7),(115,169,2,10),(116,212,2,7),(117,355,2,11),(118,1,1,20),(119,2,1,15),(120,3,1,20),(121,4,1,15),(122,5,1,15),(123,8,1,25),(124,9,1,20),(125,10,1,25),(126,11,1,20),(127,12,1,20),(128,13,1,20),(129,14,1,20),(130,15,1,30),(131,16,1,20),(132,17,1,20),(133,18,1,20),(134,19,1,20),(135,20,1,30),(136,21,1,30),(137,22,1,30),(138,23,1,25),(139,24,1,20),(140,25,1,30),(141,26,1,20),(142,28,1,20),(143,29,1,20),(144,30,1,20),(145,31,1,25),(146,32,1,20),(147,33,1,20),(148,34,1,20),(149,42,1,20),(150,43,1,20),(151,44,1,20),(152,45,1,20),(153,46,1,20),(154,47,1,20),(155,48,1,20),(156,49,1,25),(157,50,1,20),(158,51,1,30),(159,52,1,20),(160,53,1,25),(161,54,1,20),(162,55,1,20),(163,56,1,20),(164,57,1,20),(165,58,1,20),(166,59,1,20),(167,60,1,20),(168,61,1,20),(169,62,1,20),(170,63,1,20),(171,64,1,20),(172,99,1,20),(173,100,1,20),(174,101,1,20),(175,114,1,20),(176,115,1,20),(177,116,1,20),(178,134,1,20),(179,175,1,17),(180,211,1,15),(181,212,1,20),(182,355,1,30),(183,1,5,200),(184,2,5,100),(185,3,5,200),(186,4,5,100),(187,5,5,100),(188,9,5,200),(189,10,5,130),(190,11,5,200),(191,12,5,200),(192,13,5,200),(193,14,5,200),(194,15,5,160),(195,16,5,200),(196,17,5,200),(197,18,5,200),(198,19,5,200),(199,20,5,150),(200,21,5,160),(201,22,5,160),(202,23,5,170),(203,24,5,150),(204,25,5,160),(205,26,5,200),(206,28,5,200),(207,29,5,200),(208,30,5,200),(209,31,5,185),(210,32,5,200),(211,33,5,140),(212,34,5,200),(213,35,5,70),(214,36,5,70),(215,37,5,70),(216,38,5,70),(217,39,5,70),(218,40,5,70),(219,41,5,70),(220,42,5,185),(221,43,5,200),(222,44,5,200),(223,45,5,200),(224,46,5,200),(225,47,5,200),(226,48,5,200),(227,49,5,160),(228,50,5,200),(229,51,5,160),(230,52,5,200),(231,53,5,200),(232,54,5,200),(233,55,5,200),(234,56,5,200),(235,57,5,200),(236,58,5,200),(237,59,5,200),(238,60,5,200),(239,61,5,200),(240,62,5,200),(241,63,5,200),(242,64,5,200),(243,93,5,200),(244,94,5,200),(245,95,5,200),(246,96,5,200),(247,99,5,200),(248,100,5,200),(249,101,5,200),(250,103,5,200),(251,104,5,200),(252,114,5,200),(253,115,5,200),(254,116,5,200),(255,134,5,200),(256,138,5,200),(257,155,5,152),(258,212,5,200),(259,275,5,130),(260,281,5,125),(261,355,5,220),(262,141,6,55),(263,142,6,100),(264,143,6,90),(265,144,6,12),(266,221,6,50),(267,223,6,12),(268,225,6,42),(269,226,6,35),(270,250,6,130),(271,251,6,130.5),(272,252,6,50),(273,253,6,130),(274,254,6,55),(275,255,6,1000),(276,256,6,240),(277,257,6,1000),(278,258,6,50),(279,259,6,50),(280,260,6,1000),(281,261,6,350),(282,262,6,110),(283,263,6,165),(284,264,6,135),(285,265,6,26),(286,266,6,85),(287,267,6,30),(288,268,6,125),(289,270,6,200),(290,291,6,100),(291,292,6,75),(292,294,6,30),(293,295,6,150),(294,296,6,250),(295,298,6,50),(296,299,6,250),(297,300,6,125),(298,301,6,75),(299,302,6,100),(300,303,6,50),(301,304,6,4),(302,305,6,145),(303,306,6,20),(304,307,6,95),(305,308,6,100),(306,309,6,20),(307,310,6,200),(308,311,6,1000),(309,313,6,100),(310,1,3,30);
/*!40000 ALTER TABLE `ingredients_unit_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phases`
--

DROP TABLE IF EXISTS `phases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phases` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '1',
  `proteins_percentage` float NOT NULL,
  `carbs_percentage` float NOT NULL,
  `fats_percentage` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phases_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phases`
--

LOCK TABLES `phases` WRITE;
/*!40000 ALTER TABLE `phases` DISABLE KEYS */;
INSERT INTO `phases` VALUES (1,'Molecular',1,0.25,0.25,0.5),(2,'Adaptive',2,0.25,0.35,0.4),(3,'Intuitive',3,0.2,0.4,0.4);
/*!40000 ALTER TABLE `phases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programs`
--

DROP TABLE IF EXISTS `programs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `purpose_id` bigint(20) NOT NULL,
  `calories_difference_coefficient` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `programs_id_uindex` (`id`),
  KEY `programs_purposes_id_fk` (`purpose_id`),
  CONSTRAINT `programs_purposes_id_fk` FOREIGN KEY (`purpose_id`) REFERENCES `purposes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programs`
--

LOCK TABLES `programs` WRITE;
/*!40000 ALTER TABLE `programs` DISABLE KEYS */;
INSERT INTO `programs` VALUES (1,'Novalong weight lose program ',2,0.8),(2,'Schwarzenegger muscle gain program',1,1.2);
/*!40000 ALTER TABLE `programs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programs_dish_types`
--

DROP TABLE IF EXISTS `programs_dish_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programs_dish_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `program_id` bigint(20) NOT NULL,
  `dish_type_id` bigint(20) NOT NULL,
  `calories_percentage` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `programs_dish_types_id_uindex` (`id`),
  KEY `programs_dish_types_programs_id_fk` (`program_id`),
  KEY `programs_dish_types_dish_types_id_fk` (`dish_type_id`),
  CONSTRAINT `programs_dish_types_dish_types_id_fk` FOREIGN KEY (`dish_type_id`) REFERENCES `dish_types` (`id`),
  CONSTRAINT `programs_dish_types_programs_id_fk` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programs_dish_types`
--

LOCK TABLES `programs_dish_types` WRITE;
/*!40000 ALTER TABLE `programs_dish_types` DISABLE KEYS */;
INSERT INTO `programs_dish_types` VALUES (1,1,1,0.2),(2,1,2,0.3),(3,1,3,0.3),(4,1,5,0.2),(5,2,1,0.2),(6,2,2,0.1),(7,2,3,0.3),(8,2,4,0.1),(9,2,5,0.2),(10,2,6,0.1);
/*!40000 ALTER TABLE `programs_dish_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purposes`
--

DROP TABLE IF EXISTS `purposes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purposes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `purposes_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purposes`
--

LOCK TABLES `purposes` WRITE;
/*!40000 ALTER TABLE `purposes` DISABLE KEYS */;
INSERT INTO `purposes` VALUES (1,'GAIN','Muscle Gain'),(2,'LOSE','Weight Lose');
/*!40000 ALTER TABLE `purposes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_USER');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unit_data_types`
--

DROP TABLE IF EXISTS `unit_data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unit_data_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `data_type_code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unit_data_types_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unit_data_types`
--

LOCK TABLES `unit_data_types` WRITE;
/*!40000 ALTER TABLE `unit_data_types` DISABLE KEYS */;
INSERT INTO `unit_data_types` VALUES (1,'Грамм','FLOAT'),(2,'Процент','FLOAT'),(3,'Месяц','STRING');
/*!40000 ALTER TABLE `unit_data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unit_types`
--

DROP TABLE IF EXISTS `unit_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unit_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `unit_data_type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unit_types_id_uindex` (`id`),
  UNIQUE KEY `unit_types_code_uindex` (`code`),
  KEY `unit_types_unit_data_types_id_fk` (`unit_data_type_id`),
  CONSTRAINT `unit_types_unit_data_types_id_fk` FOREIGN KEY (`unit_data_type_id`) REFERENCES `unit_data_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unit_types`
--

LOCK TABLES `unit_types` WRITE;
/*!40000 ALTER TABLE `unit_types` DISABLE KEYS */;
INSERT INTO `unit_types` VALUES (1,'TABLESPOON','столовая ложка',1),(2,'TEASPOON','чайная ложка',1),(3,'HANDFUL','горсть',1),(4,'GRAMS','грамм',1),(5,'GLASS','Стакан',1),(6,'PIECE','Штук',1);
/*!40000 ALTER TABLE `unit_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_daily_rations`
--

DROP TABLE IF EXISTS `user_daily_rations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_daily_rations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_ration_id` bigint(20) NOT NULL,
  `dish_type_id` bigint(20) NOT NULL,
  `dish_id` bigint(20) NOT NULL,
  `day_code` varchar(10) NOT NULL,
  `protein` float NOT NULL,
  `carbohydrate` float NOT NULL,
  `fat` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_daily_rations_id_uindex` (`id`),
  UNIQUE KEY `ration_id_dish_type_id_dish_id_uindex` (`user_ration_id`,`dish_type_id`,`dish_id`),
  KEY `user_daily_rations_dish_types_id_fk` (`dish_type_id`),
  KEY `user_daily_rations_dishes_id_fk` (`dish_id`),
  CONSTRAINT `user_daily_rations_dish_types_id_fk` FOREIGN KEY (`dish_type_id`) REFERENCES `dish_types` (`id`),
  CONSTRAINT `user_daily_rations_dishes_id_fk` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`),
  CONSTRAINT `user_daily_rations_user_rations_id_fk` FOREIGN KEY (`user_ration_id`) REFERENCES `user_rations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=283 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_daily_rations`
--

LOCK TABLES `user_daily_rations` WRITE;
/*!40000 ALTER TABLE `user_daily_rations` DISABLE KEYS */;
INSERT INTO `user_daily_rations` VALUES (276,35,3,35,'WED',99.9,40.4,63),(277,35,1,37,'WED',20,177.6,3),(278,35,2,18,'WED',9,3,17),(279,35,5,36,'WED',96,210.7,58),(280,35,1,5,'WED',88,89,77),(281,35,1,34,'WED',4.4,32.2,1),(282,35,2,6,'WED',58,90,144);
/*!40000 ALTER TABLE `user_daily_rations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_rations`
--

DROP TABLE IF EXISTS `user_rations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_rations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `program_id` bigint(20) NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `total_calories_needed` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_rations_id_uindex` (`id`),
  UNIQUE KEY `user_id_program_id_uindex` (`user_id`,`program_id`),
  KEY `user_rations_programs_id_fk` (`program_id`),
  CONSTRAINT `user_rations_programs_id_fk` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`),
  CONSTRAINT `user_rations_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_rations`
--

LOCK TABLES `user_rations` WRITE;
/*!40000 ALTER TABLE `user_rations` DISABLE KEYS */;
INSERT INTO `user_rations` VALUES (35,1,1,'2018-07-25 05:21:26','2018-07-26 05:21:26',1719);
/*!40000 ALTER TABLE `user_rations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `gender_id` bigint(20) NOT NULL,
  `age` int(11) NOT NULL,
  `height` double NOT NULL,
  `weight` double NOT NULL,
  `purpose_id` bigint(20) DEFAULT NULL,
  `activity_level_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) NOT NULL,
  `current_phase_id` bigint(20) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `phase_update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_id_uindex` (`id`),
  UNIQUE KEY `username_uindex` (`username`),
  KEY `users_roles_id_fk` (`role_id`),
  KEY `users_genders_id_fk` (`gender_id`),
  KEY `users_purposes_id_fk` (`purpose_id`),
  KEY `users_activity_levels_id_fk` (`activity_level_id`),
  KEY `users_phases_id_fk` (`current_phase_id`),
  CONSTRAINT `users_activity_levels_id_fk` FOREIGN KEY (`activity_level_id`) REFERENCES `activity_levels` (`id`),
  CONSTRAINT `users_genders_id_fk` FOREIGN KEY (`gender_id`) REFERENCES `genders` (`id`),
  CONSTRAINT `users_phases_id_fk` FOREIGN KEY (`current_phase_id`) REFERENCES `phases` (`id`),
  CONSTRAINT `users_purposes_id_fk` FOREIGN KEY (`purpose_id`) REFERENCES `purposes` (`id`),
  CONSTRAINT `users_roles_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'tyrion','$2a$10$LTu15SRR31VNZ3P8OtAf1exnKzZ41TparWDh4gR79dPM/eqIbdNZm',1,42,120,41,2,3,1,1,1,'2018-07-16 16:15:51'),(2,'rhaegar','$2a$10$dCcpU10OFTP9CBb8fbFqYuy1Gu39QQlUM7sKWNzDpz1KP94W2USAa',1,39,187,82,1,3,2,1,0,'2018-07-16 16:15:51'),(3,'aegon','$2a$10$LTu15SRR31VNZ3P8OtAf1exnKzZ41TparWDh4gR79dPM/eqIbdNZm',1,49,190,81,1,1,2,1,1,'2018-07-16 16:15:51'),(4,'sansa','$2a$10$dCcpU10OFTP9CBb8fbFqYuy1Gu39QQlUM7sKWNzDpz1KP94W2USAa',2,17,164,47,2,2,2,1,1,'2018-07-16 16:15:51'),(5,'arya','$2a$10$VI56Jp0VR6hEBSNp1QkxN.VI6LqAMQNVubkpWONZpu2ONEwRBA0Ne',2,14,153,42,2,3,2,1,1,'2018-07-16 16:15:51'),(6,'sersey','$2a$10$UjKxdt0kmA85x8MuWAzrHu2S/2IAoKkWVLr8XexRdh4UEsiAmB9cm',2,34,170,51,2,1,2,1,1,'2018-07-16 16:15:51'),(7,'lianna',NULL,2,17,153,42,2,3,2,1,1,'2018-07-16 16:15:51'),(8,'test',NULL,1,17,153,42,2,3,2,1,1,'2018-07-16 16:15:51'),(9,'john',NULL,1,22,164,55,1,3,2,1,1,'2018-07-16 16:15:51');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_dietary_restrictions`
--

DROP TABLE IF EXISTS `users_dietary_restrictions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_dietary_restrictions` (
  `user_id` bigint(20) NOT NULL,
  `dietary_restriction_id` bigint(20) NOT NULL,
  KEY `user_dietary_restrictions_users_id_fk` (`user_id`),
  KEY `user_dietary_restrictions_dietary_restrictions_id_fk` (`dietary_restriction_id`),
  CONSTRAINT `user_dietary_restrictions_dietary_restrictions_id_fk` FOREIGN KEY (`dietary_restriction_id`) REFERENCES `dietary_restrictions` (`id`),
  CONSTRAINT `user_dietary_restrictions_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_dietary_restrictions`
--

LOCK TABLES `users_dietary_restrictions` WRITE;
/*!40000 ALTER TABLE `users_dietary_restrictions` DISABLE KEYS */;
INSERT INTO `users_dietary_restrictions` VALUES (4,2),(4,1),(5,3);
/*!40000 ALTER TABLE `users_dietary_restrictions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `week_days`
--

DROP TABLE IF EXISTS `week_days`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `week_days` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `week_days_id_uindex` (`id`),
  UNIQUE KEY `week_days_code_uindex` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `week_days`
--

LOCK TABLES `week_days` WRITE;
/*!40000 ALTER TABLE `week_days` DISABLE KEYS */;
INSERT INTO `week_days` VALUES (1,'MON','Monday'),(2,'TUE','Tuesday'),(3,'WED','Wednesday'),(4,'THU','Thursday'),(5,'FRI','Friday'),(6,'SAT','Saturday'),(7,'SUN','Sunday');
/*!40000 ALTER TABLE `week_days` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-26 11:09:38
