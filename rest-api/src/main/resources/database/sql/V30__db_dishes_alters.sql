ALTER TABLE dishes
  MODIFY COLUMN active tinyint(4) NOT NULL DEFAULT '1' AFTER photo;

ALTER TABLE ingredients ADD kilocalories float NOT NULL AFTER carbohydrate;

INSERT INTO ingredients_categories_codes (category_id, code) VALUE
  ((SELECT id FROM ingredients_categories WHERE name LIKE 'Алкогольные напитки'), 'ALCOHOL');


# script recalculates kilocalories for each ingredient according to prots, carbs, fats
drop procedure if exists CALCULATE_INGREDIENTS_KILOCALORIES;
delimiter ;;

create procedure CALCULATE_INGREDIENTS_KILOCALORIES()
  begin
    declare i int default 0;
    declare n int default 0;

    select COUNT(*) from ingredients into n;

    set i = 0;

    while i < n do
      set @kilocals = (select protein from ingredients limit 1 offset i) * 4 +
                      (select carbohydrate from ingredients limit 1 offset i) * 4 +
                      (select fat from ingredients limit 1 offset i) * 9;

      set @curr_id = (select id from ingredients limit 1 offset i);

      update ingredients set kilocalories = round(@kilocals, 2)
      where id = @curr_id;
      set i = i + 1;
    end while ;
  end;;
delimiter ;

call CALCULATE_INGREDIENTS_KILOCALORIES();