-- MySQL dump 10.13  Distrib 5.7.23, for Linux (x86_64)
--
-- Host: localhost    Database: novalong_db
-- ------------------------------------------------------
-- Server version	5.7.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_levels`
--

DROP TABLE IF EXISTS `activity_levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_levels` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `coefficient` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `activity_levels_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_levels`
--

LOCK TABLES `activity_levels` WRITE;
/*!40000 ALTER TABLE `activity_levels` DISABLE KEYS */;
INSERT INTO `activity_levels` VALUES (1,'SEDENTARY','Sedentary or light activity','Office worker getting little or no exercise',1.53),(2,'ACTIVE','Active or moderately active','Construction worker or person running one hour daily',1.76),(3,'EXTREME','Vigorously active','Agricultural worker (non mechanized) or person swimming two hours daily	',2.25);
/*!40000 ALTER TABLE `activity_levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alternatives_categories`
--

DROP TABLE IF EXISTS `alternatives_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alternatives_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alternatives_categories_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alternatives_categories`
--

LOCK TABLES `alternatives_categories` WRITE;
/*!40000 ALTER TABLE `alternatives_categories` DISABLE KEYS */;
INSERT INTO `alternatives_categories` VALUES (1,'PRICE'),(2,'HEALTH');
/*!40000 ALTER TABLE `alternatives_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alternatives_relations`
--

DROP TABLE IF EXISTS `alternatives_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alternatives_relations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `pair_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alternatives_relations_id_uindex` (`id`),
  KEY `alternatives_relations_alternatives_relations_id_fk` (`pair_id`),
  CONSTRAINT `alternatives_relations_alternatives_relations_id_fk` FOREIGN KEY (`pair_id`) REFERENCES `alternatives_relations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alternatives_relations`
--

LOCK TABLES `alternatives_relations` WRITE;
/*!40000 ALTER TABLE `alternatives_relations` DISABLE KEYS */;
INSERT INTO `alternatives_relations` VALUES (1,'LESS',2),(2,'MORE',1);
/*!40000 ALTER TABLE `alternatives_relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cooking_types`
--

DROP TABLE IF EXISTS `cooking_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cooking_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cooking_types_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cooking_types`
--

LOCK TABLES `cooking_types` WRITE;
/*!40000 ALTER TABLE `cooking_types` DISABLE KEYS */;
INSERT INTO `cooking_types` VALUES (1,'Boiling'),(2,'Roasting');
/*!40000 ALTER TABLE `cooking_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dietary_restrictions`
--

DROP TABLE IF EXISTS `dietary_restrictions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dietary_restrictions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dietary_restrictions_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dietary_restrictions`
--

LOCK TABLES `dietary_restrictions` WRITE;
/*!40000 ALTER TABLE `dietary_restrictions` DISABLE KEYS */;
INSERT INTO `dietary_restrictions` VALUES (1,'DIABETES','Sugar diabetes'),(2,'PREGNANCY','Pregnancy'),(3,'HYPERTENSION','Hypertension'),(4,'GASTRITIS','Gastritis');
/*!40000 ALTER TABLE `dietary_restrictions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dish_categories`
--

DROP TABLE IF EXISTS `dish_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dish_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dish_categories_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dish_categories`
--

LOCK TABLES `dish_categories` WRITE;
/*!40000 ALTER TABLE `dish_categories` DISABLE KEYS */;
INSERT INTO `dish_categories` VALUES (1,'Другое'),(2,'Гарниры'),(3,'Овощные гарниры'),(4,'Мясные'),(5,'Каши'),(6,'Мучное');
/*!40000 ALTER TABLE `dish_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dish_ingredients_alternatives`
--

DROP TABLE IF EXISTS `dish_ingredients_alternatives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dish_ingredients_alternatives` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dish_id` bigint(20) NOT NULL,
  `ingredient_id` bigint(20) NOT NULL,
  `alternative_ingredient_id` bigint(20) NOT NULL,
  `alternative_category_id` bigint(20) NOT NULL,
  `alternative_relation_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dish_ingredients_alternatives_id_uindex` (`id`),
  UNIQUE KEY `dish_id_ingredient_id_alternative_i_id_category_id_uindex` (`dish_id`,`ingredient_id`,`alternative_ingredient_id`,`alternative_category_id`),
  KEY `dish_ingredients_alternatives_ingredients_id_fk` (`ingredient_id`),
  KEY `dish_ingredients_alternatives_alternatives_relations_id_fk` (`alternative_relation_id`),
  KEY `dish_ingredients_alternatives_alternatives_categories_id_fk` (`alternative_category_id`),
  KEY `dish_ingredients_alternatives_ingredients_id_fk_2` (`alternative_ingredient_id`),
  CONSTRAINT `dish_ingredients_alternatives_alternatives_categories_id_fk` FOREIGN KEY (`alternative_category_id`) REFERENCES `alternatives_categories` (`id`),
  CONSTRAINT `dish_ingredients_alternatives_alternatives_relations_id_fk` FOREIGN KEY (`alternative_relation_id`) REFERENCES `alternatives_relations` (`id`),
  CONSTRAINT `dish_ingredients_alternatives_dishes_id_fk` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`),
  CONSTRAINT `dish_ingredients_alternatives_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `dish_ingredients_alternatives_ingredients_id_fk_2` FOREIGN KEY (`alternative_ingredient_id`) REFERENCES `ingredients` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dish_ingredients_alternatives`
--

LOCK TABLES `dish_ingredients_alternatives` WRITE;
/*!40000 ALTER TABLE `dish_ingredients_alternatives` DISABLE KEYS */;
/*!40000 ALTER TABLE `dish_ingredients_alternatives` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dish_types`
--

DROP TABLE IF EXISTS `dish_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dish_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dish_type_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dish_types`
--

LOCK TABLES `dish_types` WRITE;
/*!40000 ALTER TABLE `dish_types` DISABLE KEYS */;
INSERT INTO `dish_types` VALUES (1,'Breakfast'),(2,'Second breakfast'),(3,'Lunch'),(4,'Snacks'),(5,'Dinner'),(6,'Second dinner');
/*!40000 ALTER TABLE `dish_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dish_unit_types`
--

DROP TABLE IF EXISTS `dish_unit_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dish_unit_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dish_id` bigint(20) NOT NULL,
  `unit_type_id` bigint(20) NOT NULL,
  `weight` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dish_unit_types_id_uindex` (`id`),
  UNIQUE KEY `dish_id_unit_id_uindex` (`dish_id`,`unit_type_id`),
  KEY `dish_unit_types_unit_types_id_fk` (`unit_type_id`),
  CONSTRAINT `dish_unit_types_dishes_id_fk` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`),
  CONSTRAINT `dish_unit_types_unit_types_id_fk` FOREIGN KEY (`unit_type_id`) REFERENCES `unit_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dish_unit_types`
--

LOCK TABLES `dish_unit_types` WRITE;
/*!40000 ALTER TABLE `dish_unit_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `dish_unit_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dishes`
--

DROP TABLE IF EXISTS `dishes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dishes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `recipe_description` text,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `weight` float NOT NULL,
  `protein` float NOT NULL,
  `fat` float NOT NULL,
  `carbohydrate` float NOT NULL,
  `dish_category_id` bigint(20) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `dishes_id_uindex` (`id`),
  KEY `dishes_dish_categories_id_fk` (`dish_category_id`),
  CONSTRAINT `dishes_dish_categories_id_fk` FOREIGN KEY (`dish_category_id`) REFERENCES `dish_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dishes`
--

LOCK TABLES `dishes` WRITE;
/*!40000 ALTER TABLE `dishes` DISABLE KEYS */;
INSERT INTO `dishes` VALUES (40,'Бауырсаки',NULL,NULL,'2018-07-30 08:52:43',1186.5,71.27,63.62,543.67,4,1),(41,'Бефстроганов из говядины','','1. Подготавливаем все ингредиенты. Мясо помойте и обсушите.\n2. Не разрезая говядину слегка отбиваем её. Я делаю это через пищевую плёнку, чтобы мясо не разбивалось, а молоток не забивался кусочками мяса, которые потом очень сложно вымыть.\n3. Нарезаем мясо поперёк волокон на тонкие ломтики (толщина около 5 мм).\n4. Ломтики нарезаем тонкой соломкой. Чем тоньше мы нарежем мясо, тем быстрее оно обжариться и тем больше сока останется внутри.\n5. Лук нарезаем тонкими четвертькольцами.\n6. Немного растительного масла наливаем в сковороду и очень хорошо разогреваем. \n7. Будем обжаривать мясо в очень горячей сковороде небольшими порциями, так, чтобы мясо лежало на поверхности сковороды в один слой. Нам нужно, чтобы мясные кусочки сразу схватились корочкой, и тогда весь сок останется внутри. Если же мы выложим в сковороду сразу много мяса, оно отдаст сок и может быть жёстким. Солить на этом этапе не нужно, т.к. соль также провоцирует выделение сока из мяса, сделаем это позже. Обжариваем мясо 2-3 минуты, переворачивая, до образования корочки. Т.к. кусочки у нас очень тонкие, этого времени достаточно, мясо успеет прожариться.\n8. Выкладываем обжаренную говядину в тарелку, и таким же образом жарим всё оставшееся мясо.\n9. В ту же сковороду наливаем немного растительного масла, снова хорошо его разогреваем и обжариваем лук до золотистого цвета.\n10. Обжаренный лук посыпаем мукой, хорошенько перемешиваем и обжариваем ещё около 1 минуты.\n11. Добавляем сметану и томатную пасту.\n12. Наливаем воду и хорошо всё перемешиваем.\n13. Выкладываем в сковороду мясо, солим, перчим, хорошо перемешиваем и тушим ещё около 5 минут.\n14. Через 5 минут открываем крышку и пробуем мясо, и теперь существует несколько вариантов развития дальнейших событий. Если оно мягкое, значит вам повезло с говядиной, вы правильно её порезали и обжарили, да и вообще, в таком случае, ваш бефстроганов готов. И это был идеальный вариант, однако в связи с качеством говядины в наших магазинах, всё чаще случается другой вариант: мясо получилось жёсткое. Если так произошло, то мы не расстраиваемся, а просто закрываем его снова крышкой и продолжаем тушить дальше на небольшом огне до мягкости. На это может потребоваться от 10 до 40 минут. Если придётся тушить долго и соус сильно загустеет, просто добавьте в него ещё немного воды. Когда говядина наконец станет мягкой, снимаем сковороду с огня.\nПодбирайте гарнир к бефстроганову на свой вкус, он отлично сочетается с картошкой (жареной и пюре), рисом, макаронными изделиями. \n','2018-08-02 07:31:08',915,102.6,77.33,32.55,2,1),(42,'Рис','https://lifehacker.ru/kak-varit-ris/\n\nСтандартная порция на одного — 65 мл сухого риса.','Подготовка.\n\nЕсли вы хотите сварить рассыпчатый рис, перед приготовлением его нужно промыть под холодной водой. Так вы избавитесь от крахмала, который отвечает за клейкость. Промойте рис около пяти раз или больше, пока вода не станет прозрачной. Удобнее всего выполнять эту процедуру с помощью мелкого сита.\nДля приготовления некоторых блюд, таких как ризотто, нужен клейкий рис. В этом случае промывать его не стоит. В крайнем случае можно ограничиться одним ополаскиванием, чтобы смыть всё лишнее.\n\nЧтобы рис сварился быстрее, его можно замочить на 30–60 минут. Тогда время приготовления сократится почти вдвое. Однако в этом случае лучше уменьшить количество воды, которое используется для варки.\n\nПропорции.\n\nОбычно считается, что для приготовления риса нужно в два раза больше воды. Но это приблизительная пропорция. Лучше отмерять объём воды исходя из вида риса:\nдля длиннозёрного — 1 : 1,5–2;\nдля среднезёрного — 1 : 2–2,5;\nдля круглозёрного — 1 : 2,5–3;\nдля пропаренного — 1 : 2;\nдля коричневого — 1 : 2,5–3;\nдля дикого — 1 : 3,5.\n\nПосуда\n\nРис лучше варить в кастрюле с толстым дном: в ней температура распределяется равномерно. Также можно приготовить рис на большой сковороде. Для плова традиционно используется казан.\n\nПравила варки\n\nЕсли вы варите рис в кастрюле, сначала доведите подсоленную воду до кипения, а после высыпьте в неё крупу. Перемешайте рис один раз, чтобы зёрнышки не прилипали к дну. Затем дождитесь, когда блюдо начнёт бурлить, убавьте огонь до минимума и накройте кастрюлю крышкой.\n\nВ процессе приготовления не поднимайте крышку — иначе рис будет готовиться дольше. Если вы хотите, чтобы рис был рассыпчатым, не перемешивайте его (за исключением первого раза). В противном случае зёрна будут ломаться и выделять крахмал.\n\nСреднее время варки в зависимости от вида составляет:\n\nдля белого риса — 20 минут;\nдля пропаренного риса — 30 минут;\nдля коричневого риса — 40 минут;\nдля дикого риса — 40–60 минут.\nКогда рис приготовится, снимите его с огня и дайте постоять 10–15 минут под крышкой. Если в готовом рисе осталась вода, слейте её или накройте кастрюлю сухим полотенцем: оно впитает излишки влаги.\n\nЕсли вы готовите рис на сковороде, используйте посуду с диаметром от 24 см, высокими бортиками и крышкой. Рис готовится в ней почти так же, как в кастрюле, за исключением одного нюанса: зёрна предварительно нужно быстро обжарить на растительном масле. Делайте это 1–2 минуты, постоянно помешивая, чтобы зёрнышки покрылись маслом: тогда рис будет рассыпчатым. Затем его нужно залить кипятком и готовить так, как описано выше.\n\nПриправы\n\nРис хорош тем, что его вкус всегда можно немного изменить. Например, с помощью следующих приправ:\n\nшафран;\nкарри;\nкардамон;\nзира;\nтмин;\nкорица;\nгвоздика.\nСпеции добавляются в воду при варке или в уже готовое блюдо.\n\nТакже рис можно дополнить вкусом пряных трав, цедрой цитрусовых или сварить не на воде, а на мясном или курином бульоне.','2018-08-02 07:44:36',611,13.4,1.4,157.8,1,1),(43,'Яичница',NULL,'Recipe....','2018-08-03 05:26:48',120.5,13.98,21.99,0.78,5,1),(44,'Гречневая каша',NULL,'1. 1 стакан гречки залить 2 стаканами кипятка и оставить разбухать\n2. Утром довести до кипения и сразу снять с плиты\n3. Добавить молоко, масло, соль и сахар\n','2018-08-03 06:58:40',1110,33.02,44.61,224.63,5,1),(45,'Овсяная каша',NULL,'1. Отмерить 1 стакан хлопьев.\n2. Вскипятить молоко.\n3. В кипящее молоко всыпать овсяные хлопья.\n4. Помешивая, варить овсянку на небольшом огне до загустения (5-10 минут). \n5. Добавить масло, соль, сахар\nКаша готова!\n','2018-08-03 07:11:56',610.5,20.21,43.27,160.71,3,1),(46,'Мюсли  творогом',NULL,'1. Блендером смешать творог, орехи, подсластитель и воду/молоко (около 50/100 мл до консистенции, как у кефира) и сухофрукты по желанию.\n2. Залить полученной смесью мюсли.\nИсточник: www.fitbreak.ru/diet/9-7-vkusnih-i-poleznih-zavtrakov\n','2018-08-03 07:28:02',200,34.4,10,3.6,3,1),(47,'Творожный крем с фруктами ','https://fitbreak.ru/diet/9-7-vkusnih-i-poleznih-zavtrakov','Взбить белки, \nИзмельчить орехи\nИзмельчить фрукты\nдобавить творог, отруби, орехи, отруби/клетчатку, подсластитель и фрукты/ягоды\nВсе перемешать. При желании воспользоваться блендером - крем станет однородным\nМожно употребить сразу, а можно оставить на ночь - отруби/клетчатка набухнут и крем будет не отличить от магазинного творожка\n','2018-08-06 07:58:48',280,1.44,1.12,25.6,5,1),(48,'Овощной омлет','https://fitbreak.ru/diet/9-7-vkusnih-i-poleznih-zavtrakov','Порезать лук, помидоры и перец\nОбжарить лук\nДобавить помидоры и перец. Обжарить до мягкости\nДобавить специи\nЗалить яйцами\n','2018-08-06 08:31:32',380.5,23.49,23.19,12.44,5,1),(49,'Творожная запеканка','https://fitbreak.ru/diet/9-7-vkusnih-i-poleznih-zavtrakov\nАльтернатива: манка/мука','Порезать сухофрукты\nЯйца взбить с подсластителем\nПри желании добавить лимонную цедру\nСмешать с творогом в однородную массу\nДобавить манку или муку (подходит рисовая, пшеничная - экспериментируйте)\nСмесь тщательно перемешать\nДобавить сухофрукты (курага, кумкват, вишня)\nЗапекать в духовке (30-60 минут) при 150-180°С\nОстужать не вынимая формы\nУбрать в холодильник на 1-2 часа\n\n','2018-08-06 08:48:38',706,93.13,32.84,139.25,5,1),(50,'Творожно-овсяный пирог','https://fitbreak.ru/diet/9-7-vkusnih-i-poleznih-zavtrakov','Порезать сухофрукты.\nЯйца взбить с подсластителем. \nДобавить при желании лимонную цедру.\nСмешать с творогом в однородную массу.\nДобавить геркулес.\nСмесь тщательно перемешать.\nДобавить порезанные сухофрукты.\nГотовить в духовке (45 минут при 150°С).\nОстужать, не вынимая из формы.\nУбрать в холодильник на 1-2 часа.\n','2018-08-06 09:08:21',726,96.79,37.12,146.41,4,1),(51,'Каша \"Яблочный пирог\" ','Это теплая, успокаивающая каша, приправленная классическими ароматами домашнего яблочного пирога.\n\nЕсть фото\n\nВремя подготовки: 10 минут\nВремя готовки: 5 минут\n','1.	Яблоко нарезать кубиками\n2.	Поместить все ингредиенты, кроме корицы, в кастрюлю и включить плиту.\n3.	Помешивая, довести до кипения.\n4.	Снизить огонь и, часто помешивая, дать постоять еще 5 минут.\n5.	Налить кашу в тарелку, добавив корицу.\n','2018-08-06 09:14:46',419,12.41,8.71,55.32,3,1),(52,'Банано-овсяный смузи',NULL,'Взбейте все ингредиенты в блендере до однородности. ','2018-08-06 09:58:16',374,15.12,9.12,71.46,5,1),(53,'Манная каша','Налить в кастрюлю молоко и воду\nпоставить кастрюлю на огонь\nПостоянно помешивая, всыпать манку\nДождаться, пока загустеет\nДобавить сахар и сливочное масло\n',NULL,'2018-08-06 10:05:03',1100,29.94,26.3,240.1,3,1),(54,'Рисовая каша',NULL,'Поставить кастрюлю с молоком на плиту\nДобавить соль и сахар\nДовести до кипения\nзасыпать рис\nКипятить 2-5 минут\nСнять с огня\nНакрыть крышкой\nНакрыть кастрюлю полотенцем\nОставить на 20 минут\nКрупа сама разбухнет, и получится густая каша\n','2018-08-06 10:10:12',200,13.4,1.4,157.8,3,1),(55,'Пшенная каша',NULL,'Промыть крупу.\nЗалить водой пшенную крупу и поставить на огонь.\nДовести массу до кипения.\nСнять пенку.\nОтваривать на среднем огне, пока вода не выпарится.\nВлить прогретое молоко.\nУбавить огонь.\nДобавить соль, сахар.\nОтваривать, помешивая, до загустения.\nВыключить огонь через 20 минут.\nДобавить масло, перемешать.\nНакрыть крышкой.\nОставить настояться 10 минут.\n','2018-08-06 10:34:49',825.5,37.2,33.6,262.08,3,1),(56,'Ячневая каша',NULL,'Ячневую крупу залить двумя стаканами холодной воды и поставить на средний огонь.\nКак только вода закипит, убавить огонь и варить кашу до загустения.\nДобавить стакан молока.\nОпять довести до кипения.\nНакрыть ячневую кашу крышкой и варить на маленько огне 2-3 минуты.\nОгонь выключить. \nДобавить сахара, масло и дать каше настояться при закрытой крышке еще минут 10.\n','2018-08-06 10:39:59',920,26.6,22.1,241.98,3,1),(57,'Сырники',NULL,'Изюм распарить в горячей воде и дать обсохнуть.\nТворог с сахаром протереть через сито.\nДобавить муку и яйца.\nВымесить тесто.\nДобавить изюм.\nПеремешать.\nМуку просеять на рабочую поверхность.\nВлажными руками формировать из теста лепешки, обваливая каждую в муке.\nОставить на 10 минут.\nОбвалять в муке еще раз.\nРазогреть в сковороде растительное масло, обжаривать сырники по 1,5 минуты с каждой стороны или запечь в духовке, разогретой до 180°С на 15 минут.\n\n','2018-08-06 10:45:13',745,92,33.38,196.72,5,1),(58,'Овсяноблин',NULL,'Измельчить хлопья в муку с помощью блендера. \nДобавить соль и 2 яйца. \nВзбить. \nПрожарить блин на небольшом огне по 2-3 минуты с каждой стороны.\n','2018-08-06 10:51:22',147,17.92,18.95,19.82,5,1),(59,'Говядина в духовке',NULL,'Чеснок почистить и мелко порезать.\nДля маринада смешать все ингредиенты, кроме говядины.\nНа говядине вдоль куска сделать надрезы толщиной в 1-2 см.\nКаждый надрез изнутри смазать маринадом.\nМясо поместить в рукав для запекания и отправить на ночь в холодильник.\nПосле чего выпекать при 200°С 2 часа.\n','2018-08-06 11:20:16',687,116.49,87.45,8.64,2,1);
/*!40000 ALTER TABLE `dishes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dishes_dish_types`
--

DROP TABLE IF EXISTS `dishes_dish_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dishes_dish_types` (
  `dish_id` bigint(20) NOT NULL,
  `dish_type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`dish_id`,`dish_type_id`),
  UNIQUE KEY `dishes_id_dish_types_id_uindex` (`dish_id`,`dish_type_id`),
  KEY `dishes_dish_types_dish_types_id_fk` (`dish_type_id`),
  CONSTRAINT `dishes_dish_types_dish_types_id_fk` FOREIGN KEY (`dish_type_id`) REFERENCES `dish_types` (`id`),
  CONSTRAINT `dishes_dish_types_dishes_id_fk` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dishes_dish_types`
--

LOCK TABLES `dishes_dish_types` WRITE;
/*!40000 ALTER TABLE `dishes_dish_types` DISABLE KEYS */;
INSERT INTO `dishes_dish_types` VALUES (43,1),(44,1),(45,1),(46,1),(47,1),(48,1),(49,1),(50,1),(51,1),(52,1),(53,1),(54,1),(55,1),(56,1),(57,1),(58,1),(47,2),(49,2),(50,2),(52,2),(57,2),(41,3),(42,3),(59,3),(40,4),(47,4),(51,4),(52,4),(57,4),(41,5),(42,5),(59,5);
/*!40000 ALTER TABLE `dishes_dish_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dishes_ingredients`
--

DROP TABLE IF EXISTS `dishes_ingredients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dishes_ingredients` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dish_id` bigint(20) NOT NULL,
  `ingredient_id` bigint(20) NOT NULL,
  `weight` float NOT NULL,
  `unit_type_id` bigint(20) NOT NULL,
  `unit_type_amount` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dishes_ingredients_id_uindex` (`id`),
  UNIQUE KEY `dish_id_ingr_id_uindex` (`dish_id`,`ingredient_id`),
  KEY `dishes_ingredients_ingredients_id_fk` (`ingredient_id`),
  KEY `dishes_ingredients_unit_types_id_fk` (`unit_type_id`),
  CONSTRAINT `dishes_ingredients_dishes_id_fk` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`),
  CONSTRAINT `dishes_ingredients_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `dishes_ingredients_unit_types_id_fk` FOREIGN KEY (`unit_type_id`) REFERENCES `unit_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=325 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dishes_ingredients`
--

LOCK TABLES `dishes_ingredients` WRITE;
/*!40000 ALTER TABLE `dishes_ingredients` DISABLE KEYS */;
INSERT INTO `dishes_ingredients` VALUES (155,43,175,10,2,2),(156,43,355,0.5,4,0.5),(157,43,141,110,6,2),(167,44,23,170,5,1),(168,44,49,100,1,4),(169,44,94,400,5,2),(170,44,135,40,1,2),(171,44,356,400,5,2),(180,45,135,40,1,2),(181,45,94,400,5,2),(182,45,35,70,5,1),(183,45,355,0.5,4,0.5),(184,45,49,100,1,4),(185,46,109,200,4,200),(186,40,355,16.5,2,1.5),(187,40,1,10,4,10),(188,40,94,200,5,1),(189,40,175,50,4,50),(190,40,10,700,4,700),(191,40,49,10,2,1),(192,40,356,200,5,1),(193,47,263,200,4,200),(194,47,276,80,4,80),(195,48,175,5,2,1),(196,48,301,37.5,6,0.5),(197,48,355,5.5,2,0.5),(198,48,307,95,6,1),(199,48,141,165,6,3),(200,48,305,72.5,6,0.5),(218,49,158,80,4,80),(219,49,141,110,6,2),(220,49,360,6,6,1),(221,49,25,60,1,2),(222,49,109,400,4,400),(223,49,49,50,1,2),(224,50,360,6,6,1),(225,50,158,80,4,80),(226,50,109,400,4,400),(227,50,49,50,1,2),(228,50,141,110,6,2),(229,50,35,80,1,5),(238,51,263,165,6,1),(239,51,361,4,2,0.5),(240,51,94,200,4,200),(241,51,35,50,4,50),(243,52,262,110,6,1),(244,52,94,200,5,1),(245,52,35,64,1,4),(251,53,25,180,1,6),(252,53,135,20,1,1),(253,53,356,400,5,2),(254,53,94,400,5,2),(255,53,49,100,1,4),(256,54,28,200,5,1),(262,55,135,20,1,1),(263,55,355,5.5,2,0.5),(264,55,362,200,5,1),(265,55,94,500,5,2.5),(266,55,49,100,1,4),(272,56,32,200,5,1),(273,56,356,400,5,2),(274,56,135,20,1,1),(275,56,49,100,1,4),(276,56,94,200,5,1),(283,57,141,110,6,2),(284,57,49,80,5,0.5),(285,57,109,400,4,400),(286,57,10,75,1,3),(287,57,155,80,4,80),(292,41,356,150,4,150),(293,41,79,500,4,500),(294,41,201,10,4,10),(295,41,114,150,4,150),(296,41,355,5,4,5),(297,41,10,25,1,1),(298,41,301,75,6,1),(305,42,356,400,5,2),(306,42,355,11,2,1),(307,42,28,200,5,1),(314,59,304,8,6,2),(315,59,79,600,4,600),(316,59,210,20,1,1),(317,59,355,22,2,2),(318,59,176,10,1,1),(319,59,363,20,1,1),(320,59,364,7,2,1),(321,58,175,5,2,1),(322,58,35,32,1,2),(323,58,355,0,8,0),(324,58,141,110,6,2);
/*!40000 ALTER TABLE `dishes_ingredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genders`
--

DROP TABLE IF EXISTS `genders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `genders_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genders`
--

LOCK TABLES `genders` WRITE;
/*!40000 ALTER TABLE `genders` DISABLE KEYS */;
INSERT INTO `genders` VALUES (1,'MALE','Male'),(2,'FEMALE','Female');
/*!40000 ALTER TABLE `genders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients`
--

DROP TABLE IF EXISTS `ingredients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `protein` float NOT NULL,
  `fat` float NOT NULL,
  `carbohydrate` float NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `is_allergic` tinyint(4) NOT NULL DEFAULT '0',
  `glycemic_index` float NOT NULL DEFAULT '0',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_id_uindex` (`id`),
  KEY `ingredients_ingredients_category_id_fk` (`category_id`),
  CONSTRAINT `ingredients_ingredients_category_id_fk` FOREIGN KEY (`category_id`) REFERENCES `ingredients_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=365 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients`
--

LOCK TABLES `ingredients` WRITE;
/*!40000 ALTER TABLE `ingredients` DISABLE KEYS */;
INSERT INTO `ingredients` VALUES (1,'дрожжи',12.7,2.7,0,2,0,0,1),(2,'панировочные сухари',9.7,1.9,77.6,2,1,0,1),(3,'пектин',0,0,89.6,2,0,0,1),(4,'желатин',87.2,0.4,0.8,2,0,0,1),(5,'мак',17.5,47.5,2,2,0,0,1),(6,'краситель пищевой',0,0,0,2,0,0,1),(7,'лимонная кислота',0,0,0,2,0,0,1),(8,'сода и разрыхлитель',0,0,0,2,0,0,1),(9,'марципан',6.8,21.2,65.3,2,0,0,1),(10,'пшеничная мука',9.2,1.2,74.9,3,1,0,1),(11,'миндальная мука',20.1,39.5,13.5,3,1,0,1),(12,'кукурузная мука',7.2,1.5,70.2,3,1,0,1),(13,'ржаная мука',9,1,73,3,1,0,1),(14,'рисовая мука',7.4,0.6,82,3,1,0,1),(15,'амарантовая мука',8.9,1.7,61.7,3,1,0,1),(16,'соевая мука',36.5,18.6,17.9,3,1,0,1),(17,'льняная мука',36,10,9,3,1,0,1),(18,'гороховая цельносмолотая мука',21,2,49,3,1,0,1),(19,'овсяная мука',13,6.8,64.9,3,1,0,1),(20,'крахмал картофельный    ',0.1,0,79.6,3,0,0,1),(21,'чечевичная мука цельносмолотая',28,1,56,3,1,0,1),(22,'ячменная цельносмолотая мука',10,1.6,56.1,3,0,0,1),(23,'гречневая крупа',12.6,3.3,62.1,4,0,0,1),(24,'кукурузная крупа',8.3,1.2,75,4,0,0,1),(25,'манная крупа',10.3,1,67.4,4,1,0,1),(26,'пшеничная крупа',11.5,1.3,62,4,1,0,1),(27,'Пшенная крупа',11.5,3.3,69.3,4,1,0,0),(28,'рис белый',6.7,0.7,78.9,4,0,0,1),(29,'рис бурый',7.4,1.8,72.9,4,0,0,1),(30,'киноа',14.1,6.1,57.2,4,0,0,1),(31,'перловая',9.3,1.1,73.7,4,0,0,1),(32,'ячневая',10.4,1.3,66.3,4,0,0,1),(33,'булгур',12.3,1.3,57.6,4,0,0,1),(34,'кускус',3.8,0.2,21.8,4,0,0,1),(35,'овсяные хлопья',12.3,6.1,59.5,5,1,0,1),(36,'хлопья 3 злака \"Царь\"',11.7,3,63.5,5,1,0,1),(37,'Хлопья 4 злака',11.5,3.5,63.8,5,1,0,1),(38,'хлопья овсяные',12.3,6.2,61.8,5,1,0,0),(39,'хлопья 5 злаков',12.15,4.8,61.3,5,1,0,1),(40,'хлопья 6 злаков',11.5,3.2,62.87,5,1,0,1),(41,'хлопья 7 злаков',10,2.3,67.4,5,1,0,1),(42,'горох',20.5,2,53.3,6,1,0,1),(43,'горох маш',23.5,2,46,6,1,0,1),(44,'нут',19,6,61,6,1,0,1),(45,'фасоль',20.9,1.8,64,6,1,0,1),(46,'чечевица красная',21.6,1.1,48,6,1,0,1),(47,'чечевица жёлтая',24,1.5,42.7,6,1,0,1),(48,'чечевица зеленая',24,1.5,42.7,6,1,0,1),(49,'сахар  ',0,0,99.7,7,0,0,1),(50,'сахарная пудра',0,0,99.8,7,0,0,1),(51,'сахар тростниковый',0,0,99.4,7,0,0,1),(52,'сахар ванильный',0,0,98.5,7,0,0,1),(53,'Мёд',0.8,0,81.5,7,1,0,1),(54,'сироп агавы',0,0.5,76,7,0,0,1),(55,'кленовый сироп',0,0.1,67.4,7,0,0,1),(56,'ксилит',0,0,97.9,7,0,0,1),(57,'вермишель',10.4,1.1,69.7,8,1,0,1),(58,'рожки',10.4,1.1,69.7,8,1,0,1),(59,'спагетти',10.4,1.1,69.7,8,1,0,1),(60,'фигурные макароны',10.4,1.1,69.7,8,1,0,1),(61,'пшеничная',12,1,65,9,1,0,0),(62,'гречневая',14.7,0.9,70.5,9,1,0,1),(63,'рисовая',7,0,79,9,1,0,1),(64,'фунчоза',0.7,0.5,84,8,0,0,1),(65,'перепел',21.8,4.5,0,11,0,0,1),(66,'гусь',15.2,39,0,11,0,0,1),(67,'суповой набор',16.5,12.6,0.4,12,0,0,1),(68,'бедра',21.3,11,0.1,12,0,0,1),(69,'крылышки',19.2,12.2,0,12,0,0,1),(70,'окорочка',16.8,10.2,0,12,0,0,1),(71,'куриное филе',23.1,1.2,0,12,0,0,1),(72,'цыпленок бройлерный',18.7,16.1,0.5,12,0,0,1),(73,'куриная печень',19.1,6.3,0.6,12,0,0,1),(74,'куриные желудочки',18.2,4.2,0.6,12,0,0,1),(75,'печень',19.5,22,0,13,0,0,1),(76,'бедро',15.7,8.9,0,13,0,0,1),(77,'голень',15.7,8.9,0,13,0,0,1),(78,'грудка',19.2,0.7,0,13,0,0,1),(79,'говядина     ',18.9,12.4,0,10,0,0,1),(80,'баранина   ',15.6,16.3,0,10,0,0,1),(81,'свинина',16,21.6,0,10,0,0,1),(82,'конина',20.2,7,0,10,0,0,1),(83,'кролик',21,8,0,10,0,0,1),(84,'Плавленный сыр',16.8,11.2,23.8,15,1,0,1),(85,'фетакса',8,25,1,16,1,0,1),(86,'тофу',8.1,4.2,0.6,16,1,0,1),(87,'брынза',14.6,25.5,0,16,1,0,1),(88,'сыр фета',17,16,1,16,1,0,1),(89,'сыр чечил',22.42,17.55,1.2,16,1,0,1),(90,'сыр моцарелла \"Galbani\"',13.3,13.1,20.2,16,1,0,1),(91,'сыр моцарелла \"агропродукт\" 40%',22.3,21.2,0,16,1,0,1),(92,'брынза 45%',12,16.7,3.7,16,1,0,1),(93,'молоко 1,5%',2.8,1.5,4.7,18,1,0,1),(94,'молоко 2,5%',2.8,2.5,4.7,18,1,0,1),(95,'молоко 3,2%',2.8,3.2,4.7,18,1,0,1),(96,'молоко 6%',2.9,6,4.69,18,1,0,1),(97,'молоко \"Шадринское\"',8,7.1,8.4,19,1,0,1),(98,'йогурт',2.8,2.4,14.5,14,1,0,1),(99,'ряженка 2,5%',2.9,2.5,4.2,20,1,0,1),(100,'ряженка 4%',2.8,4,4.2,20,1,0,1),(101,'кумыс',2.1,1.9,5,20,1,0,1),(102,'шубат',4.11,5.53,5.45,20,1,0,1),(103,'кефир 1%',2.8,1,4,20,1,0,1),(104,'кефир 2,5%',2.8,2.5,3.9,20,1,0,1),(105,'кефир 3,2%',2.8,3.2,4.1,20,1,0,1),(106,'творог 0%',16.5,0,1.3,21,1,0,1),(107,'творог 0,6%',18,0.6,1.8,21,1,0,1),(108,'творог 2%',18,2,3.3,21,1,0,1),(109,'творог 5%',17.2,5,1.8,21,1,0,1),(110,'творог 9%',16.7,9,2,21,1,0,1),(111,'творог 15%',15.99,12.5,1.28,21,1,0,1),(112,'иримшик',32.1,29.5,23.1,21,1,0,1),(113,'курт',25,16,2.7,21,1,0,1),(114,'сметана 10%',3,10,2.9,22,1,0,1),(115,'сметана 15%',2.6,15,3,22,1,0,1),(116,'сметана 20%',2.8,20,3.2,22,1,0,1),(117,'сливки 10%',3,10,4,23,1,0,1),(118,'сливки 20%',2.8,20,3.7,23,1,0,1),(119,'йогурт детский \"живой\" \"Амиран\" 2,8%',2.8,2.8,4.7,24,1,0,1),(120,'творог \"Амиран\" зернистый детский 9%',16,9,2,24,1,0,1),(121,'паста \"Амиран\" творожная детская 9%',18,9,2,24,1,0,1),(122,'продукт кисломолочный \"Амиран\" детский 3,5%',2.8,3.5,4.7,24,1,0,1),(123,'сыворотка молочная',0.8,0.2,3.2,24,1,0,1),(124,'кефир \"Амиран\" живой 2,5%',2.9,2.5,3.9,24,1,0,1),(125,'кефир \"Амиран\" живой 3,2%',2.8,3.2,3.6,24,1,0,1),(126,'молоко \"Амиран\" живое 2,5%',2.8,2.5,4.7,24,1,0,1),(127,'молоко \"Амиран\" живое 3,2%',2.8,3.2,4.7,24,1,0,1),(128,'простокваша \"Амиран\" кисломолочная 3,5%',2.8,3.5,4.7,24,1,0,1),(129,'сметана \"Амиран\" 20%',2.8,20,3.2,24,1,0,1),(130,'Творог \"Амиран\" живой 9%',16,9,2,24,1,0,1),(131,'Творог \"Амиран\" живой 0%',18,0.6,1.5,24,1,0,1),(132,'масло топленое \"Амиран\"',0,99,0,24,1,0,1),(133,'молоко питьевое детское и для приготовления каш \"Амиран\" 3,2%',2.8,3.2,4.7,24,1,0,1),(134,'Масло сливочное облегченное, 60%',1.3,60,1.7,25,1,0,1),(135,'масло сливочное 72,5%',1,72.5,1.4,25,1,0,1),(136,'масло сливочноее 82,5%',0.5,82.5,0.8,25,1,0,1),(137,'маргарин 80%',0.3,82,1,25,1,0,1),(138,'кокосовое молоко',1.8,14.9,2.7,26,1,0,1),(139,'миндальное молоко',3.6,11.1,5.6,26,1,0,1),(140,'соевое молоко',3.3,1.8,5.7,26,1,0,1),(141,'яйцо куриное',12.71,10.91,0.71,27,1,0,1),(142,'яйцо гусиное',13.9,13.3,1.4,27,1,0,1),(143,'яйцо утиное',13.3,14.5,0.1,27,1,0,1),(144,'яйца перепелиные',11.92,13.08,0.58,27,1,0,1),(145,'арахис',26.3,45.2,9.2,29,1,0,1),(146,'грецкий орех',15.2,65.2,7,29,1,0,1),(147,'кедровые орешки',15.6,56,28.4,29,1,0,1),(148,'миндаль',18.6,57.7,16.2,29,1,0,1),(149,'кешью',18.5,48.5,22.5,29,1,0,1),(150,'фисташки',20,50,7,29,1,0,1),(151,'фундук',16.1,66.9,9.9,29,1,0,1),(152,'пекан',9.2,72,4.3,29,1,0,1),(153,'финики',2.5,0.5,69.2,30,0,0,1),(154,'алыча сушеная',2.3,0.5,50.2,30,0,0,1),(155,'изюм ',2.9,0.6,66,30,0,0,1),(156,'инжир сушеный',3.1,0.8,57.9,30,0,0,1),(157,'клюква сушеная',0.1,1.4,76.5,30,0,0,1),(158,'курага',5.2,0.3,51,30,0,0,1),(159,'пастила',0.5,0,80.8,30,0,0,1),(160,'ягоды годжи',11.1,2.6,53.4,30,0,0,1),(161,'чернослив',2.3,0.7,57.5,30,0,0,1),(162,'цукаты',2,1,71,30,0,0,1),(163,'яблоки сушеные',2.2,0.1,59,30,0,0,1),(164,'урюк сушеный',5,0.4,50.6,30,0,0,1),(165,'дыня сушеная',0.7,0.1,82.2,30,0,0,1),(166,'груша сушеная',2.3,0.6,62.6,30,0,0,1),(167,'персики сушеные',3,0.4,57.7,30,0,0,1),(168,'семечки подсолнечные нечищеные',20.7,52.9,3.4,31,0,0,1),(169,'семечки подсолнечные очищенные',20.7,59.9,10.5,31,0,0,1),(170,'семечки тыквенные нечищеные',11.92,20.08,6.5,31,0,0,1),(171,'семечки тыквенные очищенные',24.47,44.83,8.26,31,0,0,1),(172,'семена льна',18.3,42.2,28.9,31,0,0,1),(173,'семена чиа',16.5,30.7,42.1,31,0,0,1),(174,'кунжут семена',19.4,48.7,12.2,31,0,0,1),(175,'масло подсолнечное ',0,99.9,0,33,0,0,1),(176,'масло оливковое ',0,99.8,0,33,0,0,1),(177,'масло кукурузное',0,99.9,0,33,0,0,1),(178,'масло льняное ',0,99.8,0,33,0,0,1),(179,'масло ореховое ',0,100,0,33,1,0,1),(180,'масло горчичное',0,99.8,0,33,0,0,1),(181,'масло грецкого ореха',0,99.8,0,33,1,0,1),(182,'масло шиповника',0,99.8,0,33,0,0,1),(183,'масло кокосовое',0,99.9,0,33,0,0,1),(184,'масло облепиховое',0,99.5,0,33,0,0,1),(185,'масло хлопковое',0,99.7,0,33,0,0,1),(186,'масло кунжутное',0,99.9,0,33,1,0,1),(187,'масло соевое',0,99.9,0,33,1,0,1),(188,'масло из семян рыжика',0,99.8,0,33,0,0,1),(189,'масло из виноградных косточек',0,99.8,0,33,0,0,1),(190,'масло чесночное',0,99.8,0,33,0,0,1),(191,'масло абрикосовое нерафинированное',0,99.8,0,33,0,0,1),(192,'масло расторопши',0,99.8,0,33,0,0,1),(193,'масло конопляное нерафинированное',0,99.8,0,33,0,0,1),(194,'масло зародышей пшеницы',0,99.8,0,33,1,0,1),(195,'масло авокадо',0,99.8,0,33,0,0,1),(196,'масло тыквенное нерафинированное',0,99.8,0,33,0,0,1),(197,'масло острого перца чили',0,99.8,0,33,0,0,1),(198,'масло черного тмина',0,99.8,0,33,0,0,1),(199,'масло амарантовое',0,99.8,0,33,1,0,1),(200,'кетчуп',1.8,1,22.2,34,0,0,1),(201,'томатная паста',2.5,0.3,16.7,34,0,0,1),(202,'томатный соус',1.7,7.8,4.5,34,0,0,1),(203,'соевый соус',2.6,0,43,35,1,0,1),(204,'бальзамический соус',0.1,0,37.8,35,0,0,1),(205,'кисло-сладкий соус',0.3,0.8,39.7,35,0,0,1),(206,'острый соус томатный',2.5,0,21.8,35,0,0,1),(207,'классический майонез ',2.4,67,3.9,35,0,0,1),(208,'хрен',3.2,0.4,10.5,36,0,0,1),(209,'васаби',0,9,40,36,0,0,1),(210,'горчица',5.7,6.4,22,36,0,0,1),(211,'уксус столовый',0,0,2.3,37,0,0,1),(212,'майонез 67%',2.4,67,3.9,38,1,0,1),(213,'соус песто',5,45,6,39,0,0,1),(214,'батон нарезной',7.5,2.9,50.9,41,1,0,1),(215,'хлеб белый',8.1,1,48.8,41,1,0,1),(216,'хлеб бородинский',6.9,1.3,40.9,41,1,0,1),(217,'чиабатта классическая',7.7,3.8,47.8,41,1,0,1),(218,'хлеб ржаной зерновой',13,3,40,41,1,0,1),(219,'лаваш',7.9,1,47.6,41,1,0,1),(220,'шелпек',6,11,36,41,1,0,1),(221,'баурсаки',6.8,2.6,45.8,41,1,0,1),(222,'багет',7.5,2.9,51.4,41,1,0,1),(223,'хлебцы',11,2.7,58,40,1,0,1),(224,'сушки',11.3,4.4,70.5,43,1,0,1),(225,'баранки',16,1,70,43,1,0,1),(226,'булочки, сдобные и слоеные изделия',8.4,2.2,52.8,40,1,0,1),(227,'вешенки',2.5,0.3,6.5,45,0,0,1),(228,'шампиньоны',4.3,1,0.1,45,0,0,1),(229,'укроп',2.5,0.5,6.3,46,0,0,1),(230,'петрушка',3.7,0.4,7.6,46,0,0,1),(231,'лук зеленый',1.3,0,4.6,46,0,0,1),(232,'кинза',2.1,0.5,1.9,46,0,0,1),(233,'базилик ',2.5,0.6,4.3,46,0,0,1),(234,'джусай',1.4,0.2,8.2,46,0,0,1),(235,'мята',3.7,0.4,8,46,0,0,1),(236,'розмарин свежий',3.3,5.9,20.7,46,0,0,1),(237,'руккола',2.6,0.7,2.1,46,0,0,1),(238,'Салат айсберг',0.9,0.1,1.8,46,0,0,1),(239,'салат Радичио',1.5,0.2,3.3,46,0,0,1),(240,'салат ромэн',1.8,1.1,2.7,46,0,0,1),(241,'салат листовой',1.2,0.3,1.3,46,0,0,1),(242,'салат Лолло Росса',1.5,0.2,2,46,0,0,1),(243,'Сельдерей зелень',0.9,0.1,2.1,46,0,0,1),(244,'тимьян свежий',5.6,1.7,10.5,46,0,0,1),(245,'фенхель',1.2,0.2,7.3,46,0,0,1),(246,'чесночные стрелки',1.3,0.1,3.4,46,0,0,1),(247,'щавель',1.5,0.3,2.9,46,0,0,1),(248,'шпинат',2.9,0.3,2,46,0,0,1),(249,'виноград',0.6,0.2,16.8,47,0,0,1),(250,'апельсин',0.9,0.2,8.1,48,0,0,1),(251,'грейпфрут',0.7,0.2,6.5,48,0,0,1),(252,'лайм',0.9,0.1,3,48,0,0,1),(253,'лимон',0.9,0.1,3,48,0,0,1),(254,'мандарин',0.8,0.2,7.51,48,0,0,1),(255,'памело',0.6,0.2,6.7,48,0,0,1),(256,'авокадо',2,20,6,49,0,0,1),(257,'ананас',0.4,0.2,10.6,49,0,0,1),(258,'киви',0.5,0.3,5.15,49,0,0,1),(259,'кокос',1.7,16.75,3.1,49,0,0,1),(260,'папайя',0.6,0.1,9.5,49,0,0,1),(261,'манго',0.5,0.3,11.5,49,0,0,1),(262,'банан',1.5,0.2,21.8,49,0,0,1),(263,'яблоко',0.4,0.4,9.8,47,0,0,1),(264,'груши',0.4,0.3,10.9,47,0,0,1),(265,'абрикос',0.88,0.12,9,47,0,0,1),(266,'персик',0.91,0.11,11.31,47,0,0,1),(267,'слива',0.8,0.3,9.6,47,0,0,1),(268,'гранат',0.9,0,13.9,47,0,0,1),(269,'дыня',0.6,0.3,7.4,47,0,0,1),(270,'хурма',0.5,0.3,15.3,47,0,0,1),(271,'арбуз',0.6,0.1,5.8,47,0,0,1),(272,'черешня',1.1,0.4,11.5,50,0,0,1),(273,'асаи',0.4,0,12.6,50,0,0,1),(274,'инжир',0.7,0.2,13.7,50,0,0,1),(275,'вишня',0.8,0.5,11.3,50,0,0,1),(276,'клубника',0.8,0.4,7.5,50,0,0,1),(277,'малина   ',0.8,0.5,8.3,50,0,0,1),(278,'ежевика    ',2,0,6.4,50,0,0,1),(279,'ежемалина',1.5,0.3,7.7,50,0,0,1),(280,'голубика',1,0,8.2,50,0,0,1),(281,'смородина',0.6,0.2,7.7,50,0,0,1),(282,'жимолость',0,0,7.8,50,0,0,1),(283,'черника',1.1,0.4,7.6,50,0,0,1),(284,'шиповник',1.6,0,14,50,0,0,1),(285,'крыжовник',0.7,0.2,12,50,0,0,1),(286,'капуста белокочанная',1.8,0.1,4.7,53,0,0,1),(287,'капуста брокколи',3,0.4,5.2,53,0,0,1),(288,'капуста пекинская',1.2,0.2,2,53,0,0,1),(289,'капуста краснокочанная',0.8,0,7.6,53,0,0,1),(290,'капуста цветная',2.5,0.3,5.4,53,0,0,1),(291,'картофель',2,0.4,16.1,53,0,0,1),(292,'морковь',1.31,0.11,6.94,53,0,0,1),(293,'репа',1.5,0.1,6.2,53,0,0,1),(294,'редис',1.2,0.1,3.4,53,0,0,1),(295,'редька',1.9,0.2,6.7,53,0,0,1),(296,'редис дайкон',1.2,0,4.1,53,0,0,1),(297,'корень имбиря',1.8,0.8,15.8,53,0,0,1),(298,'топинамбур',2.1,0.1,12.8,53,0,0,1),(299,'корень сельдерея',1.3,0.3,6.5,53,0,0,1),(300,'свекла',1.5,0.1,8.8,53,0,0,1),(301,'лук репчатый',1.4,0,10.4,53,0,0,1),(302,'лук красный',1.4,0,9.1,53,0,0,1),(303,'лук-порей',2,0,8.2,53,0,0,1),(304,'чеснок ',6.5,0.5,30,53,0,0,1),(305,'перец болгарский',1.3,0,5.3,53,0,0,1),(306,'перец чили ',2,0.2,9.5,53,0,0,1),(307,'помидоры обычные',1.11,0.2,3.71,53,0,0,1),(308,'огурцы',0.8,0.1,2.8,53,0,0,1),(309,'помидоры черри',0.8,0.1,2.8,53,0,0,1),(310,'баклажаны ',1.2,0.1,4.5,53,0,0,1),(311,'кабачки',0.6,0.3,4.6,53,0,0,1),(312,'тыква',1.3,0.3,7.7,53,0,0,1),(313,'сельдерей (стебель)',0.9,0.1,2.1,53,0,0,1),(314,'спаржа (стебель)',1.9,0.1,3.1,53,0,0,1),(315,'фасоль свежая',2,0.2,3.6,53,1,0,1),(316,'патиссон   ',0.6,0.1,4.3,53,0,0,1),(317,'соя проросшая',34.9,17.3,17.3,53,1,0,1),(318,'огурцы маринованные',2.8,0,1.3,54,0,0,1),(319,'капуста квашеная',1.8,0.1,4.4,54,0,0,1),(320,'хлорелла',66.6,0.5,16.7,56,0,0,1),(321,'спирулина',4.2,2.5,9.9,56,0,0,1),(322,'отруби пшеничные',14.7,4.1,20.6,58,1,0,1),(323,'клетчатка',0.5,0.5,4,58,0,0,1),(324,'сазан филе',18.4,5.3,0,60,1,0,1),(325,'судак филе',19.2,0.7,0,60,1,0,1),(326,'анчоус',20.1,6.1,0,60,1,0,1),(327,'сёмга',21.6,6,0,60,1,0,1),(328,'минтай',15.9,0.9,0,60,1,0,1),(329,'дорадо',18,3,0,60,1,0,1),(330,'сибас',18,3,0,60,1,0,1),(331,'форель',19.2,2.1,0,60,1,0,1),(332,'скумбрия',18,13.2,0,60,1,0,1),(333,'лосось',19.8,6.3,0,60,1,0,1),(334,'мойва',13.4,11.5,0,60,1,0,1),(335,'треска',17.7,0.7,0,60,1,0,1),(336,'палтус',18.9,3,0,60,1,0,1),(337,'окунь',18.5,0.9,0,60,1,0,1),(338,'филе тилапии',20.1,1.7,0,60,1,0,1),(339,'филе пангасиуса',15,3,0,60,1,0,1),(340,'пикша',17.2,0.2,0,60,1,0,1),(341,'камбала',16.5,1.8,0,60,1,0,1),(342,'горбуша ',20.5,6.5,0,60,1,0,1),(343,'килька',17.1,7.6,0,60,1,0,1),(344,'скумбрия ',18,13.2,0,60,1,0,1),(345,'сельдь',16.3,10.7,0,60,1,0,1),(346,'креветки чищенные',22,1,0,61,1,0,1),(347,'кальмар    ',19,2.5,1.8,61,0,0,1),(348,'крабовые палочки',6,1,10,61,1,0,1),(349,'казы сырые',13.5,37.4,0.2,63,0,0,1),(350,'сосиски молочные',11,23.9,1.6,63,0,0,1),(351,'манты   ',2.2,0,11.2,64,0,0,1),(352,'кукуруза консервированная',3.6,0.1,9.8,65,0,0,1),(353,'горошек консервированный',6.7,0.3,17.4,65,0,0,1),(354,'морская капуста',0.8,5.1,0,70,0,0,1),(355,'соль',0,0,0,70,0,0,1),(356,'Вода',0,0,0,27,0,0,1),(357,'Мюсли медовые с тропическими фруктами',17,13,58,5,1,0,1),(358,'отруби овсяные',18,7.7,45.3,5,1,0,1),(359,'Творог мягкий обезжиренный',7,0,1,21,0,0,1),(360,'Цедра лимона',0.1,0,3,48,1,0,1),(361,'Корица',0,0,0,2,0,0,1),(362,'Пшенная крупа',11.5,3.3,69.3,4,1,0,1),(363,'Французская горчица',7.16,8.75,9.18,27,0,0,1),(364,'Паприка',0,0,0,27,0,0,1);
/*!40000 ALTER TABLE `ingredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_alternatives`
--

DROP TABLE IF EXISTS `ingredients_alternatives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_alternatives` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ingredient_id` bigint(20) DEFAULT NULL,
  `alternative_ingredient_id` bigint(20) DEFAULT NULL,
  `alternative_category_id` bigint(20) DEFAULT NULL,
  `alternative_relation_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_alternatives_id_uindex` (`id`),
  UNIQUE KEY `ingredient_id_alternative_i_id_cateory_id_uindex` (`ingredient_id`,`alternative_ingredient_id`,`alternative_category_id`),
  KEY `ingredients_alternatives_ingredients_id_fk_2` (`alternative_ingredient_id`),
  KEY `ingredients_alternatives_alternatives_categories_id_fk` (`alternative_category_id`),
  KEY `ingredients_alternatives_alternatives_relations_id_fk` (`alternative_relation_id`),
  CONSTRAINT `ingredients_alternatives_alternatives_categories_id_fk` FOREIGN KEY (`alternative_category_id`) REFERENCES `alternatives_categories` (`id`),
  CONSTRAINT `ingredients_alternatives_alternatives_relations_id_fk` FOREIGN KEY (`alternative_relation_id`) REFERENCES `alternatives_relations` (`id`),
  CONSTRAINT `ingredients_alternatives_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `ingredients_alternatives_ingredients_id_fk_2` FOREIGN KEY (`alternative_ingredient_id`) REFERENCES `ingredients` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_alternatives`
--

LOCK TABLES `ingredients_alternatives` WRITE;
/*!40000 ALTER TABLE `ingredients_alternatives` DISABLE KEYS */;
/*!40000 ALTER TABLE `ingredients_alternatives` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_categories`
--

DROP TABLE IF EXISTS `ingredients_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_category_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_category_id_uindex` (`id`),
  KEY `ingredients_categories_ingredients_categories_id_fk` (`parent_category_id`),
  CONSTRAINT `ingredients_categories_ingredients_categories_id_fk` FOREIGN KEY (`parent_category_id`) REFERENCES `ingredients_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_categories`
--

LOCK TABLES `ingredients_categories` WRITE;
/*!40000 ALTER TABLE `ingredients_categories` DISABLE KEYS */;
INSERT INTO `ingredients_categories` VALUES (1,'Бакалея',NULL),(2,'Ингредиенты для выпечки',1),(3,'Мука и смеси для выпечки',1),(4,'Крупы, бобы',1),(5,'Овсяные и злаковые хлопья, мюсли',4),(6,'Бобовые',4),(7,'Сахар и другие подсластители',1),(8,'Макароны высшего сорта',1),(9,'Лапша',8),(10,'Мясо',NULL),(11,'Птица',10),(12,'Курица',10),(13,'Индейка',12),(14,'Молочные продукты',NULL),(15,'Сырная лавка',14),(16,'Рассольный сыр',15),(17,'натуральные закваски',14),(18,'молоко',14),(19,'молоко сгущенное и концентрированное',18),(20,'кисломолочные продукты',14),(21,'творог и творожные изделия',14),(22,'сметана',14),(23,'сливки',14),(24,'Молочная продукция \"Амиран\"',14),(25,'сливочное масло, маргарин',14),(26,'растительное молоко',14),(27,'яйца',NULL),(28,'Орехи, сухофрукты, семена',NULL),(29,'орехи весовые',28),(30,'сухофрукты',28),(31,'семена',28),(32,'масла, соусы, заправки',NULL),(33,'растительное масло',32),(34,'кетчуп, томатная паста',32),(35,'соусы',32),(36,'хрен, горчица, уксус, маринад',32),(37,'уксус',32),(38,'майонез',32),(39,'прочие соусы и лимонады',32),(40,'хлеб, сдоба, хлебцы',NULL),(41,'хлеб',40),(42,'цельнозерновой хлеб',40),(43,'сушки и баранки',40),(44,'грибы',NULL),(45,'грибы свежие',44),(46,'зелень',NULL),(47,'фрукты, ягоды',NULL),(48,'цитрусовые',47),(49,'экзотические',47),(50,'ягоды свежие',47),(51,'фрукты и ягоды замороженные',47),(52,'овощи',NULL),(53,'овощи свежие',52),(54,'овощи маринованные',52),(55,'овощи замороженные',52),(56,'БАДы',NULL),(57,'диетическое питание',NULL),(58,'отруби и семена',57),(59,'рыба, морепродукты и икра',NULL),(60,'рыба',59),(61,'морепродукты',59),(62,'полуфабрикаты',NULL),(63,'мясные полуфабрикаты',62),(64,'пельмени, вареники, манты',62),(65,'консервы',NULL),(66,'грибные консервы',65),(67,'оливки и маслины',65),(68,'овощные консервы',65),(69,'чипсы, снэхи, сухарики',NULL),(70,'азиатская кухня',NULL);
/*!40000 ALTER TABLE `ingredients_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_changable_props`
--

DROP TABLE IF EXISTS `ingredients_changable_props`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_changable_props` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_changable_props_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_changable_props`
--

LOCK TABLES `ingredients_changable_props` WRITE;
/*!40000 ALTER TABLE `ingredients_changable_props` DISABLE KEYS */;
INSERT INTO `ingredients_changable_props` VALUES (1,'PROTEIN','Protein'),(2,'CARBOHYDRATE','Carbohydrate'),(3,'FAT','Fat'),(4,'WEIGHT','Weight'),(5,'VOLUME','Volume');
/*!40000 ALTER TABLE `ingredients_changable_props` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_cooking_types`
--

DROP TABLE IF EXISTS `ingredients_cooking_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_cooking_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ingredient_id` bigint(20) NOT NULL,
  `cooking_type_id` bigint(20) NOT NULL,
  `difference_coefficient` double NOT NULL,
  `ingredients_changable_prop_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_cooking_types_id_uindex` (`id`),
  UNIQUE KEY `ingr_id_cooking_type_id_prop_id_uindex` (`ingredient_id`,`cooking_type_id`,`ingredients_changable_prop_id`),
  KEY `ingredients_cooking_types_cooking_types_id_fk` (`cooking_type_id`),
  KEY `ingredients_cooking_types_ingredients_changable_props_id_fk` (`ingredients_changable_prop_id`),
  CONSTRAINT `ingredients_cooking_types_cooking_types_id_fk` FOREIGN KEY (`cooking_type_id`) REFERENCES `cooking_types` (`id`),
  CONSTRAINT `ingredients_cooking_types_ingredients_changable_props_id_fk` FOREIGN KEY (`ingredients_changable_prop_id`) REFERENCES `ingredients_changable_props` (`id`),
  CONSTRAINT `ingredients_cooking_types_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_cooking_types`
--

LOCK TABLES `ingredients_cooking_types` WRITE;
/*!40000 ALTER TABLE `ingredients_cooking_types` DISABLE KEYS */;
INSERT INTO `ingredients_cooking_types` VALUES (18,28,1,1.3,5);
/*!40000 ALTER TABLE `ingredients_cooking_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_ingredients_props`
--

DROP TABLE IF EXISTS `ingredients_ingredients_props`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_ingredients_props` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ingredient_id` bigint(20) NOT NULL,
  `ingredient_props_category_id` bigint(20) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_ingredients_props_id_uindex` (`id`),
  KEY `ingredients_ingredients_props_ingredients_props_categories_id_fk` (`ingredient_props_category_id`),
  KEY `ingredients_ingredients_props_ingredients_id_fk` (`ingredient_id`),
  CONSTRAINT `ingredients_ingredients_props_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `ingredients_ingredients_props_ingredients_props_categories_id_fk` FOREIGN KEY (`ingredient_props_category_id`) REFERENCES `ingredients_props_categories` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_ingredients_props`
--

LOCK TABLES `ingredients_ingredients_props` WRITE;
/*!40000 ALTER TABLE `ingredients_ingredients_props` DISABLE KEYS */;
/*!40000 ALTER TABLE `ingredients_ingredients_props` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_props`
--

DROP TABLE IF EXISTS `ingredients_props`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_props` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_properties_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_props`
--

LOCK TABLES `ingredients_props` WRITE;
/*!40000 ALTER TABLE `ingredients_props` DISABLE KEYS */;
INSERT INTO `ingredients_props` VALUES (1,'Жирность'),(2,'Сезонность');
/*!40000 ALTER TABLE `ingredients_props` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_props_categories`
--

DROP TABLE IF EXISTS `ingredients_props_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_props_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ingredient_prop_id` bigint(20) NOT NULL,
  `ingredient_category_id` bigint(20) NOT NULL,
  `unit_data_type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_props_categories_map_id_uindex` (`id`),
  UNIQUE KEY `props_id_category_id_uindex` (`ingredient_prop_id`,`ingredient_category_id`),
  KEY `ingredients_props_categories_map_ingredients_categories_id_fk` (`ingredient_category_id`),
  KEY `ingredients_props_categories_map_unit_data_types_id_fk` (`unit_data_type_id`),
  CONSTRAINT `ingredients_props_categories_map_ingredients_categories_id_fk` FOREIGN KEY (`ingredient_category_id`) REFERENCES `ingredients_categories` (`id`),
  CONSTRAINT `ingredients_props_categories_map_ingredients_props_id_fk` FOREIGN KEY (`ingredient_prop_id`) REFERENCES `ingredients_props` (`id`),
  CONSTRAINT `ingredients_props_categories_map_unit_data_types_id_fk` FOREIGN KEY (`unit_data_type_id`) REFERENCES `unit_data_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_props_categories`
--

LOCK TABLES `ingredients_props_categories` WRITE;
/*!40000 ALTER TABLE `ingredients_props_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `ingredients_props_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_unit_types`
--

DROP TABLE IF EXISTS `ingredients_unit_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_unit_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ingredient_id` bigint(20) NOT NULL,
  `unit_type_id` bigint(20) NOT NULL,
  `weight` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_unit_types_id_uindex` (`id`),
  UNIQUE KEY `ingredients_unit_types_u` (`ingredient_id`,`unit_type_id`),
  KEY `ingredients_unit_types_unit_types_id_fk` (`unit_type_id`),
  CONSTRAINT `ingredients_unit_types_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `ingredients_unit_types_unit_types_id_fk` FOREIGN KEY (`unit_type_id`) REFERENCES `unit_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=868 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_unit_types`
--

LOCK TABLES `ingredients_unit_types` WRITE;
/*!40000 ALTER TABLE `ingredients_unit_types` DISABLE KEYS */;
INSERT INTO `ingredients_unit_types` VALUES (54,1,2,7),(55,2,2,5),(56,3,2,7),(57,4,2,5),(58,5,2,5),(59,8,2,8),(60,9,2,7),(61,10,2,8),(62,11,2,7),(63,12,2,7),(64,13,2,7),(65,14,2,7),(66,15,2,10),(67,16,2,7),(68,17,2,7),(69,18,2,7),(70,19,2,7),(71,20,2,10),(72,21,2,10),(73,22,2,10),(74,23,2,8),(75,24,2,7),(76,25,2,10),(77,26,2,7),(78,28,2,7),(79,29,2,7),(80,30,2,7),(81,31,2,8),(82,32,2,7),(83,33,2,7),(84,34,2,7),(85,42,2,7),(86,43,2,7),(87,44,2,7),(88,45,2,7),(89,46,2,7),(90,47,2,7),(91,48,2,7),(93,50,2,7),(94,51,2,10),(95,52,2,7),(96,53,2,8),(97,54,2,7),(98,55,2,7),(99,56,2,7),(100,57,2,7),(101,58,2,7),(102,59,2,7),(103,60,2,7),(104,61,2,7),(105,62,2,7),(106,63,2,7),(107,64,2,7),(108,99,2,7),(109,100,2,7),(110,101,2,7),(111,114,2,7),(112,115,2,7),(113,116,2,7),(115,169,2,10),(116,212,2,7),(118,1,1,20),(119,2,1,15),(120,3,1,20),(121,4,1,15),(122,5,1,15),(123,8,1,25),(124,9,1,20),(125,10,1,25),(126,11,1,20),(127,12,1,20),(128,13,1,20),(129,14,1,20),(130,15,1,30),(131,16,1,20),(132,17,1,20),(133,18,1,20),(134,19,1,20),(135,20,1,30),(136,21,1,30),(137,22,1,30),(138,23,1,25),(139,24,1,20),(140,25,1,30),(141,26,1,20),(142,28,1,20),(143,29,1,20),(144,30,1,20),(145,31,1,25),(146,32,1,20),(147,33,1,20),(148,34,1,20),(149,42,1,20),(150,43,1,20),(151,44,1,20),(152,45,1,20),(153,46,1,20),(154,47,1,20),(155,48,1,20),(157,50,1,20),(158,51,1,30),(159,52,1,20),(160,53,1,25),(161,54,1,20),(162,55,1,20),(163,56,1,20),(164,57,1,20),(165,58,1,20),(166,59,1,20),(167,60,1,20),(168,61,1,20),(169,62,1,20),(170,63,1,20),(171,64,1,20),(172,99,1,20),(173,100,1,20),(174,101,1,20),(175,114,1,20),(176,115,1,20),(177,116,1,20),(180,211,1,15),(181,212,1,20),(183,1,5,200),(184,2,5,100),(185,3,5,200),(186,4,5,100),(187,5,5,100),(188,9,5,200),(189,10,5,130),(190,11,5,200),(191,12,5,200),(192,13,5,200),(193,14,5,200),(194,15,5,160),(195,16,5,200),(196,17,5,200),(197,18,5,200),(198,19,5,200),(199,20,5,150),(200,21,5,160),(201,22,5,160),(202,23,5,170),(203,24,5,150),(204,25,5,160),(205,26,5,200),(206,28,5,200),(207,29,5,200),(208,30,5,200),(209,31,5,185),(210,32,5,200),(211,33,5,140),(212,34,5,200),(214,36,5,70),(215,37,5,70),(216,38,5,70),(217,39,5,70),(218,40,5,70),(219,41,5,70),(220,42,5,185),(221,43,5,200),(222,44,5,200),(223,45,5,200),(224,46,5,200),(225,47,5,200),(226,48,5,200),(228,50,5,200),(229,51,5,160),(230,52,5,200),(231,53,5,200),(232,54,5,200),(233,55,5,200),(234,56,5,200),(235,57,5,200),(236,58,5,200),(237,59,5,200),(238,60,5,200),(239,61,5,200),(240,62,5,200),(241,63,5,200),(242,64,5,200),(243,93,5,200),(244,94,5,200),(245,95,5,200),(246,96,5,200),(247,99,5,200),(248,100,5,200),(249,101,5,200),(250,103,5,200),(251,104,5,200),(252,114,5,200),(253,115,5,200),(254,116,5,200),(256,138,5,200),(257,155,5,152),(258,212,5,200),(259,275,5,130),(260,281,5,125),(263,142,6,100),(264,143,6,90),(265,144,6,12),(266,221,6,50),(267,223,6,12),(268,225,6,42),(269,226,6,35),(270,250,6,130),(271,251,6,130.5),(272,252,6,50),(273,253,6,130),(274,254,6,55),(275,255,6,1000),(276,256,6,240),(277,257,6,1000),(278,258,6,50),(279,259,6,50),(280,260,6,1000),(281,261,6,350),(282,262,6,110),(284,264,6,135),(285,265,6,26),(286,266,6,85),(287,267,6,30),(288,268,6,125),(289,270,6,200),(290,291,6,100),(291,292,6,75),(292,294,6,30),(293,295,6,150),(294,296,6,250),(295,298,6,50),(296,299,6,250),(297,300,6,125),(298,301,6,75),(299,302,6,100),(300,303,6,50),(301,304,6,4),(302,305,6,145),(303,306,6,20),(304,307,6,95),(305,308,6,100),(306,309,6,20),(307,310,6,200),(308,311,6,1000),(309,313,6,100),(310,1,3,30),(311,1,4,1),(312,2,4,1),(313,3,4,1),(314,4,4,1),(315,5,4,1),(316,8,4,1),(317,9,4,1),(318,10,4,1),(319,11,4,1),(320,12,4,1),(321,13,4,1),(322,14,4,1),(323,15,4,1),(324,16,4,1),(325,17,4,1),(326,18,4,1),(327,19,4,1),(328,20,4,1),(329,21,4,1),(330,22,4,1),(331,23,4,1),(332,24,4,1),(333,25,4,1),(334,26,4,1),(335,28,4,1),(336,29,4,1),(337,30,4,1),(338,31,4,1),(339,32,4,1),(340,33,4,1),(341,34,4,1),(342,42,4,1),(343,43,4,1),(344,44,4,1),(345,45,4,1),(346,46,4,1),(347,47,4,1),(348,48,4,1),(350,50,4,1),(351,51,4,1),(352,52,4,1),(353,53,4,1),(354,54,4,1),(355,55,4,1),(356,56,4,1),(357,57,4,1),(358,58,4,1),(359,59,4,1),(360,60,4,1),(361,61,4,1),(362,62,4,1),(363,63,4,1),(364,64,4,1),(365,99,4,1),(366,100,4,1),(367,101,4,1),(368,114,4,1),(369,115,4,1),(370,116,4,1),(372,169,4,1),(373,212,4,1),(437,211,4,1),(497,93,4,1),(498,94,4,1),(499,95,4,1),(500,96,4,1),(504,103,4,1),(505,104,4,1),(510,138,4,1),(511,155,4,1),(513,275,4,1),(514,281,4,1),(517,142,4,1),(518,143,4,1),(519,144,4,1),(520,221,4,1),(521,223,4,1),(522,225,4,1),(523,226,4,1),(524,250,4,1),(525,251,4,1),(526,252,4,1),(527,253,4,1),(528,254,4,1),(529,255,4,1),(530,256,4,1),(531,257,4,1),(532,258,4,1),(533,259,4,1),(534,260,4,1),(535,261,4,1),(536,262,4,1),(538,264,4,1),(539,265,4,1),(540,266,4,1),(541,267,4,1),(542,268,4,1),(543,270,4,1),(544,291,4,1),(545,292,4,1),(546,294,4,1),(547,295,4,1),(548,296,4,1),(549,298,4,1),(550,299,4,1),(551,300,4,1),(552,301,4,1),(553,302,4,1),(554,303,4,1),(555,304,4,1),(556,305,4,1),(557,306,4,1),(558,307,4,1),(559,308,4,1),(560,309,4,1),(561,310,4,1),(562,311,4,1),(563,313,4,1),(564,145,4,1),(565,195,4,1),(566,6,4,1),(567,7,4,1),(569,65,4,1),(570,66,4,1),(571,67,4,1),(572,68,4,1),(573,69,4,1),(574,70,4,1),(575,71,4,1),(576,72,4,1),(577,73,4,1),(578,74,4,1),(579,75,4,1),(580,76,4,1),(581,77,4,1),(582,78,4,1),(583,79,4,1),(584,80,4,1),(585,81,4,1),(586,82,4,1),(587,83,4,1),(588,84,4,1),(589,85,4,1),(590,86,4,1),(591,87,4,1),(592,88,4,1),(593,89,4,1),(594,90,4,1),(595,91,4,1),(596,92,4,1),(597,97,4,1),(598,98,4,1),(599,102,4,1),(600,105,4,1),(601,106,4,1),(602,107,4,1),(603,108,4,1),(604,109,4,1),(605,110,4,1),(606,111,4,1),(607,112,4,1),(608,113,4,1),(609,117,4,1),(610,118,4,1),(611,119,4,1),(612,120,4,1),(613,121,4,1),(614,122,4,1),(615,123,4,1),(616,124,4,1),(617,125,4,1),(618,126,4,1),(619,127,4,1),(620,128,4,1),(621,129,4,1),(622,130,4,1),(623,131,4,1),(624,132,4,1),(625,133,4,1),(628,137,4,1),(629,139,4,1),(630,140,4,1),(632,147,4,1),(633,148,4,1),(634,149,4,1),(635,150,4,1),(636,151,4,1),(637,152,4,1),(638,153,4,1),(639,154,4,1),(640,156,4,1),(641,157,4,1),(642,158,4,1),(643,159,4,1),(644,160,4,1),(645,161,4,1),(646,162,4,1),(647,163,4,1),(648,164,4,1),(649,165,4,1),(650,166,4,1),(651,167,4,1),(652,168,4,1),(653,170,4,1),(654,171,4,1),(655,172,4,1),(656,173,4,1),(657,174,4,1),(659,177,4,1),(660,178,4,1),(661,179,4,1),(662,180,4,1),(663,181,4,1),(664,182,4,1),(665,183,4,1),(666,184,4,1),(667,185,4,1),(668,186,4,1),(669,187,4,1),(670,188,4,1),(671,189,4,1),(672,190,4,1),(673,191,4,1),(674,192,4,1),(675,193,4,1),(676,194,4,1),(677,196,4,1),(678,197,4,1),(679,198,4,1),(680,199,4,1),(681,200,4,1),(683,202,4,1),(684,203,4,1),(685,204,4,1),(686,205,4,1),(687,206,4,1),(688,207,4,1),(689,208,4,1),(690,209,4,1),(692,213,4,1),(693,214,4,1),(694,215,4,1),(695,216,4,1),(696,217,4,1),(697,218,4,1),(698,219,4,1),(699,220,4,1),(700,222,4,1),(701,224,4,1),(702,227,4,1),(703,228,4,1),(704,229,4,1),(705,230,4,1),(706,231,4,1),(707,232,4,1),(708,233,4,1),(709,234,4,1),(710,235,4,1),(711,236,4,1),(712,237,4,1),(713,238,4,1),(714,239,4,1),(715,240,4,1),(716,241,4,1),(717,242,4,1),(718,243,4,1),(719,244,4,1),(720,245,4,1),(721,246,4,1),(722,247,4,1),(723,248,4,1),(724,249,4,1),(725,269,4,1),(726,271,4,1),(727,272,4,1),(728,273,4,1),(729,274,4,1),(730,276,4,1),(731,277,4,1),(732,278,4,1),(733,279,4,1),(734,280,4,1),(735,282,4,1),(736,283,4,1),(737,284,4,1),(738,285,4,1),(739,286,4,1),(740,287,4,1),(741,288,4,1),(742,289,4,1),(743,290,4,1),(744,293,4,1),(745,297,4,1),(746,312,4,1),(747,314,4,1),(748,315,4,1),(749,316,4,1),(750,317,4,1),(751,318,4,1),(752,319,4,1),(753,320,4,1),(754,321,4,1),(755,322,4,1),(756,323,4,1),(757,324,4,1),(758,325,4,1),(759,326,4,1),(760,329,4,1),(761,330,4,1),(762,331,4,1),(763,332,4,1),(764,333,4,1),(765,334,4,1),(766,335,4,1),(767,336,4,1),(768,337,4,1),(769,338,4,1),(770,339,4,1),(771,340,4,1),(772,341,4,1),(773,342,4,1),(774,343,4,1),(775,344,4,1),(776,345,4,1),(777,346,4,1),(778,347,4,1),(779,348,4,1),(780,349,4,1),(781,350,4,1),(782,351,4,1),(783,352,4,1),(784,353,4,1),(785,354,4,1),(786,356,2,5),(787,356,4,1),(788,356,5,200),(789,356,1,18),(790,141,4,1),(791,141,6,55),(792,175,2,5),(793,175,1,17),(794,175,4,1),(795,134,2,7),(796,134,5,200),(797,134,4,1),(798,134,1,20),(801,135,1,20),(802,135,2,7),(803,135,4,1),(806,136,2,7),(807,136,4,1),(808,136,1,20),(811,357,5,70),(812,357,4,1),(813,357,1,16),(814,263,6,165),(815,263,4,1),(816,146,6,6),(817,146,4,1),(818,358,4,1),(819,358,1,9),(820,359,4,1),(821,360,4,1),(822,360,6,6),(823,35,5,70),(824,35,1,16),(825,35,4,1),(826,361,2,8),(827,361,4,1),(828,27,4,1),(831,362,5,200),(832,362,4,1),(835,201,2,10),(836,201,1,25),(837,201,4,1),(838,363,2,7),(839,363,4,1),(840,363,1,20),(841,364,2,7),(842,364,4,1),(843,364,1,20),(846,210,4,1),(847,210,1,20),(848,210,2,7),(851,176,4,1),(852,176,1,10),(853,176,2,5),(858,355,4,1),(859,355,5,220),(860,355,1,30),(861,355,8,0),(862,355,2,11),(863,49,1,25),(864,49,8,0),(865,49,2,10),(866,49,4,1),(867,49,5,160);
/*!40000 ALTER TABLE `ingredients_unit_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phases`
--

DROP TABLE IF EXISTS `phases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phases` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '1',
  `proteins_percentage` float NOT NULL,
  `carbs_percentage` float NOT NULL,
  `fats_percentage` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phases_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phases`
--

LOCK TABLES `phases` WRITE;
/*!40000 ALTER TABLE `phases` DISABLE KEYS */;
INSERT INTO `phases` VALUES (1,'Molecular',1,0.25,0.25,0.5),(2,'Adaptive',2,0.25,0.35,0.4),(3,'Intuitive',3,0.2,0.4,0.4);
/*!40000 ALTER TABLE `phases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programs`
--

DROP TABLE IF EXISTS `programs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `purpose_id` bigint(20) NOT NULL,
  `calories_difference_coefficient` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `programs_id_uindex` (`id`),
  KEY `programs_purposes_id_fk` (`purpose_id`),
  CONSTRAINT `programs_purposes_id_fk` FOREIGN KEY (`purpose_id`) REFERENCES `purposes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programs`
--

LOCK TABLES `programs` WRITE;
/*!40000 ALTER TABLE `programs` DISABLE KEYS */;
INSERT INTO `programs` VALUES (1,'Novalong weight lose program ',2,0.8),(2,'Schwarzenegger muscle gain program',1,1.2);
/*!40000 ALTER TABLE `programs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programs_dish_types`
--

DROP TABLE IF EXISTS `programs_dish_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programs_dish_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `program_id` bigint(20) NOT NULL,
  `dish_type_id` bigint(20) NOT NULL,
  `calories_percentage` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `programs_dish_types_id_uindex` (`id`),
  KEY `programs_dish_types_programs_id_fk` (`program_id`),
  KEY `programs_dish_types_dish_types_id_fk` (`dish_type_id`),
  CONSTRAINT `programs_dish_types_dish_types_id_fk` FOREIGN KEY (`dish_type_id`) REFERENCES `dish_types` (`id`),
  CONSTRAINT `programs_dish_types_programs_id_fk` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programs_dish_types`
--

LOCK TABLES `programs_dish_types` WRITE;
/*!40000 ALTER TABLE `programs_dish_types` DISABLE KEYS */;
INSERT INTO `programs_dish_types` VALUES (1,1,1,0.2),(2,1,2,0.3),(3,1,3,0.3),(4,1,5,0.2),(5,2,1,0.2),(6,2,2,0.1),(7,2,3,0.3),(8,2,4,0.1),(9,2,5,0.2),(10,2,6,0.1);
/*!40000 ALTER TABLE `programs_dish_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purposes`
--

DROP TABLE IF EXISTS `purposes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purposes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `purposes_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purposes`
--

LOCK TABLES `purposes` WRITE;
/*!40000 ALTER TABLE `purposes` DISABLE KEYS */;
INSERT INTO `purposes` VALUES (1,'GAIN','Muscle Gain'),(2,'LOSE','Weight Lose');
/*!40000 ALTER TABLE `purposes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_USER');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unit_data_types`
--

DROP TABLE IF EXISTS `unit_data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unit_data_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `data_type_code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unit_data_types_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unit_data_types`
--

LOCK TABLES `unit_data_types` WRITE;
/*!40000 ALTER TABLE `unit_data_types` DISABLE KEYS */;
INSERT INTO `unit_data_types` VALUES (1,'Грамм','FLOAT'),(2,'Процент','FLOAT'),(3,'Месяц','STRING');
/*!40000 ALTER TABLE `unit_data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unit_types`
--

DROP TABLE IF EXISTS `unit_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unit_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `unit_data_type_id` bigint(20) NOT NULL,
  `is_dish` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unit_types_id_uindex` (`id`),
  UNIQUE KEY `unit_types_code_uindex` (`code`),
  KEY `unit_types_unit_data_types_id_fk` (`unit_data_type_id`),
  CONSTRAINT `unit_types_unit_data_types_id_fk` FOREIGN KEY (`unit_data_type_id`) REFERENCES `unit_data_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unit_types`
--

LOCK TABLES `unit_types` WRITE;
/*!40000 ALTER TABLE `unit_types` DISABLE KEYS */;
INSERT INTO `unit_types` VALUES (1,'TABLESPOON','Столовая ложка',1,0),(2,'TEASPOON','Чайная ложка',1,0),(3,'HANDFUL','Горсть',1,0),(4,'GRAMS','Грамм',1,0),(5,'GLASS','Стакан',1,0),(6,'PIECE','Штука',1,0),(7,'LITER','Литр',1,0),(8,'BYTASTE','По вкусу',1,0),(9,'SERVING','Порция',1,1);
/*!40000 ALTER TABLE `unit_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_daily_rations`
--

DROP TABLE IF EXISTS `user_daily_rations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_daily_rations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_ration_id` bigint(20) NOT NULL,
  `dish_type_id` bigint(20) NOT NULL,
  `dish_id` bigint(20) NOT NULL,
  `day_code` varchar(10) NOT NULL,
  `protein` float NOT NULL,
  `carbohydrate` float NOT NULL,
  `fat` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_daily_rations_id_uindex` (`id`),
  UNIQUE KEY `ration_id_dish_type_id_dish_id_uindex` (`user_ration_id`,`dish_type_id`,`dish_id`),
  KEY `user_daily_rations_dish_types_id_fk` (`dish_type_id`),
  KEY `user_daily_rations_dishes_id_fk` (`dish_id`),
  CONSTRAINT `user_daily_rations_dish_types_id_fk` FOREIGN KEY (`dish_type_id`) REFERENCES `dish_types` (`id`),
  CONSTRAINT `user_daily_rations_dishes_id_fk` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`),
  CONSTRAINT `user_daily_rations_user_rations_id_fk` FOREIGN KEY (`user_ration_id`) REFERENCES `user_rations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=299 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_daily_rations`
--

LOCK TABLES `user_daily_rations` WRITE;
/*!40000 ALTER TABLE `user_daily_rations` DISABLE KEYS */;
INSERT INTO `user_daily_rations` VALUES (292,41,1,44,'FRI',33.02,224.63,45),(293,41,3,42,'FRI',13.4,157.8,1),(294,41,5,42,'FRI',13.4,157.8,1),(295,41,1,46,'FRI',34.4,3.6,10),(296,41,3,41,'FRI',102.6,32.55,77),(297,41,5,41,'FRI',102.6,32.55,77),(298,41,1,45,'FRI',20.21,160.71,43);
/*!40000 ALTER TABLE `user_daily_rations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_rations`
--

DROP TABLE IF EXISTS `user_rations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_rations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `program_id` bigint(20) NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `total_calories_needed` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_rations_id_uindex` (`id`),
  UNIQUE KEY `user_id_program_id_uindex` (`user_id`,`program_id`),
  KEY `user_rations_programs_id_fk` (`program_id`),
  CONSTRAINT `user_rations_programs_id_fk` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`),
  CONSTRAINT `user_rations_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_rations`
--

LOCK TABLES `user_rations` WRITE;
/*!40000 ALTER TABLE `user_rations` DISABLE KEYS */;
INSERT INTO `user_rations` VALUES (41,1,1,'2018-08-03 19:30:01','2018-08-04 19:30:01',1719);
/*!40000 ALTER TABLE `user_rations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `gender_id` bigint(20) NOT NULL,
  `age` int(11) NOT NULL,
  `height` double NOT NULL,
  `weight` double NOT NULL,
  `purpose_id` bigint(20) DEFAULT NULL,
  `activity_level_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) NOT NULL,
  `current_phase_id` bigint(20) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `phase_update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_id_uindex` (`id`),
  UNIQUE KEY `username_uindex` (`username`),
  KEY `users_roles_id_fk` (`role_id`),
  KEY `users_genders_id_fk` (`gender_id`),
  KEY `users_purposes_id_fk` (`purpose_id`),
  KEY `users_activity_levels_id_fk` (`activity_level_id`),
  KEY `users_phases_id_fk` (`current_phase_id`),
  CONSTRAINT `users_activity_levels_id_fk` FOREIGN KEY (`activity_level_id`) REFERENCES `activity_levels` (`id`),
  CONSTRAINT `users_genders_id_fk` FOREIGN KEY (`gender_id`) REFERENCES `genders` (`id`),
  CONSTRAINT `users_phases_id_fk` FOREIGN KEY (`current_phase_id`) REFERENCES `phases` (`id`),
  CONSTRAINT `users_purposes_id_fk` FOREIGN KEY (`purpose_id`) REFERENCES `purposes` (`id`),
  CONSTRAINT `users_roles_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'tyrion','$2a$10$LTu15SRR31VNZ3P8OtAf1exnKzZ41TparWDh4gR79dPM/eqIbdNZm',1,42,120,41,2,3,1,1,1,'2018-07-16 16:15:51'),(2,'rhaegar','$2a$10$dCcpU10OFTP9CBb8fbFqYuy1Gu39QQlUM7sKWNzDpz1KP94W2USAa',1,39,187,82,1,3,2,1,0,'2018-07-16 16:15:51'),(3,'aegon','$2a$10$LTu15SRR31VNZ3P8OtAf1exnKzZ41TparWDh4gR79dPM/eqIbdNZm',1,49,190,81,1,1,2,1,1,'2018-07-16 16:15:51'),(4,'sansa','$2a$10$dCcpU10OFTP9CBb8fbFqYuy1Gu39QQlUM7sKWNzDpz1KP94W2USAa',2,17,164,47,2,2,2,1,1,'2018-07-16 16:15:51'),(5,'arya','$2a$10$VI56Jp0VR6hEBSNp1QkxN.VI6LqAMQNVubkpWONZpu2ONEwRBA0Ne',2,14,153,42,2,3,2,1,1,'2018-07-16 16:15:51'),(6,'sersey','$2a$10$UjKxdt0kmA85x8MuWAzrHu2S/2IAoKkWVLr8XexRdh4UEsiAmB9cm',2,34,170,51,2,1,2,1,1,'2018-07-16 16:15:51'),(7,'lianna',NULL,2,17,153,42,2,3,2,1,1,'2018-07-16 16:15:51'),(8,'test',NULL,1,17,153,42,2,3,2,1,1,'2018-07-16 16:15:51'),(9,'john',NULL,1,22,164,55,1,3,2,1,1,'2018-07-16 16:15:51');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_dietary_restrictions`
--

DROP TABLE IF EXISTS `users_dietary_restrictions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_dietary_restrictions` (
  `user_id` bigint(20) NOT NULL,
  `dietary_restriction_id` bigint(20) NOT NULL,
  KEY `user_dietary_restrictions_users_id_fk` (`user_id`),
  KEY `user_dietary_restrictions_dietary_restrictions_id_fk` (`dietary_restriction_id`),
  CONSTRAINT `user_dietary_restrictions_dietary_restrictions_id_fk` FOREIGN KEY (`dietary_restriction_id`) REFERENCES `dietary_restrictions` (`id`),
  CONSTRAINT `user_dietary_restrictions_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_dietary_restrictions`
--

LOCK TABLES `users_dietary_restrictions` WRITE;
/*!40000 ALTER TABLE `users_dietary_restrictions` DISABLE KEYS */;
INSERT INTO `users_dietary_restrictions` VALUES (4,2),(4,1),(5,3);
/*!40000 ALTER TABLE `users_dietary_restrictions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `week_days`
--

DROP TABLE IF EXISTS `week_days`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `week_days` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `week_days_id_uindex` (`id`),
  UNIQUE KEY `week_days_code_uindex` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `week_days`
--

LOCK TABLES `week_days` WRITE;
/*!40000 ALTER TABLE `week_days` DISABLE KEYS */;
INSERT INTO `week_days` VALUES (1,'MON','Monday'),(2,'TUE','Tuesday'),(3,'WED','Wednesday'),(4,'THU','Thursday'),(5,'FRI','Friday'),(6,'SAT','Saturday'),(7,'SUN','Sunday');
/*!40000 ALTER TABLE `week_days` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-08 14:17:10
