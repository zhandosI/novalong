-- MySQL dump 10.13  Distrib 5.7.22, for macos10.13 (x86_64)
--
-- Host: localhost    Database: novalong_db
-- ------------------------------------------------------
-- Server version	5.7.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ingredients_categories`
--

DROP TABLE IF EXISTS `ingredients_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_category_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_category_id_uindex` (`id`),
  KEY `ingredients_categories_ingredients_categories_id_fk` (`parent_category_id`),
  CONSTRAINT `ingredients_categories_ingredients_categories_id_fk` FOREIGN KEY (`parent_category_id`) REFERENCES `ingredients_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_categories`
--

LOCK TABLES `ingredients_categories` WRITE;
/*!40000 ALTER TABLE `ingredients_categories` DISABLE KEYS */;
INSERT INTO `ingredients_categories` VALUES (1,'ROOT',NULL),(2,'Ингредиенты для выпечки',71),(3,'Мука и смеси для выпечки',71),(4,'Крупы, бобы',71),(5,'Овсяные и злаковые хлопья, мюсли',4),(6,'Бобовые',4),(7,'Сахар и другие подсластители',71),(8,'Макароны высшего сорта',71),(9,'Лапша',8),(10,'Мясо',1),(11,'Птица',10),(12,'Курица',10),(13,'Индейка',12),(14,'Молочные продукты',1),(15,'Сырная лавка',14),(16,'Рассольный сыр',15),(17,'Натуральные закваски',14),(18,'Молоко',14),(19,'Молоко сгущенное и концентрированное',18),(20,'Кисломолочные продукты',14),(21,'Творог и творожные изделия',14),(22,'Сметана',14),(23,'Сливки',14),(24,'Молочная продукция \"Амиран\"',14),(25,'Сливочное масло, маргарин',14),(26,'Растительное молоко',14),(27,'Яйца',1),(28,'Орехи, сухофрукты, семена',1),(29,'Орехи весовые',28),(30,'Сухофрукты',28),(31,'Семена',28),(32,'Масла, соусы, заправки',1),(33,'Растительное масло',32),(34,'Кетчуп, томатная паста',32),(35,'Соусы',32),(36,'Хрен, горчица, уксус, маринад',32),(37,'Уксус',32),(38,'Майонез',32),(39,'Прочие соусы и лимонады',32),(40,'Хлеб, сдоба, хлебцы',1),(41,'Хлеб',40),(42,'Цельнозерновой хлеб',40),(43,'Сушки и баранки',40),(44,'Грибы',1),(45,'Грибы свежие',44),(46,'Зелень',1),(47,'Фрукты, ягоды',1),(48,'Цитрусовые',47),(49,'Экзотические',47),(50,'Ягоды свежие',47),(51,'Фрукты и ягоды замороженные',47),(52,'Овощи',1),(53,'Овощи свежие',52),(54,'Овощи маринованные',52),(55,'Овощи замороженные',52),(56,'БАДы',1),(57,'Диетическое питание',1),(58,'Отруби и семена',57),(59,'Рыба, морепродукты и икра',1),(60,'Рыба',59),(61,'Морепродукты',59),(62,'Полуфабрикаты',1),(63,'Мясные полуфабрикаты',62),(64,'Пельмени, вареники, манты',62),(65,'Консервы',1),(66,'Грибные консервы',65),(67,'Оливки и маслины',65),(68,'Овощные консервы',65),(69,'Чипсы, снэхи, сухарики',1),(70,'Азиатская кухня',1),(71,'Бакалея',1);
/*!40000 ALTER TABLE `ingredients_categories` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-20 16:22:02
