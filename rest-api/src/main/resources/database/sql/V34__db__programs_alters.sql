ALTER TABLE user_daily_rations ADD active tinyint(1) DEFAULT 1 NOT NULL;
ALTER TABLE user_daily_rations MODIFY fat float NOT NULL;

ALTER TABLE user_rations MODIFY total_calories_needed float NOT NULL;
ALTER TABLE user_rations MODIFY start_date date NOT NULL;
ALTER TABLE user_rations MODIFY end_date date NOT NULL;

CREATE TABLE dish_categories_codes
(
  category_id bigint NOT NULL,
  code varchar(40) NOT NULL,
  CONSTRAINT dish_categories_codes_pk PRIMARY KEY (category_id, code)
);
CREATE UNIQUE INDEX dish_categories_codes_category_id_code_uindex ON dish_categories_codes (category_id, code);

ALTER TABLE dish_categories_codes
  ADD CONSTRAINT dish_categories_codes_dish_categories_id_fk
FOREIGN KEY (category_id) REFERENCES dish_categories (id);

INSERT INTO dish_categories_codes
VALUES (4, 'MEAT_MEAL'), (2, 'GARNISH'), (10, 'SALAD'), (8, 'PRIMARY_MEAL'), (3, 'VEGETABLE_GARNISH');


-- ---------------------------- PROGRAM DISH TYPE ORDER -----------------------------
INSERT INTO programs_dish_types (program_id, dish_type_id, calories_percentage, generation_order, active)
VALUE (1, 4, 0.1, 5, 1);
UPDATE programs_dish_types SET calories_percentage = 0.2 WHERE dish_type_id = 1;
UPDATE programs_dish_types SET calories_percentage = 0.1 WHERE dish_type_id = 2;
UPDATE programs_dish_types SET calories_percentage = 0.3 WHERE dish_type_id = 3;
UPDATE programs_dish_types SET calories_percentage = 0.3 WHERE dish_type_id = 5;
-- ----------------------------------------------------------------------------------



-- ------------------------- OUT OF MEAL FOR COOKING TYPES -------------------------
ALTER TABLE cooking_types ADD out_of_meal_plan tinyint DEFAULT 0 NOT NULL;
ALTER TABLE cooking_types
  MODIFY COLUMN active tinyint(1) DEFAULT '1' AFTER out_of_meal_plan;
update cooking_types c set c.out_of_meal_plan = 1 where c.name like '%Жарка на сковороде с большим количеством масла%';

update novalong_db.catalog
set json_template = '{ "id": -1, "name": "name", "colorCodeId": -1, "outOfMealPlan": false, "active": true }'
where name = 'cookingtypes';
-- ----------------------------------------------------------------------------------


-- ------------------------------- UPDATED PROCEDURES -------------------------------
drop procedure if exists RANDOM_DISHES_BY_DISH_TYPE_CATEGORIES;
delimiter //
create procedure RANDOM_DISHES_BY_DISH_TYPE_CATEGORIES (in dt_id bigint, in dc_names text, in dc_ratios text, in total_cals double)
  begin
    declare _next text default null;
    declare _nextlen int default null;
    declare _value text default null;

    declare _next_ratio text default null;
    declare _nextlen_ratio int default null;
    declare _value_ratio text default null;

    declare j int default 0;

    drop table if exists tmp;
    create temporary table if not exists tmp (
      id bigint(20),
      name varchar(255),
      weight float,
      protein float,
      fat float,
      carbohydrate float
    );

    -- table for tracking dish id's in order to eliminate duplicates
    drop table if exists tmp_ids;
    create temporary table if not exists tmp_ids (
      id bigint(20)
    );

    iterator:
    loop
      -- ensure the loop won't exceed 10 times
      set j = j + 1;
      if j > 10 then
        leave iterator;
      end if;

      -- exit the loop if the list seems empty or was null;
      -- this extra caution is necessary to avoid an endless loop in the proc.
      if LENGTH(TRIM(dc_names)) = 0 or dc_names is null then
        leave iterator;
      end if;
      if LENGTH(TRIM(dc_ratios)) = 0 or dc_ratios is null then
        leave iterator;
      end if;

      -- capture the next value from the list
      set _next = SUBSTRING_INDEX(dc_names,',',1);
      set _next_ratio = SUBSTRING_INDEX(dc_ratios,',',1);

      -- save the length of the captured value; we will need to remove this
      -- many characters + 1 from the beginning of the string
      -- before the next iteration
      set _nextlen = CHAR_LENGTH(_next);
      set _nextlen_ratio = CHAR_LENGTH(_next_ratio);

      -- trim the value of leading and trailing spaces, in case of sloppy CSV strings
      set _value = TRIM(_next);
      set _value_ratio = TRIM(_next_ratio);

      -- --------

      set @count_query = concat('select count(*) into @count
                      from dishes d
                      inner join dishes_dish_types ddt on d.id = ddt.dish_id
                      inner join dish_types dt on ddt.dish_type_id = dt.id
                      inner join dishes_dish_categories ddc on d.id = ddc.dish_id
                      inner join dish_categories dc on ddc.dish_category_id = dc.id
                      inner join dish_categories_codes dcc on dc.id = dcc.category_id
                      inner join dishes_cooking_types dct on d.id = dct.dish_id
                      inner join cooking_types ct on dct.cooking_type_id = ct.id
                      where dt.id = ', dt_id, ' and dcc.code = \'', _value, '\' and
                            (d.protein * 4 + d.fat * 9 + d.carbohydrate * 4) <> 0 and
                            ct.out_of_meal_plan = 0');
      prepare count_stmt from @count_query;
      execute count_stmt;
      set @random_offset = FLOOR(RAND() * @count);

      -- --------

      set @query_ids = concat('select d.id into @dish_id from dishes d
         inner join dishes_dish_types ddt on d.id = ddt.dish_id
         inner join dish_types dt on ddt.dish_type_id = dt.id
         inner join dishes_dish_categories ddc on d.id = ddc.dish_id
         inner join dish_categories dc on ddc.dish_category_id = dc.id
         inner join dish_categories_codes dcc on dc.id = dcc.category_id
         inner join dishes_cooking_types dct on d.id = dct.dish_id
         inner join cooking_types ct on dct.cooking_type_id = ct.id
         where dt.id = ', dt_id, ' and dcc.code = \'', _value, '\' and
               (d.protein * 4 + d.fat * 9 + d.carbohydrate * 4) <> 0 and
               ct.out_of_meal_plan = 0
         limit 1 offset ', @random_offset);

      prepare ids_stmt from @query_ids;
      execute ids_stmt;

      if (select count(*) from tmp_ids where id = @dish_id) > 0 then
        iterate iterator;
      end if ;

      insert into tmp_ids value (@dish_id);

      -- --------

      -- insert the extracted value into the target table
      set @query = concat('insert into tmp
        (select d.id, d.name,
         round((', _value_ratio, ' * ', total_cals, ' * d.weight) / (d.protein * 4 + d.fat * 9 + d.carbohydrate * 4) / 10) * 10 as weight,
         d.protein / if(d.weight = 0, 1, d.weight) * (round((', _value_ratio, ' * ', total_cals, ' * d.weight) / (d.protein * 4 + d.fat * 9 + d.carbohydrate * 4) / 10) * 10) as protein,
         d.fat / if(d.weight = 0, 1, d.weight) * (round((', _value_ratio, ' * ', total_cals, ' * d.weight) / (d.protein * 4 + d.fat * 9 + d.carbohydrate * 4) / 10) * 10) as fat,
         d.carbohydrate / if(d.weight = 0, 1, d.weight) * (round((', _value_ratio, ' * ', total_cals, ' * d.weight) / (d.protein * 4 + d.fat * 9 + d.carbohydrate * 4) / 10) * 10) as carbohydrate
         from dishes d
         inner join dishes_dish_types ddt on d.id = ddt.dish_id
         inner join dish_types dt on ddt.dish_type_id = dt.id
         inner join dishes_dish_categories ddc on d.id = ddc.dish_id
         inner join dish_categories dc on ddc.dish_category_id = dc.id
         inner join dish_categories_codes dcc on dc.id = dcc.category_id
         inner join dishes_cooking_types dct on d.id = dct.dish_id
         inner join cooking_types ct on dct.cooking_type_id = ct.id
         where dt.id = ', dt_id, ' and dcc.code = \'', _value, '\' and
               (d.protein * 4 + d.fat * 9 + d.carbohydrate * 4) <> 0 and
               ct.out_of_meal_plan = 0
         limit 1 offset ', @random_offset, ')');
      prepare stmt from @query;
      execute stmt;

      -- rewrite the original string using the `INSERT()` string function,
      -- args are original string, start position, how many characters to remove,
      -- and what to "insert" in their place (in this case, we "insert"
      -- an empty string, which removes _nextlen + 1 characters)
      set dc_names = INSERT(dc_names, 1, _nextlen + 1, '');
      set dc_ratios = INSERT(dc_ratios, 1, _nextlen_ratio + 1, '');
    end loop ;

    select * from tmp;
    drop table if exists tmp;
    drop table if exists tmp_ids;
  end //
delimiter ;


drop procedure if exists RANDOM_DISHES_BY_DISH_TYPE;
delimiter //
create procedure RANDOM_DISHES_BY_DISH_TYPE (in dt_id bigint, in dishes_num int, in total_cals double)
  begin
    declare i int default 1;
    declare j int default 0;

    drop table if exists tmp;
    create temporary table if not exists tmp (
      id bigint(20),
      name varchar(255),
      weight float,
      protein float,
      fat float,
      carbohydrate float
    );

    drop table if exists tmp_ids;
    create temporary table if not exists tmp_ids (
      id bigint(20)
    );

    set @count_query = concat('select count(*) into @count from dishes d
                      inner join dishes_dish_types ddt on d.id = ddt.dish_id
                      inner join dish_types dt on ddt.dish_type_id = dt.id
                      inner join dishes_cooking_types dct on d.id = dct.dish_id
                      inner join cooking_types ct on dct.cooking_type_id = ct.id
                      where ddt.dish_type_id = ', dt_id, ' and
                            ct.out_of_meal_plan = 0');
    prepare count_stmt from @count_query;
    execute count_stmt;

    if @count < dishes_num then
      set dishes_num = @count;
    end if;

    select_loop: while i <= dishes_num and j <= 10 do
      set j = j + 1;

      set @random_offset = FLOOR(RAND() * @count);

      set @query_ids = concat('select d.id into @dish_id from dishes d
            inner join dishes_dish_types ddt on d.id = ddt.dish_id
            inner join dishes_cooking_types dct on d.id = dct.dish_id
            inner join cooking_types ct on dct.cooking_type_id = ct.id
            where ddt.dish_type_id = ', dt_id, ' and
                  (d.protein * 4 + d.fat * 9 + d.carbohydrate * 4) <> 0 and
                  ct.out_of_meal_plan = 0
            limit 1 offset ', @random_offset);
      prepare ids_stmt from @query_ids;
      execute ids_stmt;


      if (select count(*) from tmp_ids where id = @dish_id) > 0 then
        iterate select_loop;
      end if ;

      insert into tmp_ids value (@dish_id);

      set @query = concat('insert into tmp
        (select d.id, d.name,
         round((', 1 / dishes_num, ' * ', total_cals, ' * d.weight) / (d.protein * 4 + d.fat * 9 + d.carbohydrate * 4) / 10) * 10 as weight,
         d.protein / if(d.weight = 0, 1, d.weight) * (round((', 1 / dishes_num, ' * ', total_cals, ' * d.weight) / (d.protein * 4 + d.fat * 9 + d.carbohydrate * 4) / 10) * 10) as protein,
         d.fat / if(d.weight = 0, 1, d.weight) * (round((', 1 / dishes_num, ' * ', total_cals, ' * d.weight) / (d.protein * 4 + d.fat * 9 + d.carbohydrate * 4) / 10) * 10) as fat,
         d.carbohydrate / if(d.weight = 0, 1, d.weight) * (round((', 1 / dishes_num, ' * ', total_cals, ' * d.weight) / (d.protein * 4 + d.fat * 9 + d.carbohydrate * 4) / 10) * 10) as carbohydrate
         from dishes d
         inner join dishes_dish_types ddt on d.id = ddt.dish_id
         inner join dishes_cooking_types dct on d.id = dct.dish_id
         inner join cooking_types ct on dct.cooking_type_id = ct.id
         where ddt.dish_type_id = ', dt_id, ' and
               (d.protein * 4 + d.fat * 9 + d.carbohydrate * 4) <> 0 and
               ct.out_of_meal_plan = 0
         limit 1 offset ', @random_offset, ')');

      prepare stmt from @query;
      execute stmt;

      set i = i + 1;
    end while;

    select * from tmp;
    drop table if exists tmp;
    drop table if exists tmp_ids;
  end //
delimiter ;
