ALTER TABLE user_rations DROP FOREIGN KEY user_rations_users_id_fk;
ALTER TABLE user_rations DROP FOREIGN KEY user_rations_programs_id_fk;

drop index user_rations_programs_id_fk on user_rations;

ALTER TABLE user_rations DROP KEY user_id_program_id_uindex;

ALTER TABLE user_rations ADD active tinyint(1) DEFAULT 1 NOT NULL;

ALTER TABLE user_rations ADD CONSTRAINT user_rations_users_id_fk
  FOREIGN KEY (user_id) REFERENCES users (id);
ALTER TABLE user_rations ADD CONSTRAINT user_rations_programs_id_fk
  FOREIGN KEY (program_id) REFERENCES programs (id);

-- ---------------------------------------------------------
