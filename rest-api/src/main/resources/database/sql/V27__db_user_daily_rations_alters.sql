ALTER TABLE user_daily_rations DROP FOREIGN KEY user_daily_rations_user_rations_id_fk;
ALTER TABLE user_daily_rations DROP FOREIGN KEY user_daily_rations_dish_types_id_fk;
ALTER TABLE user_daily_rations DROP FOREIGN KEY user_daily_rations_dishes_id_fk;
ALTER TABLE user_daily_rations DROP FOREIGN KEY user_daily_rations_unit_types_id_fk;
ALTER TABLE user_daily_rations DROP KEY ration_id_dish_type_id_dish_id_uindex;

ALTER TABLE user_daily_rations ADD CONSTRAINT `user_daily_rations_dish_types_id_fk` FOREIGN KEY (`dish_type_id`) REFERENCES `dish_types` (`id`);
ALTER TABLE user_daily_rations ADD CONSTRAINT `user_daily_rations_dishes_id_fk` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`);
ALTER TABLE user_daily_rations ADD CONSTRAINT `user_daily_rations_unit_types_id_fk` FOREIGN KEY (`unit_type_id`) REFERENCES `unit_types` (`id`);
ALTER TABLE user_daily_rations ADD CONSTRAINT `user_daily_rations_user_rations_id_fk` FOREIGN KEY (`user_ration_id`) REFERENCES `user_rations` (`id`);

