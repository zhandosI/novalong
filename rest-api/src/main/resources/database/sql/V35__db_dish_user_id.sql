ALTER TABLE novalong_db.dishes ADD COLUMN user_id BIGINT NULL AFTER id,
  ADD FOREIGN KEY (user_id) REFERENCES novalong_db.users(id);