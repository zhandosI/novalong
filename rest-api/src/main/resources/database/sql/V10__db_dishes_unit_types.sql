CREATE TABLE dish_unit_types
(
  id bigint PRIMARY KEY NOT NULL AUTO_INCREMENT,
  dish_id bigint NOT NULL,
  unit_type_id bigint NOT NULL,
  weight double NOT NULL
);
CREATE UNIQUE INDEX dish_unit_types_id_uindex ON dish_unit_types (id);

ALTER TABLE dish_unit_types
  ADD CONSTRAINT dish_id_unit_id_uindex
UNIQUE (dish_id, unit_type_id);

ALTER TABLE dish_unit_types
  ADD CONSTRAINT dish_unit_types_dishes_id_fk
FOREIGN KEY (dish_id) REFERENCES dishes (id);

ALTER TABLE dish_unit_types
  ADD CONSTRAINT dish_unit_types_unit_types_id_fk
FOREIGN KEY (unit_type_id) REFERENCES unit_types (id);

-- -----------------------------------------------------------------

ALTER TABLE unit_types ADD is_dish TINYINT(1) DEFAULT false  NOT NULL;

UPDATE unit_types SET name = 'Штука' WHERE id = 6;
INSERT INTO unit_types VALUE (9, 'SERVING', 'Порция', 1, 1);