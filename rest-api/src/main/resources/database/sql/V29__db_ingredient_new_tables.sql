-- 04.10

CREATE TABLE ingredients_codes
(
    ingredient_id bigint(20) NOT NULL,
    code varchar(40) NOT NULL,
    CONSTRAINT ingredients_codes_ingredients_id_fk FOREIGN KEY (ingredient_id)
    REFERENCES ingredients (id)
);
CREATE UNIQUE INDEX ingredients_codes_ingredient_id_code_uindex
  ON ingredients_codes (ingredient_id, code);
ALTER TABLE ingredients_codes ADD PRIMARY KEY (ingredient_id, code);

INSERT INTO ingredients_codes VALUES
  ((SELECT id FROM ingredients WHERE name LIKE 'Сахар'), 'SUGAR'),
  ((SELECT id FROM ingredients WHERE name LIKE 'Вода'), 'WATER'),
  ((SELECT id FROM ingredients WHERE name LIKE 'Соль'), 'SALT');

-- -----------------------------------------------------------------------

CREATE TABLE color_codes
(
  id bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  code varchar(40) NOT NULL,
  hex_value varchar(40) NOT NULL,
  factor double NOT NULL
);
CREATE UNIQUE INDEX color_codes_id_uindex ON color_codes (id);
CREATE UNIQUE INDEX color_codes_code_uindex ON color_codes (code);

INSERT INTO color_codes (code, hex_value, factor) VALUES
  ('RED', '#FF0000', 0), ('YELLOW', '#FFFF00', 0.5), ('GREEN', '#00FF00', 1);

-- -----------------------------------------------------------------------

CREATE TABLE dishes_cooking_types
(
  dish_id bigint(20) NOT NULL,
  cooking_type_id bigint(20) NOT NULL,
  CONSTRAINT dishes_cooking_types_dish_id_cooking_type_id_pk PRIMARY KEY (dish_id, cooking_type_id),
  CONSTRAINT dishes_cooking_types_dishes_id_fk FOREIGN KEY (dish_id) REFERENCES dishes (id),
  CONSTRAINT dishes_cooking_types_cooking_types_id_fk FOREIGN KEY (cooking_type_id) REFERENCES cooking_types (id)
);
CREATE UNIQUE INDEX dishes_cooking_types_dish_id_cooking_type_id_uindex ON dishes_cooking_types (dish_id, cooking_type_id);

-- -----------------------------------------------------------------------

ALTER TABLE cooking_types ADD color_code_id bigint(20) NOT NULL;

UPDATE cooking_types SET name = 'Гриль мяса', color_code_id = 1 WHERE id = 1;
UPDATE cooking_types SET name = 'Жарка на сковороде с большим количеством масла', color_code_id = 1 WHERE id = 2;


INSERT INTO cooking_types (name, color_code_id) VALUES
  ('Жарка на сковороде с небольшим количеством масла', 1),
  ('Гриль овощей',2),
  ('Тушение', 2),
  ('Жарка на сковороде с небольшим количеством масла предварительно немаринованного мяса', 2),
  ('Сырой продукт', 3),
  ('Приготовление на пару', 3),
  ('Отваривание', 3), ('Запекание', 3),
  ('Жарение на сухой сковороде', 3),
  ('Медленная варка', 3),
  ('Жарка на сковороде с небольшим количеством масла предварительно замаринованного мяса', 2),
  ('Сувид - готовка под вакуумом', 3),
  ('Варка под давлением', 3),
  ('Готовка в микроволновой печи', 3);


ALTER TABLE cooking_types ADD CONSTRAINT cooking_types_color_codes_id_fk
FOREIGN KEY (color_code_id) REFERENCES color_codes (id);

-- -----------------------------------------------------------------------

CREATE TABLE ingredients_opt_param_names
(
  id bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  code varchar(40) NOT NULL,
  name varchar(255) NOT NULL
);
CREATE UNIQUE INDEX ingredients_opt_param_names_id_uindex ON ingredients_opt_param_names (id);
CREATE UNIQUE INDEX ingredients_opt_param_names_code_uindex ON ingredients_opt_param_names (code);

CREATE TABLE ingredients_opt_param_values
(
  id bigint(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  opt_param_name_id bigint(20) NOT NULL,
  ingredient_id bigint(20) NOT NULL,
  weight double NOT NULL,
  CONSTRAINT ingredients_opt_param_values_ingredients_id_fk FOREIGN KEY (ingredient_id) REFERENCES ingredients (id),
  CONSTRAINT ingredients_opt_param_values_ingredients_opt_param_names_id_fk FOREIGN KEY (opt_param_name_id) REFERENCES ingredients_opt_param_names (id)
);
CREATE UNIQUE INDEX ingredients_opt_param_values_id_uindex ON ingredients_opt_param_values (id);
CREATE UNIQUE INDEX ingredients_opt_param_name_id_ingredient_id_uindex ON ingredients_opt_param_values (opt_param_name_id, ingredient_id);

INSERT INTO novalong_db.ingredients_opt_param_names (code, name) VALUES
  ('FATTY_ACID_ISOMERS', 'Транс-изомеры жирных кислот'),('SALT', 'Соль'), ('SUGAR', 'Сахар');

-- -----------------------------------------------------------------------

ALTER DATABASE `novalong_db` CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
ALTER TABLE `ingredients_opt_param_names` CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
ALTER TABLE ingredients_opt_param_names CONVERT TO CHARACTER SET utf8mb4;

-- -----------------------------------------------------------------------

CREATE TABLE ingredients_categories_codes
(
  category_id bigint(20) NOT NULL,
  code varchar(40) NOT NULL,
  CONSTRAINT ingredients_categories_codes_category_id_code_pk PRIMARY KEY (category_id, code),
  CONSTRAINT ingredients_categories_codes_ingredients_categories_id_fk FOREIGN KEY (category_id) REFERENCES ingredients_categories (id)
);
CREATE UNIQUE INDEX ingredients_categories_codes_category_id_code_uindex ON ingredients_categories_codes (category_id, code);

INSERT INTO ingredients_categories_codes (category_id, code)
  VALUE ((SELECT id FROM ingredients_categories WHERE name LIKE '%Сахар и другие подсластители%'), 'SUGAR');