-- MySQL dump 10.13  Distrib 5.7.22-ndb-7.6.6, for Linux (x86_64)
--
-- Host: localhost    Database: novalong_db
-- ------------------------------------------------------
-- Server version	5.7.22-ndb-7.6.6

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activity_levels`
--

DROP TABLE IF EXISTS `activity_levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_levels` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `coefficient` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `activity_levels_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_levels`
--

LOCK TABLES `activity_levels` WRITE;
/*!40000 ALTER TABLE `activity_levels` DISABLE KEYS */;
INSERT INTO `activity_levels` VALUES (1,'SEDENTARY','Sedentary or light activity','Office worker getting little or no exercise',1.53),(2,'ACTIVE','Active or moderately active','Construction worker or person running one hour daily',1.76),(3,'EXTREME','Vigorously active','Agricultural worker (non mechanized) or person swimming two hours daily	',2.25);
/*!40000 ALTER TABLE `activity_levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alternatives_categories`
--

DROP TABLE IF EXISTS `alternatives_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alternatives_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alternatives_categories_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alternatives_categories`
--

LOCK TABLES `alternatives_categories` WRITE;
/*!40000 ALTER TABLE `alternatives_categories` DISABLE KEYS */;
INSERT INTO `alternatives_categories` VALUES (1,'PRICE'),(2,'HEALTH');
/*!40000 ALTER TABLE `alternatives_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `alternatives_relations`
--

DROP TABLE IF EXISTS `alternatives_relations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alternatives_relations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `pair_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alternatives_relations_id_uindex` (`id`),
  KEY `alternatives_relations_alternatives_relations_id_fk` (`pair_id`),
  CONSTRAINT `alternatives_relations_alternatives_relations_id_fk` FOREIGN KEY (`pair_id`) REFERENCES `alternatives_relations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `alternatives_relations`
--

LOCK TABLES `alternatives_relations` WRITE;
/*!40000 ALTER TABLE `alternatives_relations` DISABLE KEYS */;
INSERT INTO `alternatives_relations` VALUES (1,'LESS',2),(2,'MORE',1);
/*!40000 ALTER TABLE `alternatives_relations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dietary_restrictions`
--

DROP TABLE IF EXISTS `dietary_restrictions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dietary_restrictions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dietary_restrictions_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dietary_restrictions`
--

LOCK TABLES `dietary_restrictions` WRITE;
/*!40000 ALTER TABLE `dietary_restrictions` DISABLE KEYS */;
INSERT INTO `dietary_restrictions` VALUES (1,'DIABETES','Sugar diabetes'),(2,'PREGNANCY','Pregnancy'),(3,'HYPERTENSION','Hypertension'),(4,'GASTRITIS','Gastritis');
/*!40000 ALTER TABLE `dietary_restrictions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dish_categories`
--

DROP TABLE IF EXISTS `dish_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dish_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dish_categories_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dish_categories`
--

LOCK TABLES `dish_categories` WRITE;
/*!40000 ALTER TABLE `dish_categories` DISABLE KEYS */;
INSERT INTO `dish_categories` VALUES (1,'Garnishes'),(2,'Meats'),(3,'Cereals'),(4,'Floury'),(5,'Other');
/*!40000 ALTER TABLE `dish_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dish_ingredients_alternatives`
--

DROP TABLE IF EXISTS `dish_ingredients_alternatives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dish_ingredients_alternatives` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dish_id` bigint(20) NOT NULL,
  `ingredient_id` bigint(20) NOT NULL,
  `alternative_ingredient_id` bigint(20) NOT NULL,
  `alternative_category_id` bigint(20) NOT NULL,
  `alternative_relation_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dish_ingredients_alternatives_id_uindex` (`id`),
  UNIQUE KEY `dish_id_ingredient_id_alternative_i_id_category_id_uindex` (`dish_id`,`ingredient_id`,`alternative_ingredient_id`,`alternative_category_id`),
  KEY `dish_ingredients_alternatives_ingredients_id_fk` (`ingredient_id`),
  KEY `dish_ingredients_alternatives_alternatives_relations_id_fk` (`alternative_relation_id`),
  KEY `dish_ingredients_alternatives_alternatives_categories_id_fk` (`alternative_category_id`),
  KEY `dish_ingredients_alternatives_ingredients_id_fk_2` (`alternative_ingredient_id`),
  CONSTRAINT `dish_ingredients_alternatives_alternatives_categories_id_fk` FOREIGN KEY (`alternative_category_id`) REFERENCES `alternatives_categories` (`id`),
  CONSTRAINT `dish_ingredients_alternatives_alternatives_relations_id_fk` FOREIGN KEY (`alternative_relation_id`) REFERENCES `alternatives_relations` (`id`),
  CONSTRAINT `dish_ingredients_alternatives_dishes_id_fk` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`),
  CONSTRAINT `dish_ingredients_alternatives_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `dish_ingredients_alternatives_ingredients_id_fk_2` FOREIGN KEY (`alternative_ingredient_id`) REFERENCES `ingredients` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dish_ingredients_alternatives`
--

LOCK TABLES `dish_ingredients_alternatives` WRITE;
/*!40000 ALTER TABLE `dish_ingredients_alternatives` DISABLE KEYS */;
INSERT INTO `dish_ingredients_alternatives` VALUES (1,1,5,3,2,1),(3,1,5,7,2,1),(4,16,3,2,2,2),(5,16,3,18,1,2),(6,17,18,13,1,2),(7,17,18,15,1,2),(8,17,18,19,2,1),(9,17,18,2,1,2),(10,17,18,6,2,2),(11,17,18,13,2,2),(12,17,18,6,1,2),(13,17,18,3,1,1),(14,17,18,17,1,1),(15,17,18,1,1,1),(16,17,18,20,1,1),(17,17,17,18,1,2),(18,18,20,13,1,2),(19,18,18,13,1,2),(20,18,18,6,1,2),(21,18,18,3,1,1),(22,18,18,20,1,1),(23,18,18,19,2,1),(24,18,18,6,2,2),(25,18,18,2,1,2),(26,18,18,15,1,2),(27,18,18,17,1,1),(28,18,18,13,2,2),(29,18,18,1,1,1),(30,19,3,18,1,2),(31,19,3,2,2,2),(32,24,5,4,1,1),(33,24,5,12,1,2),(34,26,3,18,1,2),(35,26,3,2,2,2),(36,28,3,2,2,2),(37,28,3,18,1,2),(38,34,3,2,2,2),(39,34,3,18,1,2),(40,35,1,2,1,1),(41,35,1,1,1,1),(42,35,1,19,2,1),(43,35,3,2,2,2),(44,35,3,18,1,2),(45,36,20,13,1,2),(46,36,18,13,1,2),(47,36,18,13,2,2),(48,36,18,6,2,2),(49,36,18,17,1,1),(50,36,18,6,1,2),(51,36,18,2,1,2),(52,36,18,20,1,1),(53,36,18,19,2,1),(54,36,18,3,1,1),(55,36,18,15,1,2),(56,36,18,1,1,1),(57,37,3,2,2,2),(58,37,3,18,1,2);
/*!40000 ALTER TABLE `dish_ingredients_alternatives` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dish_types`
--

DROP TABLE IF EXISTS `dish_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dish_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dish_type_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dish_types`
--

LOCK TABLES `dish_types` WRITE;
/*!40000 ALTER TABLE `dish_types` DISABLE KEYS */;
INSERT INTO `dish_types` VALUES (1,'Breakfast'),(2,'Second breakfast'),(3,'Lunch'),(4,'Snacks'),(5,'Dinner'),(6,'Second dinner');
/*!40000 ALTER TABLE `dish_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dishes`
--

DROP TABLE IF EXISTS `dishes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dishes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `dish_category_id` bigint(20) NOT NULL,
  `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `weight` float NOT NULL,
  `protein` float NOT NULL,
  `fat` float NOT NULL,
  `carbohydrate` float NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `dishes_id_uindex` (`id`),
  KEY `dishes_dish_categories_id_fk` (`dish_category_id`),
  CONSTRAINT `dishes_dish_categories_id_fk` FOREIGN KEY (`dish_category_id`) REFERENCES `dish_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dishes`
--

LOCK TABLES `dishes` WRITE;
/*!40000 ALTER TABLE `dishes` DISABLE KEYS */;
INSERT INTO `dishes` VALUES (1,'Beef under the sour cream','Delicious staff........Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla consequat mi id quam dapibus fringilla. Sed pellentesque nisi sed urna dignissim, quis pharetra risus ullamcorper. Donec scelerisque, massa a sagittis laoreet, elit ligula tempor felis, eu consectetur massa mi vel lectus. Etiam non mattis orci, et iaculis leo.',3,'2018-06-27 05:22:16',124,54,65,4,1),(2,'Plain sandwich','nisi sed urna dignissim, quis pharetra risus ullamcorper. Donec scelerisque, massa a sagittis laoreet, elit ligula tempor felis, eu consectetur massa mi vel lectus. Etiam non mattis orci, et iaculis leo.',4,'2018-07-03 07:25:04',583,77,5,65,1),(3,'Tyrkey sandwich','nisi sed urna dignissim, quis pharetra risus ullamcorper. Donec scelerisque, massa a sagittis laoreet, elit ligula tempor felis, eu consectetur massa mi vel lectus. Etiam non mattis orci, et iaculis leo.',4,'2018-07-03 07:27:12',857,12,87,44,1),(4,'Plov','Required is 100% missing on an incremental watch build.\nDoesn\'t happen all the time.\n\nFor instance, using reactjs key mirror, on a list of constants, I\'d require(\'react/lib/keyMirror\'). On first build, no problem. On watch build, keyMirror module ',4,'2018-07-03 12:11:12',85,0,0,0,0),(5,'Test dish','sdsadsadsdsad',4,'2018-07-04 07:17:01',785,88,77,89,1),(6,'Besparmakkk','rara',4,'2018-07-06 10:28:11',5785,58,144,90,1),(7,'Kuirdakkkk','dsa',4,'2018-07-06 12:18:15',58,65,140,89,1),(8,'ice cream','tasty...',4,'2018-07-08 18:11:03',58,25,17,84,1),(9,'Feature','dsadsadsd',4,'2018-07-08 18:13:39',587,0,0,0,0),(10,'axaxa','rerere',4,'2018-07-08 18:37:25',885,0,0,0,0),(11,'tte','test',4,'2018-07-08 18:38:27',875,0,0,0,0),(12,'ew','ew',4,'2018-07-08 18:39:18',587,0,0,0,0),(13,'rere','rere',4,'2018-07-08 18:41:26',768,0,0,0,0),(14,'rere','rere',4,'2018-07-08 18:43:10',77,0,0,0,0),(15,'rere','rere',4,'2018-07-08 18:43:19',8,0,0,0,0),(16,'rere','rere',4,'2018-07-08 18:52:27',58,0,0,0,0),(17,'ffff','fdsfsdf',4,'2018-07-09 05:20:38',572,0,0,0,0),(18,'Bug','rrrr',4,'2018-07-09 10:18:52',25,9,17,3,1),(19,'tedED','test',4,'2018-07-10 06:43:48',12,0,0,0,0),(20,'rererererere','re',4,'2018-07-11 11:15:03',0,0,0,0,0),(21,'ew','w',4,'2018-07-11 11:17:18',0,0,0,0,0),(22,'ew','w',4,'2018-07-11 11:23:52',0,0,0,0,0),(23,'a','a',4,'2018-07-11 11:24:23',0,0,0,0,0),(24,'gg','g',4,'2018-07-11 11:45:06',0,0,0,0,0),(25,'gaaaa','g',4,'2018-07-11 11:46:19',0,0,0,0,0),(26,'32','32',4,'2018-07-11 15:27:43',0,0,0,0,0),(28,'fuuuuu','ew',4,'2018-07-11 15:51:20',0,94,57,12,1),(29,'test ddddd','ddd',4,'2018-07-12 06:17:42',499,0,0,0,0),(30,'test','te',4,'2018-07-12 06:20:42',34,0,0,0,0),(31,'testddd','dd',4,'2018-07-12 06:21:28',507,0,0,0,0),(32,'tw','we',4,'2018-07-12 06:27:32',507,0,0,0,0),(33,'TESTETETET','eu consectetur massa mi vel lectus. Etiam non mattis orci, et iaculis leo.',3,'2018-07-16 13:11:20',0,0,0,0,0),(34,'Featuredadad','dada',1,'2018-07-16 14:08:56',46,4.4,0.6,32.2,1),(35,'Beef sausage','ssss',2,'2018-07-16 14:13:18',556,99.9,62.8,40.4,1),(36,'SUPPPER DISH','untf8 ???????? ????',4,'2018-07-17 15:55:47',1096,96,58.1,210.7,1),(37,'Oatmeal','wee',3,'2018-07-17 16:00:13',245,20,3.3,177.6,1);
/*!40000 ALTER TABLE `dishes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dishes_dish_types`
--

DROP TABLE IF EXISTS `dishes_dish_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dishes_dish_types` (
  `dish_id` bigint(20) NOT NULL,
  `dish_type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`dish_id`,`dish_type_id`),
  UNIQUE KEY `dishes_id_dish_types_id_uindex` (`dish_id`,`dish_type_id`),
  KEY `dishes_dish_types_dish_types_id_fk` (`dish_type_id`),
  CONSTRAINT `dishes_dish_types_dish_types_id_fk` FOREIGN KEY (`dish_type_id`) REFERENCES `dish_types` (`id`),
  CONSTRAINT `dishes_dish_types_dishes_id_fk` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dishes_dish_types`
--

LOCK TABLES `dishes_dish_types` WRITE;
/*!40000 ALTER TABLE `dishes_dish_types` DISABLE KEYS */;
INSERT INTO `dishes_dish_types` VALUES (5,1),(6,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(25,1),(26,1),(28,1),(29,1),(34,1),(37,1),(1,2),(4,2),(5,2),(6,2),(7,2),(12,2),(13,2),(14,2),(15,2),(16,2),(17,2),(18,2),(25,2),(30,2),(32,2),(33,2),(37,2),(2,3),(3,3),(4,3),(5,3),(8,3),(24,3),(31,3),(35,3),(36,4),(36,5);
/*!40000 ALTER TABLE `dishes_dish_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dishes_ingredients`
--

DROP TABLE IF EXISTS `dishes_ingredients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dishes_ingredients` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dish_id` bigint(20) NOT NULL,
  `ingredient_id` bigint(20) NOT NULL,
  `weight` float NOT NULL,
  `unit_type_id` bigint(20) NOT NULL,
  `unit_type_amount` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dishes_ingredients_id_uindex` (`id`),
  UNIQUE KEY `dish_id_ingr_id_weight_uindex` (`dish_id`,`ingredient_id`,`weight`),
  KEY `dishes_ingredients_ingredients_id_fk` (`ingredient_id`),
  KEY `dishes_ingredients_unit_types_id_fk` (`unit_type_id`),
  CONSTRAINT `dishes_ingredients_dishes_id_fk` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`),
  CONSTRAINT `dishes_ingredients_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `dishes_ingredients_unit_types_id_fk` FOREIGN KEY (`unit_type_id`) REFERENCES `unit_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dishes_ingredients`
--

LOCK TABLES `dishes_ingredients` WRITE;
/*!40000 ALTER TABLE `dishes_ingredients` DISABLE KEYS */;
INSERT INTO `dishes_ingredients` VALUES (1,1,1,542,2,0),(2,1,5,25,1,0),(3,1,6,0,2,0),(4,1,9,57254,1,0),(5,3,24,52,1,0),(6,3,29,524,2,0),(7,3,30,0,1,0),(8,8,15,0,1,0),(9,8,48,0,1,0),(10,9,3,0,1,0),(11,9,13,0,1,0),(12,10,3,0,1,0),(13,10,45,0,1,0),(14,11,3,0,1,0),(15,12,3,0,1,0),(16,13,3,0,1,0),(17,14,3,0,1,0),(18,15,3,0,1,0),(19,16,3,0,1,0),(20,17,17,0,1,0),(21,17,18,0,1,0),(22,18,18,0,1,0),(23,18,20,0,1,0),(24,19,3,0,1,0),(25,28,3,12,4,12),(26,28,50,15,4,15),(27,28,13,13,4,13),(28,32,55,432,4,432),(29,32,57,75,1,3),(30,34,3,43,4,43),(31,34,50,3,4,3),(32,35,1,502,4,502),(33,35,3,54,4,54),(34,36,56,74,4,74),(35,36,4,135,4,135),(36,36,6,45,4,45),(37,36,20,34,3,1),(38,36,7,588,4,588),(39,36,18,220,1,10),(40,37,2,125,1,5),(41,37,3,120,4,120);
/*!40000 ALTER TABLE `dishes_ingredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genders`
--

DROP TABLE IF EXISTS `genders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `genders` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `genders_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genders`
--

LOCK TABLES `genders` WRITE;
/*!40000 ALTER TABLE `genders` DISABLE KEYS */;
INSERT INTO `genders` VALUES (1,'MALE','Male'),(2,'FEMALE','Female');
/*!40000 ALTER TABLE `genders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients`
--

DROP TABLE IF EXISTS `ingredients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `protein` float NOT NULL,
  `fat` float NOT NULL,
  `carbohydrate` float NOT NULL,
  `category_id` bigint(20) NOT NULL,
  `is_allergic` tinyint(4) NOT NULL DEFAULT '0',
  `glycemic_index` float NOT NULL DEFAULT '0',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_id_uindex` (`id`),
  KEY `ingredients_ingredients_category_id_fk` (`category_id`),
  CONSTRAINT `ingredients_ingredients_category_id_fk` FOREIGN KEY (`category_id`) REFERENCES `ingredients_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients`
--

LOCK TABLES `ingredients` WRITE;
/*!40000 ALTER TABLE `ingredients` DISABLE KEYS */;
INSERT INTO `ingredients` VALUES (1,'Beef',18.9,12.4,0,7,0,0,1),(2,'Corn flour',7.2,1.5,70.2,1,0,0,1),(3,'Wheat flour',9.2,1.2,74.9,1,1,0,1),(4,'Sour cream 10%',3,10,2,2,1,0,1),(5,'Sour cream 15%',2.6,15,3,2,1,0,1),(6,'Cherry tomato',0.8,0.1,2.8,5,0,0,1),(7,'Champignon mushroom',4.3,1,0.1,4,0,0,1),(8,'Limon',0.9,0.1,3,6,0,0,1),(9,'Vinegar',0,0,2.3,3,0,0,1),(10,'Mutton',15.6,16.3,0,7,0,0,1),(11,'Rice flour',7.4,0.6,82,1,0,0,1),(12,'Sour cream 20%',3.2,20,2.8,2,1,0,1),(13,'Wine vinegar',0,0,2.1,3,0,0,1),(14,'Pork',16,21.6,0,7,0,0,1),(15,'Banana',2.5,0.4,25.1,1,0,0,1),(16,'fdfdf',54,565,0,1,1,0,0),(17,'Rice flour',7.72,0.5,74.2,1,0,0,1),(18,'Almond flour',7.72,0.5,74.2,1,0,0,1),(19,'Soy flour',6.2,0.5,74.5,1,0,0,1),(20,'Honey agaric',18.9,12.4,0,4,0,0,1),(21,'Honey agaric',18.9,12.4,0,4,0,0,0),(22,'Honey agaric',18.9,12.4,0,4,0,0,0),(23,'Chicken',25,34,7,7,0,15,1),(24,'Tyrkey',26,19,4,7,1,15,1),(25,'Mandarin',4,2,25,6,1,52,1),(26,'Avokado',7,1,14,5,0,24,1),(27,'Avokado',7,1,14,5,0,24,0),(28,'avocado',14,1,24,5,0,1,0),(29,'test',1,1,1,4,0,1,0),(30,'test',1,1,1,4,0,1,0),(31,'test',1,1,1,4,0,1,0),(32,'Pamelo',7,2,19,6,1,28,1),(33,'test test',18.9,12.4,0,4,0,0,0),(34,'test test',18.9,12.4,0,4,0,0,0),(35,'test e23e3es',18.9,12.4,0,4,0,0,0),(36,'Bug',3,332,42,4,1,32,0),(37,'Bug',32,2,2,7,1,43,0),(38,'ewq',4324,443,432,8,0,432,0),(39,'ds',32,32,32,8,0,32,0),(40,'dw',32,320,32,8,0,43,0),(41,'dsa',321,312,3213,4,1,3123,0),(42,'tetrtretrrterter',4324,423,32423,4,0,432,0),(43,'rrr',321,312,32,7,1,312,0),(44,'ttt',43,2,43,8,0,3,0),(45,'Feature',4,432,432,1,1,423,0),(46,'sooooo',43,43,43,4,0,44,0),(47,'neeeeeee',2,21,210,7,0,2,0),(48,'foobarrrrr',645,546,54,7,0,654,0),(49,'testttt',3230,32,32,7,0,32,0),(50,'Whole milk',18,3.2,2,14,1,21,1),(51,'Skim milk',19,0,1,14,1,14,1),(52,'ggtgtgt',312,312,321,4,0,32,1),(53,'Super fatty milk',17,20,1,14,1,17,1),(54,'Sour cream 20%',210,2,202,2,1,2,1),(55,'besteee',43,432,423,8,0,43,1),(56,'Simple Tomato',58,856,58,5,0,58,1),(57,'test ingrrrr',333,321,2,4,1,321,1);
/*!40000 ALTER TABLE `ingredients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_alternatives`
--

DROP TABLE IF EXISTS `ingredients_alternatives`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_alternatives` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ingredient_id` bigint(20) DEFAULT NULL,
  `alternative_ingredient_id` bigint(20) DEFAULT NULL,
  `alternative_category_id` bigint(20) DEFAULT NULL,
  `alternative_relation_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_alternatives_id_uindex` (`id`),
  UNIQUE KEY `ingredient_id_alternative_i_id_cateory_id_uindex` (`ingredient_id`,`alternative_ingredient_id`,`alternative_category_id`),
  KEY `ingredients_alternatives_ingredients_id_fk_2` (`alternative_ingredient_id`),
  KEY `ingredients_alternatives_alternatives_categories_id_fk` (`alternative_category_id`),
  KEY `ingredients_alternatives_alternatives_relations_id_fk` (`alternative_relation_id`),
  CONSTRAINT `ingredients_alternatives_alternatives_categories_id_fk` FOREIGN KEY (`alternative_category_id`) REFERENCES `alternatives_categories` (`id`),
  CONSTRAINT `ingredients_alternatives_alternatives_relations_id_fk` FOREIGN KEY (`alternative_relation_id`) REFERENCES `alternatives_relations` (`id`),
  CONSTRAINT `ingredients_alternatives_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `ingredients_alternatives_ingredients_id_fk_2` FOREIGN KEY (`alternative_ingredient_id`) REFERENCES `ingredients` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_alternatives`
--

LOCK TABLES `ingredients_alternatives` WRITE;
/*!40000 ALTER TABLE `ingredients_alternatives` DISABLE KEYS */;
INSERT INTO `ingredients_alternatives` VALUES (1,3,2,2,2),(2,17,18,1,2),(3,3,18,1,2),(4,18,3,1,1),(8,18,17,1,1),(9,18,19,2,1),(11,15,13,2,1),(13,18,6,2,2),(24,18,6,1,2),(26,5,4,1,1),(27,5,12,1,2),(28,45,13,1,2),(29,45,7,1,2),(30,47,13,1,2),(35,47,13,2,1),(39,18,13,1,2),(41,18,13,2,2),(43,18,1,1,1),(44,18,15,1,2),(45,18,20,1,1),(46,18,2,1,2),(47,1,2,1,1),(48,1,1,1,1),(49,1,19,2,1),(50,20,13,1,2),(51,15,3,1,2),(52,49,3,1,2),(53,49,5,2,1),(54,52,13,1,1),(55,52,50,2,1),(56,15,19,1,2),(57,15,18,2,1);
/*!40000 ALTER TABLE `ingredients_alternatives` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_categories`
--

DROP TABLE IF EXISTS `ingredients_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_category_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_category_id_uindex` (`id`),
  KEY `ingredients_categories_ingredients_categories_id_fk` (`parent_category_id`),
  CONSTRAINT `ingredients_categories_ingredients_categories_id_fk` FOREIGN KEY (`parent_category_id`) REFERENCES `ingredients_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_categories`
--

LOCK TABLES `ingredients_categories` WRITE;
/*!40000 ALTER TABLE `ingredients_categories` DISABLE KEYS */;
INSERT INTO `ingredients_categories` VALUES (1,'Flour and baking mixes',9),(2,'Sour creams',11),(3,'Vinegars',10),(4,'Mushrooms',NULL),(5,'Tomatoes and cucumbers',13),(6,'Citrus',12),(7,'Meat',NULL),(8,'Ingredients for baking',9),(9,'Grocery',NULL),(10,'Sauces and Oils',NULL),(11,'Dairy produce',NULL),(12,'Fruits and berries',NULL),(13,'Vegetables',NULL),(14,'Milk',11);
/*!40000 ALTER TABLE `ingredients_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_ingredients_props`
--

DROP TABLE IF EXISTS `ingredients_ingredients_props`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_ingredients_props` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ingredient_id` bigint(20) NOT NULL,
  `ingredient_props_category_id` bigint(20) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_ingredients_props_id_uindex` (`id`),
  KEY `ingredients_ingredients_props_ingredients_props_categories_id_fk` (`ingredient_props_category_id`),
  KEY `ingredients_ingredients_props_ingredients_id_fk` (`ingredient_id`),
  CONSTRAINT `ingredients_ingredients_props_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `ingredients_ingredients_props_ingredients_props_categories_id_fk` FOREIGN KEY (`ingredient_props_category_id`) REFERENCES `ingredients_props_categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_ingredients_props`
--

LOCK TABLES `ingredients_ingredients_props` WRITE;
/*!40000 ALTER TABLE `ingredients_ingredients_props` DISABLE KEYS */;
INSERT INTO `ingredients_ingredients_props` VALUES (1,50,1,'3.2'),(2,51,1,'0'),(3,6,5,'October - December'),(4,53,3,'20'),(5,54,4,'20'),(6,56,5,'\"October\"');
/*!40000 ALTER TABLE `ingredients_ingredients_props` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_props`
--

DROP TABLE IF EXISTS `ingredients_props`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_props` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_properties_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_props`
--

LOCK TABLES `ingredients_props` WRITE;
/*!40000 ALTER TABLE `ingredients_props` DISABLE KEYS */;
INSERT INTO `ingredients_props` VALUES (1,'Fat content'),(2,'Seasonality');
/*!40000 ALTER TABLE `ingredients_props` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_props_categories`
--

DROP TABLE IF EXISTS `ingredients_props_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_props_categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ingredient_prop_id` bigint(20) NOT NULL,
  `ingredient_category_id` bigint(20) NOT NULL,
  `unit_data_type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_props_categories_map_id_uindex` (`id`),
  UNIQUE KEY `props_id_category_id_uindex` (`ingredient_prop_id`,`ingredient_category_id`),
  KEY `ingredients_props_categories_map_ingredients_categories_id_fk` (`ingredient_category_id`),
  KEY `ingredients_props_categories_map_unit_data_types_id_fk` (`unit_data_type_id`),
  CONSTRAINT `ingredients_props_categories_map_ingredients_categories_id_fk` FOREIGN KEY (`ingredient_category_id`) REFERENCES `ingredients_categories` (`id`),
  CONSTRAINT `ingredients_props_categories_map_ingredients_props_id_fk` FOREIGN KEY (`ingredient_prop_id`) REFERENCES `ingredients_props` (`id`),
  CONSTRAINT `ingredients_props_categories_map_unit_data_types_id_fk` FOREIGN KEY (`unit_data_type_id`) REFERENCES `unit_data_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_props_categories`
--

LOCK TABLES `ingredients_props_categories` WRITE;
/*!40000 ALTER TABLE `ingredients_props_categories` DISABLE KEYS */;
INSERT INTO `ingredients_props_categories` VALUES (1,1,11,2),(2,2,13,3),(3,1,14,2),(4,1,2,2),(5,2,5,3);
/*!40000 ALTER TABLE `ingredients_props_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredients_unit_types`
--

DROP TABLE IF EXISTS `ingredients_unit_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredients_unit_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ingredient_id` bigint(20) NOT NULL,
  `unit_type_id` bigint(20) NOT NULL,
  `weight` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ingredients_unit_types_id_uindex` (`id`),
  UNIQUE KEY `ingredients_unit_types_u` (`ingredient_id`,`unit_type_id`),
  KEY `ingredients_unit_types_unit_types_id_fk` (`unit_type_id`),
  CONSTRAINT `ingredients_unit_types_ingredients_id_fk` FOREIGN KEY (`ingredient_id`) REFERENCES `ingredients` (`id`),
  CONSTRAINT `ingredients_unit_types_unit_types_id_fk` FOREIGN KEY (`unit_type_id`) REFERENCES `unit_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredients_unit_types`
--

LOCK TABLES `ingredients_unit_types` WRITE;
/*!40000 ALTER TABLE `ingredients_unit_types` DISABLE KEYS */;
INSERT INTO `ingredients_unit_types` VALUES (1,2,1,25),(2,2,2,8),(3,18,2,12),(4,18,1,22),(5,19,1,19),(6,19,2,9),(7,20,3,34),(8,21,3,34),(9,22,3,34),(10,23,2,17),(11,23,3,14),(12,24,3,15),(13,25,2,22),(14,31,1,11),(15,32,3,17),(16,48,1,222),(17,49,1,2323),(18,52,1,32),(19,55,4,1),(20,1,4,1),(21,2,4,1),(22,3,4,1),(23,4,4,1),(24,5,4,1),(25,6,4,1),(26,7,4,1),(27,8,4,1),(28,9,4,1),(29,10,4,1),(30,11,4,1),(31,12,4,1),(32,13,4,1),(33,14,4,1),(34,15,4,1),(35,17,4,1),(36,18,4,1),(37,19,4,1),(38,20,4,1),(39,23,4,1),(40,24,4,1),(41,25,4,1),(42,26,4,1),(43,32,4,1),(44,50,4,1),(45,51,4,1),(46,52,4,1),(47,53,4,1),(48,54,4,1),(49,56,4,1),(50,56,3,185),(51,57,1,25),(52,57,4,1),(53,57,2,15);
/*!40000 ALTER TABLE `ingredients_unit_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phases`
--

DROP TABLE IF EXISTS `phases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phases` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '1',
  `proteins_percentage` float NOT NULL,
  `carbs_percentage` float NOT NULL,
  `fats_percentage` float NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phases_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phases`
--

LOCK TABLES `phases` WRITE;
/*!40000 ALTER TABLE `phases` DISABLE KEYS */;
INSERT INTO `phases` VALUES (1,'Molecular',1,0.25,0.25,0.5),(2,'Adaptive',2,0.25,0.35,0.4),(3,'Intuitive',3,0.2,0.4,0.4);
/*!40000 ALTER TABLE `phases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programs`
--

DROP TABLE IF EXISTS `programs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `purpose_id` bigint(20) NOT NULL,
  `calories_difference_coefficient` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `programs_id_uindex` (`id`),
  KEY `programs_purposes_id_fk` (`purpose_id`),
  CONSTRAINT `programs_purposes_id_fk` FOREIGN KEY (`purpose_id`) REFERENCES `purposes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programs`
--

LOCK TABLES `programs` WRITE;
/*!40000 ALTER TABLE `programs` DISABLE KEYS */;
INSERT INTO `programs` VALUES (1,'Novalong weight lose program ',2,0.8),(2,'Schwarzenegger muscle gain program',1,1.2);
/*!40000 ALTER TABLE `programs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programs_dish_types`
--

DROP TABLE IF EXISTS `programs_dish_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programs_dish_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `program_id` bigint(20) NOT NULL,
  `dish_type_id` bigint(20) NOT NULL,
  `calories_percentage` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `programs_dish_types_id_uindex` (`id`),
  KEY `programs_dish_types_programs_id_fk` (`program_id`),
  KEY `programs_dish_types_dish_types_id_fk` (`dish_type_id`),
  CONSTRAINT `programs_dish_types_dish_types_id_fk` FOREIGN KEY (`dish_type_id`) REFERENCES `dish_types` (`id`),
  CONSTRAINT `programs_dish_types_programs_id_fk` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programs_dish_types`
--

LOCK TABLES `programs_dish_types` WRITE;
/*!40000 ALTER TABLE `programs_dish_types` DISABLE KEYS */;
INSERT INTO `programs_dish_types` VALUES (1,1,1,0.2),(2,1,2,0.3),(3,1,3,0.3),(4,1,5,0.2),(5,2,1,0.2),(6,2,2,0.1),(7,2,3,0.3),(8,2,4,0.1),(9,2,5,0.2),(10,2,6,0.1);
/*!40000 ALTER TABLE `programs_dish_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purposes`
--

DROP TABLE IF EXISTS `purposes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purposes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `purposes_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purposes`
--

LOCK TABLES `purposes` WRITE;
/*!40000 ALTER TABLE `purposes` DISABLE KEYS */;
INSERT INTO `purposes` VALUES (1,'GAIN','Muscle Gain'),(2,'LOSE','Weight Lose');
/*!40000 ALTER TABLE `purposes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_USER');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unit_data_types`
--

DROP TABLE IF EXISTS `unit_data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unit_data_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `data_type_code` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unit_data_types_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unit_data_types`
--

LOCK TABLES `unit_data_types` WRITE;
/*!40000 ALTER TABLE `unit_data_types` DISABLE KEYS */;
INSERT INTO `unit_data_types` VALUES (1,'Grams','FLOAT'),(2,'Percents','FLOAT'),(3,'Months','STRING');
/*!40000 ALTER TABLE `unit_data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `unit_types`
--

DROP TABLE IF EXISTS `unit_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `unit_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `unit_data_type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unit_types_id_uindex` (`id`),
  UNIQUE KEY `unit_types_code_uindex` (`code`),
  KEY `unit_types_unit_data_types_id_fk` (`unit_data_type_id`),
  CONSTRAINT `unit_types_unit_data_types_id_fk` FOREIGN KEY (`unit_data_type_id`) REFERENCES `unit_data_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `unit_types`
--

LOCK TABLES `unit_types` WRITE;
/*!40000 ALTER TABLE `unit_types` DISABLE KEYS */;
INSERT INTO `unit_types` VALUES (1,'TABLESPOON','Tablespoon',1),(2,'TEASPOON','Teaspoon',1),(3,'HANDFUL','Handful',1),(4,'GRAMS','Grams',1);
/*!40000 ALTER TABLE `unit_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_daily_rations`
--

DROP TABLE IF EXISTS `user_daily_rations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_daily_rations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_ration_id` bigint(20) NOT NULL,
  `dish_type_id` bigint(20) NOT NULL,
  `dish_id` bigint(20) NOT NULL,
  `day_code` varchar(10) NOT NULL,
  `protein` float NOT NULL,
  `carbohydrate` float NOT NULL,
  `fat` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_daily_rations_id_uindex` (`id`),
  UNIQUE KEY `ration_id_dish_type_id_dish_id_uindex` (`user_ration_id`,`dish_type_id`,`dish_id`),
  KEY `user_daily_rations_dish_types_id_fk` (`dish_type_id`),
  KEY `user_daily_rations_dishes_id_fk` (`dish_id`),
  CONSTRAINT `user_daily_rations_dish_types_id_fk` FOREIGN KEY (`dish_type_id`) REFERENCES `dish_types` (`id`),
  CONSTRAINT `user_daily_rations_dishes_id_fk` FOREIGN KEY (`dish_id`) REFERENCES `dishes` (`id`),
  CONSTRAINT `user_daily_rations_user_rations_id_fk` FOREIGN KEY (`user_ration_id`) REFERENCES `user_rations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_daily_rations`
--

LOCK TABLES `user_daily_rations` WRITE;
/*!40000 ALTER TABLE `user_daily_rations` DISABLE KEYS */;
INSERT INTO `user_daily_rations` VALUES (219,30,3,5,'WED',88,89,77),(220,30,2,37,'WED',20,177.6,3),(221,30,1,6,'WED',58,90,144),(222,30,5,36,'WED',96,210.7,58),(223,30,3,3,'WED',12,44,87),(224,30,2,7,'WED',65,89,140),(225,30,1,18,'WED',9,3,17),(226,30,2,18,'WED',9,3,17),(227,30,1,5,'WED',88,89,77),(228,30,2,5,'THU',88,89,77),(229,30,2,6,'THU',58,90,144),(230,30,1,34,'THU',4.4,32.2,1),(231,30,3,35,'FRI',99.9,40.4,63),(232,30,1,8,'FRI',25,84,17),(233,30,3,8,'FRI',25,84,17),(234,30,3,2,'SAT',77,65,5),(235,30,1,28,'SAT',94,12,57),(236,30,1,37,'SUN',20,177.6,3),(237,30,2,1,'TUE',54,4,65),(238,31,1,5,'WED',88,89,77),(239,31,2,7,'WED',65,89,140),(240,31,3,35,'WED',99.9,40.4,63),(241,31,5,36,'WED',96,210.7,58),(242,31,1,6,'WED',58,90,144),(243,31,3,3,'WED',12,44,87),(244,31,1,8,'WED',25,84,17),(245,31,2,18,'WED',9,3,17),(246,31,3,5,'WED',88,89,77);
/*!40000 ALTER TABLE `user_daily_rations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_rations`
--

DROP TABLE IF EXISTS `user_rations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_rations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `program_id` bigint(20) NOT NULL,
  `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `total_calories_needed` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_rations_id_uindex` (`id`),
  UNIQUE KEY `user_id_program_id_uindex` (`user_id`,`program_id`),
  KEY `user_rations_programs_id_fk` (`program_id`),
  CONSTRAINT `user_rations_programs_id_fk` FOREIGN KEY (`program_id`) REFERENCES `programs` (`id`),
  CONSTRAINT `user_rations_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_rations`
--

LOCK TABLES `user_rations` WRITE;
/*!40000 ALTER TABLE `user_rations` DISABLE KEYS */;
INSERT INTO `user_rations` VALUES (30,8,1,'2018-07-18 14:09:39','2018-07-25 14:09:39',2333.25),(31,4,1,'2018-07-18 14:09:51','2018-07-19 14:09:51',1758.59);
/*!40000 ALTER TABLE `user_rations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `gender_id` bigint(20) NOT NULL,
  `age` int(11) NOT NULL,
  `height` double NOT NULL,
  `weight` double NOT NULL,
  `purpose_id` bigint(20) DEFAULT NULL,
  `activity_level_id` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) NOT NULL,
  `current_phase_id` bigint(20) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `phase_update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_id_uindex` (`id`),
  UNIQUE KEY `username_uindex` (`username`),
  KEY `users_roles_id_fk` (`role_id`),
  KEY `users_genders_id_fk` (`gender_id`),
  KEY `users_purposes_id_fk` (`purpose_id`),
  KEY `users_activity_levels_id_fk` (`activity_level_id`),
  KEY `users_phases_id_fk` (`current_phase_id`),
  CONSTRAINT `users_activity_levels_id_fk` FOREIGN KEY (`activity_level_id`) REFERENCES `activity_levels` (`id`),
  CONSTRAINT `users_genders_id_fk` FOREIGN KEY (`gender_id`) REFERENCES `genders` (`id`),
  CONSTRAINT `users_phases_id_fk` FOREIGN KEY (`current_phase_id`) REFERENCES `phases` (`id`),
  CONSTRAINT `users_purposes_id_fk` FOREIGN KEY (`purpose_id`) REFERENCES `purposes` (`id`),
  CONSTRAINT `users_roles_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'tyrion','$2a$10$LTu15SRR31VNZ3P8OtAf1exnKzZ41TparWDh4gR79dPM/eqIbdNZm',1,42,120,41,1,3,1,1,1,'2018-07-16 16:15:51'),(2,'rhaegar','$2a$10$dCcpU10OFTP9CBb8fbFqYuy1Gu39QQlUM7sKWNzDpz1KP94W2USAa',1,39,187,82,1,3,2,1,0,'2018-07-16 16:15:51'),(3,'aegon','$2a$10$LTu15SRR31VNZ3P8OtAf1exnKzZ41TparWDh4gR79dPM/eqIbdNZm',1,49,190,81,1,1,2,1,1,'2018-07-16 16:15:51'),(4,'sansa','$2a$10$dCcpU10OFTP9CBb8fbFqYuy1Gu39QQlUM7sKWNzDpz1KP94W2USAa',2,17,164,47,2,2,2,1,1,'2018-07-16 16:15:51'),(5,'arya','$2a$10$VI56Jp0VR6hEBSNp1QkxN.VI6LqAMQNVubkpWONZpu2ONEwRBA0Ne',2,14,153,42,2,3,2,1,1,'2018-07-16 16:15:51'),(6,'sersey','$2a$10$UjKxdt0kmA85x8MuWAzrHu2S/2IAoKkWVLr8XexRdh4UEsiAmB9cm',2,34,170,51,2,1,2,1,1,'2018-07-16 16:15:51'),(7,'lianna',NULL,2,17,153,42,2,3,2,1,1,'2018-07-16 16:15:51'),(8,'test',NULL,1,17,153,42,2,3,2,1,1,'2018-07-16 16:15:51'),(9,'john',NULL,1,22,164,55,1,3,2,1,1,'2018-07-16 16:15:51');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_dietary_restrictions`
--

DROP TABLE IF EXISTS `users_dietary_restrictions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_dietary_restrictions` (
  `user_id` bigint(20) NOT NULL,
  `dietary_restriction_id` bigint(20) NOT NULL,
  KEY `user_dietary_restrictions_users_id_fk` (`user_id`),
  KEY `user_dietary_restrictions_dietary_restrictions_id_fk` (`dietary_restriction_id`),
  CONSTRAINT `user_dietary_restrictions_dietary_restrictions_id_fk` FOREIGN KEY (`dietary_restriction_id`) REFERENCES `dietary_restrictions` (`id`),
  CONSTRAINT `user_dietary_restrictions_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_dietary_restrictions`
--

LOCK TABLES `users_dietary_restrictions` WRITE;
/*!40000 ALTER TABLE `users_dietary_restrictions` DISABLE KEYS */;
INSERT INTO `users_dietary_restrictions` VALUES (4,2),(4,1),(5,3);
/*!40000 ALTER TABLE `users_dietary_restrictions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `week_days`
--

DROP TABLE IF EXISTS `week_days`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `week_days` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `week_days_id_uindex` (`id`),
  UNIQUE KEY `week_days_code_uindex` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `week_days`
--

LOCK TABLES `week_days` WRITE;
/*!40000 ALTER TABLE `week_days` DISABLE KEYS */;
INSERT INTO `week_days` VALUES (1,'MON','Monday'),(2,'TUE','Tuesday'),(3,'WED','Wednesday'),(4,'THU','Thursday'),(5,'FRI','Friday'),(6,'SAT','Saturday'),(7,'SUN','Sunday');
/*!40000 ALTER TABLE `week_days` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-22 17:50:23
