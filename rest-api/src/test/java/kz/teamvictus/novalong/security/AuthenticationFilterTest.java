package kz.teamvictus.novalong.security;

import kz.teamvictus.novalong.rest.model.entity.Role;
import kz.teamvictus.novalong.rest.model.entity.User;
import kz.teamvictus.novalong.security.service.TokenService;
import kz.teamvictus.novalong.security.service.UserDetailsServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.ServletException;
import java.io.IOException;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationFilterTest {

    private static final String TEST_URI = "/api/test";
    private String token;

    @Mock
    private TokenService tokenService;

    @Mock
    private UserDetailsServiceImpl userDetailsService;

    private AuthenticationFilter authenticationFilter;

    @Before
    public void setUp() {
        Role role = new Role();
        role.setName("ROLE_ADMIN");

        User user = new User();
        user.setId(1L);
        user.setUsername("TEST_USERNAME");
        user.setRole(role);

        this.authenticationFilter = new AuthenticationFilter(userDetailsService, tokenService);
        this.token = "Bearer" + tokenService.generateToken(user);
    }

    @Test
    public void doFilterPositiveScenario() throws ServletException, IOException {
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(AUTHORIZATION, token);
        request.setRequestURI(TEST_URI);

        MockHttpServletResponse response = new MockHttpServletResponse();
        MockFilterChain chain = new MockFilterChain();
        authenticationFilter.doFilterInternal(request, response, chain);
        assertThat(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    }

}
